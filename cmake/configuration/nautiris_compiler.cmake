include_guard()

macro(_NAUTIRIS_COMPILER_CONFIG_MSVC)
  list(APPEND NAUTIRIS_COMPILE_DEFINITIONS_COMMON
    _CRT_NONSTDC_NO_DEPRECATE
    _CRT_SECURE_NO_DEPRECATE
    _CRT_SECURE_NO_WARNINGS
    _SCL_SECURE_NO_WARNINGS
    NOMINMAX # disable min/max problem in windows.h
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    /bigobj # Compiler Fatal error C1128: Object Limit Exceed
    /Oi # enable built-in functions
    /permissive-
    /source-charset:utf-8
    /volatile:iso # Specifies how the volatile keyword is to be interpreted.
    /wd4996 # mark deprecated (/W3)
    $<$<AND:$<BOOL:${NAUTIRIS_WARNINGS_AS_ERRORS}>,$<VERSION_GREATER:MSVC_VERSION,1900>>:/WX>
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    /Wall
    )
  if(CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      /GR- # Disable RTTI
      )
  endif()
endmacro()

macro(_NAUTIRIS_COMPILER_CONFIG_GCC)
  # GNU Compiler Collection
  # Learn more at https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    -fmax-errors=5
    -finput-charset=UTF8
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    -fprofile-abs-path
    # --coverage is equivalent to -ftest-profile -fprofile-arcs
    $<$<NOT:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"">>>:--coverage>
    )
  if(CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -fno-threadsafe-statics
      -fmerge-all-constants
      $<$<NOT:$<STREQUAL:${NAUTIRIS_USE_STDLIB},>>:-nostdinc++>
      )
  endif()
  foreach(dir IN LISTS NAUTIRIS_STDLIB_INCLUDEDIRS)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON -isystem${dir})
  endforeach()
endmacro()

macro(_NAUTIRIS_COMPILER_CONFIG_CLANG)
  # Clang
  # Learn more at https://clang.llvm.org/docs/UsersManual.html
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    -ferror-limit=5
    -finput-charset=UTF-8
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    -fdiagnostics-absolute-paths
    # --coverage is equivalent to -ftest-profile -fprofile-arcs
    $<$<NOT:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"">>>:
      $<IF:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"gcovr">>,
        --coverage,
        -fprofile-instr-generate\;-fcoverage-mapping>>
    )
  if(CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -Wthread-safety
      $<$<BOOL:$<STREQUAL:${NAUTIRIS_USE_STDLIB},cxx>>:-stdlib=libc++>
      )
  endif()
endmacro()

get_property(NAUTIRIS_LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)
if(NAUTIRIS_COMPILER_IS_MSVC)
  _NAUTIRIS_COMPILER_CONFIG_MSVC()
else()
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    -fno-common
    $<$<BOOL:${NAUTIRIS_WARNINGS_AS_ERRORS}>:-Werror\;-Wno-error=deprecated-declarations>
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    -Wall
    -fno-inline
    -pedantic
    # from lua Makefile, enable compiler warning
    -Wdisabled-optimization
    -Wdouble-promotion
    -Wextra
    -Wmissing-declarations
    -Wredundant-decls
    -Wshadow
    -Wsign-compare
    -Wundef
    # -Wfatal-errors
    # from kDE cmake
    -Wcast-align
    -Wno-long-long
    )
  if(CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -fno-rtti # Disable RTTI
      )
  endif()
  if(NAUTIRIS_COMPILER_IS_GCC)
    _NAUTIRIS_COMPILER_CONFIG_GCC()
  elseif(NAUTIRIS_COMPILER_IS_CLANG)
    _NAUTIRIS_COMPILER_CONFIG_CLANG()
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "Unknow compiler ${CMAKE_CXX_COMPILER_ID}.")
  endif()
endif()
unset(NAUTIRIS_LANGUAGES)
