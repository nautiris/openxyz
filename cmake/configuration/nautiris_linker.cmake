include_guard()

function(nautiris_target_ipo TARGET_NAME)
  if(NOT NAUTIRIS_ENABLE_IPO)
    return()
  endif()
  include(CheckIPOSupported)
  check_ipo_supported(RESULT result OUTPUT output)
  if(result)
    set_target_properties(${TARGET_NAME} PROPERTIES
      INTERPROCEDURAL_OPTIMIZATION ON)
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "IPO is not supported: ${output}.")
  endif()
endfunction()

macro(_NAUTIRIS_LINKER_CONFIG_GCC)
  list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
    $<$<NOT:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"">>>:--coverage>
    )
  if(NAUTIRIS_USE_STDLIB)
    list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
      -lm
      -lc
      -nodefaultlibs
      $<$<PLATFORM_ID:Linux>:-lgcc_s\;-lgcc>
      $<IF:$<AND:$<BOOL:${NAUTIRIS_TARGET_IS_32BIT}>,$<NOT:$<BOOL:${NAUTIRIS_ARCH_IS_32BIT}>>>,-m32,-march=native>
    )
  endif()
  if(NAUTIRIS_USE_STDLIB AND C IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -nostdlib
      )
  endif()
  if(NAUTIRIS_USE_STDLIB AND CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -nostdlib++
      $<$<STREQUAL:${NAUTIRIS_USE_STDLIB},cxx>:-lc++\;-lc++abi\;-lsupc++>
      $<$<STREQUAL:${NAUTIRIS_USE_STDLIB},stdcxx>:-lstdc++>
      )
  endif()
endmacro()

macro(_NAUTIRIS_LINKER_CONFIG_CLANG)
  list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
    # --coverage is equivalent to -ftest-profile -fprofile-arcs
    $<$<NOT:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"">>>:
      $<IF:$<BOOL:$<STREQUAL:${NAUTIRIS_USE_COVERAGE},"gcovr">>,
        --coverage,
        -fprofile-instr-generate\;-fcoverage-mapping>>
    )
  if(NAUTIRIS_USE_STDLIB AND CXX IN_LIST NAUTIRIS_LANGUAGES)
    list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
      $<$<STREQUAL:${NAUTIRIS_USE_STDLIB},cxx>:-stdlib=libc++\;-lc++>
      )
  endif()
endmacro()

get_property(NAUTIRIS_LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)
if(NAUTIRIS_COMPILER_IS_MSVC)
else()
  list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
    $<$<BOOL:${NAUTIRIS_USE_LINKER}>:-fuse-ld=${NAUTIRIS_USE_LINKER}>
    )
  if(NAUTIRIS_COMPILER_IS_GCC)
    _NAUTIRIS_LINKER_CONFIG_GCC()
  elseif(NAUTIRIS_COMPILER_IS_CLANG)
    _NAUTIRIS_LINKER_CONFIG_CLANG()
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "Unknow compiler ${CMAKE_CXX_COMPILER_ID}.")
  endif()
endif()
unset(NAUTIRIS_LANGUAGES)
