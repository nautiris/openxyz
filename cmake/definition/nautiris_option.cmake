include_guard()

include(nautiris_policy)

# flag

set(NAUTIRIS_STDLIB_INCLUDEDIRS ""
  CACHE STRING "Specify header directories of stdlib, must be specify if not use default")
# e.g.
#   stdcxx: /usr/include/c++/7;/usr/include/x86_64-linux-gnu/c++/7
#   cxx: /usr/lib/llvm-15/include/c++/v1

set(NAUTIRIS_USE_SANITIZER ""
  CACHE STRING "Build with sanitizers, e.g. `Address;Undefined'.")

set(NAUTIRIS_USE_STDLIB ""
  CACHE STRING "Use std library of C++.")
set_property(CACHE NAUTIRIS_USE_STDLIB PROPERTY STRINGS
  ""
  "stdcxx"
  "cxx"
  )

set(NAUTIRIS_USE_LINKER ""
  CACHE STRING "Use a specific linker, e.g. `bfd'")
set_property(CACHE NAUTIRIS_USE_LINKER PROPERTY STRINGS
  "" # default is bfd
  "bfd"
  "gold"
  "lld"
  )

set(NAUTIRIS_USE_CACHE ""
  CACHE STRING "Use a specific cache, e.g. `ccache'")
set_property(CACHE NAUTIRIS_USE_LINKER PROPERTY STRINGS
  "" # default None
  "ccache"
  )

set(NAUTIRIS_USE_COVERAGE ""
  CACHE STRING "Use a specific coverage, e.g. `gcovr'")
set_property(CACHE NAUTIRIS_USE_COVERAGE PROPERTY STRINGS
  "" # default None
  "gcovr"
  "lcov"
  )

# option

option(NAUTIRIS_WARNINGS_AS_ERRORS "Warnings as errors?" OFF)

option(NAUTIRIS_BUILD_32BITS "Build 32 bits executables and libraries." OFF)

option(NAUTIRIS_BUILD_SHARED "Build shared libraries." OFF)

option(NAUTIRIS_BUILD_TESTS "Build tests?" OFF)

option(NAUTIRIS_BUILD_TOOLS "Build tools?" ON)

option(NAUTIRIS_ENABLE_IPO "Enable interprocedual optimization." OFF)

option(NAUTIRIS_ENABLE_PCH "Enable precompile headers." OFF)

option(NAUTIRIS_ENABLE_LINT "Use code check by clang tidy." OFF)

option(NAUTIRIS_ENABLE_DOXYGEN "Generate documentation with doxygen." OFF)

# Set off means to use the default way to link.
option(NAUTIRIS_LINK_STATIC "Statically link 3rdpatry libraries." OFF)
