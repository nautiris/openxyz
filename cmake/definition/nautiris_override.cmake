include_guard()

include(nautiris_defvar)
include(nautiris_option)

if(NAUTIRIS_ARCH_IS_32BIT)
  set(NAUTIRIS_TARGET_IS_64BIT OFF)
  set(NAUTIRIS_TARGET_IS_32BIT ON)
endif()

if(NAUTIRIS_BUILD_SHARED)
  set(CMAKE_POSITION_INDEPENDENT_CODE ON)
endif()

if(NAUTIRIS_USE_STDLIB AND NOT NAUTIRIS_COMPILER_IS_CLANG AND
    NAUTIRIS_STDLIB_INCLUDEDIRS STREQUAL "")
  message(FATAL_ERROR "Must specify header directory of stdlib if don't use default stdlib")
elseif(NOT NAUTIRIS_USE_STDLIB)
  set(NAUTIRIS_STDLIB_INCLUDEDIRS "" CACHE STRING "Don't specify header dir when use default stdlib." FORCE)
endif()

if(NOT PROJECT_IS_TOP_LEVEL)
  set(NAUTIRIS_USE_SANITIZER "" CACHE STRING "Disable sanitizers when project is submodule." FORCE)
  set(NAUTIRIS_BUILD_TESTS OFF CACHE BOOL "Build tests override when project is submodule." FORCE)
  set(NAUTIRIS_BUILD_TOOLS OFF CACHE BOOL "Build tools override when project is submodule." FORCE)
  set(NAUTIRIS_USE_COVERAGE "" CACHE STRING "Build coverage override when project is submodule." FORCE)
  set(NAUTIRIS_ENABLE_LINT OFF CACHE BOOL "Disable code check override when project is submodule." FORCE)
elseif(NAUTIRIS_BUILD_TESTS)
  set(NAUTIRIS_BUILD_TOOLS ON CACHE BOOL "Build tools override by NAUTIRIS_BUILD_TESTS." FORCE)
elseif(NAUTIRIS_USE_COVERAGE) # Not build tests, disable coverage
  set(NAUTIRIS_USE_COVERAGE "" CACHE STRING "Disable code coverage override by NAUTIRIS_BUILD_TESTS." FORCE)
endif()
