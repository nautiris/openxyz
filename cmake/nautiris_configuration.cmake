include_guard()

include(nautiris_definition)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/configuration")

include(nautiris_compiler)
include(nautiris_generator)
include(nautiris_linker)
include(nautiris_precompile_header)

list(REMOVE_AT CMAKE_MODULE_PATH -1)
