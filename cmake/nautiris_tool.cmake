include_guard()

include(nautiris_definition)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/tool")

include(nautiris_cache)
include(nautiris_coverage)
if(NAUTIRIS_ENABLE_DOXYGEN)
  include(nautiris_doxygen)
endif()
include(nautiris_lint)
include(nautiris_sanitizer)

list(REMOVE_AT CMAKE_MODULE_PATH -1)
