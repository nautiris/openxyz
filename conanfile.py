# -*- coding: utf-8 -*-
#!/usr/bin/env python3

# cat <<-EOF |tee $HOME/.conan2/profiles/clang-cxx
# [settings]
# arch=x86_64
# build_type=Release
# compiler=clang
# compiler.cppstd=gnu17
# compiler.libcxx=libc++
# compiler.version=18
# os=Linux
#
# [conf]
# tools.build:compiler_executables={'c': 'clang', 'cpp': 'clang++'}
# tools.cmake.cmaketoolchain:generator=Ninja Multi-Config
# EOF
# conan install conanfile.py --build="*" -pr:b=clang-cxx -pr:h=clang-cxx -s=build_type=Debug -of=_3rdparty
# conan install conanfile.py --build="*" -pr:b=clang-cxx -pr:h=clang-cxx -s=build_type=Release -of=_3rdparty
# CMAKE_PREFIX_PATH=_3rdparty/build/generators \
#     cmake -S$HOME/Projects/openxyz -B$HOME/Projects/openxyz/_build \
#     -DCMAKE_CXX_COMPILER=clang++ -G'Ninja Multi-Config' -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
#     -DNAUTIRIS_BUILD_TESTS=ON -DNAUTIRIS_ENABLE_LINT=ON \
#     -DNAUTIRIS_USE_COVERAGE=lcov -DNAUTIRIS_USE_LINKER=lld -DNAUTIRIS_USE_SANITIZER='Undefined' \
#     -DNAUTIRIS_USE_STDLIB=cxx -DNAUTIRIS_STDLIB_INCLUDEDIRS=/usr/include/c++/v1

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps, cmake_layout

class App(ConanFile):
    settings = "os", "arch", "compiler", "build_type"

    def requirements(self):
        self.test_requires("catch2/3.6.0")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.set_property("catch2", "cmake_find_mode", "both")
        deps.generate()
