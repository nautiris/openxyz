#pragma once

#if !defined(OPENXYZ_IMAGE_HPP)
#define OPENXYZ_IMAGE_HPP

#include "image/common.hxx"
#include "image/extents.hxx"
#include "image/image.hxx"
#include "image/layout.hxx"
#include "image/pixel.hxx"
#include "image/pyramid.hxx"
#include "image/pyramid.tcc"
#include "image/view.hxx"

#endif  // OPENXYZ_IMAGE_HPP
