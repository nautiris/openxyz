#pragma once

#if !defined(OPENXYZ_IMAGE_ACCESSOR_HXX)
#define OPENXYZ_IMAGE_ACCESSOR_HXX

#include "image/image.hxx"
#include "image/pixel.hxx"
#include "math.hpp"

#include <stddef.h>
#include <type_traits>

namespace image {
template <typename PixelTrait> struct DefaultAccessor {
 public:
  using offset_policy_type = DefaultAccessor;
  using pixel_trait_type = PixelTrait;
  using size_type = size_t;
  using difference_type = ptrdiff_t;

  using underlying_type = typename Image::data_type;
  using underlying_pointer = underlying_type*;
  using underlying_const_pointer = underlying_type const*;

  using element_type =
      ::std::conditional_t<not pixel_trait_type::is_dynamic,
                           typename pixel_trait_type::element_type,
                           underlying_type>;
  using vector_type =
      ::std::conditional_t<not pixel_trait_type::is_dynamic,
                           typename pixel_trait_type::type, underlying_type>;
  using reference = vector_type&;
  using const_reference = vector_type const&;
  using pointer = vector_type*;
  using const_pointer = vector_type const*;

  constexpr auto offset(const_pointer ptr, difference_type idx,
                        pixel_trait_type const& ty) const noexcept
      -> const_pointer {
    static_assert(::std::is_same_v<const_pointer,
                                   typename pixel_trait_type::const_pointer>);
    return offset<pixel_trait_type>(
        reinterpret_cast<underlying_const_pointer>(ptr), idx, ty.get_stride());
  }

  template <PixelType CvtTy>
  constexpr auto offset(const_pointer ptr, difference_type idx) const noexcept
      -> typename PixelTraits<CvtTy>::const_pointer {
    static_assert(kDynamicPixel != CvtTy,
                  "target type should not be dynamic pixel");
    using PixelT = PixelTraits<CvtTy>;
    return offset<PixelT>(reinterpret_cast<underlying_const_pointer>(ptr), idx,
                          PixelT{}.get_stride());
  }

 private:
  template <typename PixelT>
  constexpr auto offset(underlying_const_pointer ptr, difference_type idx,
                        size_type stride) const noexcept ->
      typename PixelT::const_pointer {
    return reinterpret_cast<typename PixelT::const_pointer>(ptr + idx * stride);
  }
};

}  // namespace image

#endif  // OPENXYZ_IMAGE_ACCESSOR_HXX
