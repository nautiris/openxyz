#pragma once

#if !defined(OPENXYZ_IMAGE_COMMON_HXX)
#define OPENXYZ_IMAGE_COMMON_HXX

#include <stddef.h>

namespace image::_details {
void* const kDeadPtr = reinterpret_cast<void*>(0xDEAD'BEEF);
}

#endif  // OPENXYZ_IMAGE_COMMON_HXX
