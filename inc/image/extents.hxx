#pragma once

#if !defined(OPENXYZ_IMAGE_EXTENTS_HXX)
#define OPENXYZ_IMAGE_EXTENTS_HXX

#include <algorithm>
#include <array>
#include <limits>
#include <stddef.h>
#include <type_traits>

namespace image {

static constexpr ssize_t kDynamicExtents =
    ::std::numeric_limits<ssize_t>::max();

namespace _details {
template <ssize_t... KExtents> constexpr auto rank_dynamic() -> ssize_t {
  return ((KExtents == kDynamicExtents ? 1 : 0) + ...);
}
}  // namespace _details

template <typename IndexT, ssize_t... KExtents>
class Extents
    : private ::std::array<IndexT, _details::rank_dynamic<KExtents...>()> {
 public:
  // Member types
  using index_type = IndexT;
  using size_type = ::std::make_unsigned_t<index_type>;
  using rank_type = size_t;

 private:
  static constexpr rank_type kRank = sizeof...(KExtents);
  static constexpr rank_type kDynRank = _details::rank_dynamic<KExtents...>();
  using underlying_type = ::std::array<index_type, kDynRank>;

 public:
  // Constructor
  constexpr Extents() noexcept = default;
  template <typename OtherIndexType, ssize_t... OtherExtents>
  constexpr explicit Extents(
      Extents<OtherIndexType, OtherExtents...> const& other) noexcept {
    using SelfT = Extents;
    using OtherT =
        ::std::remove_cv_t<::std::remove_reference_t<decltype(other)>>;
    rank_type dyn_cnt = 0;
    for (rank_type cnt = 0; cnt < ::std::min(SelfT::rank(), OtherT::rank()) &&
                            dyn_cnt < SelfT::rank_dynamic();
         ++cnt) {
      if (SelfT::static_extent(cnt) == kDynamicExtents) {
        underlying_type::operator[](dyn_cnt++) = other.extent(cnt);
      }
    }
    if constexpr (SelfT::rank_dynamic() > OtherT::rank_dynamic()) {
      ::std::fill_n(underlying_type::begin() + dyn_cnt,
                    SelfT::rank_dynamic() - dyn_cnt, 1);
    }
  }
  template <typename... OtherIndexTypes,
            typename = ::std::enable_if_t<
                (::std::is_convertible_v<OtherIndexTypes, index_type> && ...)>*>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Extents(OtherIndexTypes... exts)
      : underlying_type{static_cast<index_type>(exts)...} {}
  template <typename OtherIndexType, size_t N,
            typename = ::std::enable_if_t<N == kRank && N == kDynRank>*>
  constexpr explicit Extents(
      ::std::array<OtherIndexType, N> const& exts) noexcept {
    using SelfT = Extents;
    ::std::copy_n(exts.begin(), ::std::min(SelfT::rank_dynamic(), N),
                  underlying_type::begin());
    if constexpr (SelfT::rank_dynamic() > N) {
      ::std::fill_n(underlying_type::begin() + N, SelfT::rank_dynamic() - N, 1);
    }
  }

  // Observers
  static constexpr auto rank() noexcept -> rank_type { return Extents::kRank; }

  static constexpr auto rank_dynamic() noexcept -> rank_type {
    return Extents::kDynRank;
  }

  static constexpr auto static_extent(rank_type idx) noexcept -> index_type {
    index_type extent = 0;
    rank_type count = 0;
    ((count == idx ? extent = KExtents : 0, ++count), ...);
    return extent;
  }

  constexpr auto extent(rank_type idx) const noexcept -> index_type {
    index_type extent = 0;
    rank_type count = 0;
    rank_type dyn_count = 0;
    ((count == idx ? extent = (KExtents == kDynamicExtents
                                   ? underlying_type::operator[](dyn_count)
                                   : KExtents)
                   : 0,
      ++count, KExtents == kDynamicExtents ? ++dyn_count : 0),
     ...);
    return extent;
  }

  // Non-member functions
  template <typename OtherIndexType, ssize_t... OtherExtents>
  friend constexpr auto
  operator==(Extents const& lhs,
             Extents<OtherIndexType, OtherExtents...> const& rhs) noexcept
      -> bool {
    if constexpr (sizeof...(KExtents) == sizeof...(OtherExtents)) {
      rank_type count = 0;
      return ((++count, static_cast<void>(KExtents),
               lhs.extent(count - 1) == rhs.extent(count - 1)) &&
              ...);
    } else {
      return false;
    }
  }
  template <typename OtherIndexType, ssize_t... OtherExtents>
  friend constexpr auto
  operator!=(Extents const& lhs,
             Extents<OtherIndexType, OtherExtents...> const& rhs) noexcept
      -> bool {
    return !(lhs == rhs);
  }

  // Non-member functions
  template <typename I, ssize_t... E>
  friend auto swap(Extents<I, E...>& lhs, Extents<I, E...>& rhs) noexcept
      -> void;
};

namespace _details {
template <typename IndexT, typename IndexSeq> struct MakeDynExtentsAux;

template <typename IndexT, ssize_t... Indexes>
struct MakeDynExtentsAux<IndexT, ::std::integer_sequence<ssize_t, Indexes...>> {
  using type =
      Extents<IndexT, (static_cast<void>(Indexes), kDynamicExtents)...>;
};
}  // namespace _details

template <size_t Rank>
using DynExtents = typename _details::MakeDynExtentsAux<
    ssize_t, ::std::make_integer_sequence<ssize_t, Rank>>::type;

template <typename... OtherIndexTypes>
Extents(OtherIndexTypes...)
    -> Extents<ssize_t, (static_cast<void>(sizeof(OtherIndexTypes)),
                         kDynamicExtents)...>;

template <typename IndexT, ssize_t... KExtents>
auto swap(Extents<IndexT, KExtents...>& lhs,
          Extents<IndexT, KExtents...>& rhs) noexcept -> void {
  using UnderlyingT = typename Extents<IndexT, KExtents...>::underlying_type;
  ::std::swap(static_cast<UnderlyingT&>(lhs), static_cast<UnderlyingT&>(rhs));
}

}  // namespace image

namespace std {
template <typename IndexT, ssize_t... KExtents>
auto swap(::image::Extents<IndexT, KExtents...>& lhs,
          ::image::Extents<IndexT, KExtents...>& rhs) noexcept -> void {
  ::image::swap(lhs, rhs);
}
}

#endif  // OPENXYZ_IMAGE_EXTENTS_HXX
