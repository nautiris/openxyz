#pragma once

#if !defined(OPENXYZ_IMAGE_IMAGE_HXX)
#define OPENXYZ_IMAGE_IMAGE_HXX

#include "image/pixel.hxx"

#include <algorithm>
#include <array>

namespace image {

class Image {
 public:
  using size_type = size_t;
  using data_type = unsigned char;
  using pointer = data_type*;
  using const_pointer = data_type const*;

  Image() : Image{0, nullptr, PixelType::kTypeNone} {}
  Image(size_type row, size_type col, PixelType type,
        const_pointer data = nullptr)
      : Image{
            ::std::array<size_type, 2>{row, col},
             type, data
  } {}  // 2d image
  template <size_type N>
  Image(::std::array<size_type, N> const& dims, PixelType type,
        const_pointer data = nullptr)
      : Image{N, dims.data(), type, data} {}
  Image(size_type ndim, size_type const* sizes, PixelType type,
        const_pointer data = nullptr);  // nd image
  Image(Image const& other);
  Image(Image&& other) noexcept = default;
  ~Image() noexcept;

  auto operator=(Image const& other) -> Image&;
  auto operator=(Image&& other) noexcept -> Image& = default;

  [[nodiscard]] auto type() const noexcept -> PixelType { return type_; }
  [[nodiscard]] auto depth() const noexcept -> PixelDepth {
    return DynPixelTrait{type_}.get_depth();
  }
  [[nodiscard]] auto channel() const noexcept -> size_type {
    return DynPixelTrait{type_}.get_channel();
  }
  [[nodiscard]] auto dims() const noexcept -> size_type { return dims_; }
  [[nodiscard]] auto size(size_type dim = 0) const noexcept -> size_type {
    return sizes_[dim];
  }
  [[nodiscard]] auto stride(size_type dim = 0) const noexcept -> size_type {
    return strides_[dim];
  }
  auto data() noexcept -> pointer {
    return const_cast<pointer>(static_cast<Image const*>(this)->data());
  }
  [[nodiscard]] auto data() const noexcept -> const_pointer {
    return reinterpret_cast<const_pointer>(reinterpret_cast<uintptr_t>(data_) &
                                           0x0000'FFFF'FFFF'FFFF);
  }
  auto offset(size_type off) noexcept -> pointer { return data() + off; }
  [[nodiscard]] auto offset(size_type off) const noexcept -> const_pointer {
    return data() + off;
  }
  [[nodiscard]] auto empty() const noexcept -> bool { return total_ == 0; }

  [[nodiscard]] auto rect(size_type row, size_type col, size_type width,
                          size_type height) const -> Image;

  auto operator==(Image const& rhs) const noexcept -> bool {
    return this->total_ == rhs.total_ &&
           ::std::equal(data(), offset(this->total_), rhs.data());
  }

  auto operator!=(Image const& rhs) const noexcept -> bool {
    return !(*this == rhs);
  }

 private:
  auto destroy() noexcept -> void;

  PixelType type_;
  pointer data_;
  size_type dims_;
  // total size of data in bytes
  size_type total_;
  size_type* sizes_;
  size_type* strides_;

 public:
  // Non-member functions
  friend auto swap(Image& lhs, Image& rhs) noexcept -> void;
};

inline auto swap(Image& lhs, Image& rhs) noexcept -> void {
  ::std::swap(lhs.type_, rhs.type_);
  ::std::swap(lhs.data_, rhs.data_);
  ::std::swap(lhs.dims_, rhs.dims_);
  ::std::swap(lhs.total_, rhs.total_);
  ::std::swap(lhs.sizes_, rhs.sizes_);
  ::std::swap(lhs.strides_, rhs.strides_);
}

}  // namespace image

namespace std {
inline auto swap(::image::Image& lhs, ::image::Image& rhs) noexcept -> void {
  ::image::swap(lhs, rhs);
}
}

#endif  // OPENXYZ_IMAGE_IMAGE_HXX
