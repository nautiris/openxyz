#pragma once

#if !defined(OPENXYZ_IMAGE_LAYOUT_HXX)
#define OPENXYZ_IMAGE_LAYOUT_HXX

#include <algorithm>
#include <array>
#include <stddef.h>
#include <type_traits>
#include <utility>

namespace image {

#define LAYOUT_MAPPING_DECLARATION(LAYOUT_POLICY)                              \
  struct LAYOUT_POLICY {                                                       \
    template <typename Extents> class Mapping;                                 \
  };

LAYOUT_MAPPING_DECLARATION(LayoutRight)
LAYOUT_MAPPING_DECLARATION(LayoutLeft)
LAYOUT_MAPPING_DECLARATION(LayoutStride)

#undef LAYOUT_MAPPING_DECLARATION

template <typename Extents, typename LayoutType> struct LayoutTraits {
  using layout_type = LayoutType;
  using mapping_type = typename layout_type::template Mapping<Extents>;
  using extents_type = Extents;
  using index_type = typename extents_type::index_type;
  using size_type = typename extents_type::size_type;
  using rank_type = typename extents_type::rank_type;
};

template <typename MappingType> struct LayoutMappingTraits {
  using mapping_type = MappingType;
  using layout_type = typename mapping_type ::layout_type;
  using extents_type = typename mapping_type ::extents_type;
  using index_type =
      typename LayoutTraits<extents_type, layout_type>::index_type;
  using size_type = typename LayoutTraits<extents_type, layout_type>::size_type;
  using rank_type = typename LayoutTraits<extents_type, layout_type>::rank_type;
};

namespace _details {

template <typename Extents, size_t... Indexes>
static constexpr auto
fold_mul(Extents const& exts,
         ::std::integer_sequence<size_t, Indexes...> /*unused*/) noexcept ->
    typename Extents::index_type {
  return (exts.extent(Indexes) * ...);
}

}  // namespace _details

template <typename Extents, typename LayoutType>
struct LayoutMappingBase
    : private LayoutTraits<Extents, LayoutType>::extents_type {
 private:
  using trait = LayoutTraits<Extents, LayoutType>;

 public:
  using layout_type = typename trait::layout_type;
  using mapping_type = typename trait::mapping_type;
  using extents_type = typename trait::extents_type;
  using index_type = typename trait::index_type;
  using size_type = typename trait::size_type;
  using rank_type = typename trait::rank_type;

  constexpr LayoutMappingBase() noexcept = default;
  template <typename... IdxTs,
            typename = ::std::enable_if_t<
                (::std::is_convertible_v<IdxTs, index_type> && ...) &&
                sizeof...(IdxTs) == extents_type::rank() &&
                sizeof...(IdxTs) == extents_type::rank_dynamic()>*>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr LayoutMappingBase(IdxTs... idxes)
      : LayoutMappingBase{extents_type{static_cast<index_type>(idxes)...}} {}
  template <typename IndexT, size_t N,
            typename = ::std::enable_if_t<N == extents_type::rank() &&
                                          N == extents_type::rank_dynamic()>*>
  constexpr explicit LayoutMappingBase(::std::array<IndexT, N> exts)
      : LayoutMappingBase{extents_type{exts}} {}
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr LayoutMappingBase(extents_type const& exts) : extents_type{exts} {}
  constexpr LayoutMappingBase(LayoutMappingBase const& other) = default;
  constexpr LayoutMappingBase(LayoutMappingBase&& other) noexcept = default;
  ~LayoutMappingBase() noexcept = default;

  constexpr auto operator=(LayoutMappingBase const& other)
      -> LayoutMappingBase& = default;
  constexpr auto operator=(LayoutMappingBase&& other) noexcept
      -> LayoutMappingBase& = default;

  constexpr auto extents() noexcept -> extents_type& {
    return static_cast<extents_type&>(*this);
  }
  constexpr auto extents() const noexcept -> extents_type const& {
    return static_cast<extents_type const&>(*this);
  }

  constexpr auto required_span_size() const noexcept -> index_type {
    return underlying()->stride();
  }

  template <typename... IndexTs,
            typename =
                ::std::enable_if_t<sizeof...(IndexTs) == extents_type::rank()>*>
  constexpr auto operator()(IndexTs... indexes) const noexcept -> index_type {
    return operator()(::std::array<index_type, extents_type::rank()>{
        static_cast<index_type>(indexes)...});
  }
  template <typename IndexT, size_t N,
            typename = ::std::enable_if_t<N == extents_type::rank()>*>
  constexpr auto
  operator()(::std::array<IndexT, N> const& indexes) const noexcept
      -> index_type {
    return underlying()->offset(
        indexes, ::std::make_index_sequence<extents_type::rank()>{});
  }

  static constexpr auto is_always_unique() noexcept -> bool { return true; }
  static constexpr auto is_always_exhaustive() noexcept -> bool { return true; }
  static constexpr auto is_always_strided() noexcept -> bool { return true; }
  static constexpr auto is_unique() noexcept -> bool { return true; }
  static constexpr auto is_exhaustive() noexcept -> bool { return true; }
  static constexpr auto is_strided() noexcept -> bool { return true; }

  constexpr auto stride(rank_type idx) const noexcept -> index_type {
    return underlying()->stride(idx);
  }

  static auto swap(LayoutMappingBase& lhs, LayoutMappingBase& rhs) noexcept
      -> void {
    swap(static_cast<extents_type&>(lhs.extents()),
         static_cast<extents_type&>(rhs.extents()));
  }

 private:
  constexpr auto underlying() -> mapping_type* {
    return const_cast<mapping_type*>(
        static_cast<::std::add_const_t<decltype(this)>>(this)->underlying());
  }
  constexpr auto underlying() const -> mapping_type const* {
    return static_cast<mapping_type const*>(this);
  }
};

// LayoutPolicy
template <typename Extents>
class LayoutRight::Mapping : public LayoutMappingBase<Extents, LayoutRight> {
 private:
  using base_type = LayoutMappingBase<Extents, LayoutRight>;
  friend base_type;

 public:
  using layout_type = typename base_type::layout_type;
  using mapping_type = typename base_type::mapping_type;
  using extents_type = typename base_type::extents_type;
  using index_type = typename base_type::index_type;
  using size_type = typename base_type::size_type;
  using rank_type = typename base_type::rank_type;

  using base_type::base_type;

  constexpr auto stride(rank_type idx) const noexcept -> index_type {
    return stride_aux(idx + 1);
  }

 private:
  constexpr auto stride() const noexcept -> index_type { return stride_aux(0); }

  constexpr auto stride_aux(rank_type idx) const noexcept -> index_type {
    index_type value = 1;
    for (rank_type rnk = Extents::rank(); rnk > idx; --rnk) {
      value *= base_type::extents().extent(rnk - 1);
    }
    return value;
  }

  template <typename IndexT, size_t N, size_t... Indexes>
  constexpr auto
  offset(::std::array<IndexT, N> const& idxes,
         ::std::integer_sequence<size_t, Indexes...> /*unused*/) const noexcept
      -> index_type {
    return ((::std::get<Indexes>(idxes) * stride(Indexes)) + ...);
  }

 public:
  // Non-member functions
  template <typename E>
  friend auto
  swap(typename LayoutTraits<E, layout_type>::mapping_type& lhs,
       typename LayoutTraits<E, layout_type>::mapping_type& rhs) noexcept
      -> void;
};

template <typename Extents>
class LayoutLeft::Mapping : public LayoutMappingBase<Extents, LayoutLeft> {
 private:
  using base_type = LayoutMappingBase<Extents, LayoutLeft>;
  friend base_type;

 public:
  using layout_type = typename base_type::layout_type;
  using mapping_type = typename base_type::mapping_type;
  using extents_type = typename base_type::extents_type;
  using index_type = typename base_type::index_type;
  using size_type = typename base_type::size_type;
  using rank_type = typename base_type::rank_type;

  using base_type::base_type;

  constexpr auto stride(rank_type idx) const noexcept -> index_type {
    index_type value = 1;
    for (rank_type rnk = 0; rnk < idx; ++rnk) {
      value *= base_type::extents().extent(rnk);
    }
    return value;
  }

 private:
  constexpr auto stride() const noexcept -> index_type {
    return stride(Extents::rank());
  }

  template <typename IndexT, size_t N, size_t... Indexes>
  constexpr auto offset(::std::array<IndexT, N> const& idxes,
                        ::std::integer_sequence<size_t, Indexes...> /*unused*/
  ) const noexcept -> index_type {
    return ((::std::get<Indexes>(idxes) * stride(Indexes)) + ...);
  }

 public:
  // Non-member functions
  template <typename E>
  friend auto
  swap(typename LayoutTraits<E, layout_type>::mapping_type& lhs,
       typename LayoutTraits<E, layout_type>::mapping_type& rhs) noexcept
      -> void;
};

template <typename Extents>
class LayoutStride::Mapping : public LayoutMappingBase<Extents, LayoutStride> {
 private:
  using base_type = LayoutMappingBase<Extents, LayoutStride>;
  friend base_type;

 public:
  using layout_type = typename base_type::layout_type;
  using mapping_type = typename base_type::mapping_type;
  using extents_type = typename base_type::extents_type;
  using index_type = typename base_type::index_type;
  using size_type = typename base_type::size_type;
  using rank_type = typename base_type::rank_type;
  using strides_type = ::std::array<index_type, extents_type::rank()>;

  constexpr Mapping() noexcept : Mapping{extents_type{}} {}
  template <typename... IdxTs,
            typename = ::std::enable_if_t<
                (::std::is_convertible_v<IdxTs, index_type> && ...) &&
                sizeof...(IdxTs) == extents_type::rank() &&
                sizeof...(IdxTs) == extents_type::rank_dynamic()>*>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Mapping(IdxTs... idxes)
      : Mapping{extents_type{static_cast<index_type>(idxes)...}} {}
  template <typename IndexT, size_t N,
            typename = ::std::enable_if_t<N == extents_type::rank() &&
                                          N == extents_type::rank_dynamic()>*>
  constexpr explicit Mapping(::std::array<IndexT, N> exts)
      : Mapping{extents_type{exts}} {}
  constexpr explicit Mapping(extents_type const& exts)
      : Mapping{exts, init_stride(exts)} {}
  constexpr Mapping(extents_type const& exts, strides_type const& strs)
      : base_type{exts}, strides_{strs} {}
  template <typename OtherMapping,
            typename = ::std::void_t<typename OtherMapping::extents_type,
                                     typename OtherMapping::layout_type>>
  constexpr explicit Mapping(OtherMapping const& other) noexcept
      : Mapping{other.extents()} {}
  constexpr Mapping(Mapping const& other) = default;
  constexpr Mapping(Mapping&& other) noexcept = default;
  ~Mapping() noexcept = default;

  constexpr auto operator=(Mapping const& other) -> Mapping& = default;
  constexpr auto operator=(Mapping&& other) noexcept -> Mapping& = default;

  constexpr auto strides() const noexcept -> strides_type const& {
    return strides_;
  }

  constexpr auto stride(rank_type idx) const noexcept -> index_type {
    return strides()[idx];
  }

  constexpr auto required_span_size() const noexcept -> index_type {
    index_type span_size = 1;
    for (rank_type rnk = 0; rnk < extents_type::rank(); ++rnk) {
      if (base_type::extents().extent(rnk) == 0) {
        return 0;
      }
      span_size += (base_type::extents().extent(rnk) - 1) * stride(rnk);
    }
    return span_size;
  }

  static constexpr auto is_always_exhaustive() noexcept -> bool {
    return false;
  }
  constexpr auto is_exhaustive() const noexcept -> bool {
    if constexpr (extents_type::rank() == 0) {
      return true;
    } else {
      index_type const kSpanSize = required_span_size();
      if (kSpanSize == static_cast<index_type>(0)) {
        if constexpr (extents_type::rank() == 1) {
          return stride(0) == 1;
        } else {
          auto cit = ::std::max_element(strides().cbegin(), strides().cend());
          rank_type const kRLargest = cit - strides().cbegin();
          for (rank_type rnk = 0; rnk < extents_type::rank(); ++rnk) {
            if (base_type::extents().extent(rnk) == 0 && rnk != kRLargest) {
              return false;
            }
          }
          return true;
        }
      } else {
        return kSpanSize ==
               _details::fold_mul(
                   base_type::extents(),
                   ::std::make_index_sequence<extents_type::rank()>{});
      }
    }
  }

 private:
  template <typename IndexT, size_t N, size_t... Indexes>
  constexpr auto offset(::std::array<IndexT, N> const& idxes,
                        ::std::integer_sequence<size_t, Indexes...> /*unused*/
  ) const noexcept -> index_type {
    return ((::std::get<Indexes>(idxes) * stride(Indexes)) + ...);
  }

  static constexpr auto init_stride(extents_type const& exts) noexcept
      -> strides_type {
    if constexpr (extents_type::rank() > 0) {
      return init_stride_aux(
          exts, strides_type{}, 1,
          ::std::integral_constant<index_type, extents_type::rank()>{},
          ::std::make_index_sequence<extents_type::rank()>{});
    } else {
      return {};
    }
  }

  template <index_type I, size_t... Indexes>
  static constexpr auto
  init_stride_aux(extents_type const& exts, strides_type const& strs,
                  index_type stride,
                  ::std::integral_constant<index_type, I> /*unused*/,
                  ::std::integer_sequence<size_t, Indexes...> /*unused*/)
      -> strides_type {
    if constexpr (I == 0) {
      return strs;
    } else {
      return init_stride_aux(
          exts,
          strides_type{
              (I - 1 == Indexes ? stride : ::std::get<Indexes>(strs))...},
          stride * exts.extent(I - 1),
          ::std::integral_constant<index_type, I - 1>{},
          ::std::make_index_sequence<extents_type::rank()>{});
    }
  }

  strides_type strides_;

 public:
  //  Non-member functions
  template <typename E>
  friend auto
  swap(typename LayoutTraits<E, layout_type>::mapping_type& lhs,
       typename LayoutTraits<E, layout_type>::mapping_type& rhs) noexcept
      -> void;
};

template <typename ExtentsType>
auto swap(
    typename LayoutTraits<ExtentsType, LayoutRight>::mapping_type& lhs,
    typename LayoutTraits<ExtentsType, LayoutRight>::mapping_type& rhs) noexcept
    -> void {
  using MappingT =
      typename LayoutTraits<ExtentsType, LayoutRight>::mapping_type;
  using BaseT = typename MappingT::base_type;
  BaseT::swap(static_cast<BaseT&>(lhs), static_cast<BaseT&>(rhs));
}

template <typename ExtentsType>
auto swap(
    typename LayoutTraits<ExtentsType, LayoutLeft>::mapping_type& lhs,
    typename LayoutTraits<ExtentsType, LayoutLeft>::mapping_type& rhs) noexcept
    -> void {
  using MappingT = typename LayoutTraits<ExtentsType, LayoutLeft>::mapping_type;
  using BaseT = typename MappingT::base_type;
  BaseT::swap(static_cast<BaseT&>(lhs), static_cast<BaseT&>(rhs));
}

template <typename ExtentsType>
auto swap(typename LayoutTraits<ExtentsType, LayoutStride>::mapping_type& lhs,
          typename LayoutTraits<ExtentsType, LayoutStride>::mapping_type&
              rhs) noexcept -> void {
  using MappingT =
      typename LayoutTraits<ExtentsType, LayoutStride>::mapping_type;
  using BaseT = typename MappingT::base_type;
  ::std::swap(lhs.strides_, rhs.strides_);
  BaseT::swap(static_cast<BaseT&>(lhs), static_cast<BaseT&>(rhs));
}

}  // namespace image

namespace std {
template <typename ExtentsType>
auto swap(typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutRight>::mapping_type& lhs,
          typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutRight>::mapping_type& rhs) noexcept
    -> void {
  ::image::swap(lhs, rhs);
}
template <typename ExtentsType>
auto swap(typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutLeft>::mapping_type& lhs,
          typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutLeft>::mapping_type& rhs) noexcept
    -> void {
  ::image::swap(lhs, rhs);
}
template <typename ExtentsType>
auto swap(typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutStride>::mapping_type& lhs,
          typename ::image::LayoutTraits<
              ExtentsType, ::image::LayoutStride>::mapping_type& rhs) noexcept
    -> void {
  ::image::swap(lhs, rhs);
}
}  // namespace std

#endif  // OPENXYZ_IMAGE_LAYOUT_HXX
