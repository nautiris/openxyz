#pragma once

#if !defined(OPENXYZ_IMAGE_PIXEL_HXX)
#define OPENXYZ_IMAGE_PIXEL_HXX

#include "math.hpp"

#include <assert.h>
#include <stddef.h>

namespace image {

enum PixelDepth : uint8_t {
  kDepthNone,
  k8U,
  k8S,
  k16U,
  k16S,
  k32U,
  k32S,
  k32F,
  k64U,
  k64S,
  k64F,
};

namespace _details {

constexpr uint32_t kPixelDepthMask{0x0000'0FFF};
constexpr uint8_t kPixelDepthShift{0};
constexpr uint32_t kPixelChannelMask{0x0000'F000};
constexpr uint8_t kPixelChannelShift{12};

constexpr auto make_pixel_type(::image::PixelDepth depth, uint8_t channel)
    -> uint32_t {
  return static_cast<uint32_t>(depth) << kPixelDepthShift |
         static_cast<uint32_t>(channel - 1) << kPixelChannelShift;
}

}  // namespace _details

enum PixelType : uint16_t {
  kTypeNone = 0,
  k8UC1 = _details::make_pixel_type(::image::PixelDepth::k8U, 1),
  k8UC2 = _details::make_pixel_type(::image::PixelDepth::k8U, 2),
  k8UC3 = _details::make_pixel_type(::image::PixelDepth::k8U, 3),
  k8UC4 = _details::make_pixel_type(::image::PixelDepth::k8U, 4),
  k8SC1 = _details::make_pixel_type(::image::PixelDepth::k8S, 1),
  k8SC2 = _details::make_pixel_type(::image::PixelDepth::k8S, 2),
  k8SC3 = _details::make_pixel_type(::image::PixelDepth::k8S, 3),
  k8SC4 = _details::make_pixel_type(::image::PixelDepth::k8S, 4),
  k16UC1 = _details::make_pixel_type(::image::PixelDepth::k16U, 1),
  k16UC2 = _details::make_pixel_type(::image::PixelDepth::k16U, 2),
  k16UC3 = _details::make_pixel_type(::image::PixelDepth::k16U, 3),
  k16UC4 = _details::make_pixel_type(::image::PixelDepth::k16U, 4),
  k16SC1 = _details::make_pixel_type(::image::PixelDepth::k16S, 1),
  k16SC2 = _details::make_pixel_type(::image::PixelDepth::k16S, 2),
  k16SC3 = _details::make_pixel_type(::image::PixelDepth::k16S, 3),
  k16SC4 = _details::make_pixel_type(::image::PixelDepth::k16S, 4),
  k32UC1 = _details::make_pixel_type(::image::PixelDepth::k32U, 1),
  k32UC2 = _details::make_pixel_type(::image::PixelDepth::k32U, 2),
  k32UC3 = _details::make_pixel_type(::image::PixelDepth::k32U, 3),
  k32UC4 = _details::make_pixel_type(::image::PixelDepth::k32U, 4),
  k32SC1 = _details::make_pixel_type(::image::PixelDepth::k32S, 1),
  k32SC2 = _details::make_pixel_type(::image::PixelDepth::k32S, 2),
  k32SC3 = _details::make_pixel_type(::image::PixelDepth::k32S, 3),
  k32SC4 = _details::make_pixel_type(::image::PixelDepth::k32S, 4),
  k32FC1 = _details::make_pixel_type(::image::PixelDepth::k32F, 1),
  k32FC2 = _details::make_pixel_type(::image::PixelDepth::k32F, 2),
  k32FC3 = _details::make_pixel_type(::image::PixelDepth::k32F, 3),
  k32FC4 = _details::make_pixel_type(::image::PixelDepth::k32F, 4),
  k64UC1 = _details::make_pixel_type(::image::PixelDepth::k64U, 1),
  k64UC2 = _details::make_pixel_type(::image::PixelDepth::k64U, 2),
  k64UC3 = _details::make_pixel_type(::image::PixelDepth::k64U, 3),
  k64UC4 = _details::make_pixel_type(::image::PixelDepth::k64U, 4),
  k64SC1 = _details::make_pixel_type(::image::PixelDepth::k64S, 1),
  k64SC2 = _details::make_pixel_type(::image::PixelDepth::k64S, 2),
  k64SC3 = _details::make_pixel_type(::image::PixelDepth::k64S, 3),
  k64SC4 = _details::make_pixel_type(::image::PixelDepth::k64S, 4),
  k64FC1 = _details::make_pixel_type(::image::PixelDepth::k64F, 1),
  k64FC2 = _details::make_pixel_type(::image::PixelDepth::k64F, 2),
  k64FC3 = _details::make_pixel_type(::image::PixelDepth::k64F, 3),
  k64FC4 = _details::make_pixel_type(::image::PixelDepth::k64F, 4),
};

namespace _details {

constexpr auto channel(PixelType type) noexcept -> uint8_t {
  if (type == PixelType::kTypeNone) {
    return 0;
  }
  return ((static_cast<uint32_t>(type) & _details::kPixelChannelMask) >>
          _details::kPixelChannelShift) +
         1;
}

constexpr auto depth(PixelType type) noexcept -> PixelDepth {
  if (type == PixelType::kTypeNone) {
    return PixelDepth::kDepthNone;
  }
  return static_cast<PixelDepth>(
      (static_cast<uint32_t>(type) & _details::kPixelDepthMask) >>
      _details::kPixelDepthShift);
}

template <PixelDepth D> struct DepthTraits {
  using type = void;
  static constexpr size_t size = 0;
  static constexpr PixelDepth depth = PixelDepth::kDepthNone;
};
template <> struct DepthTraits<PixelDepth::k8U> {
  using type = uint8_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k8U;
};
template <> struct DepthTraits<PixelDepth::k8S> {
  using type = int8_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k8S;
};
template <> struct DepthTraits<PixelDepth::k16U> {
  using type = uint16_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k16U;
};
template <> struct DepthTraits<PixelDepth::k16S> {
  using type = int16_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k16S;
};
template <> struct DepthTraits<PixelDepth::k32U> {
  using type = uint32_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32U;
};
template <> struct DepthTraits<PixelDepth::k32S> {
  using type = int32_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32S;
};
template <> struct DepthTraits<PixelDepth::k32F> {
  using type = float;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32F;
};
template <> struct DepthTraits<PixelDepth::k64U> {
  using type = uint64_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64U;
};
template <> struct DepthTraits<PixelDepth::k64S> {
  using type = int64_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64S;
};
template <> struct DepthTraits<PixelDepth::k64F> {
  using type = double;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64F;
};

template <typename T> struct DepthRevTraits;
template <> struct DepthRevTraits<uint8_t> {
  using type = uint8_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k8U;
};
template <> struct DepthRevTraits<int8_t> {
  using type = int8_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k8S;
};
template <> struct DepthRevTraits<uint16_t> {
  using type = uint16_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k16U;
};
template <> struct DepthRevTraits<int16_t> {
  using type = int16_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k16S;
};
template <> struct DepthRevTraits<uint32_t> {
  using type = uint32_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32U;
};
template <> struct DepthRevTraits<int32_t> {
  using type = int32_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32U;
};
template <> struct DepthRevTraits<float> {
  using type = float;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k32F;
};
template <> struct DepthRevTraits<uint64_t> {
  using type = uint64_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64U;
};
template <> struct DepthRevTraits<int64_t> {
  using type = int64_t;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64S;
};
template <> struct DepthRevTraits<double> {
  using type = double;
  static constexpr size_t size = sizeof(type);
  static constexpr PixelDepth depth = PixelDepth::k64F;
};

NAUTIRIS_ALWAYS_INLINE constexpr auto depth_size(PixelDepth depth) -> size_t {
  switch (depth) {
  case ::image::PixelDepth::kDepthNone:
    return 0;
  case ::image::PixelDepth::k8U:
    [[fallthrough]];
  case ::image::PixelDepth::k8S:
    return 1;
  case ::image::PixelDepth::k16U:
    [[fallthrough]];
  case ::image::PixelDepth::k16S:
    return 2;
  case ::image::PixelDepth::k32U:  // NOLINT(bugprone-branch-clone)
    [[fallthrough]];
  case ::image::PixelDepth::k32S:
    [[fallthrough]];
  case ::image::PixelDepth::k32F:
    return 4;
  case ::image::PixelDepth::k64U:  // NOLINT(bugprone-branch-clone)
    [[fallthrough]];
  case ::image::PixelDepth::k64S:
    [[fallthrough]];
  case ::image::PixelDepth::k64F:
    return 8;
  }
  return 0;
}

}  // namespace _details

// NOLINTBEGIN(modernize-use-nodiscard)

template <typename T, size_t Channel> struct MapToPixel {
 private:
  using Traits = ::math::GraphicsVectorTraits<T, Channel>;

 public:
  using element_type = typename Traits::value_type;
  using type = typename Traits::vector_type;
  using reference = type&;
  using const_reference = type const&;
  using pointer = type*;
  using const_pointer = type const*;
  static constexpr PixelDepth depth =
      _details::DepthRevTraits<element_type>::depth;
  static constexpr size_t channel = Traits::dimension;
  static constexpr PixelType ty =
      static_cast<PixelType>(_details::make_pixel_type(depth, channel));
  static constexpr size_t stride = sizeof(type);

  NAUTIRIS_ALWAYS_INLINE constexpr MapToPixel() noexcept = default;
  NAUTIRIS_ALWAYS_INLINE constexpr auto get_depth() const noexcept
      -> PixelDepth {
    return depth;
  }
  NAUTIRIS_ALWAYS_INLINE constexpr auto get_channel() const noexcept -> size_t {
    return channel;
  }
  NAUTIRIS_ALWAYS_INLINE constexpr auto get_stride() const noexcept -> size_t {
    return stride;
  }
};

namespace _details {
constexpr auto check_swizzle(size_t channel, ::math::Channel swz_x,
                             ::math::Channel swz_y, ::math::Channel swz_z,
                             ::math::Channel swz_w) -> bool {
  bool pass = true;
  switch (channel) {
  case 4:
    pass = pass && (static_cast<uint8_t>(swz_w) < channel ||
                    swz_w == ::math::Channel::Dyn);
    [[fallthrough]];
  case 3:
    pass = pass && (static_cast<uint8_t>(swz_z) < channel ||
                    swz_z == ::math::Channel::Dyn);
    [[fallthrough]];
  case 2:
    pass = pass && (static_cast<uint8_t>(swz_y) < channel ||
                    swz_y == ::math::Channel::Dyn);
    [[fallthrough]];
  default:
    pass = pass && (static_cast<uint8_t>(swz_x) < channel ||
                    swz_x == ::math::Channel::Dyn);
    break;
  }
  return pass;
}

template <::math::Channel... KChannels>
constexpr auto channel_dynamic() -> size_t {
  return ((KChannels == ::math::Channel::Dyn ? 1 : 0) + ...);
}
}  // namespace _details

template <PixelType Ty, ::math::Channel X = ::math::Channel::X,
          ::math::Channel Y = ::math::Channel::Y,
          ::math::Channel Z = ::math::Channel::Z,
          ::math::Channel W = ::math::Channel::W>
struct PixelTraits
    : private ::std::array<uint8_t, _details::channel_dynamic<X, Y, Z, W>()> {
 private:
  using Traits = ::math::GraphicsVectorTraits<
      typename _details::DepthTraits<_details::depth(Ty)>::type,
      _details::channel(Ty)>;

 public:
  using element_type = typename Traits::value_type;
  using type = typename Traits::vector_type;
  using reference = type&;
  using const_reference = type const&;
  using pointer = type*;
  using const_pointer = type const*;

  static constexpr PixelType ty = Ty;
  static constexpr bool is_dynamic = ty == PixelType::kTypeNone;
  static constexpr PixelDepth depth = _details::depth(ty);
  static constexpr size_t channel = Traits::dimension;
  static constexpr size_t stride = is_dynamic ? 0 : sizeof(type);

  static_assert(is_dynamic || _details::check_swizzle(channel, X, Y, Z, W),
                "Swizzle's channel cannot out of bounds.");

 private:
  static constexpr size_t dyn_channel = _details::channel_dynamic<X, Y, Z, W>();
  using underlying_type = ::std::array<uint8_t, dyn_channel>;
  ::std::array<PixelType, is_dynamic ? 1 : 0> type_;

 public:
  constexpr PixelTraits() noexcept
      : PixelTraits{is_dynamic ? PixelType::k32FC4 : ty} {}
  constexpr explicit PixelTraits(
      PixelType pixel_type, ::math::Channel swz_x = ::math::Channel::X,
      ::math::Channel swz_y = ::math::Channel::Y,
      ::math::Channel swz_z = ::math::Channel::Z,
      ::math::Channel swz_w = ::math::Channel::W) noexcept
      : type_{} {
    if constexpr (is_dynamic) {
      type_[0] = pixel_type;
    }
    size_t idx = 0;
    if (X == ::math::Channel::Dyn &&
        idx < ::std::min(dyn_channel, get_channel())) {
      assert(uint8_t(swz_x) < get_channel());
      underlying_type::operator[](idx++) = static_cast<uint8_t>(swz_x);
    }
    if (Y == ::math::Channel::Dyn &&
        idx < ::std::min(dyn_channel, get_channel())) {
      assert(uint8_t(swz_y) < get_channel());
      underlying_type::operator[](idx++) = static_cast<uint8_t>(swz_y);
    }
    if (Z == ::math::Channel::Dyn &&
        idx < ::std::min(dyn_channel, get_channel())) {
      assert(uint8_t(swz_z) < get_channel());
      underlying_type::operator[](idx++) = static_cast<uint8_t>(swz_z);
    }
    if (W == ::math::Channel::Dyn &&
        idx < ::std::min(dyn_channel, get_channel())) {
      assert(uint8_t(swz_w) < get_channel());
      underlying_type::operator[](idx++) = static_cast<uint8_t>(swz_w);
    }
  }
  template <PixelType OtherTy, ::math::Channel... Swizzles>
  constexpr explicit PixelTraits(
      PixelTraits<OtherTy, Swizzles...> const& other) noexcept
      : PixelTraits{other.type_enum()} {
    using other_underlying_type =
        typename PixelTraits<OtherTy, Swizzles...>::underlying_type;
    for (size_t i = 0, end = ::std::min(dyn_channel, other.dyn_channel);
         i < end; ++i) {
      underlying_type::operator[](i) = other_underlying_type::operator[](i);
    }
  }
  constexpr PixelTraits(PixelTraits const&) = default;
  constexpr PixelTraits(PixelTraits&&) noexcept = default;
  ~PixelTraits() noexcept = default;

  constexpr auto operator=(PixelTraits const&) -> PixelTraits& = default;
  constexpr auto operator=(PixelTraits&&) -> PixelTraits& = default;

  NAUTIRIS_ALWAYS_INLINE [[nodiscard]] constexpr auto type_enum() const noexcept
      -> PixelType {
    if constexpr (is_dynamic) {
      return type_[0];
    } else {
      return ty;
    }
  }
  NAUTIRIS_ALWAYS_INLINE [[nodiscard]] constexpr auto get_depth() const noexcept
      -> PixelDepth {
    return _details::depth(type_enum());
  }
  NAUTIRIS_ALWAYS_INLINE [[nodiscard]] constexpr auto
  get_channel() const noexcept -> size_t {
    return _details::channel(type_enum());
  }
  NAUTIRIS_ALWAYS_INLINE [[nodiscard]] constexpr auto
  get_stride() const noexcept -> size_t {
    return _details::depth_size(get_depth()) * get_channel();
  }
  constexpr auto get_swizzle() const noexcept -> ::std::array<uint8_t, 4> {
    constexpr ::std::array<uint8_t, 4> kSwizzle{
        static_cast<uint8_t>(X), static_cast<uint8_t>(Y),
        static_cast<uint8_t>(Z), static_cast<uint8_t>(W)};
    ::std::array<uint8_t, 4> ret{0};
    if constexpr (dyn_channel == 0) {
      memcpy(ret.data(), kSwizzle.data(), get_channel());
    } else {
      for (size_t i = 0, j = 0, end = get_channel(); i < end; ++i) {
        ret[i] = kSwizzle[i] == static_cast<uint8_t>(::math::Channel::Dyn)
                     ? underlying_type::operator[](j++)
                     : kSwizzle[i];
      }
    }
    return ret;
  }
};

PixelTraits(PixelType)
    -> PixelTraits<PixelType::kTypeNone, ::math::Channel::Dyn,
                   ::math::Channel::Dyn, ::math::Channel::Dyn,
                   ::math::Channel::Dyn>;
PixelTraits(PixelType, ::math::Channel)
    -> PixelTraits<PixelType::kTypeNone, ::math::Channel::Dyn,
                   ::math::Channel::Dyn, ::math::Channel::Dyn,
                   ::math::Channel::Dyn>;
PixelTraits(PixelType, ::math::Channel, ::math::Channel)
    -> PixelTraits<PixelType::kTypeNone, ::math::Channel::Dyn,
                   ::math::Channel::Dyn, ::math::Channel::Dyn>;
PixelTraits(PixelType, ::math::Channel, ::math::Channel, ::math::Channel)
    -> PixelTraits<PixelType::kTypeNone, ::math::Channel::Dyn,
                   ::math::Channel::Dyn, ::math::Channel::Dyn,
                   ::math::Channel::Dyn>;
PixelTraits(PixelType, ::math::Channel, ::math::Channel, ::math::Channel,
            ::math::Channel)
    -> PixelTraits<PixelType::kTypeNone, ::math::Channel::Dyn,
                   ::math::Channel::Dyn, ::math::Channel::Dyn,
                   ::math::Channel::Dyn>;

template <PixelType Type> using Pixel = typename PixelTraits<Type>::type;
using PixelU8C1 = Pixel<PixelType::k8UC1>;
using PixelU8C2 = Pixel<PixelType::k8UC2>;
using PixelU8C3 = Pixel<PixelType::k8UC3>;
using PixelU8C4 = Pixel<PixelType::k8UC4>;
using PixelS8C1 = Pixel<PixelType::k8SC1>;
using PixelS8C2 = Pixel<PixelType::k8SC2>;
using PixelS8C3 = Pixel<PixelType::k8SC3>;
using PixelS8C4 = Pixel<PixelType::k8SC4>;
using PixelU16C1 = Pixel<PixelType::k16UC1>;
using PixelU16C2 = Pixel<PixelType::k16UC2>;
using PixelU16C3 = Pixel<PixelType::k16UC3>;
using PixelU16C4 = Pixel<PixelType::k16UC4>;
using PixelS16C1 = Pixel<PixelType::k16SC1>;
using PixelS16C2 = Pixel<PixelType::k16SC2>;
using PixelS16C3 = Pixel<PixelType::k16SC3>;
using PixelS16C4 = Pixel<PixelType::k16SC4>;
using PixelU32C1 = Pixel<PixelType::k32UC1>;
using PixelU32C2 = Pixel<PixelType::k32UC2>;
using PixelU32C3 = Pixel<PixelType::k32UC3>;
using PixelU32C4 = Pixel<PixelType::k32UC4>;
using PixelS32C1 = Pixel<PixelType::k32SC1>;
using PixelS32C2 = Pixel<PixelType::k32SC2>;
using PixelS32C3 = Pixel<PixelType::k32SC3>;
using PixelS32C4 = Pixel<PixelType::k32SC4>;
using PixelF32C1 = Pixel<PixelType::k32FC1>;
using PixelF32C2 = Pixel<PixelType::k32FC2>;
using PixelF32C3 = Pixel<PixelType::k32FC3>;
using PixelF32C4 = Pixel<PixelType::k32FC4>;
using PixelU64C1 = Pixel<PixelType::k64UC1>;
using PixelU64C2 = Pixel<PixelType::k64UC2>;
using PixelU64C3 = Pixel<PixelType::k64UC3>;
using PixelU64C4 = Pixel<PixelType::k64UC4>;
using PixelS64C1 = Pixel<PixelType::k64SC1>;
using PixelS64C2 = Pixel<PixelType::k64SC2>;
using PixelS64C3 = Pixel<PixelType::k64SC3>;
using PixelS64C4 = Pixel<PixelType::k64SC4>;
using PixelF64C1 = Pixel<PixelType::k64FC1>;
using PixelF64C2 = Pixel<PixelType::k64FC2>;
using PixelF64C3 = Pixel<PixelType::k64FC3>;
using PixelF64C4 = Pixel<PixelType::k64FC4>;

static constexpr PixelType kDynamicPixel = PixelType::kTypeNone;
using DynPixelTrait =
    PixelTraits<kDynamicPixel, ::math::Channel::Dyn, ::math::Channel::Dyn,
                ::math::Channel::Dyn, ::math::Channel::Dyn>;

// NOLINTEND(modernize-use-nodiscard)

}  // namespace image

#endif  // OPENXYZ_IMAGE_PIXEL_HXX
