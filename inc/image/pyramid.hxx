#pragma once

#ifndef OPENXYZ_IMAGE_PYRAMID_HXX
#define OPENXYZ_IMAGE_PYRAMID_HXX

#include "image.hxx"
#include "view.hxx"

#include <utility>

namespace image::pyramid {

static constexpr uint32_t kAverageDim = 3;
static constexpr uint32_t kAverageKernel1D[kAverageDim] = {
    1,
    1,
    1,
};
static constexpr uint32_t kAverageKernel2D[kAverageDim][kAverageDim] = {
    {1, 1, 1},
    {1, 1, 1},
    {1, 1, 1},
};

static constexpr uint32_t kGaussianDim = 5;
static constexpr uint32_t kGaussianKernel1D[kGaussianDim] = {
    1, 4, 6, 4, 1,
};
static constexpr uint32_t kGaussianKernel2D[kGaussianDim][kGaussianDim] = {
    {1,  4,  6,  4, 1},
    {4, 16, 24, 16, 4},
    {6, 24, 36, 24, 6},
    {4, 16, 24, 16, 4},
    {1,  4,  6,  4, 1},
};

enum class FilterMode : uint8_t {
  kAverage,
  kGaussian,
};

template <PixelType Format, typename Extents, typename LayoutPolicy,
          typename Accessor>
auto create_texture1d(
    View<DynPixelTrait, Extents, LayoutPolicy, Accessor> const& view,
    FilterMode mode = FilterMode::kAverage) -> ::std::tuple<Image, size_t>;

template <typename PixelTrait, typename Extents, typename LayoutPolicy,
          typename Accessor,
          typename = ::std ::enable_if_t<!PixelTrait ::is_dynamic>*>
auto create_texture1d(
    View<PixelTrait, Extents, LayoutPolicy, Accessor> const& view,
    FilterMode mode = FilterMode::kAverage) -> ::std::tuple<Image, size_t>;

}  // namespace image::pyramid

#endif  // OPENXYZ_IMAGE_PYRAMID_HXX
