#pragma once

#if !defined(OPENXYZ_IMAGE_PYRAMID_TCC)
#define OPENXYZ_IMAGE_PYRAMID_TCC

#include "pyramid.hxx"
#include "utils.hpp"

#include <numeric>

namespace image::pyramid {

namespace _details {

template <PixelType Format, typename PixelTrait, typename Extents,
          typename Layout, typename Accessor, typename IndexT>
constexpr auto
accumulate(Pixel<Format>& pixel,
           View<PixelTrait, Extents, Layout, Accessor> const& src, IndexT idx,
           uint32_t dim, double weight, uint32_t const* factor) -> void {
  using DataT = typename Pixel<Format>::value_type;
  for (IndexT offset = 0, anchor = offset - (dim >> 1); offset < IndexT(dim);
       ++offset) {
    pixel += src.template as<Format>(idx + anchor + offset) *
             static_cast<DataT>(factor[offset]);
  }
  pixel = static_cast<Pixel<Format>>(pixel * weight);
}

template <PixelType Format, typename PixelTraitSrc, typename ExtentsSrc,
          typename LayoutPolicySrc, typename AccessorSrc,
          typename PixelTraitDst, typename ExtentsDst, typename LayoutPolicyDst,
          typename AccessorDst>
auto create_texture1d_impl(
    View<PixelTraitSrc, ExtentsSrc, LayoutPolicySrc, AccessorSrc> const& src,
    View<PixelTraitDst, ExtentsDst, LayoutPolicyDst, AccessorDst>& dst,
    uint32_t dim, uint32_t const* kernel) -> void {
  NAUTIRIS_ASSUME(dim == 3 || dim == 5);
  using ExtentT = typename ExtentsSrc::index_type;
  ExtentT const kLoopCount =
      ::std::max(src.extents().extent(0) >> 1,
                 static_cast<ExtentT>(dim == 5 ? 1 : 0)) -
      (dim == 5 ? 1 : 0);
  ExtentT const kCleanLast =
      src.extents().extent(0) - (kLoopCount << 1) - (dim == 5 ? 1 : 0);
  uint32_t factor[5]{0};
  memcpy(factor + (dim >> 1), kernel + (dim >> 1),
         sizeof(dim) * (dim - (dim >> 1)));
  double weight =
      1. / static_cast<double>(::std::accumulate(factor, factor + dim, 0U));
  size_t sampler = 0;
  for (ExtentT i = 0; i < kLoopCount; ++i) {
    accumulate<Format>(dst.template as<Format>(sampler++), src, i << 1, dim,
                       weight, factor);
    NAUTIRIS_UNLIKELY_IF (i == 0) {
      memcpy(factor, kernel, sizeof(dim) * dim);
      weight =
          1. / static_cast<double>(::std::accumulate(factor, factor + dim, 0U));
    }
  }
  if (kCleanLast != 0) {
    ExtentT i = src.extents().extent(0) - (kCleanLast == (dim >> 1) ? 1 : 2);
    memset(factor + (dim - kCleanLast), 0, sizeof(dim) * kCleanLast);
    weight =
        1. / static_cast<double>(::std::accumulate(factor, factor + dim, 0U));
    accumulate<Format>(dst.template as<Format>(sampler), src, i, dim, weight,
                       factor);
  }
}

template <PixelType Format, typename PixelTrait, typename Extents,
          typename LayoutPolicy, typename Accessor,
          typename = ::std::enable_if_t<PixelTraits<Format>::channel == 4>*>
auto create_texture1d(
    View<PixelTrait, Extents, LayoutPolicy, Accessor> const& view,
    FilterMode mode) -> ::std::tuple<Image, size_t> {
  static_assert(Extents::rank() == 1);
  assert(view.pixel_trait().type_enum() == Format);
  uint32_t dim;
  uint32_t const* kernel;
  switch (mode) {
  case FilterMode::kAverage:
    dim = kAverageDim;
    kernel = kAverageKernel1D;
    break;
  case FilterMode::kGaussian:
    dim = kGaussianDim;
    kernel = kGaussianKernel1D;
    break;
  }
  size_t const kSizeInBytes = view.size() * view.pixel_trait().get_stride();
  size_t num_levels = 0;
  Image img{::std::array{view.size() * 2}, Format};
  memcpy(img.data(), view.data(), kSizeInBytes);
  memset(img.data() + kSizeInBytes, 0, kSizeInBytes);
  auto* data = reinterpret_cast<Pixel<Format>*>(img.data());
  for (auto size = view.size() >> 1, prev = view.size(); size > 0;
       data += prev, prev = size, size >>= 1) {
    auto src =
        View{Format, reinterpret_cast<Image::data_type const*>(data), prev};
    auto dst = View{
        Format, reinterpret_cast<Image::data_type const*>(data + prev), size};
    create_texture1d_impl<Format>(src, dst, dim, kernel);
    ++num_levels;
  }
  return ::std::make_tuple(img, num_levels + 1);
}

}  // namespace _details

template <PixelType Format, typename Extents, typename LayoutPolicy,
          typename Accessor>
auto create_texture1d(
    View<DynPixelTrait, Extents, LayoutPolicy, Accessor> const& view,
    FilterMode mode) -> ::std::tuple<Image, size_t> {
  return _details::create_texture1d<Format>(view, mode);
}

template <typename PixelTrait, typename Extents, typename LayoutPolicy,
          typename Accessor, typename>
auto create_texture1d(
    View<PixelTrait, Extents, LayoutPolicy, Accessor> const& view,
    FilterMode mode) -> ::std::tuple<Image, size_t> {
  return _details::create_texture1d<PixelTrait::ty>(view, mode);
}

}  // namespace image::pyramid

#endif  // OPENXYZ_IMAGE_PYRAMID_TCC
