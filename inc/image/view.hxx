#pragma once

#if !defined(OPENXYZ_IMAGE_VIEW_HXX)
#define OPENXYZ_IMAGE_VIEW_HXX

#include "image/accessor.hxx"
#include "image/extents.hxx"
#include "image/image.hxx"
#include "image/layout.hxx"
#include "image/pixel.hxx"

#include <array>
#include <stddef.h>
#include <stdlib.h>
#include <type_traits>
#include <utility>

namespace image {

template <
    typename PixelType, typename Extents, typename LayoutPolicy = LayoutRight,
    typename Accessor = DefaultAccessor<PixelType>,
    typename = ::std::void_t<
        typename PixelType::element_type,
        typename LayoutTraits<Extents, LayoutPolicy>::layout_type,
        typename LayoutTraits<Extents, LayoutPolicy>::mapping_type>,
    typename = ::std::enable_if_t<
        (Extents::rank() > 0) && (Extents::rank() >= Extents::rank_dynamic())>*>
class View;

namespace _details {
template <typename Extents, size_t... Indexes>
static constexpr auto
empty_aux(Extents const& exts,
          ::std::integer_sequence<size_t, Indexes...> /*unused*/) noexcept
    -> bool {
  using IdxT = typename Extents::index_type;
  return ((exts.extent(Indexes) == static_cast<IdxT>(0)) || ...);
}
}  // namespace _details

template <typename PixelTrait, typename Extents, typename LayoutPolicy,
          typename Accessor>
class View<PixelTrait, Extents, LayoutPolicy, Accessor>
    : private PixelTrait
    , private LayoutTraits<Extents, LayoutPolicy>::mapping_type
    , private Accessor {
 public:
  // Member types
  using pixel_trait_type = PixelTrait;
  using accessor_type = Accessor;
  using extents_type = Extents;
  using layout_type = LayoutPolicy;
  using mapping_type =
      typename LayoutTraits<extents_type, layout_type>::mapping_type;

  using index_type = typename mapping_type::index_type;
  using size_type = typename mapping_type::size_type;
  using rank_type = typename mapping_type::rank_type;

  using element_type = typename accessor_type::vector_type;
  using reference = typename accessor_type::reference;
  using const_reference = typename accessor_type::const_reference;
  using pointer = typename accessor_type::pointer;
  using const_pointer = typename accessor_type::const_pointer;

  using underlying_pointer = typename accessor_type::underlying_pointer;
  using underlying_const_pointer =
      typename accessor_type::underlying_const_pointer;

  // Constructor
  constexpr View() noexcept = default;

  template <typename... OtherIndexTypes,
            typename = ::std::enable_if_t<
                (::std::is_convertible_v<OtherIndexTypes, index_type> && ...) &&
                sizeof...(OtherIndexTypes) == extents_type::rank() &&
                sizeof...(OtherIndexTypes) == extents_type::rank_dynamic()>*>
  constexpr View(pixel_trait_type pixel_type, underlying_const_pointer data,
                 OtherIndexTypes... exts)
      : View{pixel_type, data,
             extents_type{::std::forward<OtherIndexTypes>(exts)...}} {}

  template <
      typename OtherIndexType, size_type N,
      typename = ::std::enable_if_t<
          ::std::is_convertible_v<OtherIndexType, index_type> &&
          N == extents_type::rank() && N == extents_type::rank_dynamic()>*>
  constexpr View(pixel_trait_type ty, underlying_const_pointer data,
                 ::std::array<OtherIndexType, N> const& exts)
      : View{ty, data, mapping_type{exts}} {}

  constexpr View(pixel_trait_type pixel_type, underlying_const_pointer data,
                 extents_type const& exts)
      : View{pixel_type, data, mapping_type{exts}} {}

  constexpr View(pixel_trait_type pixel_type, underlying_const_pointer data,
                 mapping_type const& map)
      : pixel_trait_type{pixel_type}, mapping_type{map},
        ptr_{const_cast<pointer>(reinterpret_cast<const_pointer>(data))} {}

  template <typename... OtherIndexTypes,
            typename = ::std::enable_if_t<
                sizeof...(OtherIndexTypes) == extents_type::rank() &&
                sizeof...(OtherIndexTypes) == extents_type::rank_dynamic()>*>
  constexpr View(PixelType pixel_type, underlying_const_pointer data,
                 OtherIndexTypes... exts)
      : View{pixel_trait_type{pixel_type}, data,
             mapping_type{::std::forward<OtherIndexTypes>(exts)...}} {}

  template <
      typename OtherIndexType, size_type N,
      typename = ::std::enable_if_t<
          ::std::is_convertible_v<OtherIndexType, index_type> &&
          N == extents_type::rank() && N == extents_type::rank_dynamic()>*>
  constexpr View(PixelType pixel_type, underlying_const_pointer data,
                 ::std::array<OtherIndexType, N> const& exts)
      : View{pixel_trait_type{pixel_type}, data, mapping_type{exts}} {}

  constexpr View(PixelType pixel_type, underlying_const_pointer data,
                 extents_type const& exts)
      : View{pixel_trait_type{pixel_type}, data, mapping_type{exts}} {}

  constexpr View(PixelType pixel_type, underlying_const_pointer data,
                 mapping_type const& map)
      : View{pixel_trait_type{pixel_type}, data, map} {}

  template <typename... OtherIndexTypes>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr View(Image const& img, OtherIndexTypes... exts)
      : View{pixel_trait_type{img.type()}, img.data(),
             ::std::forward<OtherIndexTypes>(exts)...} {}

  template <typename OtherIndexType, size_type N>
  constexpr View(Image const& img, ::std::array<OtherIndexType, N> const& exts)
      : View{pixel_trait_type{img.type()}, img.data(), exts} {}

  constexpr View(Image const& img, extents_type const& exts)
      : View{pixel_trait_type{img.type()}, img.data(), exts} {}

  constexpr View(Image const& img, mapping_type const& map)
      : View{pixel_trait_type{img.type()}, img.data(), map} {}

  template <typename OtherPixelType, typename OtherExtents,
            typename OtherLayout>
  constexpr explicit View(
      View<OtherPixelType, OtherExtents, OtherLayout> const& other)
      : pixel_trait_type{other.pixel_type::type_enum()},
        layout_type{other.layout_type::extents()}, ptr_{other.data()} {}

  ~View() noexcept = default;

  constexpr View(View const& other) = default;
  constexpr View(View&& other) noexcept = default;

  // operator=
  constexpr auto operator=(View const& rhs) -> View& = default;
  constexpr auto operator=(View&& rhs) noexcept -> View& = default;

  // Observers
  constexpr auto size() const noexcept -> size_type {
    return _details::fold_mul(
        mapping_type::extents(),
        ::std::make_index_sequence<extents_type::rank()>{});
  }
  [[nodiscard]] constexpr auto empty() const noexcept -> bool {
    return extents_type::rank() == 0 ||
           _details::empty_aux(
               mapping_type::extents(),
               ::std::make_index_sequence<extents_type::rank()>{});
  }
  constexpr auto pixel_trait() noexcept -> pixel_trait_type& {
    return static_cast<pixel_trait_type&>(*this);
  }
  constexpr auto pixel_trait() const noexcept -> pixel_trait_type const& {
    return static_cast<pixel_trait_type const&>(*this);
  }
  constexpr auto stride(rank_type rnk) const noexcept -> index_type {
    return mapping_type::stride(rnk);
  }
  constexpr auto extents() const noexcept -> extents_type const& {
    return mapping_type::extents();
  }
  constexpr auto mapping() noexcept -> mapping_type& {
    return static_cast<mapping_type&>(*this);
  }
  constexpr auto mapping() const noexcept -> mapping_type const& {
    return static_cast<mapping_type const&>(*this);
  }
  constexpr auto accessor() noexcept -> accessor_type& {
    return static_cast<accessor_type&>(*this);
  }
  constexpr auto accessor() const noexcept -> accessor_type const& {
    return static_cast<accessor_type const&>(*this);
  }

  constexpr auto data() noexcept -> pointer {
    return const_cast<pointer>(static_cast<View const*>(this)->data());
  }
  constexpr auto data() const noexcept -> const_pointer { return ptr_; }

  //  Element access
  template <PixelType CvtTy, typename OtherIndexType, size_type N>
  constexpr auto as(::std::array<OtherIndexType, N> const& indices) noexcept
      -> decltype(auto) {
    using RetT = ::std::add_lvalue_reference_t<
        ::std::remove_const_t<::std::remove_reference_t<
            decltype(::std::declval<View const>()
                         .template as<CvtTy, OtherIndexType, N>(
                             ::std::declval<
                                 ::std::array<OtherIndexType, N>>()))>>>;
    return const_cast<RetT>(static_cast<View const*>(this)->as<CvtTy>(indices));
  }
  template <PixelType CvtTy, typename OtherIndexType, size_type N>
  constexpr auto
  as(::std::array<OtherIndexType, N> const& indices) const noexcept
      -> ::std::add_lvalue_reference_t<::std::remove_pointer_t<
          decltype(::std::declval<View const>().accessor_type::
                       template offset<CvtTy>(::std::declval<const_pointer>(),
                                              ::std::declval<size_type>()))>> {
    return *accessor_type::template offset<CvtTy>(
        data(), mapping_type::operator()(indices));
  }
  template <PixelType CvtTy, typename... OtherIndexTypes>
  constexpr auto as(OtherIndexTypes... indices) noexcept -> decltype(auto) {
    using RetT = ::std::add_lvalue_reference_t<
        ::std::remove_const_t<::std::remove_reference_t<
            decltype(::std::declval<View const>()
                         .template as<CvtTy, OtherIndexTypes...>(
                             ::std::declval<OtherIndexTypes>()...))>>>;
    return const_cast<RetT>(static_cast<View const*>(this)->as<CvtTy>(
        ::std::forward<OtherIndexTypes>(indices)...));
  }
  template <PixelType CvtTy, typename... OtherIndexTypes>
  constexpr auto as(OtherIndexTypes... indices) const noexcept
      -> decltype(::std::declval<View const>()
                      .template as<CvtTy, index_type,
                                   sizeof...(OtherIndexTypes)>(
                          ::std::declval<::std::array<
                              index_type, sizeof...(OtherIndexTypes)>>())) {
    return as<CvtTy>(::std::array<index_type, sizeof...(OtherIndexTypes)>{
        static_cast<index_type>(indices)...});
  }

  template <typename... OtherIndexTypes>
  constexpr auto operator()(OtherIndexTypes... indices) noexcept -> pointer {
    return const_cast<pointer>(
        static_cast<View const*>(this)->operator()(indices...));
  }
  template <typename... OtherIndexTypes>
  constexpr auto operator()(OtherIndexTypes... indices) const noexcept
      -> const_pointer {
    return operator()(::std::array<index_type, sizeof...(OtherIndexTypes)>{
        static_cast<index_type>(indices)...});
  }
  template <typename OtherIndexType, size_type N>
  constexpr auto
  operator()(::std::array<OtherIndexType, N> const& indices) noexcept
      -> pointer {
    return const_cast<pointer>(
        static_cast<View const*>(this)->operator()(indices));
  }
  template <typename OtherIndexType, size_type N>
  constexpr auto
  operator()(::std::array<OtherIndexType, N> const& indices) const noexcept
      -> const_pointer {
    return accessor_type::offset(data(), mapping_type::operator()(indices),
                                 static_cast<pixel_trait_type const&>(*this));
  }

  constexpr auto operator[](index_type indices) noexcept -> decltype(auto) {
    return operator()(indices);
  }
  constexpr auto operator[](index_type indices) const noexcept
      -> decltype(auto) {
    return operator()(indices);
  }
  template <typename OtherIndexType, size_type N>
  constexpr auto
  operator[](::std::array<OtherIndexType, N> const& indices) noexcept
      -> reference {
    return operator()(indices);
  }
  template <typename OtherIndexType, size_type N>
  constexpr auto
  operator[](::std::array<OtherIndexType, N> const& indices) const noexcept
      -> const_reference {
    return operator()(indices);
  }

 private:
  // Member
  pointer ptr_;

 public:
  // Non-member functions
  template <typename P, typename E, typename L, typename A>
  friend auto swap(View<P, E, L, A>& lhs, View<P, E, L, A>& rhs) noexcept
      -> void;
};

template <typename... OtherIndexTypes,
          typename = ::std::enable_if_t<
              (::std::is_integral_v<OtherIndexTypes> && ...) &&
              (sizeof...(OtherIndexTypes) > 0)>*>
View(PixelType, typename Image::data_type const*, OtherIndexTypes...)
    -> View<DynPixelTrait, DynExtents<sizeof...(OtherIndexTypes)>>;

template <typename OtherIndexType, size_t N,
          typename = ::std::enable_if_t<::std::is_integral_v<OtherIndexType>>*>
View(PixelType, typename Image::data_type const*,
     ::std::array<OtherIndexType, N> const&)
    -> View<DynPixelTrait, DynExtents<N>>;

template <typename... OtherIndexTypes,
          typename = ::std::enable_if_t<
              (::std::is_convertible_v<OtherIndexTypes, size_t> && ...) &&
              (sizeof...(OtherIndexTypes) > 0)>*>
View(Image, OtherIndexTypes...)
    -> View<DynPixelTrait, DynExtents<sizeof...(OtherIndexTypes)>>;

template <typename OtherIndexType, size_t N>
View(Image, ::std::array<OtherIndexType, N> const&)
    -> View<DynPixelTrait, DynExtents<N>>;

template <typename PixelTrait, typename... OtherIndexTypes,
          typename = ::std::void_t<typename PixelTrait::element_type>,
          typename = ::std::enable_if_t<
              (::std::is_convertible_v<OtherIndexTypes, size_t> && ...) &&
              (sizeof...(OtherIndexTypes) > 0)>*>
View(PixelTrait, typename Image::data_type const*, OtherIndexTypes...)
    -> View<PixelTrait, DynExtents<sizeof...(OtherIndexTypes)>>;

template <typename PixelTrait, typename OtherIndexType, size_t N,
          typename = ::std::void_t<typename PixelTrait::element_type>,
          typename = ::std::enable_if_t<
              ::std::is_convertible_v<OtherIndexType, size_t>>*>
View(PixelTrait, typename Image::data_type const*,
     ::std::array<OtherIndexType, N> const&) -> View<PixelTrait, DynExtents<N>>;

template <typename PixelTrait, typename Extents, typename LayoutPolicy,
          typename Accessor>
auto swap(View<PixelTrait, Extents, LayoutPolicy, Accessor>& lhs,
          View<PixelTrait, Extents, LayoutPolicy, Accessor>& rhs) noexcept
    -> void {
  ::std::swap(lhs.ptr_, rhs.ptr_);
  ::std::swap(lhs.pixel_trait(), rhs.pixel_trait());
  ::std::swap(lhs.mapping(), rhs.mapping());
  ::std::swap(lhs.accessor(), rhs.accessor());
}

}  // namespace image

namespace std {
template <typename PixelTrait, typename Extents, typename LayoutPolicy,
          typename Accessor>
auto swap(
    ::image::View<PixelTrait, Extents, LayoutPolicy, Accessor>& lhs,
    ::image::View<PixelTrait, Extents, LayoutPolicy, Accessor>& rhs) noexcept
    -> void {
  ::image::swap(lhs, rhs);
}
}  // namespace std

#endif  // OPENXYZ_IMAGE_VIEW_HXX
