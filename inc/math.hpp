#pragma once

#if !defined(OPENXYZ_MATH_HPP)
#define OPENXYZ_MATH_HPP

#include "math/common_operation.tcc"
#include "math/exponential_operation.tcc"
#include "math/geometric_operation.tcc"
#include "math/integer_operation.tcc"
#include "math/matrix.hxx"
#include "math/matrix.tcc"
#include "math/matrix_operation.tcc"
#include "math/operation.hxx"
#include "math/operation.tcc"
#include "math/packing_operation.tcc"
#include "math/swizzle.hxx"
#include "math/trigonometry_operation.tcc"
#include "math/type.hxx"
#include "math/utils.hxx"
#include "math/vector.hxx"
#include "math/vector.tcc"
#include "math/vector_operation.tcc"

#endif  // OPENXYZ_MATH_HPP
