#pragma once

#if !defined(OPENXYZ_MATH_COMMON_OPERATION_TCC)
#define OPENXYZ_MATH_COMMON_OPERATION_TCC

#include "math/operation.hxx"

#include <algorithm>
#include <cmath>
#include <fenv.h>
#include <limits.h>
#include <type_traits>
#include <utility>

namespace math {

template <typename T> constexpr auto abs(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::signbit(val) ? -val : val;
  } else if constexpr (::std::is_signed_v<T>) {
    return val > 0 ? val : -val;
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto abs(Vector<T, N> const& vec)
    -> Vector<decltype(abs(::std::declval<T>())), N> {
  return map(vec, &abs<T>);
}

template <typename T> constexpr auto sign(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return EqualTo<T>{}(val, static_cast<T>(0)) ? static_cast<T>(0)
           : ::std::signbit(val)                ? static_cast<T>(-1)
                                                : static_cast<T>(1);
  } else if constexpr (::std::is_signed_v<T>) {
    return val == static_cast<T>(0) ? static_cast<T>(0)
           : ((1u << (MakeFixedWidthNum<sizeof(T) * CHAR_BIT>::width - 1)) &
              val)
               ? static_cast<T>(-1)
               : static_cast<T>(1);
  } else {
    return static_cast<T>(val != 0);
  }
}

template <typename T, size_t N>
constexpr auto sign(Vector<T, N> const& vec)
    -> Vector<decltype(sign(::std::declval<T>())), N> {
  return map(vec, &sign<T>);
}

template <typename T> constexpr auto ceil(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::ceil(val);
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto ceil(Vector<T, N> const& vec)
    -> Vector<decltype(ceil(::std::declval<T>())), N> {
  return map(vec, &ceil<T>);
}

template <typename T> constexpr auto floor(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::floor(val);
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto floor(Vector<T, N> const& vec)
    -> Vector<decltype(floor(::std::declval<T>())), N> {
  return map(vec, &floor<T>);
}

template <typename T> constexpr auto trunc(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::trunc(val);
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto trunc(Vector<T, N> const& vec)
    -> Vector<decltype(trunc(::std::declval<T>())), N> {
  return map(vec, &trunc<T>);
}

template <typename T> constexpr auto round(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::round(val);
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto round(Vector<T, N> const& vec)
    -> Vector<decltype(round(::std::declval<T>())), N> {
  return map(vec, &round<T>);
}

template <typename T> constexpr auto round_even(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    int const kCurrentEnv = ::fegetround();
    ::fesetround(FE_TONEAREST);
    T ans = ::std::rint(val);
    ::fesetround(kCurrentEnv);
    return ans;
  } else {
    return val;
  }
}

template <typename T, size_t N>
constexpr auto round_even(Vector<T, N> const& vec) -> Vector<T, N> {
  if constexpr (::std::is_floating_point_v<T>) {
    int const kCurrentEnv = ::fegetround();
    ::fesetround(FE_TONEAREST);
    Vector<T, N> ans =
        map(vec, [](T val) constexpr -> T { return ::std::rint(val); });
    ::fesetround(kCurrentEnv);
    return ans;
  } else {
    return vec;
  }
}

template <typename T> constexpr auto fract(T val) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return val - ::std::floor(val);
  } else {
    return 0;
  }
}

template <typename T, size_t N>
constexpr auto fract(Vector<T, N> const& vec)
    -> Vector<decltype(fract(::std::declval<T>())), N> {
  return map(vec, &fract<T>);
}

template <typename T, typename> constexpr auto mod(T dividend, T divisor) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return dividend - divisor * floor(dividend / divisor);
  } else {
    return ::std::modulus<T>{}(dividend, divisor);
  }
}

template <typename T, size_t N>
constexpr auto mod(Vector<T, N> const& vec, T divisor)
    -> Vector<decltype(mod(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, divisor, &mod<T>);
}

template <typename T, size_t N>
constexpr auto mod(Vector<T, N> const& vec, Vector<T, N> const& ref)
    -> Vector<decltype(mod(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, ref, &mod<T>);
}

template <typename T, typename> constexpr auto modf(T val, T& integer) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::modf(val, &integer);
  } else {
    integer = val;
    return static_cast<T>(0);
  }
}

template <typename T, size_t N>
constexpr auto modf(Vector<T, N> const& vec, Vector<T, N>& integer)
    -> Vector<decltype(modf(::std::declval<T>(), ::std::declval<T&>())), N> {
  return _details::operation_with_multiple_params_ret_aux(
      &modf<T>, const_cast<Vector<T, N>&>(vec), integer);
}

template <typename T, typename> constexpr auto min(T val, T ref) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::fmin(val, ref);
  } else {
    return ::std::min(val, ref);
  }
}

template <typename T, size_t N>
constexpr auto min(Vector<T, N> const& vec, T ref)
    -> Vector<decltype(min(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, ref, &min<T>);
}

template <typename T, size_t N>
constexpr auto min(Vector<T, N> const& vec, Vector<T, N> const& ref)
    -> Vector<decltype(min(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, ref, &min<T>);
}

template <typename T, typename> constexpr auto max(T val, T ref) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::fmax(val, ref);
  } else {
    return ::std::max(val, ref);
  }
}

template <typename T, size_t N>
constexpr auto max(Vector<T, N> const& vec, T ref)
    -> Vector<decltype(max(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, ref, &max<T>);
}

template <typename T, size_t N>
constexpr auto max(Vector<T, N> const& vec, Vector<T, N> const& ref)
    -> Vector<decltype(max(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(vec, ref, &max<T>);
}

template <typename T, typename> constexpr auto clamp(T val, T min, T max) -> T {
  return ::std::clamp(val, min, max);
}

template <typename T, size_t N>
constexpr auto clamp(Vector<T, N> const& vec, T min_val, T max_val)
    -> Vector<decltype(clamp(::std::declval<T>(), ::std::declval<T>(),
                             ::std::declval<T>())),
              N> {
  using RetT = decltype(clamp(::std::declval<T>(), ::std::declval<T>(),
                              ::std::declval<T>()));
  return map(vec, [min = min_val, max = max_val](T val) constexpr -> RetT {
    return clamp(val, min, max);
  });
}

template <typename T, size_t N>
constexpr auto clamp(Vector<T, N> const& vec, Vector<T, N> const& min_val,
                     Vector<T, N> const& max_val)
    -> Vector<decltype(clamp(::std::declval<T>(), ::std::declval<T>(),
                             ::std::declval<T>())),
              N> {
  return _details::operation_with_multiple_params_aux(&clamp<T>, vec, min_val,
                                                      max_val);
}

template <typename T, typename U, typename>
constexpr auto mix(T lo, T hi, U interpolant) -> T {
  if constexpr (::std::is_same_v<U, bool>) {
    return interpolant ? hi : lo;
  } else {
    return lo * (static_cast<T>(1) - static_cast<T>(interpolant)) +
           hi * static_cast<T>(interpolant);
  }
}

template <typename T, size_t N, typename U>
constexpr auto mix(Vector<T, N> const& begin, Vector<T, N> const& end, U step)
    -> Vector<decltype(mix(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<U>())),
              N> {
  return map(begin, end, [interpolant = step](T lo, T hi) constexpr -> T {
    return mix(lo, hi, interpolant);
  });
}

template <typename T, size_t N, typename U>
constexpr auto mix(Vector<T, N> const& begin, Vector<T, N> const& end,
                   Vector<U, N> const& step)
    -> Vector<decltype(mix(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<U>())),
              N> {
  return _details::operation_with_multiple_params_aux(&mix<T, U>, begin, end,
                                                      step);
}

template <typename T, typename> constexpr auto step(T interpolant, T val) -> T {
  return Less<T>{}(val, interpolant) ? static_cast<T>(0) : static_cast<T>(1);
}

template <typename T, size_t N>
constexpr auto step(T edge, Vector<T, N> const& vec)
    -> Vector<decltype(step(::std::declval<T>(), ::std::declval<T>())), N> {
  using RetT = decltype(step(::std::declval<T>(), ::std::declval<T>()));
  return map(vec, [interpolant = edge](T val) constexpr -> RetT {
    return step(interpolant, val);
  });
}

template <typename T, size_t N>
constexpr auto step(Vector<T, N> const& edge, Vector<T, N> const& vec)
    -> Vector<decltype(step(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(edge, vec, &step<T>);
}

template <typename T, typename>
constexpr auto smoothstep(T min, T max, T val) -> T {
  T tmp = ::std::clamp(static_cast<T>(val - min) / static_cast<T>(max - min),
                       static_cast<T>(0), static_cast<T>(1));
  return tmp * tmp * (static_cast<T>(3) - static_cast<T>(2) * tmp);
}

template <typename T, size_t N>
constexpr auto smoothstep(T edge0, T edge1, Vector<T, N> const& vec)
    -> Vector<decltype(smoothstep(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T>())),
              N> {
  using RetT = decltype(smoothstep(::std::declval<T>(), ::std::declval<T>(),
                                   ::std::declval<T>()));
  return map(vec, [min = edge0, max = edge1](T val) constexpr -> RetT {
    return smoothstep(min, max, val);
  });
}

template <typename T, size_t N>
constexpr auto smoothstep(Vector<T, N> const& edge0, Vector<T, N> const& edge1,
                          Vector<T, N> const& vec)
    -> Vector<decltype(smoothstep(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T>())),
              N> {
  return _details::operation_with_multiple_params_aux(&smoothstep<T>, edge0,
                                                      edge1, vec);
}

template <typename T> constexpr auto isnan(T val) -> bool {
  if constexpr (::std::is_integral_v<T>) {
    return false;
  } else {
    return ::std::isnan(val);
  }
}

template <typename T, size_t N>
constexpr auto isnan(Vector<T, N> const& vec)
    -> Vector<decltype(isnan(::std::declval<T>())), N> {
  return map(vec, &isnan<T>);
}

template <typename T> constexpr auto isinf(T val) -> bool {
  if constexpr (::std::is_integral_v<T>) {
    return false;
  } else {
    return ::std::isinf(val);
  }
}

template <typename T, size_t N>
constexpr auto isinf(Vector<T, N> const& vec)
    -> Vector<decltype(isinf(::std::declval<T>())), N> {
  return map(vec, &isinf<T>);
}

template <typename T, typename>
constexpr auto float_bits_to_int(T val) -> decltype(auto) {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  using RetT = typename MakeFixedWidthNum<sizeof(T) * CHAR_BIT>::signed_type;
  // NOTE: using ::std::bitcast on C++20
  return static_cast<RetT>(*reinterpret_cast<RetT const*>(&val));
}

template <typename T, size_t N>
constexpr auto float_bits_to_int(Vector<T, N> const& vec)
    -> Vector<decltype(float_bits_to_int(::std::declval<T>())), N> {
  return map(vec, &float_bits_to_int<T>);
}

template <typename T, typename>
constexpr auto float_bits_to_uint(T val) -> decltype(auto) {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  using RetT = typename MakeFixedWidthNum<sizeof(T) * CHAR_BIT>::unsigned_type;
  // NOTE: using ::std::bitcast on C++20
  return static_cast<RetT>(*reinterpret_cast<RetT const*>(&val));
}

template <typename T, size_t N>
constexpr auto float_bits_to_uint(Vector<T, N> const& vec)
    -> Vector<decltype(float_bits_to_uint(::std::declval<T>())), N> {
  return map(vec, &float_bits_to_uint<T>);
}

template <typename T, typename>
constexpr auto int_bits_to_float(T val) -> decltype(auto) {
  static_assert(::std::is_integral_v<T> && ::std::is_signed_v<T>,
                "T must be signed integer.");
  using RetT = typename MakeFixedWidthNum<sizeof(T) * CHAR_BIT>::floating_type;
  // NOTE: using ::std::bitcast on C++20
  return static_cast<RetT>(*reinterpret_cast<RetT const*>(&val));
}

template <typename T, size_t N>
constexpr auto int_bits_to_float(Vector<T, N> const& vec)
    -> Vector<decltype(int_bits_to_float(::std::declval<T>())), N> {
  return map(vec, &int_bits_to_float<T>);
}

template <typename T, typename>
constexpr auto uint_bits_to_float(T val) -> decltype(auto) {
  static_assert(::std::is_integral_v<T> && ::std::is_unsigned_v<T>,
                "T must be unsigned integer.");
  using RetT = typename MakeFixedWidthNum<sizeof(T) * CHAR_BIT>::floating_type;
  // NOTE: using ::std::bitcast on C++20
  return static_cast<RetT>(*reinterpret_cast<RetT const*>(&val));
}

template <typename T, size_t N>
constexpr auto uint_bits_to_float(Vector<T, N> const& vec)
    -> Vector<decltype(uint_bits_to_float(::std::declval<T>())), N> {
  return map(vec, &uint_bits_to_float<T>);
}

template <typename T, typename> constexpr auto fma(T lhs, T rhs, T acc) -> T {
  if constexpr (::std::is_floating_point_v<T>) {
    return ::std::fma(lhs, rhs, acc);
  } else {
    return lhs * rhs + acc;
  }
}

template <typename T, size_t N>
constexpr auto fma(Vector<T, N> const& vec_a, Vector<T, N> const& vec_b,
                   T accumulated)
    -> Vector<decltype(fma(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<T>())),
              N> {
  return ::math::map(
      vec_a, vec_b,
      [acc = accumulated](T const& lhs, T const& rhs) constexpr -> T {
        return fma(lhs, rhs, acc);
      });
}

template <typename T, size_t N>
constexpr auto fma(Vector<T, N> const& vec_a, Vector<T, N> const& vec_b,
                   Vector<T, N> const& accumulated)
    -> Vector<decltype(fma(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<T>())),
              N> {
  return _details::operation_with_multiple_params_aux(&fma<T>, vec_a, vec_b,
                                                      accumulated);
}

template <typename T> constexpr auto frexp(T significand, int& exp) -> T {
  return ::std::frexp(significand, &exp);
}

template <typename T, size_t N>
constexpr auto frexp(Vector<T, N> const& vec, Vector<int, N>& exp)
    -> Vector<decltype(frexp(::std::declval<T>(), ::std::declval<int&>())), N> {
  return _details::operation_with_multiple_params_ret_aux(
      &frexp<T>, const_cast<Vector<T, N>&>(vec), exp);
}

template <typename T> constexpr auto ldexp(T significand, int exponent) -> T {
  return ::std::ldexp(significand, exponent);
}

template <typename T, size_t N>
constexpr auto ldexp(Vector<T, N> const& vec, int exp)
    -> Vector<decltype(ldexp(::std::declval<T>(), ::std::declval<int>())), N> {
  return ::math::map(vec, exp, &ldexp<T>);
}

template <typename T, size_t N>
constexpr auto ldexp(Vector<T, N> const& vec, Vector<int, N> const& exp)
    -> Vector<decltype(ldexp(::std::declval<T>(), ::std::declval<int>())), N> {
  return ::math::map(vec, exp, &ldexp<T>);
}

}  // namespace math

#endif  // OPENXYZ_MATH_COMMON_OPERATION_TCC
