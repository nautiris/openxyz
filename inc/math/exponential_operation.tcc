#pragma once

#if !defined(OPENXYZ_MATH_EXPONENTIAL_OPERATION_TCC)
#define OPENXYZ_MATH_EXPONENTIAL_OPERATION_TCC

#include "math/operation.hxx"

#include <cmath>
#include <limits.h>
#include <limits>
#include <type_traits>

namespace math {

template <typename T, typename U, typename>
constexpr auto pow(T base, U exp) -> decltype(auto) {
  return ::std::pow(base, exp);
}

template <typename T, size_t N, typename U>
constexpr auto pow(Vector<T, N> const& vec, U exp)
    -> Vector<decltype(pow(::std::declval<T>(), ::std::declval<U>())), N> {
  return map(vec, exp, &pow<T, U>);
}

template <typename T, size_t N, typename U>
constexpr auto pow(Vector<T, N> const& vec, Vector<U, N> const& exps)
    -> Vector<decltype(pow(::std::declval<T>(), ::std::declval<U>())), N> {
  return map(vec, exps, &pow<T, U>);
}

template <typename T> constexpr auto exp(T num) -> decltype(auto) {
  return ::std::exp(num);
}

template <typename T, size_t N>
constexpr auto exp(Vector<T, N> const& vec)
    -> Vector<decltype(exp(::std::declval<T>())), N> {
  return map(vec, &exp<T>);
}

template <typename T> constexpr auto log(T num) -> decltype(auto) {
  return ::std::log(num);
}

template <typename T, size_t N>
constexpr auto log(Vector<T, N> const& vec)
    -> Vector<decltype(log(::std::declval<T>())), N> {
  return map(vec, &log<T>);
}

template <typename T> constexpr auto exp2(T num) -> decltype(auto) {
  return ::std::exp2(num);
}

template <typename T, size_t N>
constexpr auto exp2(Vector<T, N> const& vec)
    -> Vector<decltype(exp2(::std::declval<T>())), N> {
  return map(vec, &exp2<T>);
}

template <typename T> constexpr auto log2(T num) -> decltype(auto) {
  return ::std::log2(num);
}

template <typename T, size_t N>
constexpr auto log2(Vector<T, N> const& vec)
    -> Vector<decltype(log2(::std::declval<T>())), N> {
  return map(vec, &log2<T>);
}

template <typename T> constexpr auto sqrt(T num) -> decltype(auto) {
  return ::std::sqrt(num);
}

template <typename T, size_t N>
constexpr auto sqrt(Vector<T, N> const& vec)
    -> Vector<decltype(sqrt(::std::declval<T>())), N> {
  return map(vec, &sqrt<T>);
}

template <typename T> constexpr auto inversesqrt(T num) -> decltype(auto) {
  using FloatT = ::std::conditional_t<::std::is_integral_v<T>, double, T>;
  constexpr size_t kSize = sizeof(FloatT) * CHAR_BIT;
  auto floating = static_cast<FloatT>(num);
  if constexpr (::std::numeric_limits<FloatT>::is_iec559 && kSize < 128) {
    // Fast inverse square root
    // @see: https://en.wikipedia.org/wiki/Fast_inverse_square_root
    using IntT = typename MakeFixedWidthNum<kSize>::unsigned_type;
    IntT magic = static_cast<IntT>(0);
    if constexpr (kSize == 32) {
      magic = 0x5F37'59DFU;
    } else if constexpr (kSize == 64) {
      magic = 0x5FE6'EB50'C7B5'37A9U;
    } else {
      magic = (static_cast<IntT>(0x5ffe'6eb5'0c7b'537aU) << 64) |
              static_cast<IntT>(0x9cd9'f02e'504f'cfbfU);
    }
    FloatT half = floating * static_cast<FloatT>(0.5);
    auto integer = *reinterpret_cast<IntT*>(&floating);
    integer = magic - (integer >> 1);
    auto tmp = *reinterpret_cast<FloatT*>(&integer);
    tmp = tmp * (static_cast<FloatT>(1.5) - (half * tmp * tmp));
    tmp = tmp * (static_cast<FloatT>(1.5) - (half * tmp * tmp));
    if constexpr (kSize == 64) {
      tmp = tmp * (static_cast<FloatT>(1.5) - (half * tmp * tmp));
    }
    return tmp;
  } else {
    return static_cast<FloatT>(1) / ::std::sqrt(floating);
  }
}

template <typename T, size_t N>
constexpr auto inversesqrt(Vector<T, N> const& vec)
    -> Vector<decltype(inversesqrt(::std::declval<T>())), N> {
  return map(vec, &inversesqrt<T>);
}

}  // namespace math

#endif  // OPENXYZ_MATH_EXPONENTIAL_OPERATION_TCC
