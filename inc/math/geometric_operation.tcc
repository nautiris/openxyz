#pragma once

#if !defined(OPENXYZ_MATH_GEOMETRIC_OPERATION_TCC)
#define OPENXYZ_MATH_GEOMETRIC_OPERATION_TCC

#include "math/operation.hxx"
#include "math/utils.hxx"
#include "math/vector.hxx"

#include <cmath>
#include <stddef.h>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <size_t... Indexes, typename T, size_t N>
constexpr auto dot_aux(Vector<T, N> const& lhs, Vector<T, N> const& rhs,
                       ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> typename Vector<T, N>::value_type {
  return ((::std::get<Indexes>(lhs) * ::std::get<Indexes>(rhs)) + ...);
}

}  // namespace _details

template <typename T, size_t N>
constexpr auto length(Vector<T, N> const& vec) -> decltype(auto) {
  using CastT = ::std::conditional_t<::std::is_floating_point_v<T>, T, double>;
  return ::std::sqrt(static_cast<CastT>(dot(vec, vec)));
}

template <typename T, size_t N>
constexpr auto distance(Vector<T, N> const& veca, Vector<T, N> const& vecb)
    -> decltype(auto) {
  return length(veca - vecb);
}

template <typename T, size_t N>
constexpr auto dot(Vector<T, N> const& lhs, Vector<T, N> const& rhs) ->
    typename Vector<T, N>::value_type {
  return _details::dot_aux(lhs, rhs, ::std::make_index_sequence<N>{});
}

template <typename T, size_t N>
constexpr auto dot(Vector<T, N> const& lhs, Vector<T, N> const& rhs,
                   double angle) -> double {
  return length(lhs) * length(rhs) * ::std::cos(angle);
}

template <typename T>
constexpr auto cross(Vector<T, 3> const& lhs, Vector<T, 3> const& rhs)
    -> Vector<T, 3> {
  return Vector<T, 3>{::std::get<1>(lhs) * ::std::get<2>(rhs) -
                          ::std::get<2>(lhs) * ::std::get<1>(rhs),
                      ::std::get<2>(lhs) * ::std::get<0>(rhs) -
                          ::std::get<0>(lhs) * ::std::get<2>(rhs),
                      ::std::get<0>(lhs) * ::std::get<1>(rhs) -
                          ::std::get<1>(lhs) * ::std::get<0>(rhs)};
}

template <typename T, size_t N>
constexpr auto normalize(Vector<T, N> const& vec) -> decltype(auto) {
  return vec / length(vec);
}

template <typename T, size_t N>
constexpr auto faceforward(Vector<T, N> const& orient,
                           Vector<T, N> const& incident,
                           Vector<T, N> const& nref) -> Vector<T, N> {
  auto tmp = dot(nref, incident);
  return Less<decltype(tmp)>{}(tmp, static_cast<decltype(tmp)>(0)) ? orient
                                                                   : -orient;
}

template <typename T, size_t N>
constexpr auto reflect(Vector<T, N> const& incident, Vector<T, N> const& normal)
    -> decltype(auto) {
  using CastT = ::std::conditional_t<::std::is_floating_point_v<T>, T, double>;
  return incident - static_cast<CastT>(2) * dot(normal, incident) * normal;
}

template <typename T, size_t N>
constexpr auto refract(Vector<T, N> const& incident, Vector<T, N> const& normal,
                       T eta) -> decltype(auto) {
  using CastT = ::std::conditional_t<::std::is_floating_point_v<T>, T, double>;
  auto dot_val = static_cast<CastT>(dot(normal, incident));
  auto tmp = static_cast<CastT>(1) -
             static_cast<CastT>(eta) * static_cast<CastT>(eta) *
                 (static_cast<CastT>(1) - dot_val * dot_val);
  if (Less<CastT>{}(tmp, static_cast<CastT>(0))) {
    return ::math::Vector<CastT, N>{};
  }
  return static_cast<CastT>(eta) * incident -
         (static_cast<CastT>(eta) * dot_val + ::std::sqrt(tmp)) * normal;
}

}  // namespace math

#endif  // OPENXYZ_MATH_GEOMETRIC_OPERATION_TCC
