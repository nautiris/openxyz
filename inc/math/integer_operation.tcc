#pragma once

/**
 * When signed integer arithmetic operation overflows (the result does not fit
 * in the result type), the behavior is undefined, — the possible manifestations
 * of such an operation include:
 *
 *   + it wraps around according to the rules of the representation (typically
 * two's complement),
 *   + it traps — on some platforms or due to compiler options (e.g. -ftrapv in
 * GCC and Clang),
 *   + it saturates to minimal or maximal value (on many DSPs),
 *   + it is completely optimized out by the compiler.
 */

/**
 * Bit Twiddling Hacks
 * @see: https://graphics.stanford.edu/~seander/bithacks.html
 */

#if !defined(OPENXYZ_MATH_INTEGER_OPERATION_TCC)
#define OPENXYZ_MATH_INTEGER_OPERATION_TCC

#include "math/operation.hxx"

#include <limits.h>
#include <stdint.h>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <typename T>
constexpr auto mul_extended_aux(T lhs, T rhs, T& msb, T& lsb) -> int {
  using Int64 = ::std::conditional_t<::std::is_signed_v<T>, int64_t, uint64_t>;
  Int64 const kResult = static_cast<Int64>(lhs) * static_cast<Int64>(rhs);
  msb = static_cast<T>((kResult & static_cast<Int64>(0xFFFF'FFFF'0000'0000)) >>
                       32);
  lsb = static_cast<T>((kResult & static_cast<Int64>(0x0000'0000'FFFF'FFFF)) >>
                       0);
  return 0;
}

constexpr auto bitfield_mask(int offset, int bits) -> uint64_t {
  return ~(0xFFFF'FFFF'FFFF'FFFFU << bits) << offset;
}

template <typename Integer>
constexpr auto bitfield_clear(Integer value, int offset, int bits) -> uint64_t {
  return static_cast<uint64_t>(value) & ~bitfield_mask(offset, bits);
}

template <typename T> constexpr auto popcount_aux(T value) -> int {
  constexpr uint64_t k1{0x5555'5555'5555'5555U};  // -1/3
  constexpr uint64_t k2{0x3333'3333'3333'3333U};  // -1/5
  constexpr uint64_t k4{0x0f0f'0f0f'0f0f'0f0fU};  // -1/17
  constexpr uint64_t kF{0x0101'0101'0101'0101U};  // -1/255
  uint64_t integer{0};
  if constexpr (::std::is_integral_v<T>) {
    constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
    integer = static_cast<uint64_t>(value) & MakeFixedWidthNum<kWidth>::umax;
  } else if constexpr (::std::is_floating_point_v<T>) {
    integer = *reinterpret_cast<uint64_t*>(&value);
  } else {
    static_assert(false, "Unknow type for BFR.");
  }
  // put count of each 2 bits into those 2 bits
  integer = integer - ((integer >> 1) & k1);
  // put count of each 4 bits into those 4 bits
  integer = (integer & k2) + ((integer >> 2) & k2);
  // put count of each 8 bits into those 8 bits
  integer = (integer + (integer >> 4)) & k4;
  // returns 8 most significant bits of x + (x<<8) + (x<<16) + (x<<24) + ...
  integer = (integer * kF) >> 56;
  return static_cast<int>(integer);
}

// count trailing zero
template <typename T> constexpr auto ctz_aux(T value) -> int {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  UIntT integer{0};
  if constexpr (::std::is_integral_v<T>) {
    integer = static_cast<UIntT>(value) & MakeFixedWidthNum<kWidth>::umax;
  } else if constexpr (::std::is_floating_point_v<T>) {
    integer = *reinterpret_cast<UIntT*>(&value);
  } else {
    static_assert(false, "Unknow type for BFR.");
  }
  integer &= static_cast<UIntT>(~integer) + 1;
  int count = (integer ? 1 : 0) + ((integer & 0x0F0F'0F0F'0F0F'0F0FU) ? 4 : 0) +
              ((integer & 0x3333'3333'3333'3333U) ? 2 : 0) +
              ((integer & 0x5555'5555'5555'5555U) ? 1 : 0);
  if constexpr (kWidth >= 16) {
    count += (integer & 0x00FF'00FF'00FF'00FFU) ? 8 : 0;
    if constexpr (kWidth >= 32) {
      count += (integer & 0x0000'FFFF'0000'FFFFU) ? 16 : 0;
      if constexpr (kWidth >= 64) {
        count += (integer & 0x0000'0000'FFFF'FFFFU) ? 32 : 0;
      }
    }
  }
  return kWidth - count;
}

// count trailing one
template <typename T> constexpr auto ctnz_aux(T value) -> int {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  return ctz_aux(static_cast<UIntT>(~value));
}

constexpr uint8_t kClzLookup[] = {
    64U, 63U, 62U, 62U, 61U, 61U, 61U, 61U,
    60U, 60U, 60U, 60U, 60U, 60U, 60U, 60U,
};

template <typename T> constexpr auto get_bitcast_integer(T value) -> uint64_t {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  if constexpr (::std::is_integral_v<T>) {
    return static_cast<uint64_t>(value);
  } else if constexpr (::std::is_floating_point_v<T>) {
    return static_cast<uint64_t>(
        *reinterpret_cast<typename MakeFixedWidthNum<kWidth>::unsigned_type*>(
            &value));
  } else {
    static_assert(false, "Unknow type for bit convertion.");
  }
}

constexpr auto clz_high_32bits_aux(uint64_t value) -> uint64_t {
  return value >= (static_cast<uint64_t>(1) << 48)
             ? (value >= (static_cast<uint64_t>(1) << 56)
                    ? (value >= (static_cast<uint64_t>(1) << 60) ? 60 : 56)
                    : (value >= (static_cast<uint64_t>(1) << 52) ? 52 : 48))
             : (value >= (static_cast<uint64_t>(1) << 40)
                    ? (value >= (static_cast<uint64_t>(1) << 44) ? 44 : 40)
                    : (value >= (static_cast<uint64_t>(1) << 36) ? 36 : 32));
}
constexpr auto clz_low_32bits_aux(uint64_t value) -> uint64_t {
  return value >= (static_cast<uint64_t>(1) << 16)
             ? (value >= (static_cast<uint64_t>(1) << 24)
                    ? (value >= (static_cast<uint64_t>(1) << 28) ? 28 : 24)
                    : (value >= (static_cast<uint64_t>(1) << 20) ? 20 : 16))
             : (value >= (static_cast<uint64_t>(1) << 8)
                    ? (value >= (static_cast<uint64_t>(1) << 12) ? 12 : 8)
                    : (value >= (static_cast<uint64_t>(1) << 4) ? 4 : 0));
}

// count leading zero
template <typename T> constexpr auto clz_aux(T value) -> int {
  // easy implementation: ctz_aux(bitfield_reverse(value))
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  uint64_t const kInteger = get_bitcast_integer(value);
  auto const kTmp = kInteger & MakeFixedWidthNum<kWidth>::umax;
  auto const kCount = kTmp >= (static_cast<uint64_t>(1) << 32)
                          ? clz_high_32bits_aux(kTmp)
                          : clz_low_32bits_aux(kTmp);
  return kClzLookup[kTmp >> kCount] - kCount - static_cast<int>(64 - kWidth);
}

// count leading one
template <typename T> constexpr auto clnz_aux(T value) -> int {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  return clz_aux(static_cast<UIntT>(~value));
}

}  // namespace _details

template <typename T> constexpr auto uadd_carry(T lhs, T rhs, T& carry) -> T {
  static_assert(::std::is_unsigned_v<T>, "type T is not unsigned integer.");
  T result = lhs + rhs;
  carry = result < (lhs | rhs) ? static_cast<T>(1) : static_cast<T>(0);
  return result;
}

template <typename T, size_t N>
constexpr auto uadd_carry(Vector<T, N> const& lhs, Vector<T, N> const& rhs,
                          Vector<T, N>& carries)
    -> Vector<decltype(uadd_carry(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T&>())),
              N> {
  return _details::operation_with_multiple_params_ret_aux(
      &uadd_carry<T>, const_cast<Vector<T, N>&>(lhs),
      const_cast<Vector<T, N>&>(rhs), carries);
}

template <typename T> constexpr auto usub_borrow(T lhs, T rhs, T& borrow) -> T {
  static_assert(::std::is_unsigned_v<T>, "type T is not unsigned integer.");
  T result = ~lhs + rhs + 1u;
  borrow = lhs < rhs ? static_cast<T>(1) : static_cast<T>(0);
  return result;
}

template <typename T, size_t N>
constexpr auto usub_borrow(Vector<T, N> const& lhs, Vector<T, N> const& rhs,
                           Vector<T, N>& borrows)
    -> Vector<decltype(usub_borrow(::std::declval<T>(), ::std::declval<T>(),
                                   ::std::declval<T&>())),
              N> {
  return _details::operation_with_multiple_params_ret_aux(
      &usub_borrow<T>, const_cast<Vector<T, N>&>(lhs),
      const_cast<Vector<T, N>&>(rhs), borrows);
}

constexpr auto umul_extended(uint32_t lhs, uint32_t rhs, uint32_t& msb,
                             uint32_t& lsb) -> void {
  static_cast<void>(_details::mul_extended_aux(lhs, rhs, msb, lsb));
}

template <size_t N>
constexpr auto umul_extended(Vector<uint32_t, N> const& lhs,
                             Vector<uint32_t, N> const& rhs,
                             Vector<uint32_t, N>& msb, Vector<uint32_t, N>& lsb)
    -> void {
  static_cast<void>(_details::operation_with_multiple_params_ret_aux(
      &_details::mul_extended_aux<uint32_t>,
      const_cast<Vector<uint32_t, N>&>(lhs),
      const_cast<Vector<uint32_t, N>&>(rhs), msb, lsb));
}

constexpr auto imul_extended(int32_t lhs, int32_t rhs, int32_t& msb,
                             int32_t& lsb) -> void {
  static_cast<void>(_details::mul_extended_aux(lhs, rhs, msb, lsb));
}

template <size_t N>
constexpr auto imul_extended(Vector<int32_t, N> const& lhs,
                             Vector<int32_t, N> const& rhs,
                             Vector<int32_t, N>& msb, Vector<int32_t, N>& lsb)
    -> void {
  static_cast<void>(_details::operation_with_multiple_params_ret_aux(
      &_details::mul_extended_aux<int32_t>,
      const_cast<Vector<int32_t, N>&>(lhs),
      const_cast<Vector<int32_t, N>&>(rhs), msb, lsb));
}

template <typename T>
constexpr auto bitfield_extract(T value, int offset, int bits)
    -> decltype(auto) {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  using SIntT = typename MakeFixedWidthNum<kWidth>::signed_type;
  using IntT =
      ::std::conditional_t<::std::is_signed_v<T>&& ::std::is_integral_v<T>,
                           SIntT, UIntT>;
  IntT integer{0};
  if constexpr (::std::is_integral_v<T>) {
    integer = value;
  } else if constexpr (::std::is_floating_point_v<T>) {
    integer = *reinterpret_cast<IntT*>(&value);
  } else {
    static_assert(false, "Unknow type for BFE.");
  }
  return static_cast<IntT>((integer >> offset) &
                           (kWidth == bits
                                ? MakeFixedWidthNum<kWidth>::umax
                                : ((static_cast<UIntT>(1) << bits) - 1)));
}

template <typename T, size_t N>
constexpr auto bitfield_extract(Vector<T, N> const& values, int offset,
                                int bits)
    -> Vector<decltype(bitfield_extract(::std::declval<T>(),
                                        ::std::declval<int>(),
                                        ::std::declval<int>())),
              N> {
  return ::math::map(
      values, [off = offset, nbit = bits](T value) constexpr -> decltype(auto) {
        return bitfield_extract(value, off, nbit);
      });
}

template <typename T, size_t N>
constexpr auto bitfield_extract(Vector<T, N> const& values,
                                Vector<int, N> const& offsets,
                                Vector<int, N> const& nbits)
    -> Vector<decltype(bitfield_extract(::std::declval<T>(),
                                        ::std::declval<int>(),
                                        ::std::declval<int>())),
              N> {
  return _details::operation_with_multiple_params_aux(&bitfield_extract<T>,
                                                      values, offsets, nbits);
}

template <typename T>
constexpr auto bitfield_insert(T base, uint64_t insert, int offset, int bits)
    -> decltype(auto) {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  using SIntT = typename MakeFixedWidthNum<kWidth>::signed_type;
  using IntT =
      ::std::conditional_t<::std::is_signed_v<T>&& ::std::is_integral_v<T>,
                           SIntT, UIntT>;
  IntT integer{0};
  if constexpr (::std::is_integral_v<T>) {
    integer = base;
  } else if constexpr (::std::is_floating_point_v<T>) {
    integer = *reinterpret_cast<IntT*>(&base);
  } else {
    static_assert(false, "Unknow type for BFI.");
  }
  return static_cast<IntT>((_details::bitfield_clear(integer, offset, bits) |
                            (bitfield_extract(insert, 0, bits) << offset)) &
                           MakeFixedWidthNum<kWidth>::umax);
}

template <typename T, size_t N>
constexpr auto bitfield_insert(Vector<T, N> const& bases,
                               Vector<uint64_t, N> const& inserts, int offset,
                               int bits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N> {
  using RetT =
      decltype(bitfield_insert(::std::declval<T>(), ::std::declval<uint64_t>(),
                               ::std::declval<int>(), ::std::declval<int>()));
  return ::math::map(
      bases, inserts,
      [off = offset, nbit = bits](T base, T insert) constexpr -> RetT {
        return bitfield_insert(base, insert, off, nbit);
      });
}

template <typename T, size_t N>
constexpr auto bitfield_insert(Vector<T, N> const& bases, uint64_t insert,
                               int offset, int bits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N> {
  using RetT =
      decltype(bitfield_insert(::std::declval<T>(), ::std::declval<uint64_t>(),
                               ::std::declval<int>(), ::std::declval<int>()));
  return ::math::map(
      bases,
      [ins = insert, off = offset, nbit = bits](T base) constexpr -> RetT {
        return bitfield_insert(base, ins, off, nbit);
      });
}

template <typename T, size_t N>
constexpr auto
bitfield_insert(Vector<T, N> const& bases, Vector<uint64_t, N> const& inserts,
                Vector<int, N> const& offsets, Vector<int, N> const& nbits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N> {
  return _details::operation_with_multiple_params_aux(
      &bitfield_insert<T>, bases, inserts, offsets, nbits);
}

template <typename T>
constexpr auto bitfield_reverse(T value) -> decltype(auto) {
  constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
  using UIntT = typename MakeFixedWidthNum<kWidth>::unsigned_type;
  using SIntT = typename MakeFixedWidthNum<kWidth>::signed_type;
  using IntT =
      ::std::conditional_t<::std::is_signed_v<T>&& ::std::is_integral_v<T>,
                           SIntT, UIntT>;
  IntT integer{0};
  if constexpr (::std::is_integral_v<T>) {
    integer = value;
  } else if constexpr (::std::is_floating_point_v<T>) {
    integer = *reinterpret_cast<IntT*>(&value);
  } else {
    static_assert(false, "Unknow type for BFR.");
  }
  IntT result{0};
  int width = kWidth - 1;
  for (integer >>= 1; integer; integer >>= 1) {
    result <<= 1;
    result |= integer & 1;
    width--;
  }
  return result << width;
}

template <typename T, size_t N>
constexpr auto bitfield_reverse(Vector<T, N> const& values)
    -> Vector<decltype(bitfield_reverse(::std::declval<T>())), N> {
  return ::math::map(values, &bitfield_reverse<T>);
}

template <typename T> constexpr auto bit_count(T value) -> int {
  return _details::popcount_aux(value);
}

template <typename T, size_t N>
constexpr auto bit_count(Vector<T, N> const& values)
    -> Vector<decltype(bit_count(::std::declval<T>())), N> {
  return ::math::map(values, &bit_count<T>);
}

template <typename T> constexpr auto find_lsb(T value) -> int {
  if constexpr (::std::is_integral_v<T>) {
    if (value == 0) {
      return -1;
    }
  }
  return _details::ctz_aux(value);
}

template <typename T, size_t N>
constexpr auto find_lsb(Vector<T, N> const& values)
    -> Vector<decltype(find_lsb(::std::declval<T>())), N> {
  return ::math::map(values, &::math::find_lsb<T>);
}

template <typename T> constexpr auto find_msb(T value) -> int {
  if constexpr (::std::is_integral_v<T>) {
    if constexpr (::std::is_signed_v<T>) {
      if (value == static_cast<T>(-1)) {
        return -1;
      }
    }
    if (value == static_cast<T>(0)) {
      return -1;
    }
  }
  return sizeof(T) * CHAR_BIT - 1 - _details::clz_aux(value);
}

template <typename T, size_t N>
constexpr auto find_msb(Vector<T, N> const& values)
    -> Vector<decltype(find_msb(::std::declval<T>())), N> {
  return ::math::map(values, &::math::find_msb<T>);
}

}  // namespace math

#endif  // OPENXYZ_MATH_INTEGER_OPERATION_TCC
