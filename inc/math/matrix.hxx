#pragma once

#if !defined(OPENXYZ_MATH_MATRIX_HXX)
#define OPENXYZ_MATH_MATRIX_HXX

#include "math/utils.hxx"
#include "math/vector.hxx"

#include <functional>
#include <ostream>
#include <stddef.h>
#include <string>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <size_t I, size_t N, typename T> struct MatrixLeaf;

template <typename MatrixIndexes, size_t N, typename T> struct MatrixImpl;

template <typename Matrix> struct MatrixSize;

template <size_t I, size_t M, size_t N, typename U> struct MatrixVisitorAux;

}  // namespace _details

template <typename T, size_t M, size_t N,
          typename = ::std::enable_if_t<
              ::std::is_arithmetic_v<T>&& ::std::is_fundamental_v<T> &&
              (M > 0) && (N > 0)>*>
struct Matrix;

template <typename T, size_t N> using Square = Matrix<T, N, N>;

template <typename MatrixT, typename = void>
struct MatrixTraits : public ::std::false_type {};

template <typename MatrixT>
struct MatrixTraits<
    MatrixT,
    ::std::void_t<typename ::std::remove_cv_t<::std::conditional_t<
        ::std::is_reference_v<MatrixT>, ::std::remove_reference_t<MatrixT>,
        ::std::conditional_t<::std::is_pointer_v<MatrixT>,
                             ::std::remove_pointer_t<MatrixT>, MatrixT>>>::
                      value_type>> : public ::std::true_type {
  using type = ::std::remove_cv_t<::std::conditional_t<
      ::std::is_reference_v<MatrixT>, ::std::remove_reference_t<MatrixT>,
      ::std::conditional_t<::std::is_pointer_v<MatrixT>,
                           ::std::remove_pointer_t<MatrixT>, MatrixT>>>;

  using vector_type = typename type::vector_type;
  using vector_reference = typename type::vector_reference;
  using vector_const_reference = typename type::vector_const_reference;
  using vector_pointer = typename type::vector_pointer;
  using vector_const_pointer = typename type::vector_const_pointer;

  using value_type = typename type::value_type;
  using reference = typename type::reference;
  using const_reference = typename type::const_reference;
  using pointer = typename type::pointer;
  using const_pointer = typename type::const_pointer;

  static constexpr size_t row{type::row};
  static constexpr size_t col{type::col};
};

}  // namespace math

namespace std {

template <size_t I, typename T, size_t M, size_t N>
constexpr auto get(::math::Matrix<T, M, N> const& mat) noexcept ->
    typename ::math::MatrixTraits<decltype(mat)>::vector_const_reference;

template <size_t I, typename T, size_t M, size_t N>
constexpr auto get(::math::Matrix<T, M, N>& mat) noexcept ->
    typename ::math::MatrixTraits<decltype(mat)>::vector_reference {
  using VectorRef =
      typename ::math::MatrixTraits<decltype(mat)>::vector_reference;
  return const_cast<VectorRef>(
      get<I>(static_cast<::math::Matrix<T, M, N> const&>(mat)));
}

template <size_t I, size_t J, typename T, size_t M, size_t N>
constexpr auto get(::math::Matrix<T, M, N> const& mat) noexcept ->
    typename ::math::VectorTraits<typename math::MatrixTraits<
        decltype(mat)>::vector_type>::const_reference {
  return get<J>(get<I>(mat));
}

template <size_t I, size_t J, typename T, size_t M, size_t N>
constexpr auto get(::math::Matrix<T, M, N>& mat) noexcept ->
    typename ::math::VectorTraits<
        typename math::MatrixTraits<decltype(mat)>::vector_type>::reference {
  return const_cast<typename ::math::Matrix<T, M, N>::reference>(
      get<I, J>(static_cast<::math::Matrix<T, M, N> const&>(mat)));
}

template <typename T, size_t M, size_t N>
auto to_string(::math::Matrix<T, M, N> const& mat) -> ::std::string;

}  // namespace std

namespace math {

namespace _details {

template <size_t I, size_t N, typename T> struct MatrixLeaf {
  using vector_type = ::math::Vector<T, N>;
  using vector_reference = vector_type&;
  using vector_const_reference = vector_type const&;
  using vector_pointer = vector_type*;
  using vector_const_pointer = vector_type const*;

  using value_type = typename VectorTraits<vector_type>::value_type;
  using reference = typename VectorTraits<vector_type>::reference;
  using const_reference = typename VectorTraits<vector_type>::const_reference;
  using pointer = typename VectorTraits<vector_type>::pointer;
  using const_pointer = typename VectorTraits<vector_type>::const_pointer;

  static constexpr size_t row{vector_type::dimension};

  vector_type vec{};

  template <
      typename U, size_t Row,
      typename = ::std::enable_if_t<::std::is_convertible_v<U, value_type>>*>
  constexpr explicit MatrixLeaf(Vector<U, Row> const& vector) : vec{vector} {}
  template <typename U, typename = ::std::enable_if_t<
                            ::std::is_constructible_v<value_type, U>>*>
  constexpr explicit MatrixLeaf(U&& other)
      : vec{::std::integral_constant<size_t, (I < row) ? I : 0>{},
            (I < row) ? static_cast<value_type>(other)
                      : static_cast<value_type>(0)} {
    static_assert(
        !::std::is_reference_v<value_type> ||
            (::std::is_lvalue_reference_v<value_type> &&
             (::std::is_lvalue_reference_v<U> ||
              ::std::is_same_v<::std::remove_reference_t<U>,
                               ::std::reference_wrapper<
                                   ::std::remove_reference_t<value_type>>>)) ||
            (::std::is_rvalue_reference_v<value_type> &&
             !::std::is_lvalue_reference_v<U>),
        "Attempted to construct a reference element in a vector with an "
        "rvalue");
  }
  template <typename... Ts,
            typename = ::std::enable_if_t<
                sizeof...(Ts) <= row &&
                (::std::is_constructible_v<value_type, Ts> && ...)>*>
  constexpr explicit MatrixLeaf(Ts&&... others)
      : vec{::std::forward<Ts>(others)...} {}
  template <size_t... Indexes, typename... Ts,
            typename = ::std::enable_if_t<
                sizeof...(Indexes) == sizeof...(Ts) && sizeof...(Ts) <= row &&
                (::std::is_constructible_v<value_type, Ts> && ...)>*>
  constexpr MatrixLeaf(::std::integer_sequence<size_t, Indexes...> /*unused*/,
                       ::std::tuple<Ts...> others)
      : vec{::std::get<Indexes>(others)...} {}
  template <size_t RIdx, typename RT>
  constexpr explicit MatrixLeaf(MatrixLeaf<RIdx, N, RT> const& other)
      : MatrixLeaf(other.get()) {}

  constexpr MatrixLeaf() = default;
  constexpr MatrixLeaf(MatrixLeaf const&) = default;
  constexpr MatrixLeaf(MatrixLeaf&&) noexcept = default;
  ~MatrixLeaf() noexcept = default;

  constexpr auto operator=(MatrixLeaf const& other) -> MatrixLeaf& = default;
  constexpr auto operator=(MatrixLeaf&& other) noexcept
      -> MatrixLeaf& = default;
  template <typename RT> constexpr auto operator=(RT&& other) -> MatrixLeaf& {
    vec = vector_type::template make_unit<I>(::std::forward<RT>(other));
    return *this;
  }

  auto swap(MatrixLeaf& other) noexcept -> MatrixLeaf& {
    ::std::swap(get(), other.get());
    return *this;
  }

  constexpr auto get() noexcept -> vector_reference { return vec; }
  constexpr auto get() const noexcept -> vector_const_reference { return vec; }
};

template <size_t... Indexes, size_t N, typename T>
struct MatrixImpl<::std::integer_sequence<size_t, Indexes...>, N, T>
    : public MatrixLeaf<Indexes, N, T>... {
  using vector_type = ::math::Vector<T, N>;
  using vector_reference = vector_type&;
  using vector_const_reference = vector_type const&;
  using vector_pointer = vector_type*;
  using vector_const_pointer = vector_type const*;

  using value_type = typename VectorTraits<vector_type>::value_type;
  using reference = typename VectorTraits<vector_type>::reference;
  using const_reference = typename VectorTraits<vector_type>::const_reference;
  using pointer = typename VectorTraits<vector_type>::pointer;
  using const_pointer = typename VectorTraits<vector_type>::const_pointer;

  using size_type = size_t;
  static constexpr size_type row{VectorTraits<vector_type>::dimension};
  static constexpr size_type col{sizeof...(Indexes)};

  constexpr MatrixImpl() : MatrixImpl(static_cast<value_type>(1)) {}

  template <typename U,
            typename = ::std::enable_if_t<!::std::is_same_v<MatrixImpl, U>>*>
  constexpr explicit MatrixImpl(U&& value)
      : MatrixLeaf<Indexes, N, value_type>{::std::forward<U>(value)}... {}

  template <typename U>
  constexpr explicit MatrixImpl(Vector<U, row> const& vector)
      : MatrixLeaf<Indexes, N, value_type>{vector}... {}

  template <
      size_t... VectorIndexes, typename... Vs, size_t... SliceIndexes,
      typename... Ts, size_t... LastIndexes,
      typename = ::std::enable_if_t<
          sizeof...(VectorIndexes) == sizeof...(Vs) &&
          ((::math::VectorTraits<Vs>::dimension <= row) && ...) &&
          sizeof...(VectorIndexes) + sizeof...(SliceIndexes) +
                  sizeof...(LastIndexes) ==
              col &&
          (::std::is_constructible_v<
               value_type, typename ::math::VectorTraits<Vs>::value_type> &&
           ...) &&
          (::std::is_constructible_v<value_type, Ts> && ...)>*>
  constexpr MatrixImpl(
      ::std::integer_sequence<size_t, VectorIndexes...> /*unused*/,
      ::std::integer_sequence<size_t, SliceIndexes...> /*unused*/,
      ::std::integer_sequence<size_t, LastIndexes...> /*unused*/,
      ::std::tuple<Vs...>&& vectors, ::std::tuple<Ts...>&& values)
      : MatrixLeaf<VectorIndexes, row, value_type>{::std::get<VectorIndexes>(
            vectors)}...,
        MatrixLeaf<sizeof...(VectorIndexes) + SliceIndexes, row, value_type>{
            ::std::make_index_sequence<
                (sizeof...(Ts) < row + SliceIndexes * row)
                    ? (sizeof...(Ts) - SliceIndexes * row)
                    : row>{},
            tuple_slice<0 + SliceIndexes * row,
                        ::std::min(sizeof...(Ts), row + SliceIndexes * row)>(
                ::std::forward<::std::tuple<Ts...>>(values))}...,
        MatrixLeaf<sizeof...(VectorIndexes) + sizeof...(SliceIndexes) +
                       LastIndexes,
                   row, value_type>{}... {}

  constexpr MatrixImpl(MatrixImpl const&) = default;
  constexpr MatrixImpl(MatrixImpl&&) noexcept = default;
  ~MatrixImpl() noexcept = default;

  constexpr auto operator=(MatrixImpl const& other) -> MatrixImpl& = default;
  constexpr auto operator=(MatrixImpl&& other) noexcept
      -> MatrixImpl& = default;

  template <size_t I> constexpr auto get() noexcept -> vector_reference {
    return const_cast<vector_reference>(
        const_cast<MatrixImpl const*>(this)->template get<I>());
  }

  template <size_t I>
  constexpr auto get() const noexcept -> vector_const_reference {
    return static_cast<MatrixLeaf<I, row, value_type> const&>(*this).get();
  }

  static constexpr auto as_tuple(MatrixImpl const& mat) noexcept
      -> decltype(auto) {
    return ::std::make_tuple(mat.template get<Indexes>()...);
  }

 private:
  template <typename... Ts>
  constexpr void swallow(Ts&&... /*unused*/) noexcept {}
};

}  // namespace _details

template <typename T, size_t M, size_t N> struct Matrix<T, M, N> {
 private:
  template <size_t I, size_t MM, size_t NN, typename U>
  friend struct _details::MatrixVisitorAux;

  using Impl = _details::MatrixImpl<::std::make_index_sequence<N>, M, T>;
  Impl impl_;

 public:
  using vector_type = typename Impl::vector_type;
  using vector_reference = typename Impl::vector_reference;
  using vector_const_reference = typename Impl::vector_const_reference;
  using vector_pointer = typename Impl::vector_pointer;
  using vector_const_pointer = typename Impl::vector_const_pointer;

  using value_type = typename Impl::value_type;
  using reference = typename Impl::reference;
  using const_reference = typename Impl::const_reference;
  using pointer = typename Impl::pointer;
  using const_pointer = typename Impl::const_pointer;

  using size_type = typename Impl::size_type;
  static constexpr size_type row{Impl::row};
  static constexpr size_type col{Impl::col};

  constexpr explicit Matrix(value_type val) : impl_{val} {}

  constexpr explicit Matrix(vector_type vec) : impl_{vec} {}

  template <typename... ValueTs,
            size_t SliceSize = (sizeof...(ValueTs) + row - 1) / row,
            typename = ::std::enable_if_t<
                sizeof...(ValueTs) <= row * col &&
                (::std::is_arithmetic_v<ValueTs> && ...) &&
                (!kIsNarrowingConvertible<ValueTs, value_type> && ...)>*>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Matrix(ValueTs&&... values)
      : impl_{::std::make_index_sequence<0>{},
              ::std::make_index_sequence<SliceSize>{},
              ::std::make_index_sequence<col - SliceSize>{}, ::std::tuple<>{},
              ::std::forward_as_tuple(::std::forward<ValueTs>(values)...)} {}

  template <
      typename... VectorTs,
      typename = ::std::void_t<typename VectorTraits<VectorTs>::value_type...>,
      typename = ::std::enable_if_t<
          sizeof...(VectorTs) <= col &&
          (::std::is_convertible_v<VectorTs, vector_type> && ...)>*>
  constexpr explicit Matrix(VectorTs&&... vectors)
      : impl_{::std::make_index_sequence<sizeof...(VectorTs)>{},
              ::std::make_index_sequence<0>{},
              ::std::make_index_sequence<col - sizeof...(VectorTs)>{},
              ::std::forward_as_tuple(::std::forward<VectorTs>(vectors)...),
              ::std::tuple<>{}} {}

  template <
      typename... VectorTs, typename... ValueTs,
      size_t SliceSize = (sizeof...(ValueTs) + row - 1) / row,
      typename = ::std::void_t<typename VectorTraits<VectorTs>::value_type...>,
      typename = ::std::enable_if_t<
          (VectorTraits<VectorTs>::value && ...) &&
          (::std::is_arithmetic_v<ValueTs> && ...) &&
          (!kIsNarrowingConvertible<ValueTs, value_type> && ...)>*>
  constexpr explicit Matrix(::std::tuple<VectorTs...>&& vectors,
                            ::std::tuple<ValueTs...>&& values)
      : impl_{
            ::std::make_index_sequence<sizeof...(VectorTs)>{},
            ::std::make_index_sequence<SliceSize>{},
            ::std::make_index_sequence<col - sizeof...(VectorTs) - SliceSize>{},
            ::std::forward<::std::tuple<VectorTs...>>(vectors),
            ::std::forward<::std::tuple<ValueTs...>>(values)} {}

  constexpr Matrix() = default;
  constexpr Matrix(Matrix const&) = default;
  constexpr Matrix(Matrix&&) noexcept = default;
  ~Matrix() noexcept = default;

  constexpr auto operator=(Matrix const& other) -> Matrix& = default;
  constexpr auto operator=(Matrix&& other) noexcept -> Matrix& = default;

  template <typename U, size_t X, size_t Y>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr operator Matrix<U, X, Y>() const {
    return convert_aux(::std::make_index_sequence<::std::min(Y, col)>{},
                       ::std::integral_constant<size_t, X>{},
                       ::std::integral_constant<size_t, Y>{}, U{},
                       Impl::as_tuple(impl_));
  }

  constexpr auto operator[](size_type idx) const -> vector_const_reference;

  constexpr auto operator[](size_type idx) -> vector_reference {
    return const_cast<vector_reference>(
        static_cast<Matrix const*>(this)->operator[](idx));
  }

  static constexpr auto make_unit() -> Matrix {
    return Matrix{static_cast<value_type>(1)};
  }

  constexpr auto flatten() const noexcept -> decltype(auto) {
    return flatten_aux(Impl::as_tuple(impl_),
                       ::std::make_index_sequence<col>{});
  }

 private:
  constexpr explicit Matrix(Impl const& impl) : impl_{impl} {}

  template <size_t... Indexes, size_t Row, size_t Col, typename CastT,
            typename... Vs>
  static constexpr auto
  convert_aux(::std::integer_sequence<size_t, Indexes...> /*unused*/,
              ::std::integral_constant<size_t, Row> /*unused*/,
              ::std::integral_constant<size_t, Col> /*unused*/,
              CastT /*unused*/, ::std::tuple<Vs...>&& tuple)
      -> Matrix<CastT, Row, Col> {
    return Matrix<CastT, Row, Col>{
        static_cast<Vector<CastT, Row>>(::std::get<Indexes>(tuple))...};
  }

  template <size_t... Indexes, typename... VectorTs>
  static constexpr auto
  flatten_aux(::std::tuple<VectorTs...>&& tuple,
              ::std::integer_sequence<size_t, Indexes...> /*unused*/)
      -> decltype(auto) {
    return ::std::tuple_cat(::std::get<Indexes>(tuple).flatten()...);
  }
};

template <typename T, size_t Row, size_t Col, typename... Ts>
constexpr auto make_matrix(Ts&&... values) -> decltype(auto) {
  return Matrix<T, Row, Col>{static_cast<T>(::std::forward<Ts>(values))...};
}

template <typename T, size_t Dim, typename... Ts>
constexpr auto make_square(Ts&&... values) -> decltype(auto) {
  return Square<T, Dim>{static_cast<T>(::std::forward<Ts>(values))...};
}

template <typename T, size_t M, size_t N, typename... Ts,
          typename = ::std::enable_if_t<sizeof...(Ts) <= N>*>
constexpr auto make_diagonal_matrix(Ts&&... values) -> Matrix<T, M, N>;

template <typename T, size_t Dim, typename... Ts,
          typename = ::std::enable_if_t<sizeof...(Ts) <= Dim>*>
constexpr auto make_diagonal_matrix(Ts&&... values) -> Square<T, Dim> {
  return make_diagonal_matrix<T, Dim, Dim>(::std::forward<Ts>(values)...);
}

template <size_t P, size_t Q, typename T, size_t Row, size_t Col>
constexpr auto make_submatrix(Matrix<T, Row, Col> const& mat)
    -> Matrix<T, Row - 1, Col - 1>;

template <typename T, size_t M, size_t N, typename UnaryOperation>
constexpr auto map(Matrix<T, M, N> const& mat, UnaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>())), M, N>;

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation,
          typename = ::std::enable_if_t<
              !::std::is_class_v<::std::remove_cv_t<::std::conditional_t<
                  ::std::is_reference_v<U>, ::std::remove_reference_t<U>,
                  ::std::conditional_t<::std::is_pointer_v<U>,
                                       ::std::remove_pointer_t<U>, U>>>>>*>
constexpr auto map(Matrix<T, M, N> const& lhs, U&& rhs, BinaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N>;

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation>
constexpr auto map(Matrix<T, M, N> const& lhs, Matrix<U, M, N> const& rhs,
                   BinaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N>;

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation>
constexpr auto reduce(Matrix<T, M, N> const& lhs, U init, BinaryOperation&& op)
    -> decltype(op(::std::declval<T>(), ::std::declval<U>()));

template <typename T, size_t M, size_t N, typename BinaryOperation>
constexpr auto reduce(Matrix<T, M, N> const& lhs, BinaryOperation&& op)
    -> decltype(op(::std::declval<T>(), ::std::declval<T>())) {
  using RetT = decltype(op(::std::declval<T>(), ::std::declval<T>()));
  return reduce(lhs, RetT{}, ::std::forward<BinaryOperation>(op));
}

template <typename T, size_t M, size_t N>
constexpr auto operator+(Matrix<T, M, N> const& lhs, Matrix<T, M, N> const& rhs)
    -> Matrix<T, M, N> {
  return map(lhs, rhs, ::std::plus<T>{});
}

template <typename T, size_t M, size_t N>
constexpr auto operator-(Matrix<T, M, N> const& lhs, Matrix<T, M, N> const& rhs)
    -> Matrix<T, M, N> {
  return map(lhs, rhs, ::std::minus<T>{});
}

template <typename T, size_t M, size_t N, typename U>
constexpr auto operator*(Matrix<T, M, N> const& lhs, U&& val)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return map(lhs, ::std::forward<U>(val), ::std::multiplies<RetT>{});
}

template <typename T, size_t M, size_t N, typename U>
constexpr auto operator*(U&& val, Matrix<T, M, N> const& rhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  return rhs * ::std::forward<U>(val);
}

template <typename T, typename U, size_t M, size_t N>
constexpr auto operator*(Matrix<T, M, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), M>;

template <typename T, typename U, size_t M, size_t N, size_t K>
constexpr auto operator*(Matrix<U, K, N> const& rhs, Matrix<T, M, K> const& lhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N>;

template <typename T, size_t M, size_t N, typename U>
constexpr auto operator/(Matrix<T, M, N> const& lhs, U&& val)
    -> Matrix<decltype(::std::declval<T>() / ::std::declval<U>()), M, N> {
  using RetT = decltype(::std::declval<T>() / ::std::declval<U>());
  return map(lhs, ::std::forward<U>(val), ::std::divides<RetT>{});
}

template <typename T, typename U, size_t Dim1, size_t Dim2, size_t Col1,
          size_t Col2>
constexpr auto operator==(Matrix<T, Dim1, Col1> const& lhs,
                          Matrix<U, Dim2, Col2> const& rhs) -> bool;

template <typename T, typename U, size_t Dim1, size_t Dim2, size_t Col1,
          size_t Col2>
constexpr auto operator!=(Matrix<T, Dim1, Col1> const& lhs,
                          Matrix<U, Dim2, Col2> const& rhs) -> bool {
  return !(lhs == rhs);
}

template <typename T, size_t M, size_t N>
auto operator<<(::std::ostream& os, Matrix<T, M, N> const& mat)
    -> ::std::ostream&;

}  // namespace math

#endif  // OPENXYZ_MATH_MATRIX_HXX
