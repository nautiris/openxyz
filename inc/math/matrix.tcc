#pragma once

#if !defined(OPENXYZ_MATH_MATRIX_TCC)
#define OPENXYZ_MATH_MATRIX_TCC

#include "math/matrix.hxx"
#include "math/vector.hxx"

#include <utility>

namespace math {

namespace _details {

template <size_t I, size_t M, size_t N, typename T> struct MatrixVisitorAux {
  using MatrixT = Matrix<T, M, N>;

  template <size_t... Indexes>
  static constexpr auto run(MatrixT const& mat,
                            ::std::integer_sequence<typename MatrixT::size_type,
                                                    Indexes...> /*unused*/) ->
      typename MatrixTraits<MatrixT>::vector_const_reference {
    return static_cast<_details::MatrixLeaf<I, M, T> const&>(mat.impl_).get();
  }
};

template <typename T, size_t M, size_t N> struct MatrixImplAux {
  using MatrixT = Matrix<T, M, N>;

  template <size_t... Indexes>
  static constexpr auto
  visit(MatrixT const& mat, typename MatrixT::size_type idx,
        ::std::integer_sequence<typename MatrixT::size_type,
                                Indexes...> /*unused*/) ->
      typename MatrixTraits<MatrixT>::vector_const_reference {
    typename MatrixTraits<MatrixT>::vector_const_pointer ptr{nullptr};
    ((idx == Indexes && (ptr = &::std::get<Indexes>(mat))) || ...);
    return *ptr;
  }

  template <size_t... Indexes, typename UnaryOperation>
  static constexpr auto map(MatrixT const& mat, UnaryOperation&& op,
                            ::std::integer_sequence<typename MatrixT::size_type,
                                                    Indexes...> /*unused*/)
      -> ::math::Matrix<decltype(op(::std::declval<T>())), M, N> {
    return ::math::Matrix<decltype(op(::std::declval<T>())), M, N>{::math::map(
        ::std::get<Indexes>(mat), ::std::forward<UnaryOperation>(op))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation,
            typename = ::std::enable_if_t<
                !::std::is_class_v<::std::remove_cv_t<::std::conditional_t<
                    ::std::is_reference_v<U>, ::std::remove_reference_t<U>,
                    ::std::conditional_t<::std::is_pointer_v<U>,
                                         ::std::remove_pointer_t<U>, U>>>>>*>
  static constexpr auto map(MatrixT const& lhs, U&& rhs, BinaryOperation&& op,
                            ::std::integer_sequence<typename MatrixT::size_type,
                                                    Indexes...> /*unused*/)
      -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N> {
    return Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N>{
        ::math::map(::std::get<Indexes>(lhs), ::std::forward<U>(rhs),
                    ::std::forward<BinaryOperation>(op))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation>
  static constexpr auto map(MatrixT const& lhs, Matrix<U, M, N> const& rhs,
                            BinaryOperation&& op,
                            ::std::integer_sequence<typename MatrixT::size_type,
                                                    Indexes...> /*unused*/)
      -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N> {
    return Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N>{
        ::math::map(::std::get<Indexes>(lhs), ::std::get<Indexes>(rhs),
                    ::std::forward<BinaryOperation>(op))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation>
  static constexpr auto
  reduce(MatrixT const& lhs, U init, BinaryOperation&& op,
         ::std::integer_sequence<typename MatrixT::size_type,
                                 Indexes...> /*unused*/
         ) -> decltype(op(::std::declval<T>(), ::std::declval<U>())) {
    using RetT = decltype(op(::std::declval<T>(), ::std::declval<U>()));
    RetT ans = static_cast<RetT>(init);
    return ((ans = ::math::reduce(::std::get<Indexes>(lhs), ans,
                                  ::std::forward<BinaryOperation>(op))),
            ...);
  }
};

template <typename T, size_t M, size_t N, size_t... Indexes, typename... Ts>
constexpr auto
make_diagonal_aux(::std::integer_sequence<size_t, Indexes...> /*unused*/,
                  ::std::tuple<Ts...>&& values) -> Matrix<T, M, N> {
  return Matrix<T, M, N>{
      Vector<T, M>{::std::integral_constant<size_t, Indexes>{},
                   static_cast<T>(::std::get<Indexes>(values))}
       ...
  };
}

template <size_t P, size_t Q, size_t... Indexes, typename T, size_t Row,
          size_t Col>
constexpr auto
make_submatrix_aux(Matrix<T, Row, Col> const& mat,
                   ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Matrix<T, Row - 1, Col - 1> {
  auto flatten_matrix = mat.flatten();
  using tuple_type = decltype(flatten_matrix);
  return Matrix<T, Row - 1, Col - 1>{
      ::std::tuple<>{},
      ::std::tuple_cat(
          tuple_slice < Q == 0 ? Row : 0,
          Q == 0 ? Row + P : P > (static_cast<tuple_type>(flatten_matrix)),
          tuple_slice < Q == Indexes ? 0 : (P + 1 + Indexes * Row),
          Q == Indexes ? 0
                       : (Row + P + Indexes * Row) >
                             (static_cast<tuple_type>(flatten_matrix))...)};
}

template <size_t... Indexes, typename T, typename U, size_t M, size_t N>
constexpr auto gemv_aux(Matrix<T, M, N> const& lhs, Vector<U, N> const& rhs,
                        ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), M> {
  return ((::std::get<Indexes>(lhs) * ::std::get<Indexes>(rhs)) + ...);
}

template <size_t... Indexes, typename T, typename U, size_t M, size_t N,
          size_t K>
constexpr auto gemm_aux(Matrix<T, M, K> const& lhs, Matrix<U, K, N> const& rhs,
                        ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return Matrix<RetT, M, N>{(lhs * ::std::get<Indexes>(rhs))...};
}

template <size_t Row, size_t Col, typename T, typename U, bool SameDim,
          bool T2U, bool U2T>
struct MatrixCompareAux {
  template <size_t... Indexes, size_t Row2, size_t Col2>
  static constexpr auto
  equal(::math::Matrix<T, Row, Col> const& /*lhs*/,
        ::math::Matrix<U, Row2, Col2> const& /*rhs*/,
        ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    return false;
  }
};

template <size_t Row, size_t Col, typename T, typename U, bool U2T>
struct MatrixCompareAux<Row, Col, T, U, true, true, U2T> {
  using MatrixT = ::math::Matrix<T, Row, Col>;
  using MatrixU = ::math::Matrix<U, Row, Col>;
  using VectorU = ::math::Vector<U, Row>;
  template <size_t... Indexes>
  static constexpr auto
  equal(MatrixT const& lhs, MatrixU const& rhs,
        ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    return ((static_cast<VectorU>(::std::get<Indexes>(lhs)) ==
             ::std::get<Indexes>(rhs)) &&
            ...);
  }
};

template <size_t Row, size_t Col, typename T, typename U>
struct MatrixCompareAux<Row, Col, T, U, true, false, true> {
  using MatrixT = ::math::Matrix<T, Row, Col>;
  using MatrixU = ::math::Matrix<U, Row, Col>;
  using VectorT = ::math::Vector<T, Row>;
  template <size_t... Indexes>
  static constexpr auto
  equal(MatrixT const& lhs, MatrixU const& rhs,
        ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    return ((::std::get<Indexes>(lhs) ==
             static_cast<VectorT>(::std::get<Indexes>(rhs))) &&
            ...);
  }
};

template <size_t... Indexes, typename T, size_t M, size_t N>
auto to_string_aux(::math::Matrix<T, M, N> const& mat,
                   ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> ::std::string {
  ::std::string ret;
  ((ret = ret + (Indexes == 0 ? "" : ",\n") +
          ::std::to_string(::std::get<Indexes>(mat))),
   ...);
  return ret;
}

}  // namespace _details

template <typename T, size_t M, size_t N>
constexpr auto Matrix<T, M, N>::operator[](size_type idx) const
    -> vector_const_reference {
  return _details::MatrixImplAux<T, M, N>::visit(
      *this, idx, ::std::make_index_sequence<N>{});
}

template <typename T, size_t M, size_t N, typename... Ts, typename>
constexpr auto make_diagonal_matrix(Ts&&... values) -> Matrix<T, M, N> {
  return _details::make_diagonal_aux<T, M, N>(
      ::std::make_index_sequence<::std::min(sizeof...(Ts), M)>{},
      ::std::forward_as_tuple(::std::forward<Ts>(values)...));
}

template <size_t P, size_t Q, typename T, size_t Row, size_t Col>
constexpr auto make_submatrix(Matrix<T, Row, Col> const& mat)
    -> Matrix<T, Row - 1, Col - 1> {
  return _details::make_submatrix_aux<P, Q>(mat,
                                            ::std::make_index_sequence<Col>{});
}

template <typename T, size_t M, size_t N, typename UnaryOperation>
constexpr auto map(Matrix<T, M, N> const& mat, UnaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>())), M, N> {
  return _details::MatrixImplAux<T, M, N>::map(
      mat, ::std::forward<UnaryOperation>(op), ::std::make_index_sequence<N>{});
}

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation,
          typename>
constexpr auto map(Matrix<T, M, N> const& lhs, U&& rhs, BinaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N> {
  return _details::MatrixImplAux<T, M, N>::map(
      lhs, ::std::forward<U>(rhs), ::std::forward<BinaryOperation>(op),
      ::std::make_index_sequence<N>{});
}

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation>
constexpr auto map(Matrix<T, M, N> const& lhs, Matrix<U, M, N> const& rhs,
                   BinaryOperation&& op)
    -> Matrix<decltype(op(::std::declval<T>(), ::std::declval<U>())), M, N> {
  return _details::MatrixImplAux<T, M, N>::map(
      lhs, rhs, ::std::forward<BinaryOperation>(op),
      ::std::make_index_sequence<N>{});
}

template <typename T, size_t M, size_t N, typename U, typename BinaryOperation>
constexpr auto reduce(Matrix<T, M, N> const& lhs, U init, BinaryOperation&& op)
    -> decltype(op(::std::declval<T>(), ::std::declval<U>())) {
  return _details::MatrixImplAux<T, M, N>::reduce(
      lhs, init, ::std::forward<BinaryOperation>(op),
      ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t M, size_t N>
constexpr auto operator*(Matrix<T, M, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), M> {
  return _details::gemv_aux(lhs, rhs, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t M, size_t N, size_t K>
constexpr auto operator*(Matrix<U, K, N> const& rhs, Matrix<T, M, K> const& lhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  return _details::gemm_aux(lhs, rhs, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t Row1, size_t Row2, size_t Col1,
          size_t Col2>
constexpr auto operator==(Matrix<T, Row1, Col1> const& lhs,
                          Matrix<U, Row2, Col2> const& rhs) -> bool {
  return _details::MatrixCompareAux < Row1, Col1, T, U,
         Row1 == Row2 && Col1 == Col2, !kIsNarrowingConvertible<T, U>,
         !kIsNarrowingConvertible < U,
         T >> ::equal(lhs, rhs, ::std::make_index_sequence<Col1>{});
}

template <typename T, size_t M, size_t N>
auto operator<<(::std::ostream& os, Matrix<T, M, N> const& mat)
    -> ::std::ostream& {
  os << ::std::to_string(mat);
  return os;
}

}  // namespace math

namespace std {

template <size_t I, typename T, size_t M, size_t N>
constexpr auto get(::math::Matrix<T, M, N> const& mat) noexcept ->
    typename ::math::MatrixTraits<decltype(mat)>::vector_const_reference {
  return ::math::_details::MatrixVisitorAux<I, M, N, T>::run(
      mat, ::std::make_index_sequence<I + 1>{});
}

template <typename T, size_t M, size_t N>
auto to_string(::math::Matrix<T, M, N> const& mat) -> ::std::string {
  return "[" +
         ::math::_details::to_string_aux(mat, ::std::make_index_sequence<N>{}) +
         "]";
}

}  // namespace std

#endif  // OPENXYZ_MATH_MATRIX_TCC
