#pragma once

#if !defined(OPENXYZ_MATH_MATRIX_OPERATION_TCC)
#define OPENXYZ_MATH_MATRIX_OPERATION_TCC

#include "math/operation.hxx"

namespace math {

namespace _details {

template <size_t... Indexes, typename T, typename U, size_t M>
constexpr auto
hadamard_aux(Vector<T, M> const& lhs, Vector<U, M> const& rhs,
             ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), M> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return Vector<T, M>{(static_cast<RetT>(::std::get<Indexes>(lhs)) *
                       static_cast<RetT>(::std::get<Indexes>(rhs)))...};
}

template <size_t... Indexes, typename T, typename U, size_t M, size_t N>
constexpr auto
hadamard_aux(Matrix<T, M, N> const& lhs, Matrix<U, M, N> const& rhs,
             ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return Matrix<RetT, M, N>(hadamard_aux(::std::get<Indexes>(lhs),
                                         ::std::get<Indexes>(rhs),
                                         ::std::make_index_sequence<M>{})...);
}

template <size_t... Indexes, typename T, size_t N, typename U, size_t M>
constexpr auto
outer_product_aux(Vector<T, N> const& lhs, Vector<U, M> const& rhs,
                  ::std::integer_sequence<size_t, Indexes...> /*unused*/
                  )
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), N, M> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return Matrix<RetT, N, M>{(lhs * ::std::get<Indexes>(rhs))...};
}

template <size_t... Indexes, typename T, size_t N>
constexpr auto
transpose_aux(Vector<T, N> const& vec,
              ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Matrix<T, 1, N> {
  return ::math::make_matrix<T, 1, N>(::std::get<Indexes>(vec)...);
}

template <size_t... Indexes, typename T, size_t N>
constexpr auto
transpose_aux(Matrix<T, 1, N> const& mat,
              ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Vector<T, N> {
  return ::math::make_vector<T>(::std::get<Indexes, 0>(mat)...);
}

template <size_t Index, size_t... NIndexes, typename T, size_t M, size_t N>
constexpr auto
transpose_aux(Matrix<T, M, N> const& mat,
              ::std::integral_constant<size_t, Index> /*unused*/,
              ::std::integer_sequence<size_t, NIndexes...> /*unused*/)
    -> Vector<T, N> {
  return ::math::make_vector<T>(::std::get<NIndexes, Index>(mat)...);
}

template <size_t... MIndexes, typename T, size_t M, size_t N>
constexpr auto
transpose_aux(Matrix<T, M, N> const& mat,
              ::std::integer_sequence<size_t, MIndexes...> /*unused*/,
              ::std::true_type /*unused*/) -> Matrix<T, N, M> {
  return Matrix<T, N, M>{
      transpose_aux(mat, ::std::integral_constant<size_t, MIndexes>{},
                    ::std::make_index_sequence<N>{})...};
}

template <typename T, size_t N> struct DeterminantAux {
  using MatrixT = ::math::Square<T, N>;
  using ScalarT = typename MatrixTraits<MatrixT>::value_type;
  template <size_t... Indexes>
  static constexpr auto
  run(MatrixT const& mat,
      ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> ScalarT {
    return ((::math::determinant(::math::make_submatrix<0, Indexes>(mat)) *
             ((Indexes & 1) == 0 ? 1 : -1) * ::std::get<Indexes, 0>(mat)) +
            ...);
  }
};

template <typename T> struct DeterminantAux<T, 2> {
  using MatrixT = ::math::Square<T, 2>;
  using ScalarT = typename MatrixTraits<MatrixT>::value_type;
  template <size_t... Indexes>
  static constexpr auto
  run(MatrixT const& mat,
      ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> ScalarT {
    return ::std::get<0, 0>(mat) * ::std::get<1, 1>(mat) -
           ::std::get<0, 1>(mat) * ::std::get<1, 0>(mat);
  }
};

template <typename T> struct DeterminantAux<T, 1> {
  using MatrixT = ::math::Square<T, 1>;
  using ScalarT = typename MatrixTraits<MatrixT>::value_type;
  template <size_t... Indexes>
  static constexpr auto
  run(MatrixT const& mat,
      ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> ScalarT {
    return ::std::get<0, 0>(mat);
  }
};

}  // namespace _details

template <typename T, typename U, size_t M, size_t N>
constexpr auto hadamard(Matrix<T, M, N> const& lhs, Matrix<U, M, N> const& rhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N> {
  return _details::hadamard_aux(lhs, rhs, ::std::make_index_sequence<N>{});
}

template <typename T, size_t N, typename U, size_t M>
constexpr auto outer_product(Vector<T, N> const& lhs, Vector<U, M> const& rhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), N, M> {
  return _details::outer_product_aux(lhs, rhs, ::std::make_index_sequence<M>{});
}

template <typename T, size_t N>
constexpr auto transpose(Vector<T, N> const& vec) -> Matrix<T, 1, N> {
  return _details::transpose_aux(vec, ::std::make_index_sequence<N>{});
}

template <typename T, size_t N>
constexpr auto transpose(Matrix<T, 1, N> const& mat) -> Vector<T, N> {
  return _details::transpose_aux(mat, ::std::make_index_sequence<N>{});
}

template <typename T, size_t M, size_t N>
constexpr auto transpose(Matrix<T, M, N> const& mat) -> Matrix<T, N, M> {
  return _details::transpose_aux(mat, ::std::make_index_sequence<M>{},
                                 ::std::true_type{});
}

template <typename T, size_t N>
constexpr auto determinant(Square<T, N> const& mat) ->
    typename MatrixTraits<Square<T, N>>::value_type {
  return _details::DeterminantAux<T, N>::run(mat,
                                             ::std::make_index_sequence<N>{});
}

}  // namespace math

#endif  // OPENXYZ_MATH_MATRIX_OPERATION_TCC
