#pragma once

#if !defined(OPENXYZ_MATH_OPERATION_HXX)
#define OPENXYZ_MATH_OPERATION_HXX

#include "math/matrix.hxx"
#include "math/vector.hxx"

#include <stddef.h>
#include <stdint.h>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <typename... Ts, size_t N, typename Operation, size_t VecIndex>
constexpr auto operation_with_multiple_params_aux_vec_idx(
    ::std::integral_constant<size_t, VecIndex> /*unused*/, Operation&& op,
    Vector<Ts, N> const&... params) -> decltype(op(::std::declval<Ts>()...)) {
  return op(::std::get<VecIndex>(params)...);
}

template <typename... Ts, size_t N, typename Operation, size_t... Indexes>
constexpr auto operation_with_multiple_params_aux_vec_loop(
    ::std::integer_sequence<size_t, Indexes...> /*unused*/, Operation&& op,
    Vector<Ts, N> const&... params)
    -> Vector<decltype(op(::std::declval<Ts>()...)), N> {
  using RetT = decltype(op(::std::declval<Ts>()...));
  return make_vector<RetT>(operation_with_multiple_params_aux_vec_idx(
      ::std::integral_constant<size_t, Indexes>{},
      ::std::forward<Operation>(op),
      ::std::forward<Vector<Ts, N> const&>(params)...)...);
}

template <typename... Ts, size_t N, typename Operation>
constexpr auto operation_with_multiple_params_aux(
    Operation&& op, Vector<Ts, N> const&... params) -> decltype(auto) {
  return operation_with_multiple_params_aux_vec_loop(
      ::std::make_index_sequence<N>{}, ::std::forward<Operation>(op),
      ::std::forward<Vector<Ts, N> const&>(params)...);
}

template <typename... Ts, size_t N, typename Operation, size_t VecIndex>
constexpr auto operation_with_multiple_params_ret_aux_vec_idx(
    ::std::integral_constant<size_t, VecIndex> /*unused*/, Operation&& op,
    Vector<Ts, N>&... params) -> decltype(op(::std::declval<Ts&>()...)) {
  return op(::std::get<VecIndex>(params)...);
}

template <typename... Ts, size_t N, typename Operation, size_t... Indexes>
constexpr auto operation_with_multiple_params_ret_aux_vec_loop(
    ::std::integer_sequence<size_t, Indexes...> /*unused*/, Operation&& op,
    Vector<Ts, N>&... params)
    -> Vector<decltype(op(::std::declval<Ts&>()...)), N> {
  using RetT = decltype(op(::std::declval<Ts&>()...));
  return make_vector<RetT>(operation_with_multiple_params_ret_aux_vec_idx(
      ::std::integral_constant<size_t, Indexes>{},
      ::std::forward<Operation>(op),
      ::std::forward<Vector<Ts, N>&>(params)...)...);
}

template <typename... Ts, size_t N, typename Operation>
constexpr auto operation_with_multiple_params_ret_aux(Operation&& op,
                                                      Vector<Ts, N>&... params)
    -> decltype(auto) {
  return operation_with_multiple_params_ret_aux_vec_loop(
      ::std::make_index_sequence<N>{}, ::std::forward<Operation>(op),
      ::std::forward<Vector<Ts, N>&>(params)...);
}

}  // namespace _details

// NOLINTBEGIN(readability-identifier-length)

// angle and trigonometry functions

/// converts a quantity specified in degrees into radians. The return value is
/// (pi \dots degrees) / 180.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/radians.xhtml
template <typename T> constexpr auto radians(T degrees) -> decltype(auto);
template <typename T, size_t N>
constexpr auto radians(Vector<T, N> const& degreeses)
    -> Vector<decltype(radians(::std::declval<T>())), N>;

/// converts a quantity specified in degrees into radians. The return value is
/// (180 \dots radians) / pi.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/degrees.xhtml
template <typename T> constexpr auto degrees(T radians) -> decltype(auto);
template <typename T, size_t N>
constexpr auto degrees(Vector<T, N> const& radianses)
    -> Vector<decltype(degrees(::std::declval<T>())), N>;

/// returns the trigonometric sine of angle.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/sin.xhtml
template <typename T> constexpr auto sin(T angle) -> decltype(auto);
template <typename T, size_t N>
constexpr auto sin(Vector<T, N> const& angles)
    -> Vector<decltype(sin(::std::declval<T>())), N>;

/// returns the trigonometric cosine of angle.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/cos.xhtml
template <typename T> constexpr auto cos(T angle) -> decltype(auto);
template <typename T, size_t N>
constexpr auto cos(Vector<T, N> const& angles)
    -> Vector<decltype(cos(::std::declval<T>())), N>;

/// returns the trigonometric tangent of angle.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/tan.xhtml
template <typename T> constexpr auto tan(T angle) -> decltype(auto);
template <typename T, size_t N>
constexpr auto tan(Vector<T, N> const& angles)
    -> Vector<decltype(tan(::std::declval<T>())), N>;

/// returns the angle whose trigonometric sine is x. The range of values
/// returned by asin is [−pi/2,pi/2]. The result is undefined if |x|>1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/asin.xhtml
template <typename T> constexpr auto asin(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto asin(Vector<T, N> const& x)
    -> Vector<decltype(asin(::std::declval<T>())), N>;

/// returns the trigonometric cosine of angle.returns the angle whose
/// trigonometric cosine is x. The range of values returned by acos is [0,pi].
/// The result is undefined if |x|>1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/acos.xhtml
template <typename T> constexpr auto acos(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto acos(Vector<T, N> const& x)
    -> Vector<decltype(acos(::std::declval<T>())), N>;

/// returns either the angle whose trigonometric arctangent is y/x or y_over_x,
/// depending on which overload is invoked. In the first overload, the signs of
/// y and x are used to determine the quadrant that the angle lies in. The value
/// returned by atan in this case is in the range [−pi, pi]. The result is
/// undefined if x=0.
///
/// For the second overload, atan returns the angle whose tangent is y_over_x.
/// The value returned in this case is in the range [−pi/2,pi/2].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/atan.xhtml
template <typename T> constexpr auto atan(T y, T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto atan(Vector<T, N> const& y, Vector<T, N> const& x)
    -> Vector<decltype(atan(::std::declval<T>(), ::std::declval<T>())), N>;
template <typename T> constexpr auto atan(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto atan(Vector<T, N> const& x)
    -> Vector<decltype(atan(::std::declval<T>())), N>;

/// returns the hyperbolic sine of x. The hyperbolic sine of x is computed as
/// (e^(x)-e^(-x))/2.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/sinh.xhtml
template <typename T> constexpr auto sinh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto sinh(Vector<T, N> const& x)
    -> Vector<decltype(sinh(::std::declval<T>())), N>;

/// returns the hyperbolic cosine of x. The hyperbolic cosine of x is computed
/// as (e^(x)+e^(-x))/2.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/cosh.xhtml
template <typename T> constexpr auto cosh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto cosh(Vector<T, N> const& x)
    -> Vector<decltype(cosh(::std::declval<T>())), N>;

/// returns the arc hyperbolic tangent of x; the inverse of tanh. The result is
/// undefined if |x|>1.returns the hyperbolic tangent of x. The hyperbolic
/// tangent of x is computed as sinh(x)/cosh(x).
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/tanh.xhtml
template <typename T> constexpr auto tanh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto tanh(Vector<T, N> const& x)
    -> Vector<decltype(tanh(::std::declval<T>())), N>;

/// returns the arc hyperbolic sine of x; the inverse of sinh.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/asinh.xhtml
template <typename T> constexpr auto asinh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto asinh(Vector<T, N> const& x)
    -> Vector<decltype(asinh(::std::declval<T>())), N>;

/// returns the arc hyperbolic cosine of x; the non-negative inverse of cosh.
/// The result is undefined if x<1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/acosh.xhtml
template <typename T> constexpr auto acosh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto acosh(Vector<T, N> const& x)
    -> Vector<decltype(acosh(::std::declval<T>())), N>;

/// returns the arc hyperbolic tangent of x; the inverse of tanh. The result is
/// undefined if |x|>1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/atanh.xhtml
template <typename T> constexpr auto atanh(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto atanh(Vector<T, N> const& x)
    -> Vector<decltype(atanh(::std::declval<T>())), N>;

// exponential functions

/// returns the value of x raised to the y power, i.e. x^y. The result is
/// undefined if x < 0 or if x = 0 and y <= 0.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/pow.xhtml
template <typename T, typename U,
          typename = ::std::enable_if_t<
              ::std::is_arithmetic_v<U>&& ::std::is_fundamental_v<U>>*>
constexpr auto pow(T x, U y) -> decltype(auto);
template <typename T, size_t N, typename U>
constexpr auto pow(Vector<T, N> const& x, U y)
    -> Vector<decltype(pow(::std::declval<T>(), ::std::declval<U>())), N>;
template <typename T, size_t N, typename U>
constexpr auto pow(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<decltype(pow(::std::declval<T>(), ::std::declval<U>())), N>;

/// returns the natural exponentiation of x. i.e., e^x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/exp.xhtml
template <typename T> constexpr auto exp(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto exp(Vector<T, N> const& x)
    -> Vector<decltype(exp(::std::declval<T>())), N>;

/// returns the natural logarithm of x, i.e. the value y which satisfies x =
/// e^y. The result is undefined if x <= 0.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/log.xhtml
template <typename T> constexpr auto log(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto log(Vector<T, N> const& x)
    -> Vector<decltype(log(::std::declval<T>())), N>;

/// returns 2 raised to the power of x. i.e., 2^x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/exp2.xhtml
template <typename T> constexpr auto exp2(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto exp2(Vector<T, N> const& x)
    -> Vector<decltype(exp2(::std::declval<T>())), N>;

/// returns the base 2 logarithm of x, i.e. the value y which satisfies x = 2^y.
/// The result is undefined if x <= 0.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/log2.xhtml
template <typename T> constexpr auto log2(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto log2(Vector<T, N> const& x)
    -> Vector<decltype(log2(::std::declval<T>())), N>;

/// returns the square root of x, i.e. the value √x. The result is undefined if
/// x < 0.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/sqrt.xhtml
template <typename T> constexpr auto sqrt(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto sqrt(Vector<T, N> const& x)
    -> Vector<decltype(sqrt(::std::declval<T>())), N>;

/// returns the inverse of the square root of x; i.e. the value 1/(√x). The
/// result is undefined if x <= 0.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/inversesqrt.xhtml
template <typename T> constexpr auto inversesqrt(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto inversesqrt(Vector<T, N> const& x)
    -> Vector<decltype(inversesqrt(::std::declval<T>())), N>;

// common functions

/// returns x if x >= 0, otherwise it returns –x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/abs.xhtml
template <typename T> constexpr auto abs(T x) -> T;
template <typename T, size_t N>
constexpr auto abs(Vector<T, N> const& x)
    -> Vector<decltype(abs(::std::declval<T>())), N>;

/// returns 1.0 if x > 0, 0.0 if x = 0, or –1.0 if x < 0.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/sign.xhtml
template <typename T> constexpr auto sign(T x) -> T;
template <typename T, size_t N>
constexpr auto sign(Vector<T, N> const& x)
    -> Vector<decltype(sign(::std::declval<T>())), N>;

/// returns a value equal to the nearest integer that is greater than or equal
/// to x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/ceil.xhtml
template <typename T> constexpr auto ceil(T x) -> T;
template <typename T, size_t N>
constexpr auto ceil(Vector<T, N> const& x)
    -> Vector<decltype(ceil(::std::declval<T>())), N>;

/// returns a value equal to the nearest integer that is less than or equal to
/// x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/floor.xhtml
template <typename T> constexpr auto floor(T x) -> T;
template <typename T, size_t N>
constexpr auto floor(Vector<T, N> const& x)
    -> Vector<decltype(floor(::std::declval<T>())), N>;

/// returns a value equal to the nearest integer to x whose absolute value is
/// not larger than the absolute value of x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/trunc.xhtml
template <typename T> constexpr auto trunc(T x) -> T;
template <typename T, size_t N>
constexpr auto trunc(Vector<T, N> const& x)
    -> Vector<decltype(trunc(::std::declval<T>())), N>;

/// returns a value equal to the nearest integer to x. The fraction 0.5 will
/// round in a direction chosen by the implementation, presumably the direction
/// that is fastest.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/round.xhtml
template <typename T> constexpr auto round(T x) -> T;
template <typename T, size_t N>
constexpr auto round(Vector<T, N> const& x)
    -> Vector<decltype(round(::std::declval<T>())), N>;

/// returns a value equal to the nearest integer to x. The fractional part of
/// 0.5 will round toward the nearest even integer.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/roundEven.xhtml
template <typename T> constexpr auto round_even(T x) -> T;
template <typename T, size_t N>
constexpr auto round_even(Vector<T, N> const& x) -> Vector<T, N>;

/// returns the fractional part of x. This is calculated as x - floor(x).
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/fract.xhtml
template <typename T> constexpr auto fract(T x) -> T;
template <typename T, size_t N>
constexpr auto fract(Vector<T, N> const& x)
    -> Vector<decltype(fract(::std::declval<T>())), N>;

/// returns the value of x modulo y. This is computed as x - y * floor(x/y).
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/mod.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto mod(T x, T y) -> T;
template <typename T, size_t N>
constexpr auto mod(Vector<T, N> const& x, T y)
    -> Vector<decltype(mod(::std::declval<T>(), ::std::declval<T>())), N>;

template <typename T, size_t N>
constexpr auto mod(Vector<T, N> const& x, Vector<T, N> const& y)
    -> Vector<decltype(mod(::std::declval<T>(), ::std::declval<T>())), N>;

/// separates a floating point value x into its integer and fractional parts.
/// The fractional part of the number is returned from the function and the
/// integer part (as a floating point quantity) is returned in the output
/// parameter i.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/modf.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto modf(T x, T& i) -> T;
template <typename T, size_t N>
constexpr auto modf(Vector<T, N> const& x, Vector<T, N>& i)
    -> Vector<decltype(modf(::std::declval<T>(), ::std::declval<T&>())), N>;

/// returns the minimum of the two parameters. It returns y if y is less than x,
/// otherwise it returns x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/min.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto min(T x, T y) -> T;
template <typename T, size_t N>
constexpr auto min(Vector<T, N> const& x, T y)
    -> Vector<decltype(min(::std::declval<T>(), ::std::declval<T>())), N>;
template <typename T, size_t N>
constexpr auto min(Vector<T, N> const& x, Vector<T, N> const& y)
    -> Vector<decltype(min(::std::declval<T>(), ::std::declval<T>())), N>;

/// returns the maximum of the two parameters. It returns y if y is greater than
/// x, otherwise it returns x.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/max.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto max(T x, T y) -> T;
template <typename T, size_t N>
constexpr auto max(Vector<T, N> const& x, T y)
    -> Vector<decltype(max(::std::declval<T>(), ::std::declval<T>())), N>;
template <typename T, size_t N>
constexpr auto max(Vector<T, N> const& x, Vector<T, N> const& y)
    -> Vector<decltype(max(::std::declval<T>(), ::std::declval<T>())), N>;

/// returns the value of x constrained to the range minVal to maxVal. The
/// returned value is computed as min(max(x, minVal), maxVal).
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/clamp.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto clamp(T x, T min, T max) -> T;
template <typename T, size_t N>
constexpr auto clamp(Vector<T, N> const& x, T min, T max)
    -> Vector<decltype(clamp(::std::declval<T>(), ::std::declval<T>(),
                             ::std::declval<T>())),
              N>;
template <typename T, size_t N>
constexpr auto clamp(Vector<T, N> const& x, Vector<T, N> const& min_val,
                     Vector<T, N> const& max_val)
    -> Vector<decltype(clamp(::std::declval<T>(), ::std::declval<T>(),
                             ::std::declval<T>())),
              N>;

/// performs a linear interpolation between x and y using a to weight between
/// them. Returns x * (1.0 - a) + y * a. The value for a is not restricted to
/// the range [0, 1].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/mix.xhtml
template <typename T, typename U,
          typename = ::std::enable_if_t<
              ::std::is_arithmetic_v<T>&& ::std::is_fundamental_v<T>>*>
constexpr auto mix(T x, T y, U a) -> T;
template <typename T, size_t N, typename U>
constexpr auto mix(Vector<T, N> const& x, Vector<T, N> const& y, U a)
    -> Vector<decltype(mix(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<U>())),
              N>;
template <typename T, size_t N, typename U>
constexpr auto mix(Vector<T, N> const& x, Vector<T, N> const& y,
                   Vector<U, N> const& a)
    -> Vector<decltype(mix(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<U>())),
              N>;

/// generates a step function by comparing x to edge.
/// For element i of the return value, 0.0 is returned if x[i] < edge[i],
/// and 1.0 is returned otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/step.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto step(T edge, T x) -> T;
template <typename T, size_t N, typename U>
constexpr auto step(T edge, Vector<T, N> const& x)
    -> Vector<decltype(step(::std::declval<T>(), ::std::declval<T>())), N>;
template <typename T, size_t N, typename U>
constexpr auto step(Vector<T, N> const& edge, Vector<T, N> const& x)
    -> Vector<decltype(step(::std::declval<T>(), ::std::declval<T>())), N>;

/// performs smooth Hermite interpolation between 0 and 1 when edge0 < x <
/// edge1. This is useful in cases where a threshold function with a smooth
/// transition is desired. smoothstep is equivalent to:
///
///    genType t;  /* Or genDType t; */
///    t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
///    return t * t * (3.0 - 2.0 * t);
///
/// Results are undefined if edge0 ≥ edge1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/smoothstep.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto smoothstep(T min, T max, T val) -> T;
template <typename T, size_t N>
constexpr auto smoothstep(T edge0, T edge1, Vector<T, N> const& vec)
    -> Vector<decltype(smoothstep(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T>())),
              N>;
template <typename T, size_t N>
constexpr auto smoothstep(Vector<T, N> const& edge0, Vector<T, N> const& edge1,
                          Vector<T, N> const& vec)
    -> Vector<decltype(smoothstep(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T>())),
              N>;

/// For each element i of the result, isnan returns true if x[i] is positive or
/// negative floating point NaN (Not a Number) and false otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/isnan.xhtml
template <typename T> constexpr auto isnan(T x) -> bool;
template <typename T, size_t N>
constexpr auto isnan(Vector<T, N> const& x)
    -> Vector<decltype(isnan(::std::declval<T>())), N>;

/// For each element i of the result, isinf returns true if x[i] is positive or
/// negative floating point infinity and false otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/isinf.xhtml
template <typename T> constexpr auto isinf(T x) -> bool;
template <typename T, size_t N>
constexpr auto isinf(Vector<T, N> const& x)
    -> Vector<decltype(isinf(::std::declval<T>())), N>;

/// float_bits_to_int and float_bits_to_uint return the encoding of their
/// floating-point parameters as int or uint, respectively. The floating-point
/// bit-level representation is preserved.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/floatBitsToInt.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto float_bits_to_int(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto float_bits_to_int(Vector<T, N> const& x)
    -> Vector<decltype(float_bits_to_int(::std::declval<T>())), N>;
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto float_bits_to_uint(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto float_bits_to_uint(Vector<T, N> const& x)
    -> Vector<decltype(float_bits_to_uint(::std::declval<T>())), N>;

/// int_bits_to_float and uint_bits_to_float return the encoding passed in
/// parameter x as a floating-point value. If the encoding of a NaN is passed in
/// x, it will not signal and the resulting value will be undefined. If the
/// encoding of a floating point infinity is passed in parameter x, the
/// resulting floating-point value is the corresponding (positive or negative)
/// floating point infinity.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/intBitsToFloat.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto int_bits_to_float(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto int_bits_to_float(Vector<T, N> const& x)
    -> Vector<decltype(int_bits_to_float(::std::declval<T>())), N>;
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto uint_bits_to_float(T x) -> decltype(auto);
template <typename T, size_t N>
constexpr auto uint_bits_to_float(Vector<T, N> const& x)
    -> Vector<decltype(uint_bits_to_float(::std::declval<T>())), N>;

/// fma performs, where possible, a fused multiply-add operation, returning a *
/// b + c. In use cases where the return value is eventually consumed by a
/// variable declared as precise:

///   + fma() is considered a single operation, whereas the expression a * b + c
///   consumed by a variable declared as precise is considered two operations.
///   + The precision of fma() can differ from the precision of the expression a
///   * b + c.
///   + fma() will be computed with the same precision as any other fma()
///   consumed by a precise variable, giving invariant results for the same
///   input values of a, b and c.
///
/// Otherwise, in the absence of precise consumption, there are no special
/// constraints on the number of operations or difference in precision between
/// fma() and the expression a * b + c.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/fma.xhtml
template <typename T, typename = ::std::enable_if_t<::std::is_arithmetic_v<
                          T>&& ::std::is_fundamental_v<T>>*>
constexpr auto fma(T a, T b, T c) -> T;
template <typename T, size_t N>
constexpr auto fma(Vector<T, N> const& a, Vector<T, N> const& b, T c)
    -> Vector<decltype(fma(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<T>())),
              N>;
template <typename T, size_t N>
constexpr auto fma(Vector<T, N> const& a, Vector<T, N> const& b,
                   Vector<T, N> const& c)
    -> Vector<decltype(fma(::std::declval<T>(), ::std::declval<T>(),
                           ::std::declval<T>())),
              N>;

/// extracts x into a floating-point significand in the range [0.5, 1.0) and in
/// integral exponent of two, such that: x = significand \times 2^{exponent}
///
/// The significand is returned by the function and the exponent is returned in
/// the output parameter exp. For a floating-point value of zero, the
/// significand and exponent are both zero. For a floating-point value that is
/// an infinity or a floating-point NaN, the results are undefined.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/frexp.xhtml
template <typename T> constexpr auto frexp(T x, int& exp) -> T;
template <typename T, size_t N>
constexpr auto frexp(Vector<T, N> const& x, Vector<int, N>& exp)
    -> Vector<decltype(frexp(::std::declval<T>(), ::std::declval<int&>())), N>;

/// builds a floating point number from x and the corresponding integral
/// exponent of two in exp, returning: significand \times 2^{exponent}
///
/// If this product is too large to be represented in the floating point type,
/// the result is undefined.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/ldexp.xhtml
template <typename T> constexpr auto ldexp(T x, int exp) -> T;
template <typename T, size_t N>
constexpr auto ldexp(Vector<T, N> const& x, int exp)
    -> Vector<decltype(ldexp(::std::declval<T>(), ::std::declval<int>())), N>;
template <typename T, size_t N>
constexpr auto ldexp(Vector<T, N> const& x, Vector<int, N> const& exp)
    -> Vector<decltype(ldexp(::std::declval<T>(), ::std::declval<int>())), N>;

// packing and unpacking functions

/// convert each component of the normalized floating-ponit value v into 8- or
/// 16-bit integer values and then packs the results into a 32-bit unsigned
/// intgeter. The conversion for component c of v to fixed-point is performed as
/// follows:
///   + packUnorm2x16: round(clamp(c, 0.0, 1.0) * 65535.0)
///   + packSnorm2x16: round(clamp(c, -1.0, 1.0) * 32767.0)
///   + packUnorm4x8: round(clamp(c, 0.0, 1.0) * 255.0)
///   + packSnorm4x8: round(clamp(c, -1.0, 1.0) * 127.0)
///
/// The first component of the vector will be written to the least significant
/// bits of the output; the last component will be written to the most
/// significant bits.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/packUnorm.xhtml
template <typename T>
constexpr auto pack_unorm2x16(Vector<T, 2> const& x) -> uint32_t;
template <typename T>
constexpr auto pack_snorm2x16(Vector<T, 2> const& x) -> uint32_t;
template <typename T>
constexpr auto pack_unorm4x8(Vector<T, 4> const& x) -> uint32_t;
template <typename T>
constexpr auto pack_snorm4x8(Vector<T, 4> const& x) -> uint32_t;

/// unpack single 32-bit unsigned integers, specified in the parameter p into a
/// pair of 16-bit unsigned integers, four 8-bit unsigned integers or four 8-bit
/// signed integers. Then, each component is converted to a normalized
/// floating-point value to generate the returned two- or four-component vector.
/// The conversion for unpacked fixed point value f to floating-point is
/// performed as follows:
///    + packUnorm2x16: f / 65535.0
///    + packSnorm2x16: clamp(f / 32727.0, -1.0, 1.0)
///    + packUnorm4x8: f / 255.0
///    + packSnorm4x8: clamp(f / 127.0, -1.0, 1.0)
///
/// The first component of the returned vector will be extracted from the least
/// significant bits of the input; the last component will be extracted from the
/// most significant bits.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/unpackUnorm.xhtml
constexpr auto unpack_unorm2x16(uint32_t f) -> Vector<float, 2>;
constexpr auto unpack_snorm2x16(uint32_t f) -> Vector<float, 2>;
constexpr auto unpack_unorm4x8(uint32_t f) -> Vector<float, 4>;
constexpr auto unpack_snorm4x8(uint32_t f) -> Vector<float, 4>;

/// returns an unsigned integer obtained by converting the components of a
/// two-component floating-point vector to the 16-bit floating-point
/// representation found in the OpenGL Specification, and then packing these two
/// 16-bit integers into a 32-bit unsigned integer. The first vector component
/// specifies the 16 least-significant bits of the result; the second component
/// specifies the 16 most-significant bits.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/packHalf2x16.xhtml
/// TODO: implement it.
constexpr auto pack_half2x16(Vector<float, 2> const& x) -> uint32_t;

/// returns a two-component floating-point vector with components obtained by
/// unpacking a 32-bit unsigned integer into a pair of 16-bit values,
/// interpreting those values as 16-bit floating-point numbers according to the
/// OpenGL Specification, and converting them to 32-bit floating-point values.
/// The first component of the vector is obtained from the 16 least-significant
/// bits of v; the second component is obtained from the 16 most-significant
/// bits of v.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/unpackHalf2x16.xhtml
constexpr auto unpack_half2x16(uint32_t x) -> Vector<float, 2>;

/// packs the component of parameter v into a 64-bit value. If an IEEE-754
/// infinity or NaN is created, it will not signal and the resulting
/// floating-point value is undefined. Otherwise, the bit-level representation
/// of v is preserved. The first vector component (v[0]) specifies the 32 least
/// significant bits of the result; the second component (v[1]) specifies the 32
/// most significant bits.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/packDouble2x32.xhtml
/// NOTE: constexpr on gcc
auto pack_double2x32(Vector<uint32_t, 2> const& x) -> double;

/// returns a two-component unsigned integer vector representation of d. The
/// bit-level representation of d is preserved. The first component of the
/// returned vector contains the 32 least significant bits of the double; the
/// second component consists the 32 most significant bits.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/unpackDouble2x32.xhtml
/// NOTE: constexpr on gcc
auto unpack_double2x32(double x) -> Vector<uint32_t, 2>;

// geometric functions

/// returns the length of the vector, i.e.
/// \sqrt{x_{0}^{2} + x_{1}^{2} + \cdots}
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/length.xhtml
template <typename T, size_t N>
constexpr auto length(Vector<T, N> const& x) -> decltype(auto);

/// returns the distance between the two points p0 and p1. i.e., length(p0 -
/// p1);
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/distance.xhtml
template <typename T, size_t N>
constexpr auto distance(Vector<T, N> const& p0, Vector<T, N> const& p1)
    -> decltype(auto);

/// returns the dot product of two vectors, x and y. i.e.
/// x_{0} \times y_{0} + x_{1} \times y_{1} + \cdots
/// or
/// length(x) * length(y) * \cos{angle}
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/dot.xhtml
template <typename T, size_t N>
constexpr auto dot(Vector<T, N> const& x, Vector<T, N> const& y) ->
    typename Vector<T, N>::value_type;
template <typename T, size_t N>
constexpr auto dot(Vector<T, N> const& x, Vector<T, N> const& y, double angle)
    -> double;

/// returns the cross product of two vectors, x and y, i.e.
/// \lefe(
/// x_{1} \times y_{2} - y_{1} \times x_{2}
/// x_{2} \times y_{0} - y_{2} \times x_{0}
/// x_{0} \times y_{1} - y_{0} \times x_{1}
/// \right)
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/cross.xhtml
template <typename T>
constexpr auto cross(Vector<T, 3> const& x, Vector<T, 3> const& y)
    -> Vector<T, 3>;

/// returns a vector with the same direction as its parameter, v, but with
/// length 1.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/normalize.xhtml
template <typename T, size_t N>
constexpr auto normalize(Vector<T, N> const& v) -> decltype(auto);

/// orients a vector to point away from a surface as defined by its normal. If
/// dot(Nref, I) < 0 faceforward returns N, otherwise it returns -N.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/faceforward.xhtml
template <typename T, size_t N>
constexpr auto faceforward(Vector<T, N> const& n, Vector<T, N> const& i,
                           Vector<T, N> const& nref) -> Vector<T, N>;

/// For a given incident vector I and surface normal N reflect returns the
/// reflection direction calculated as I - 2.0 * dot(N, I) * N.
///
/// N should be normalized in order to achieve the desired result.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/reflect.xhtml
template <typename T, size_t N>
constexpr auto reflect(Vector<T, N> const& i, Vector<T, N> const& n)
    -> decltype(auto);

/// For a given incident vector I, surface normal N and ratio of indices of
/// refraction, eta, refract returns the refraction vector, R. R is calculated
/// as:
///    k = 1.0 - eta * eta * (1.0 - dot(N, I) * dot(N, I));
///    if (k < 0.0)
///        R = genType(0.0);       // or genDType(0.0)
///    else
///        R = eta * I - (eta * dot(N, I) + sqrt(k)) * N;
///
/// The input parameters I and N should be normalized in order to achieve the
/// desired result.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/refract.xhtml
template <typename T, size_t N>
constexpr auto refract(Vector<T, N> const& i, Vector<T, N> const& n, T eta)
    -> decltype(auto);

// matrix functions

/// performs a component-wise multiplication of two matrices, yielding a result
/// matrix where each component, result[i][j] is computed as the scalar product
/// of x[i][j] and y[i][j].
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/matrixCompMult.xhtml
template <typename T, typename U, size_t M, size_t N>
constexpr auto hadamard(Matrix<T, M, N> const& lhs, Matrix<U, M, N> const& rhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), M, N>;
template <typename T, typename U, size_t M, size_t N>
constexpr auto matrix_comp_mult(Matrix<T, M, N> const& lhs,
                                Matrix<U, M, N> const& rhs) -> decltype(auto) {
  return hadamard(lhs, rhs);
}

/// treats the first parameter c as a column vector (matrix with one column) and
/// the second parameter r as a row vector (matrix with one row) and does a
/// linear algebraic matrix multiply c * r, yielding a matrix whose number of
/// rows is the number of components in c and whose number of columns is the
/// number of components in r.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/outerProduct.xhtml
template <typename T, size_t N, typename U, size_t M>
constexpr auto outer_product(Vector<T, N> const& lhs, Vector<U, M> const& rhs)
    -> Matrix<decltype(::std::declval<T>() * ::std::declval<U>()), N, M>;

/// returns the transpose of the matrix m.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/transpose.xhtml
template <typename T, size_t N>
constexpr auto transpose(Vector<T, N> const& vec) -> Matrix<T, 1, N>;
template <typename T, size_t N>
constexpr auto transpose(Matrix<T, 1, N> const& mat) -> Vector<T, N>;
template <typename T, size_t M, size_t N>
constexpr auto transpose(Matrix<T, M, N> const& mat) -> Matrix<T, N, M>;

/// returns the determinant of the matrix m.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/determinant.xhtml
template <typename T, size_t N>
constexpr auto determinant(Square<T, N> const& mat) ->
    typename MatrixTraits<Square<T, N>>::value_type;

/// returns the inverse of the matrix m. The values in the returned matrix are
/// undefined if m is singular or poorly-conditioned (nearly singular).
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/inverse.xhtml
/// TODO: implement it.
template <typename T, size_t N>
constexpr auto inverse(Square<T, N> const& mat) ->
    typename MatrixTraits<Square<T, N>>::value_type;

// vector functions

/// returns a boolean vector in which each element i is computed as x[i] < y[i].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/lessThan.xhtml
template <typename T, typename U, size_t N>
constexpr auto less_than(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns a boolean vector in which each element i is computed as x[i] <=
/// y[i].
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/lessThanEqual.xhtml
template <typename T, typename U, size_t N>
constexpr auto less_than_equal(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns a boolean vector in which each element i is computed as x[i] > y[i].
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/greaterThan.xhtml
template <typename T, typename U, size_t N>
constexpr auto greater_than(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns a boolean vector in which each element i is computed as x[i] >=
/// y[i].
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/greaterThanEqual.xhtml
template <typename T, typename U, size_t N>
constexpr auto greater_than_equal(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns a boolean vector in which each element i is computed as x[i] ==
/// y[i].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/equal.xhtml
template <typename T, typename U, size_t N>
constexpr auto equal(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns a boolean vector in which each element i is computed as x[i] !=
/// y[i].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/notEqual.xhtml
template <typename T, typename U, size_t N>
constexpr auto not_equal(Vector<T, N> const& x, Vector<U, N> const& y)
    -> Vector<bool, N>;

/// returns true if any element of x is true and false otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/any.xhtml
template <size_t N> constexpr auto any(Vector<bool, N> const& x) -> bool;

/// returns true if all elements of x are true and false otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/all.xhtml
template <size_t N> constexpr auto all(Vector<bool, N> const& x) -> bool;

/// logically inverts the boolean vector x. It returns a new boolean vector for
/// which each element i is computed as !x[i].
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/not.xhtml
template <size_t N>
constexpr auto operator!(Vector<bool, N> const& vec) -> Vector<bool, N>;

// integer functions

/// adds two 32-bit unsigned integer variables (scalars or vectors) and
/// generates a 32-bit unsigned integer result, along with a carry output. The
/// result is the sum of x and y modulo 2^32. The value carry is set to 0 if the
/// sum is less than 2^32 and to 1 otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/uaddCarry.xhtml
template <typename T> constexpr auto uadd_carry(T x, T y, T& carry) -> T;
template <typename T, size_t N>
constexpr auto uadd_carry(Vector<T, N> const& x, Vector<T, N> const& y,
                          Vector<T, N>& carries)
    -> Vector<decltype(uadd_carry(::std::declval<T>(), ::std::declval<T>(),
                                  ::std::declval<T&>())),
              N>;

/// subtracts two 32-bit unsigned integer variables (scalars or vectors) and
/// generates a 32-bit unsigned integer result, along with a borrow output. The
/// result is the difference of x and y if non-negative, or 2^32 plus that
/// difference otherwise. The value borrow is set to 0 if x ≥ y and to 1
/// otherwise.
///
/// @see: https://registry.khronos.org/OpenGL-Refpages/gl4/html/usubBorrow.xhtml
template <typename T> constexpr auto usub_borrow(T x, T y, T& borrow) -> T;
template <typename T, size_t N>
constexpr auto usub_borrow(Vector<T, N> const& x, Vector<T, N> const& y,
                           Vector<T, N>& borrows)
    -> Vector<decltype(usub_borrow(::std::declval<T>(), ::std::declval<T>(),
                                   ::std::declval<T&>())),
              N>;

/// perform multiplication of the two 32-bit integer quantities x and y,
/// producing a 64-bit integer result. The 32 least significant bits of this
/// product are returned in lsb and the 32 most significant bits are returned in
/// msb. umulExtended and imulExtended perform unsigned and signed
/// multiplication, respectively.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/umulExtended.xhtml
constexpr auto umul_extended(uint32_t x, uint32_t y, uint32_t& msb,
                             uint32_t& lsb) -> void;
template <size_t N>
constexpr auto umul_extended(Vector<uint32_t, N> const& x,
                             Vector<uint32_t, N> const& y,
                             Vector<uint32_t, N>& msb, Vector<uint32_t, N>& lsb)
    -> void;
constexpr auto imul_extended(int32_t x, int32_t y, int32_t& msb, int32_t& lsb)
    -> void;
template <size_t N>
constexpr auto imul_extended(Vector<int32_t, N> const& x,
                             Vector<int32_t, N> const& y,
                             Vector<int32_t, N>& msb, Vector<int32_t, N>& lsb)
    -> void;

/// extracts a subset of the bits of value and returns it in the least
/// significant bits of the result. The range of bits extracted is [offset,
/// offset + bits - 1]. For unsigned data types, the most significant bits of
/// the result will be set to zero. For signed data types, the most significant
/// bits will be set to the value of bit offset + base - 1 (i.e., it is sign
/// extended to the width of the return type). If bits is zero, the result will
/// be zero. The result will be undefined if offset or bits is negative, or if
/// the sum of offset and bits is greater than the number of bits used to store
/// the operand.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/bitfieldExtract.xhtml
template <typename T>
constexpr auto bitfield_extract(T value, int offset, int bits)
    -> decltype(auto);
template <typename T, size_t N>
constexpr auto bitfield_extract(Vector<T, N> const& value, int offset, int bits)
    -> Vector<decltype(bitfield_extract(::std::declval<T>(),
                                        ::std::declval<int>(),
                                        ::std::declval<int>())),
              N>;
template <typename T, size_t N>
constexpr auto bitfield_extract(Vector<T, N> const& value,
                                Vector<int, N> const& offset,
                                Vector<int, N> const& bits)
    -> Vector<decltype(bitfield_extract(::std::declval<T>(),
                                        ::std::declval<int>(),
                                        ::std::declval<int>())),
              N>;

/// inserts the bits least significant bits of insert into base at offset
/// offset. The returned value will have bits [offset, offset + bits + 1] taken
/// from [0, bits - 1] of insert and all other bits taken directly from the
/// corresponding bits of base. If bits is zero, the result will simply be the
/// original value of base. The result will be undefined if offset or bits is
/// negative, or if the sum of offset and bits is greater than the number of
/// bits used to store the operand.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/bitfieldInsert.xhtml
template <typename T>
constexpr auto bitfield_insert(T base, uint64_t insert, int offset, int bits)
    -> decltype(auto);
template <typename T, size_t N>
constexpr auto bitfield_insert(Vector<T, N> const& bases, uint64_t insert,
                               int offset, int bits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N>;
template <typename T, size_t N>
constexpr auto bitfield_insert(Vector<T, N> const& bases,
                               Vector<uint64_t, N> const& inserts, int offset,
                               int bits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N>;
template <typename T, size_t N>
constexpr auto
bitfield_insert(Vector<T, N> const& bases, Vector<uint64_t, N> const& inserts,
                Vector<int, N> const& offsets, Vector<int, N> const& nbits)
    -> Vector<decltype(bitfield_insert(
                  ::std::declval<T>(), ::std::declval<uint64_t>(),
                  ::std::declval<int>(), ::std::declval<int>())),
              N>;

/// returns the reversal of the bits of value. The bit numbered n will be taken
/// from bit (bits - 1) - n of value, where bits is the total number of bits
/// used to represent value.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/bitfieldReverse.xhtml
template <typename T>
constexpr auto bitfield_reverse(T value) -> decltype(auto);
template <typename T, size_t N>
constexpr auto bitfield_reverse(Vector<T, N> const& values)
    -> Vector<decltype(bitfield_reverse(::std::declval<T>())), N>;

/// returns the number of bits that are set to 1 in the binary representation of
/// value.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/bitCount.xhtml
template <typename T> constexpr auto bit_count(T value) -> int;
template <typename T, size_t N>
constexpr auto bit_count(Vector<T, N> const& values)
    -> Vector<decltype(bit_count(::std::declval<T>())), N>;

/// returns the bit number of the least significant bit that is set to 1 in the
/// binary representation of value. If value is zero, -1 will be returned.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/findLSB.xhtml
template <typename T> constexpr auto find_lsb(T value) -> int;
template <typename T, size_t N>
constexpr auto find_lsb(Vector<T, N> const& values)
    -> Vector<decltype(find_lsb(::std::declval<T>())), N>;

/// returns the bit number of the most significant bit that is set to 1 in the
/// binary representation of value. For positive integers, the result will be
/// the bit number of the most significant bit that is set to 1. For negative
/// integers, the result will be the bit number of the most significant bit set
/// to 0. For a value of zero or negative 1, -1 will be returned.
///
/// @see:
/// https://registry.khronos.org/OpenGL-Refpages/gl4/html/findMSB.xhtml
template <typename T> constexpr auto find_msb(T value) -> int;
template <typename T, size_t N>
constexpr auto find_msb(Vector<T, N> const& values)
    -> Vector<decltype(find_msb(::std::declval<T>())), N>;

// NOLINTEND(readability-identifier-length)

template <typename T, size_t M, size_t N>
constexpr auto diagonal(Matrix<T, M, N> const& mat)
    -> Vector<T, ::std::min(M, N)>;

template <typename T>
constexpr auto scalar_triple(Vector<T, 3> const& vec1, Vector<T, 3> const& vec2,
                             Vector<T, 3> const& vec3) -> Vector<T, 3>;

template <typename T>
constexpr auto vector_triple(Vector<T, 3> const& vec1, Vector<T, 3> const& vec2,
                             Vector<T, 3> const& vec3) -> Vector<T, 3>;

template <typename T, size_t N>
constexpr auto
scalar_quadruple(Vector<T, N> const& vec1, Vector<T, N> const& vec2,
                 Vector<T, N> const& vec3, Vector<T, N> const& vec4) -> T;

}  // namespace math

#endif  // OPENXYZ_MATH_OPERATION_HXX
