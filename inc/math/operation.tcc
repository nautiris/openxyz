#pragma once

#if !defined(OPENXYZ_MATH_OPERATION_TCC)
#define OPENXYZ_MATH_OPERATION_TCC

#include "math/operation.hxx"

#include <math.h>

namespace math {

namespace _details {

template <size_t... Indexes, typename T, size_t M, size_t N>
constexpr auto
diagonal_aux(Matrix<T, M, N> const& mat,
             ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> Vector<T, ::std::min(M, N)> {
  return Vector<T, sizeof...(Indexes)>{::std::get<Indexes, Indexes>(mat)...};
}

}  // namespace _details

template <typename T, size_t M, size_t N>
constexpr auto diagonal(Matrix<T, M, N> const& mat)
    -> Vector<T, ::std::min(M, N)> {
  return _details::diagonal_aux(mat,
                                ::std::make_index_sequence<::std::min(M, N)>{});
}

/* a . (b x c) */
template <typename T>
constexpr auto scalar_triple(Vector<T, 3> const& vec1, Vector<T, 3> const& vec2,
                             Vector<T, 3> const& vec3) -> Vector<T, 3> {
  return det(Square<T, 3>{vec1, vec2, vec3});
}

/* a x (b x c) */
template <typename T>
constexpr auto vector_triple(Vector<T, 3> const& vec1, Vector<T, 3> const& vec2,
                             Vector<T, 3> const& vec3) -> Vector<T, 3> {
  return dot(vec1, vec3) * vec2 - dot(vec1, vec2) * vec3;
}

/* (a x b) . (c x d) */
template <typename T, size_t N>
constexpr auto
scalar_quadruple(Vector<T, N> const& vec1, Vector<T, N> const& vec2,
                 Vector<T, N> const& vec3, Vector<T, N> const& vec4) -> T {
  return dot(vec1, vec3) * dot(vec2, vec4) - dot(vec1, vec4) * dot(vec2, vec3);
}

}  // namespace math

#endif  // OPENXYZ_MATH_OPERATION_TCC
