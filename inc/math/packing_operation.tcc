#pragma once

#if !defined(OPENXYZ_MATH_PACKING_OPERATION_TCC)
#define OPENXYZ_MATH_PACKING_OPERATION_TCC

#include "math/operation.hxx"

#include <type_traits>

namespace math {

template <typename T>
constexpr auto pack_unorm2x16(Vector<T, 2> const& vec) -> uint32_t {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  auto result = ::math::map(vec, [](T val) constexpr -> uint16_t {
    return static_cast<uint16_t>(
        ::std::round(::std::clamp(val, static_cast<T>(0), static_cast<T>(1)) *
                     static_cast<T>(65'535)));
  });
  return ((::std::get<0>(result) & 0x0000'FFFFU) << 0U) |
         ((::std::get<1>(result) & 0x0000'FFFFU) << 16U);
}

template <typename T>
constexpr auto pack_snorm2x16(Vector<T, 2> const& vec) -> uint32_t {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  auto result = ::math::map(vec, [](T val) constexpr -> int16_t {
    return static_cast<int16_t>(
        ::std::round(::std::clamp(val, static_cast<T>(-1), static_cast<T>(1)) *
                     static_cast<T>(32'767)));
  });
  return ((::std::get<0>(result) & 0x0000'FFFFU) << 0U) |
         ((::std::get<1>(result) & 0x0000'FFFFU) << 16U);
}

template <typename T>
constexpr auto pack_unorm4x8(Vector<T, 4> const& vec) -> uint32_t {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  auto result = ::math::map(vec, [](T val) constexpr -> uint8_t {
    return static_cast<uint8_t>(
        ::std::round(::std::clamp(val, static_cast<T>(0), static_cast<T>(1)) *
                     static_cast<T>(255)));
  });
  return ((::std::get<0>(result) & 0x0000'00FFU) << 0U) |
         ((::std::get<1>(result) & 0x0000'00FFU) << 8U) |
         ((::std::get<2>(result) & 0x0000'00FFU) << 16U) |
         ((::std::get<3>(result) & 0x0000'00FFU) << 24U);
}

template <typename T>
constexpr auto pack_snorm4x8(Vector<T, 4> const& vec) -> uint32_t {
  static_assert(::std::is_floating_point_v<T>, "T must be floating point.");
  auto result = ::math::map(vec, [](T val) constexpr -> int8_t {
    return static_cast<int8_t>(
        ::std::round(::std::clamp(val, static_cast<T>(-1), static_cast<T>(1)) *
                     static_cast<T>(127)));
  });
  return ((::std::get<0>(result) & 0x0000'00FFU) << 0U) |
         ((::std::get<1>(result) & 0x0000'00FFU) << 8U) |
         ((::std::get<2>(result) & 0x0000'00FFU) << 16U) |
         ((::std::get<3>(result) & 0x0000'00FFU) << 24U);
}

constexpr auto unpack_unorm2x16(uint32_t packed) -> Vector<float, 2> {
  return ::math::make_vector<float>(
      static_cast<float>(static_cast<uint16_t>((packed >> 0U) & 0x0000'FFFFU)) /
          65535.0f,
      static_cast<float>(
          static_cast<uint16_t>((packed >> 16U) & 0x0000'FFFFU)) /
          65535.0f);
}

constexpr auto unpack_snorm2x16(uint32_t packed) -> Vector<float, 2> {
  return ::math::make_vector<float>(
      ::std::clamp(static_cast<float>(
                       static_cast<int16_t>((packed >> 0U) & 0x0000'FFFFU)) /
                       32727.0f,
                   -1.0f, 1.0f),
      ::std::clamp(static_cast<float>(
                       static_cast<int16_t>((packed >> 16U) & 0x0000'FFFFU)) /
                       32727.0f,
                   -1.0f, 1.0f));
}

constexpr auto unpack_unorm4x8(uint32_t packed) -> Vector<float, 4> {
  return ::math::make_vector<float>(
      static_cast<float>(static_cast<uint8_t>((packed >> 0U) & 0x0000'00FFU)) /
          255.0f,
      static_cast<float>(static_cast<uint8_t>((packed >> 8U) & 0x0000'00FFU)) /
          255.0f,
      static_cast<float>(static_cast<uint8_t>((packed >> 16U) & 0x0000'00FFU)) /
          255.0f,
      static_cast<float>(static_cast<uint8_t>((packed >> 24U) & 0x0000'00FFU)) /
          255.0f);
}

constexpr auto unpack_snorm4x8(uint32_t packed) -> Vector<float, 4> {
  return ::math::make_vector<float>(
      ::std::clamp(static_cast<float>(
                       static_cast<int8_t>((packed >> 0U) & 0x0000'00FFU)) /
                       127.0f,
                   -1.0f, 1.0f),
      ::std::clamp(static_cast<float>(
                       static_cast<int8_t>((packed >> 8U) & 0x0000'00FFU)) /
                       127.0f,
                   -1.0f, 1.0f),
      ::std::clamp(static_cast<float>(
                       static_cast<int8_t>((packed >> 16U) & 0x0000'00FFU)) /
                       127.0f,
                   -1.0f, 1.0f),
      ::std::clamp(static_cast<float>(
                       static_cast<int8_t>((packed >> 24U) & 0x0000'00FFU)) /
                       127.0f,
                   -1.0f, 1.0f));
}

inline auto pack_double2x32(Vector<uint32_t, 2> const& vec) -> double {
  uint64_t result =
      ((::std::get<0>(vec) & static_cast<uint64_t>(0x0000'0000'FFFF'FFFFU))
       << 0U) |
      ((::std::get<1>(vec) & static_cast<uint64_t>(0x0000'0000'FFFF'FFFFU))
       << 32U);
  return *reinterpret_cast<double*>(&result);
}

inline auto unpack_double2x32(double packed) -> Vector<uint32_t, 2> {
  uint64_t const kRepresentation = *reinterpret_cast<uint64_t*>(&packed);
  return ::math::make_vector<uint32_t>(
      static_cast<uint32_t>((kRepresentation >> 0U) &
                            static_cast<uint64_t>(0x0000'0000'FFFF'FFFFU)),
      static_cast<uint32_t>((kRepresentation >> 32U) &
                            static_cast<uint64_t>(0x0000'0000'FFFF'FFFFU)));
}

}  // namespace math

#endif  // OPENXYZ_MATH_PACKING_OPERATION_TCC
