#pragma once

#if !defined(OPENXYZ_MATH_SWIZZLE_HXX)
#define OPENXYZ_MATH_SWIZZLE_HXX

#include "math/vector.hxx"
#include "utils.hpp"

#include <iostream>
#include <stdint.h>
#include <string.h>
#include <type_traits>
#include <utility>

namespace math {

// NOLINTBEGIN(readability-identifier-naming)
enum class Channel : uint8_t {
  X = 0,
  Y,
  Z,
  W,
  Dyn,
};
// NOLINTEND(readability-identifier-naming)

// NOLINTBEGIN(misc-unconventional-assign-operator,google-explicit-constructor)

template <typename T, unsigned int I> struct ScalarSwizzle {
  using value_type = T;
  value_type val[1];
  NAUTIRIS_ALWAYS_INLINE auto operator=(value_type const kX) -> ScalarSwizzle& {
    val[I] = kX;
    return *this;
  }
  NAUTIRIS_ALWAYS_INLINE constexpr operator value_type() const {
    return val[I];
  }

#define ARITHMETIC_BINARY_OPERATOR(OP, INTEGER)                                \
  template <typename U,                                                        \
            typename = ::std::enable_if_t<                                     \
                ::std::is_convertible_v<U, value_type> && (INTEGER)>*>         \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP##=(U const value)          \
      ->ScalarSwizzle& {                                                       \
    operator=(*this OP static_cast<value_type>(value));                        \
    return *this;                                                              \
  }
  ARITHMETIC_BINARY_OPERATOR(+, true)
  ARITHMETIC_BINARY_OPERATOR(-, true)
  ARITHMETIC_BINARY_OPERATOR(*, true)
  ARITHMETIC_BINARY_OPERATOR(/, true)
  ARITHMETIC_BINARY_OPERATOR(%, !::std::is_floating_point_v<value_type>)
  ARITHMETIC_BINARY_OPERATOR(&, !::std::is_floating_point_v<value_type>)
  ARITHMETIC_BINARY_OPERATOR(|, !::std::is_floating_point_v<value_type>)
  ARITHMETIC_BINARY_OPERATOR(^, !::std::is_floating_point_v<value_type>)
#undef ARITHMETIC_BINARY_OPERATOR

#define LOGICAL_BINARY_OPERATOR(OP, NAME, NOT_OP)                              \
  template <typename U,                                                        \
            typename =                                                         \
                ::std::enable_if_t<::std::is_convertible_v<U, value_type>>*>   \
  constexpr auto operator OP(U const value) const->bool {                      \
    return ::math::NAME{}(static_cast<value_type>(*this),                      \
                          static_cast<value_type>(value));                     \
  }                                                                            \
  template <typename U,                                                        \
            typename =                                                         \
                ::std::enable_if_t<::std::is_convertible_v<U, value_type>>*>   \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator NOT_OP(U const value)         \
      const->bool {                                                            \
    return !(*this OP static_cast<value_type>(value));                         \
  }
  LOGICAL_BINARY_OPERATOR(==, EqualTo, !=)
  LOGICAL_BINARY_OPERATOR(<, Less, >=)
  LOGICAL_BINARY_OPERATOR(>, Greater, <=)
#undef LOGICAL_BINARY_OPERATOR

  template <typename U = value_type>
  NAUTIRIS_ALWAYS_INLINE auto operator++(int)
      -> ::std::enable_if_t<!::std::is_floating_point_v<U>, value_type> {
    return val[I]++;
  }
  template <typename U = value_type>
  NAUTIRIS_ALWAYS_INLINE auto operator++()
      -> ::std::enable_if_t<!::std::is_floating_point_v<U>, value_type> {
    return ++val[I];
  }
  template <typename U = value_type>
  NAUTIRIS_ALWAYS_INLINE auto operator--(int)
      -> ::std::enable_if_t<!::std::is_floating_point_v<U>, value_type> {
    return val[I]--;
  }
  template <typename U = value_type>
  NAUTIRIS_ALWAYS_INLINE auto operator--()
      -> ::std::enable_if_t<!::std::is_floating_point_v<U>, value_type> {
    return --val[I];
  }

  friend auto operator<<(::std::ostream& os, ScalarSwizzle const scalar)
      -> ::std::ostream& {
    os << static_cast<value_type>(scalar);
    return os;
  }
};

template <typename T, size_t N, size_t SwizzleSize, size_t... Indexes>
struct VectorSwizzle {
  using vector_type = typename VectorTraits<Vector<T, SwizzleSize>>::type;
  using value_type = typename VectorTraits<vector_type>::value_type;
  static constexpr size_t dimension{VectorTraits<vector_type>::dimension};

  value_type val[N];
  template <typename U, size_t M,
            typename = ::std::enable_if_t<M == N || M == SwizzleSize>*>
  NAUTIRIS_ALWAYS_INLINE auto operator=(Vector<U, M> const& vec)
      -> VectorSwizzle& {
    access_aux(vec, ::std::make_index_sequence<SwizzleSize>{});
    return *this;
  }
  template <typename U, size_t M, size_t OtherSSize, size_t... OtherIndexes>
  NAUTIRIS_ALWAYS_INLINE auto
  operator=(VectorSwizzle<U, M, OtherSSize, OtherIndexes...> const& vec)
      -> VectorSwizzle& {
    access_aux(vec);
    return *this;
  }
  template <size_t M,
            typename = ::std::enable_if_t<(SwizzleSize <= M) && (M <= N)>*>
  NAUTIRIS_ALWAYS_INLINE constexpr operator Vector<value_type, M>() const {
    return Vector<value_type, M>(
        ::math::make_vector<value_type>(val[Indexes]...));
  }

  friend auto operator<<(::std::ostream& os, VectorSwizzle const& vec)
      -> ::std::ostream& {
    os << static_cast<vector_type>(vec);
    return os;
  }

 private:
  template <size_t... AccessIndexes, typename U, size_t M>
  NAUTIRIS_ALWAYS_INLINE auto
  access_aux(Vector<U, M> const& vec,
             ::std::integer_sequence<size_t, AccessIndexes...> /*unused*/)
      -> void {
    ((val[Indexes] = static_cast<value_type>(::std::get<AccessIndexes>(vec))),
     ...);
  }
  template <typename U, size_t M, size_t OtherSSize, size_t... OtherIndexes>
  NAUTIRIS_ALWAYS_INLINE auto
  access_aux(VectorSwizzle<U, M, OtherSSize, OtherIndexes...> const& vec)
      -> void {
    ((val[Indexes] = static_cast<value_type>(vec.val[OtherIndexes])), ...);
  }
};

#define ARITHMETIC_BINARY_OPERATOR_DECLARE(FUNCTION)                           \
  FUNCTION(+, ::std::plus{}, true, true)                                       \
  FUNCTION(-, ::std::minus{}, true, false)                                     \
  FUNCTION(*, ::std::multiplies{}, true, true)                                 \
  FUNCTION(/, ::std::divides{}, true, false)                                   \
  FUNCTION(%, ::std::modulus{}, !::std::is_floating_point_v<T>, false)         \
  FUNCTION(&, ::std::bit_and{}, !::std::is_floating_point_v<T>, true)          \
  FUNCTION(|, ::std::bit_or{}, !::std::is_floating_point_v<T>, true)           \
  FUNCTION(^,                                                                  \
           ::std::bit_xor{                                                     \
           },                                                                  \
           !::std::is_floating_point_v<T>, true)

#define ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER(OP, FUNC, INTEGER, COMMUTATIVE) \
  template <                                                                   \
      typename T, size_t N, size_t SwizzleSize, size_t... Indexes, typename U, \
      typename = ::std::enable_if_t<::std::is_arithmetic_v<U> && (INTEGER)>*>  \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(                           \
      VectorSwizzle<T, N, SwizzleSize, Indexes...> const& lhs, U const rhs)    \
      ->typename VectorSwizzle<T, N, SwizzleSize, Indexes...>::vector_type {   \
    return map(                                                                \
        static_cast<typename VectorSwizzle<T, N, SwizzleSize,                  \
                                           Indexes...>::vector_type>(lhs),     \
        rhs, FUNC);                                                            \
  }                                                                            \
  template <typename T, typename U, size_t N, size_t SwizzleSize,              \
            size_t... Indexes,                                                 \
            typename = ::std::enable_if_t<::std::is_arithmetic_v<T> &&         \
                                          (INTEGER) && (COMMUTATIVE)>*>        \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(                           \
      T const lhs, VectorSwizzle<U, N, SwizzleSize, Indexes...> const& rhs)    \
      ->typename VectorSwizzle<U, N, SwizzleSize, Indexes...>::vector_type {   \
    return map(                                                                \
        static_cast<typename VectorSwizzle<U, N, SwizzleSize,                  \
                                           Indexes...>::vector_type>(rhs),     \
        lhs, FUNC);                                                            \
  }                                                                            \
  template <                                                                   \
      typename T, size_t N, size_t SwizzleSize, size_t... Indexes, typename U, \
      typename = ::std::enable_if_t<::std::is_arithmetic_v<U> && (INTEGER)>*>  \
  NAUTIRIS_ALWAYS_INLINE auto operator OP##=(                                  \
      VectorSwizzle<T, N, SwizzleSize, Indexes...>& lhs, U const rhs)          \
      ->VectorSwizzle<T, N, SwizzleSize, Indexes...>& {                        \
    return (lhs = lhs OP rhs);                                                 \
  }
ARITHMETIC_BINARY_OPERATOR_DECLARE(ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER)
#undef ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER

#define ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR(OP, FUNC, INTEGER, COMMUTATIVE) \
  template <typename T, size_t N1, size_t SwizzleSize1, size_t... Indexes1,    \
            typename U, size_t N2, size_t SwizzleSize2, size_t... Indexes2,    \
            typename = ::std::enable_if_t<(SwizzleSize1 > 0) &&                \
                                          (SwizzleSize2 > 0) && (INTEGER)>*>   \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(                           \
      VectorSwizzle<T, N1, SwizzleSize1, Indexes1...> const& lhs,              \
      VectorSwizzle<U, N2, SwizzleSize2, Indexes2...> const& rhs)              \
      ->decltype(auto) {                                                       \
    return map(static_cast<typename VectorSwizzle<                             \
                   T, N1, ::std::max(SwizzleSize1, SwizzleSize2),              \
                   Indexes1...>::vector_type>(lhs),                            \
               static_cast<typename VectorSwizzle<                             \
                   U, N2, ::std::max(SwizzleSize1, SwizzleSize2),              \
                   Indexes2...>::vector_type>(rhs),                            \
               FUNC);                                                          \
  }                                                                            \
  template <                                                                   \
      typename T, size_t N, size_t SwizzleSize, size_t... Indexes, typename U, \
      typename = ::std::enable_if_t<VectorTraits<U>::value && (INTEGER)>*>     \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(                           \
      VectorSwizzle<T, N, SwizzleSize, Indexes...> const& lhs, U const& rhs)   \
      ->typename VectorSwizzle<T, N, SwizzleSize, Indexes...>::vector_type {   \
    constexpr size_t kLhsDim = SwizzleSize;                                    \
    constexpr size_t kRhsDim = VectorTraits<U>::dimension;                     \
    return map(static_cast<Vector<T, ::std::max(kLhsDim, kRhsDim)>>(lhs),      \
               static_cast<Vector<typename VectorTraits<U>::value_type,        \
                                  ::std::max(kLhsDim, kRhsDim)>>(rhs),         \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, typename U, size_t N, size_t SwizzleSize,              \
            size_t... Indexes,                                                 \
            typename = ::std::enable_if_t<VectorTraits<U>::value &&            \
                                          (INTEGER) && (COMMUTATIVE)>*>        \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(                           \
      T const& lhs, VectorSwizzle<U, N, SwizzleSize, Indexes...> const& rhs)   \
      ->typename VectorSwizzle<U, N, SwizzleSize, Indexes...>::vector_type {   \
    constexpr size_t kRhsDim = SwizzleSize;                                    \
    constexpr size_t kLhsDim = VectorTraits<T>::dimension;                     \
    return map(static_cast<Vector<U, ::std::max(kLhsDim, kRhsDim)>>(rhs),      \
               static_cast<Vector<typename VectorTraits<T>::value_type,        \
                                  ::std::max(kLhsDim, kRhsDim)>>(lhs),         \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, size_t N1, size_t SwizzleSize1, size_t... Indexes1,    \
            typename U, size_t N2, size_t SwizzleSize2, size_t... Indexes2,    \
            typename = ::std::enable_if_t<(SwizzleSize1 > 0) &&                \
                                          (SwizzleSize2 > 0) && (INTEGER)>*>   \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP##=(                        \
      VectorSwizzle<T, N1, SwizzleSize1, Indexes1...> const& lhs,              \
      VectorSwizzle<U, N2, SwizzleSize2, Indexes2...> const& rhs)              \
      ->VectorSwizzle<T, N1, SwizzleSize1, Indexes1...>& {                     \
    return (lhs = lhs OP rhs);                                                 \
  }                                                                            \
  template <typename T, size_t N, size_t SwizzleSize, size_t... Indexes,       \
            typename U,                                                        \
            typename = ::std::enable_if_t<                                     \
                VectorTraits<U>::value &&                                      \
                VectorTraits<U>::dimension == SwizzleSize && (INTEGER)>*>      \
  NAUTIRIS_ALWAYS_INLINE auto operator OP##=(                                  \
      VectorSwizzle<T, N, SwizzleSize, Indexes...>& lhs, U const& rhs)         \
      ->VectorSwizzle<T, N, SwizzleSize, Indexes...>& {                        \
    return (lhs = lhs OP rhs);                                                 \
  }
ARITHMETIC_BINARY_OPERATOR_DECLARE(ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR)
#undef ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR

#undef ARITHMETIC_BINARY_OPERATOR_DECLARE

template <typename T1, size_t N1, size_t SwizzleSize1, size_t... Indexes1,
          typename T2, size_t N2, size_t SwizzleSize2, size_t... Indexes2>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator==(VectorSwizzle<T1, N1, SwizzleSize1, Indexes1...> const& lhs,
           VectorSwizzle<T2, N2, SwizzleSize2, Indexes2...> const& rhs)
    -> bool {
  return static_cast<Vector<T1, ::std::max(SwizzleSize1, SwizzleSize2)>>(lhs) ==
         static_cast<Vector<T2, ::std::max(SwizzleSize1, SwizzleSize2)>>(rhs);
}
template <typename T, size_t N, size_t SwizzleSize, size_t... Indexes,
          typename U, typename = ::std::enable_if_t<VectorTraits<U>::value>*>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator==(VectorSwizzle<T, N, SwizzleSize, Indexes...> const& lhs,
           U const& rhs) -> bool {
  return static_cast<Vector<T, SwizzleSize>>(lhs) ==
         static_cast<Vector<typename VectorTraits<U>::value_type,
                            VectorTraits<U>::dimension>>(rhs);
}
template <
    typename T, typename U, size_t N, size_t SwizzleSize, size_t... Indexes,
    typename = ::std::enable_if_t<
        !::std::is_same_v<T, VectorSwizzle<U, N, SwizzleSize, Indexes...>> &&
        VectorTraits<T>::value>*>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator==(T const& lhs,
           VectorSwizzle<U, N, SwizzleSize, Indexes...> const& rhs) -> bool {
  return static_cast<Vector<typename VectorTraits<T>::value_type,
                            VectorTraits<T>::dimension>>(lhs) ==
         static_cast<Vector<U, SwizzleSize>>(rhs);
}

template <typename T1, size_t N1, size_t SwizzleSize1, size_t... Indexes1,
          typename T2, size_t N2, size_t SwizzleSize2, size_t... Indexes2>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator!=(VectorSwizzle<T1, N1, SwizzleSize1, Indexes1...> const& lhs,
           VectorSwizzle<T2, N2, SwizzleSize2, Indexes2...> const& rhs)
    -> bool {
  return !(lhs == rhs);
}
template <typename T, size_t N, size_t SwizzleSize, size_t... Indexes,
          typename U, typename = ::std::enable_if_t<VectorTraits<U>::value>*>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator!=(VectorSwizzle<T, N, SwizzleSize, Indexes...> const& lhs,
           U const& rhs) -> bool {
  return !(lhs == rhs);
}
template <
    typename T, typename U, size_t N, size_t SwizzleSize, size_t... Indexes,
    typename = ::std::enable_if_t<
        !::std::is_same_v<T, VectorSwizzle<U, N, SwizzleSize, Indexes...>> &&
        VectorTraits<T>::value>*>
NAUTIRIS_ALWAYS_INLINE constexpr auto
operator!=(T const& lhs,
           VectorSwizzle<U, N, SwizzleSize, Indexes...> const& rhs) -> bool {
  return !(lhs == rhs);
}

#define OPENXYZ_MATH_SWIZZLE_GEN_22(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NUM1, CHAN_NUM2)                      \
  /*NOLINTNEXTLINE(bugprone-macro-parentheses)*/                               \
  ScalarSwizzle<TYPE, CHAN_NUM1> CHAN_NAME1;                                   \
  /*NOLINTNEXTLINE(bugprone-macro-parentheses)*/                               \
  ScalarSwizzle<TYPE, CHAN_NUM2> CHAN_NAME2;                                   \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM1, CHAN_NUM1> CHAN_NAME1##CHAN_NAME1;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM1, CHAN_NUM2> CHAN_NAME1##CHAN_NAME2;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM2, CHAN_NUM1> CHAN_NAME2##CHAN_NAME1;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM2, CHAN_NUM2> CHAN_NAME2##CHAN_NAME2;

#define OPENXYZ_MATH_SWIZZLE_GEN_32(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NAME3, CHAN_NUM1, CHAN_NUM2,          \
                                    CHAN_NUM3)                                 \
  OPENXYZ_MATH_SWIZZLE_GEN_22(TYPE, DIM, CHAN_NAME1, CHAN_NAME2, CHAN_NUM1,    \
                              CHAN_NUM2)                                       \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM1, CHAN_NUM3> CHAN_NAME1##CHAN_NAME3;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM2, CHAN_NUM3> CHAN_NAME2##CHAN_NAME3;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM3, CHAN_NUM1> CHAN_NAME3##CHAN_NAME1;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM3, CHAN_NUM2> CHAN_NAME3##CHAN_NAME2;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM3, CHAN_NUM3> CHAN_NAME3##CHAN_NAME3;

#define OPENXYZ_MATH_SWIZZLE_GEN_33(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NAME3, CHAN_NUM1, CHAN_NUM2,          \
                                    CHAN_NUM3)                                 \
  /*NOLINTNEXTLINE(bugprone-macro-parentheses)*/                               \
  ScalarSwizzle<TYPE, CHAN_NUM3> CHAN_NAME3;                                   \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1>                 \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2>                 \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3>                 \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1>                 \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2>                 \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3>                 \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1>                 \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2>                 \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3>                 \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1>                 \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2>                 \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3>                 \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1>                 \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2>                 \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3>                 \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1>                 \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2>                 \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3>                 \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1>                 \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2>                 \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3>                 \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1>                 \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2>                 \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3>                 \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1>                 \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2>                 \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3>                 \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME3;

#define OPENXYZ_MATH_SWIZZLE_GEN_42(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NAME3, CHAN_NAME4, CHAN_NUM1,         \
                                    CHAN_NUM2, CHAN_NUM3, CHAN_NUM4)           \
  OPENXYZ_MATH_SWIZZLE_GEN_32(TYPE, DIM, CHAN_NAME1, CHAN_NAME2, CHAN_NAME3,   \
                              CHAN_NUM1, CHAN_NUM2, CHAN_NUM3)                 \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM1, CHAN_NUM4> CHAN_NAME1##CHAN_NAME4;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM2, CHAN_NUM4> CHAN_NAME2##CHAN_NAME4;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM3, CHAN_NUM4> CHAN_NAME3##CHAN_NAME4;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM4, CHAN_NUM1> CHAN_NAME4##CHAN_NAME1;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM4, CHAN_NUM2> CHAN_NAME4##CHAN_NAME2;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM4, CHAN_NUM3> CHAN_NAME4##CHAN_NAME3;    \
  VectorSwizzle<TYPE, DIM, 2, CHAN_NUM4, CHAN_NUM4> CHAN_NAME4##CHAN_NAME4;

#define OPENXYZ_MATH_SWIZZLE_GEN_43(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NAME3, CHAN_NAME4, CHAN_NUM1,         \
                                    CHAN_NUM2, CHAN_NUM3, CHAN_NUM4)           \
  OPENXYZ_MATH_SWIZZLE_GEN_33(TYPE, DIM, CHAN_NAME1, CHAN_NAME2, CHAN_NAME3,   \
                              CHAN_NUM1, CHAN_NUM2, CHAN_NUM3)                 \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4>                 \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4>                 \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4>                 \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1>                 \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2>                 \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3>                 \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4>                 \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4>                 \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4>                 \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4>                 \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1>                 \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2>                 \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3>                 \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4>                 \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4>                 \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4>                 \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4>                 \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1>                 \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2>                 \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3>                 \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4>                 \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1>                 \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2>                 \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3>                 \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4>                 \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1>                 \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2>                 \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3>                 \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4>                 \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1>                 \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2>                 \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3>                 \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4>                 \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME4;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1>                 \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME1;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2>                 \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME2;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3>                 \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME3;                                      \
  VectorSwizzle<TYPE, DIM, 3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4>                 \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME4;

#define OPENXYZ_MATH_SWIZZLE_GEN_44(TYPE, DIM, CHAN_NAME1, CHAN_NAME2,         \
                                    CHAN_NAME3, CHAN_NAME4, CHAN_NUM1,         \
                                    CHAN_NUM2, CHAN_NUM3, CHAN_NUM4)           \
  /*NOLINTNEXTLINE(bugprone-macro-parentheses)*/                               \
  ScalarSwizzle<TYPE, CHAN_NUM4> CHAN_NAME4;                                   \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME1##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME2##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME3##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME1##CHAN_NAME4##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME1##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME2##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME3##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME2##CHAN_NAME4##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME1##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME2##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME3##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME3##CHAN_NAME4##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME1##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME2##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME3##CHAN_NAME4##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME1##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME1##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME1##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME1##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME2##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME2##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME2##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME2##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME3##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME3##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME3##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME3##CHAN_NAME4;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM1>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME4##CHAN_NAME1;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM2>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME4##CHAN_NAME2;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM3>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME4##CHAN_NAME3;                          \
  VectorSwizzle<TYPE, DIM, 4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4, CHAN_NUM4>      \
      CHAN_NAME4##CHAN_NAME4##CHAN_NAME4##CHAN_NAME4;

#define OPENXYZ_SWIZZLE_COMMON_VECTOR_GEN(DIM)                                 \
  constexpr Vector##DIM() : vec{} {}                                           \
  template <typename U,                                                        \
            typename = ::std::enable_if_t<::std::is_arithmetic_v<U>>*>         \
  constexpr Vector##DIM(U&& val) : vec{::std::forward<U>(val)} {}              \
  template <                                                                   \
      typename... Ts,                                                          \
      typename = ::std::enable_if_t<                                           \
          (sizeof...(Ts) <= (DIM)) &&                                          \
          (::std::is_arithmetic_v<::std::remove_cv_t<                          \
               ::std::remove_pointer_t<::std::remove_reference_t<Ts>>>> &&     \
           ...)>*>                                                             \
  constexpr explicit Vector##DIM(Ts&&... values)                               \
      : vec{static_cast<value_type>(values)...} {}                             \
  template <typename U,                                                        \
            typename =                                                         \
                ::std::enable_if_t<::std::is_convertible_v<U, value_type>>*>   \
  constexpr explicit Vector##DIM(Vector##DIM<U> const& other)                  \
      : vec{static_cast<typename Vector##DIM<U>::vector_type>(other)} {}       \
  template <typename U, size_t M, typename... Ts>                              \
  constexpr explicit Vector##DIM(Vector<U, M> const& other, Ts&&... values)    \
      : vec{other, static_cast<value_type>(values)...} {}                      \
  constexpr Vector##DIM(vector_type&& other)                                   \
      : vec{::std::forward<vector_type>(other)} {}                             \
  constexpr Vector##DIM(T const data[DIM]) : vec{data, data + (DIM)} {}        \
                                                                               \
  Vector##DIM(Vector##DIM const&) = default;                                   \
  Vector##DIM(Vector##DIM&&) noexcept = default;                               \
                                                                               \
  template <typename U, size_t N, size_t SwizzleSize, size_t... Indexes>       \
  constexpr Vector##DIM(VectorSwizzle<U, N, SwizzleSize, Indexes...> swizzle)  \
      : vec{swizzle.val, swizzle.val + N} {}                                   \
                                                                               \
  ~Vector##DIM() noexcept = default;                                           \
                                                                               \
  constexpr auto operator[](typename vector_type::size_type idx)               \
      ->typename vector_type::reference {                                      \
    return const_cast<typename vector_type::reference>(                        \
        static_cast<Vector##DIM const*>(this)->operator[](idx));               \
  }                                                                            \
                                                                               \
  constexpr auto operator[](typename vector_type::size_type idx) const->       \
      typename vector_type::const_reference {                                  \
    return vec[idx];                                                           \
  }                                                                            \
                                                                               \
  NAUTIRIS_ALWAYS_INLINE constexpr operator vector_type() const {              \
    return vec;                                                                \
  }                                                                            \
                                                                               \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator()(                            \
      ::std::array<uint8_t, 4> const target = {0, 1, 2, 3})                    \
      const->Vector##DIM {                                                     \
    return vector_type{vec[target[0]], vec[target[1]], vec[target[2]],         \
                       vec[target[3]]};                                        \
  }                                                                            \
                                                                               \
  auto operator=(Vector##DIM const& other)->Vector##DIM& {                     \
    if (this != &other) {                                                      \
      static_cast<void>(this->operator=(::std::move(other)));                  \
    }                                                                          \
    return *this;                                                              \
  }                                                                            \
  auto operator=(Vector##DIM&& other) noexcept -> Vector##DIM& {               \
    memcpy(&vec, &other.vec, sizeof(vec));                                     \
    return *this;                                                              \
  }                                                                            \
                                                                               \
  friend auto operator<<(::std::ostream& os, Vector##DIM const& vec)           \
      ->::std::ostream& {                                                      \
    os << static_cast<vector_type>(vec);                                       \
    return os;                                                                 \
  }

template <typename T, Channel X = Channel::X, Channel Y = Channel::Y>
struct Vector2 {
  using vector_type = typename VectorTraits<Vector<T, 2>>::type;
  using value_type = typename VectorTraits<vector_type>::value_type;
  using reference = typename VectorTraits<vector_type>::reference;
  using const_reference = typename VectorTraits<vector_type>::const_reference;
  using pointer = typename VectorTraits<vector_type>::pointer;
  using const_pointer = typename VectorTraits<vector_type>::const_pointer;
  static constexpr size_t dimension{VectorTraits<vector_type>::dimension};

  OPENXYZ_SWIZZLE_COMMON_VECTOR_GEN(2)

  union {
    vector_type vec;
    // Vertex coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_22(value_type, dimension, x, y, 0, 1)
    // RGBA color
    OPENXYZ_MATH_SWIZZLE_GEN_22(value_type, dimension, r, g, 0, 1)
    // Texture coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_22(value_type, dimension, s, t, 0, 1)
  };
};

template <typename T, Channel X = Channel::X, Channel Y = Channel::Y,
          Channel Z = Channel::Z>
struct Vector3 {
  using vector_type = typename VectorTraits<Vector<T, 3>>::type;
  using value_type = typename VectorTraits<vector_type>::value_type;
  using reference = typename VectorTraits<vector_type>::reference;
  using const_reference = typename VectorTraits<vector_type>::const_reference;
  using pointer = typename VectorTraits<vector_type>::pointer;
  using const_pointer = typename VectorTraits<vector_type>::const_pointer;
  static constexpr size_t dimension{VectorTraits<vector_type>::dimension};

  OPENXYZ_SWIZZLE_COMMON_VECTOR_GEN(3)

  union {
    vector_type vec;
    // Vertex coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_32(value_type, dimension, x, y, z, 0, 1, 2)
    OPENXYZ_MATH_SWIZZLE_GEN_33(value_type, dimension, x, y, z, 0, 1, 2)
    // RGBA color
    OPENXYZ_MATH_SWIZZLE_GEN_32(value_type, dimension, r, g, b, 0, 1, 2)
    OPENXYZ_MATH_SWIZZLE_GEN_33(value_type, dimension, r, g, b, 0, 1, 2)
    // Texture coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_32(value_type, dimension, s, t, p, 0, 1, 2)
    OPENXYZ_MATH_SWIZZLE_GEN_33(value_type, dimension, s, t, p, 0, 1, 2)
  };
};

template <typename T, Channel X = Channel::X, Channel Y = Channel::Y,
          Channel Z = Channel::Z, Channel W = Channel::W>
struct Vector4 {
  using vector_type = typename VectorTraits<Vector<T, 4>>::type;
  using value_type = typename VectorTraits<vector_type>::value_type;
  using reference = typename VectorTraits<vector_type>::reference;
  using const_reference = typename VectorTraits<vector_type>::const_reference;
  using pointer = typename VectorTraits<vector_type>::pointer;
  using const_pointer = typename VectorTraits<vector_type>::const_pointer;
  static constexpr size_t dimension{VectorTraits<vector_type>::dimension};

  OPENXYZ_SWIZZLE_COMMON_VECTOR_GEN(4)

  union {
    vector_type vec;
    // Vertex coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_42(value_type, dimension, x, y, z, w, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_43(value_type, dimension, x, y, z, w, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_44(value_type, dimension, x, y, z, w, 0, 1, 2, 3)
    // RGBA color
    OPENXYZ_MATH_SWIZZLE_GEN_42(value_type, dimension, r, g, b, a, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_43(value_type, dimension, r, g, b, a, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_44(value_type, dimension, r, g, b, a, 0, 1, 2, 3)
    // Texture coordinates
    OPENXYZ_MATH_SWIZZLE_GEN_42(value_type, dimension, s, t, p, q, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_43(value_type, dimension, s, t, p, q, 0, 1, 2, 3)
    OPENXYZ_MATH_SWIZZLE_GEN_44(value_type, dimension, s, t, p, q, 0, 1, 2, 3)
  };
};

#define ARITHMETIC_BINARY_OPERATOR_DECLARE(DIM, FUNCTION)                      \
  FUNCTION(DIM, +, ::std::plus{}, true, true)                                  \
  FUNCTION(DIM, -, ::std::minus{}, true, false)                                \
  FUNCTION(DIM, *, ::std::multiplies{}, true, true)                            \
  FUNCTION(DIM, /, ::std::divides{}, true, false)                              \
  FUNCTION(DIM, %, ::std::modulus{}, !::std::is_floating_point_v<T>, false)    \
  FUNCTION(DIM, &, ::std::bit_and{}, !::std::is_floating_point_v<T>, true)     \
  FUNCTION(DIM, |, ::std::bit_or{}, !::std::is_floating_point_v<T>, true)      \
  FUNCTION(DIM, ^,                                                             \
           ::std::bit_xor{                                                     \
           },                                                                  \
           !::std::is_floating_point_v<T>, true)

#define ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER(DIM, OP, FUNC, INTEGER,         \
                                               COMMUTATIVE)                    \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<::std::is_arithmetic_v<U> && (INTEGER)>*>   \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(Vector##DIM<T> const& lhs, \
                                                    U const rhs)               \
      ->typename Vector##DIM<T>::vector_type {                                 \
    return map(static_cast<typename Vector##DIM<T>::vector_type>(lhs), rhs,    \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename = ::std::enable_if_t<::std::is_arithmetic_v<T> &&         \
                                          (INTEGER) && (COMMUTATIVE)>*>        \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(T const lhs,               \
                                                    Vector##DIM<U> const& rhs) \
      ->typename Vector##DIM<U>::vector_type {                                 \
    return map(static_cast<typename Vector##DIM<U>::vector_type>(rhs), lhs,    \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<::std::is_arithmetic_v<U> && (INTEGER)>*>   \
  NAUTIRIS_ALWAYS_INLINE auto operator OP##=(Vector##DIM<T>& lhs, U const rhs) \
      ->Vector##DIM<T>& {                                                      \
    return (lhs = lhs OP rhs);                                                 \
  }
ARITHMETIC_BINARY_OPERATOR_DECLARE(2, ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER)
ARITHMETIC_BINARY_OPERATOR_DECLARE(3, ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER)
ARITHMETIC_BINARY_OPERATOR_DECLARE(4, ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER)
#undef ARITHMETIC_BINARY_OPERATOR_WITH_NUMBER

#define ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR(DIM, OP, FUNC, INTEGER,         \
                                               COMMUTATIVE)                    \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<VectorTraits<U>::value && (INTEGER)>*>      \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(Vector##DIM<T> const& lhs, \
                                                    U const& rhs)              \
      ->decltype(auto) {                                                       \
    constexpr size_t kLhsDim =                                                 \
        VectorTraits<typename Vector##DIM<T>::vector_type>::dimension;         \
    constexpr size_t kRhsDim = VectorTraits<U>::dimension;                     \
    return map(static_cast<Vector<T, ::std::max(kLhsDim, kRhsDim)>>(lhs),      \
               static_cast<Vector<typename VectorTraits<U>::value_type,        \
                                  ::std::max(kLhsDim, kRhsDim)>>(rhs),         \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename = ::std::enable_if_t<VectorTraits<U>::value &&            \
                                          (INTEGER) && (COMMUTATIVE)>*>        \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator OP(T const& lhs,              \
                                                    Vector##DIM<U> const& rhs) \
      ->typename Vector##DIM<U>::vector_type {                                 \
    constexpr size_t kLhsDim = VectorTraits<T>::dimension;                     \
    constexpr size_t kRhsDim =                                                 \
        VectorTraits<typename Vector##DIM<U>::vector_type>::dimension;         \
    return map(static_cast<Vector<typename VectorTraits<T>::value_type,        \
                                  ::std::max(kLhsDim, kRhsDim)>>(lhs),         \
               static_cast<Vector<U, ::std::max(kLhsDim, kRhsDim)>>(rhs),      \
               FUNC);                                                          \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<VectorTraits<U>::value && (INTEGER)>*>      \
  auto operator OP##=(Vector##DIM<T>& lhs, U const& rhs)->Vector##DIM<T>& {    \
    return (                                                                   \
        lhs = (lhs OP static_cast<                                             \
               Vector<typename VectorTraits<U>::value_type, (DIM)>>(rhs)));    \
  }
ARITHMETIC_BINARY_OPERATOR_DECLARE(2, ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR)
ARITHMETIC_BINARY_OPERATOR_DECLARE(3, ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR)
ARITHMETIC_BINARY_OPERATOR_DECLARE(4, ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR)
#undef ARITHMETIC_BINARY_OPERATOR_WITH_VECTOR

#undef ARITHMETIC_BINARY_OPERATOR_DECLARE

#define LOGICAL_COMPARASION_OPERATOR(DIM, OP, NOT)                             \
  template <typename T, typename U,                                            \
            typename = ::std::enable_if_t<VectorTraits<U>::value>*>            \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator==(Vector##DIM<T> const& lhs,  \
                                                   U const& rhs)               \
      ->bool {                                                                 \
    return static_cast<Vector<typename VectorTraits<U>::value_type,            \
                              VectorTraits<U>::dimension>>(rhs) ==             \
           static_cast<typename Vector##DIM<T>::vector_type>(lhs);             \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<!::std::is_same_v<T, Vector##DIM<U>> &&     \
                                   VectorTraits<T>::value>*>                   \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator==(T const& lhs,               \
                                                   Vector##DIM<U> const& rhs)  \
      ->bool {                                                                 \
    return static_cast<Vector<typename VectorTraits<T>::value_type,            \
                              VectorTraits<T>::dimension>>(lhs) ==             \
           static_cast<typename Vector##DIM<U>::vector_type>(rhs);             \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename = ::std::enable_if_t<VectorTraits<U>::value>*>            \
  NAUTIRIS_ALWAYS_INLINE constexpr auto operator NOT(                          \
      Vector##DIM<T> const& lhs, U const& rhs)                                 \
      ->bool {                                                                 \
    return !(lhs OP rhs);                                                      \
  }                                                                            \
  template <typename T, typename U,                                            \
            typename =                                                         \
                ::std::enable_if_t<!::std::is_same_v<T, Vector##DIM<U>> &&     \
                                   VectorTraits<T>::value>*>                   \
  constexpr auto operator NOT(T const& lhs, Vector##DIM<U> const& rhs)->bool { \
    return !(lhs OP rhs);                                                      \
  }

LOGICAL_COMPARASION_OPERATOR(2, ==, !=)
LOGICAL_COMPARASION_OPERATOR(3, ==, !=)
LOGICAL_COMPARASION_OPERATOR(4, ==, !=)
#undef LOGICAL_COMPARASION_OPERATOR

// NOLINTEND(misc-unconventional-assign-operator,google-explicit-constructor)

#undef OPENXYZ_SWIZZLE_COMMON_VECTOR_GEN
#undef OPENXYZ_MATH_SWIZZLE_GEN_22
#undef OPENXYZ_MATH_SWIZZLE_GEN_32
#undef OPENXYZ_MATH_SWIZZLE_GEN_33
#undef OPENXYZ_MATH_SWIZZLE_GEN_42
#undef OPENXYZ_MATH_SWIZZLE_GEN_43
#undef OPENXYZ_MATH_SWIZZLE_GEN_44

}  // namespace math

#endif  // OPENXYZ_MATH_SWIZZLE_HXX
