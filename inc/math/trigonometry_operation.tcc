#pragma once

#if !defined(OPENXYZ_MATH_TRIGONOMETRY_OPERATION_TCC)
#define OPENXYZ_MATH_TRIGONOMETRY_OPERATION_TCC

#include "math/operation.hxx"

#include <cmath>
#include <type_traits>

#define OPENXZY_MATH_PI 3.141592653589793

namespace math {

template <typename T> constexpr auto radians(T degrees) -> decltype(auto) {
  using RetT = ::std::conditional_t<
      ::std::is_integral_v<T> || ::std::is_same_v<double, T>, double, T>;
  return static_cast<RetT>(OPENXZY_MATH_PI) / static_cast<RetT>(180) *
         static_cast<RetT>(degrees);
}

template <typename T, size_t N>
constexpr auto radians(Vector<T, N> const& degreeses)
    -> Vector<decltype(radians(::std::declval<T>())), N> {
  return map(degreeses, &radians<T>);
}

template <typename T> constexpr auto degrees(T radians) -> decltype(auto) {
  using RetT = ::std::conditional_t<
      ::std::is_integral_v<T> || ::std::is_same_v<double, T>, double, T>;
  return static_cast<RetT>(180) / static_cast<RetT>(OPENXZY_MATH_PI) *
         static_cast<RetT>(radians);
}

template <typename T, size_t N>
constexpr auto degrees(Vector<T, N> const& radianses)
    -> Vector<decltype(degrees(::std::declval<T>())), N> {
  return map(radianses, &degrees<T>);
}

template <typename T> constexpr auto sin(T angle) -> decltype(auto) {
  return ::std::sin(angle);
}

template <typename T, size_t N>
constexpr auto sin(Vector<T, N> const& angles)
    -> Vector<decltype(sin(::std::declval<T>())), N> {
  return map(angles, &sin<T>);
}

template <typename T> constexpr auto cos(T angle) -> decltype(auto) {
  return ::std::cos(angle);
}

template <typename T, size_t N>
constexpr auto cos(Vector<T, N> const& angles)
    -> Vector<decltype(cos(::std::declval<T>())), N> {
  return map(angles, &cos<T>);
}

template <typename T> constexpr auto tan(T angle) -> decltype(auto) {
  return ::std::tan(angle);
}

template <typename T, size_t N>
constexpr auto tan(Vector<T, N> const& angles)
    -> Vector<decltype(tan(::std::declval<T>())), N> {
  return map(angles, &tan<T>);
}

template <typename T> constexpr auto asin(T num) -> decltype(auto) {
  return ::std::asin(num);
}

template <typename T, size_t N>
constexpr auto asin(Vector<T, N> const& nums)
    -> Vector<decltype(asin(::std::declval<T>())), N> {
  return map(nums, &asin<T>);
}

template <typename T> constexpr auto acos(T num) -> decltype(auto) {
  return ::std::acos(num);
}

template <typename T, size_t N>
constexpr auto acos(Vector<T, N> const& nums)
    -> Vector<decltype(acos(::std::declval<T>())), N> {
  return map(nums, &acos<T>);
}

template <typename T> constexpr auto atan(T num1, T num2) -> decltype(auto) {
  return ::std::atan2(num1, num2);
}

template <typename T, size_t N>
constexpr auto atan(Vector<T, N> const& ys, Vector<T, N> const& xes)
    -> Vector<decltype(atan(::std::declval<T>(), ::std::declval<T>())), N> {
  return map(ys, xes, [](T num1, T num2) constexpr -> decltype(auto) {
    return ::std::atan2(num1, num2);
  });
}

template <typename T> constexpr auto atan(T y_over_x) -> decltype(auto) {
  return ::std::atan(y_over_x);
}

template <typename T, size_t N>
constexpr auto atan(Vector<T, N> const& y_over_xes)
    -> Vector<decltype(atan(::std::declval<T>())), N> {
  return map(y_over_xes, [](T y_over_x) constexpr -> decltype(auto) {
    return ::std::atan(y_over_x);
  });
}

template <typename T> constexpr auto sinh(T num) -> decltype(auto) {
  return ::std::sinh(num);
}

template <typename T, size_t N>
constexpr auto sinh(Vector<T, N> const& nums)
    -> Vector<decltype(sinh(::std::declval<T>())), N> {
  return map(nums, &sinh<T>);
}

template <typename T> constexpr auto cosh(T num) -> decltype(auto) {
  return ::std::cosh(num);
}

template <typename T, size_t N>
constexpr auto cosh(Vector<T, N> const& nums)
    -> Vector<decltype(cosh(::std::declval<T>())), N> {
  return map(nums, &cosh<T>);
}

template <typename T> constexpr auto tanh(T num) -> decltype(auto) {
  return ::std::tanh(num);
}

template <typename T, size_t N>
constexpr auto tanh(Vector<T, N> const& nums)
    -> Vector<decltype(tanh(::std::declval<T>())), N> {
  return map(nums, &tanh<T>);
}

template <typename T> constexpr auto asinh(T num) -> decltype(auto) {
  return ::std::asinh(num);
}

template <typename T, size_t N>
constexpr auto asinh(Vector<T, N> const& nums)
    -> Vector<decltype(asinh(::std::declval<T>())), N> {
  return map(nums, &asinh<T>);
}

template <typename T> constexpr auto acosh(T num) -> decltype(auto) {
  return ::std::acosh(num);
}

template <typename T, size_t N>
constexpr auto acosh(Vector<T, N> const& nums)
    -> Vector<decltype(acosh(::std::declval<T>())), N> {
  return map(nums, &acosh<T>);
}

template <typename T> constexpr auto atanh(T num) -> decltype(auto) {
  return ::std::atanh(num);
}

template <typename T, size_t N>
constexpr auto atanh(Vector<T, N> const& nums)
    -> Vector<decltype(atanh(::std::declval<T>())), N> {
  return map(nums, &atanh<T>);
}

}  // namespace math

#undef OPENXYZ_MATH_PI

#endif  // OPENXYZ_MATH_TRIGONOMETRY_OPERATION_TCC
