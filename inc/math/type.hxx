#pragma once

#ifndef OPENXYZ_MATH_TYPE_HXX
#define OPENXYZ_MATH_TYPE_HXX

#include "math/matrix.hxx"
#include "math/swizzle.hxx"
#include "math/vector.hxx"

namespace math {

template <typename T, size_t Channel> struct GraphicsVectorTraits {
  using value_type = void;
  using vector_type = unsigned char;
  static constexpr size_t dimension{0};
};

template <typename T> struct GraphicsVectorTraits<T, 1> {
  using value_type = typename ::std::remove_cv_t<::std::conditional_t<
      ::std::is_reference_v<T>, ::std::remove_reference_t<T>,
      ::std::conditional_t<::std::is_pointer_v<T>, ::std::remove_pointer_t<T>,
                           T>>>;
  using vector_type = value_type;
  static constexpr size_t dimension{1};
};
template <typename T> struct GraphicsVectorTraits<T, 2> {
  using value_type = typename GraphicsVectorTraits<T, 1>::value_type;
  using vector_type = Vector2<value_type>;
  static constexpr size_t dimension{2};
};
template <typename T> struct GraphicsVectorTraits<T, 3> {
  using value_type = typename GraphicsVectorTraits<T, 1>::value_type;
  using vector_type = Vector3<value_type>;
  static constexpr size_t dimension{3};
};
template <typename T> struct GraphicsVectorTraits<T, 4> {
  using value_type = typename GraphicsVectorTraits<T, 1>::value_type;
  using vector_type = Vector4<value_type>;
  static constexpr size_t dimension{4};
};

// NOLINTBEGIN(readability-identifier-naming)

using bvec2 = typename GraphicsVectorTraits<bool, 2>::vector_type;
using bvec3 = typename GraphicsVectorTraits<bool, 3>::vector_type;
using bvec4 = typename GraphicsVectorTraits<bool, 4>::vector_type;

using ivec2 = typename GraphicsVectorTraits<int32_t, 2>::vector_type;
using ivec3 = typename GraphicsVectorTraits<int32_t, 3>::vector_type;
using ivec4 = typename GraphicsVectorTraits<int32_t, 4>::vector_type;

using uvec2 = typename GraphicsVectorTraits<uint32_t, 2>::vector_type;
using uvec3 = typename GraphicsVectorTraits<uint32_t, 3>::vector_type;
using uvec4 = typename GraphicsVectorTraits<uint32_t, 4>::vector_type;

using vec2 = typename GraphicsVectorTraits<float, 2>::vector_type;
using vec3 = typename GraphicsVectorTraits<float, 3>::vector_type;
using vec4 = typename GraphicsVectorTraits<float, 4>::vector_type;

using dvec2 = typename GraphicsVectorTraits<double, 2>::vector_type;
using dvec3 = typename GraphicsVectorTraits<double, 3>::vector_type;
using dvec4 = typename GraphicsVectorTraits<double, 4>::vector_type;

using bmat2 = Square<bool, 2>;
using bmat2x3 = Matrix<bool, 2, 3>;
using bmat2x4 = Matrix<bool, 2, 4>;

using bmat3 = Square<bool, 3>;
using bmat3x2 = Matrix<bool, 3, 2>;
using bmat3x4 = Matrix<bool, 3, 4>;

using bmat4 = Square<bool, 4>;
using bmat4x2 = Matrix<bool, 4, 2>;
using bmat4x3 = Matrix<bool, 4, 3>;

using imat2 = Square<int32_t, 2>;
using imat2x3 = Matrix<int32_t, 2, 3>;
using imat2x4 = Matrix<int32_t, 2, 4>;

using imat3 = Square<int32_t, 3>;
using imat3x2 = Matrix<int32_t, 3, 2>;
using imat3x4 = Matrix<int32_t, 3, 4>;

using imat4 = Square<int32_t, 4>;
using imat4x2 = Matrix<int32_t, 4, 2>;
using imat4x3 = Matrix<int32_t, 4, 3>;

using umat2 = Square<uint32_t, 2>;
using umat2x3 = Matrix<uint32_t, 2, 3>;
using umat2x4 = Matrix<uint32_t, 2, 4>;

using umat3 = Square<uint32_t, 3>;
using umat3x2 = Matrix<uint32_t, 3, 2>;
using umat3x4 = Matrix<uint32_t, 3, 4>;

using umat4 = Square<uint32_t, 4>;
using umat4x2 = Matrix<uint32_t, 4, 2>;
using umat4x3 = Matrix<uint32_t, 4, 3>;

using mat2 = Square<float, 2>;
using mat2x3 = Matrix<float, 2, 3>;
using mat2x4 = Matrix<float, 2, 4>;

using mat3 = Square<float, 3>;
using mat3x2 = Matrix<float, 3, 2>;
using mat3x4 = Matrix<float, 3, 4>;

using mat4 = Square<float, 4>;
using mat4x2 = Matrix<float, 4, 2>;
using mat4x3 = Matrix<float, 4, 3>;

using dmat2 = Square<double, 2>;
using dmat2x3 = Matrix<double, 2, 3>;
using dmat2x4 = Matrix<double, 2, 4>;

using dmat3 = Square<double, 3>;
using dmat3x2 = Matrix<double, 3, 2>;
using dmat3x4 = Matrix<double, 3, 4>;

using dmat4 = Square<double, 4>;
using dmat4x2 = Matrix<double, 4, 2>;
using dmat4x3 = Matrix<double, 4, 3>;

// NOLINTEND(readability-identifier-naming)

}  // namespace math

#endif  // OPENXYZ_MATH_TYPE_HXX
