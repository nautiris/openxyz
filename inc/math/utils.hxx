#pragma once

#if !defined(OPENXYZ_MATH_UTILS_HXX)
#define OPENXYZ_MATH_UTILS_HXX

#include <functional>
#include <limits.h>
#include <limits>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <tuple>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <typename From, typename To, typename = void>
struct IsNarrowingConvertibleImpl : ::std::true_type {};

template <typename From, typename To>
struct IsNarrowingConvertibleImpl<
    From, To, ::std::void_t<decltype(To{::std::declval<From>()})>>
    : std::false_type {};

template <typename Int, typename, Int Begin, bool Increasing>
struct IntegerRangeImpl;

template <typename Int, Int... N, Int Begin>
struct IntegerRangeImpl<Int, ::std::integer_sequence<Int, N...>, Begin, true> {
  using type = ::std::integer_sequence<Int, (N + Begin)...>;
};

template <typename Int, Int... N, Int Begin>
struct IntegerRangeImpl<Int, ::std::integer_sequence<Int, N...>, Begin, false> {
  using type = ::std::integer_sequence<Int, (Begin - N)...>;
};

template <typename Int, Int Begin>
struct IntegerRangeImpl<Int, std::integer_sequence<Int>, Begin, false> {
  using type = ::std::integer_sequence<Int>;
};

template <size_t... SelectIndexes, typename... Ts>
constexpr auto
tuple_slice_aux(std::tuple<Ts...>&& values,
                std::integer_sequence<size_t, SelectIndexes...> /*unused*/
                ) noexcept -> decltype(auto) {
  return ::std::make_tuple(
      ::std::forward<
          ::std::tuple_element_t<SelectIndexes, ::std::tuple<Ts...>>>(
          ::std::get<SelectIndexes>(values))...);
}

constexpr auto log2(size_t val) noexcept -> size_t {
  size_t result = (val > 0xFFFF'FFFFu ? 1 : 0) << 5;
  val >>= result;
  size_t shift = (val > 0xFFFFu ? 1 : 0) << 4;
  val >>= shift;
  result |= shift;
  shift = (val > 0xFFu ? 1 : 0) << 3;
  val >>= shift;
  result |= shift;
  shift = (val > 0xFu ? 1 : 0) << 2;
  val >>= shift;
  result |= shift;
  shift = (val > 0x3u ? 1 : 0) << 1;
  val >>= shift;
  result |= shift;
  return result | (val >> 1);
}

}  // namespace _details

template <typename From, typename To>
struct IsNarrowingConvertible
    : _details::IsNarrowingConvertibleImpl<From, To> {};

template <typename From, typename To>
inline constexpr bool kIsNarrowingConvertible =
    IsNarrowingConvertible<From, To>::value;

template <typename Int, Int Begin, Int End>
using IntegerRange = typename _details::IntegerRangeImpl<
    Int,
    ::std::make_integer_sequence<Int,
                                 (Begin < End) ? (End - Begin) : (Begin - End)>,
    Begin, (Begin < End)>::type;

template <size_t Begin, size_t End>
using MakeIndexRange = IntegerRange<size_t, Begin, End>;

template <size_t Start, size_t End, typename... Ts>
constexpr auto tuple_slice(::std::tuple<Ts...>&& values) noexcept
    -> decltype(auto) {
  return _details::tuple_slice_aux(::std::forward<::std::tuple<Ts...>>(values),
                                   MakeIndexRange<Start, End>{});
}

template <size_t N> struct MakeFixedWidthNum;

template <> struct MakeFixedWidthNum<8> {
  using unsigned_type = uint8_t;
  using signed_type = int8_t;
  static constexpr auto width = ::std::numeric_limits<unsigned_type>::digits;
  static constexpr auto umax = ::std::numeric_limits<unsigned_type>::max();
  static constexpr auto smax = ::std::numeric_limits<signed_type>::max();
  static constexpr auto smin = ::std::numeric_limits<signed_type>::min();
};

template <> struct MakeFixedWidthNum<16> {
  using unsigned_type = uint16_t;
  using signed_type = int16_t;
  static constexpr auto width = ::std::numeric_limits<unsigned_type>::digits;
  static constexpr auto umax = ::std::numeric_limits<unsigned_type>::max();
  static constexpr auto smax = ::std::numeric_limits<signed_type>::max();
  static constexpr auto smin = ::std::numeric_limits<signed_type>::min();
};

template <> struct MakeFixedWidthNum<32> {
  using unsigned_type = uint32_t;
  using signed_type = int32_t;
  using floating_type = float;
  static constexpr auto width = ::std::numeric_limits<unsigned_type>::digits;
  static constexpr auto umax = ::std::numeric_limits<unsigned_type>::max();
  static constexpr auto smax = ::std::numeric_limits<signed_type>::max();
  static constexpr auto smin = ::std::numeric_limits<signed_type>::min();
  static constexpr auto eps = 1e-5f;
};

template <> struct MakeFixedWidthNum<64> {
  using unsigned_type = uint64_t;
  using signed_type = int64_t;
  using floating_type = double;
  static constexpr auto width = ::std::numeric_limits<unsigned_type>::digits;
  static constexpr auto umax = ::std::numeric_limits<unsigned_type>::max();
  static constexpr auto smax = ::std::numeric_limits<signed_type>::max();
  static constexpr auto smin = ::std::numeric_limits<signed_type>::min();
  static constexpr auto eps = 1e-10;
};

template <> struct MakeFixedWidthNum<128> {
  using unsigned_type = __uint128_t;
  using signed_type = __int128_t;
  using floating_type = long double;
  static constexpr auto width = ::std::numeric_limits<unsigned_type>::digits;
  static constexpr auto umax = ::std::numeric_limits<unsigned_type>::max();
  static constexpr auto smax = ::std::numeric_limits<signed_type>::max();
  static constexpr auto smin = ::std::numeric_limits<signed_type>::min();
  static constexpr auto eps = 1e-13L;
};

namespace _details {
template <typename T, typename U> struct CompareTypeAux {
  static constexpr bool t2u = !kIsNarrowingConvertible<T, U>;
  static constexpr bool u2t = !kIsNarrowingConvertible<U, T>;
  using target_type =
      ::std::conditional_t<t2u, U, ::std::conditional_t<u2t, T, void>>;
};
}

template <typename T> struct EqualTo {
  using type = T;
  using result_type = bool;
  // clang-format off
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
  static constexpr bool non_finity_check[6][6] = {
      [FP_NAN] = { [FP_NAN] = true, [FP_INFINITE] = false, false},
      [FP_INFINITE] = { [FP_NAN] = false, [FP_INFINITE] = true, false},
  };
#pragma GCC diagnostic pop
  // clang-format on
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    if constexpr (::std::is_void_v<T>) {
      using TargetT = typename _details::CompareTypeAux<U, V>::target_type;
      if constexpr (::std::is_void_v<TargetT>) {
        return false;
      } else {
        return EqualTo<TargetT>{}(static_cast<TargetT>(lhs),
                                  static_cast<TargetT>(rhs));
      }
    } else if constexpr (::std::is_integral_v<T>) {
      return ::std::equal_to<T>{}(lhs, rhs);
    } else {
      if (!::std::isfinite(lhs) || !::std::isfinite(rhs)) {
        return non_finity_check[::std::fpclassify(lhs)]
                               [::std::fpclassify(rhs)] &&
               ::std::signbit(lhs) == ::std::signbit(rhs);
      }
      type diff = lhs - rhs;
      constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
      return (diff < MakeFixedWidthNum<kWidth>::eps) &&
             (diff > -MakeFixedWidthNum<kWidth>::eps);
    }
  }
};

EqualTo() -> EqualTo<void>;

template <typename T> struct NotEqualTo {
  using type = T;
  using result_type = bool;
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    return !EqualTo<type>{}(lhs, rhs);
  }
};

NotEqualTo() -> NotEqualTo<void>;

template <typename T> struct Less {
  using type = T;
  using result_type = bool;
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    if constexpr (std::is_void_v<T>) {
      using TargetT = typename _details::CompareTypeAux<U, V>::target_type;
      if constexpr (::std::is_void_v<TargetT>) {
        return false;
      } else {
        return Less<TargetT>{}(static_cast<TargetT>(lhs),
                               static_cast<TargetT>(rhs));
      }
    } else if constexpr (::std::is_integral_v<T>) {
      return ::std::less<T>{}(lhs, rhs);
    } else {
      if (!::std::isfinite(lhs) || !::std::isfinite(rhs)) {
        return ::std::less<T>{}(lhs, rhs);
      }
      type diff = lhs - rhs;
      constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
      return (diff < -MakeFixedWidthNum<kWidth>::eps) &&
             (-diff > MakeFixedWidthNum<kWidth>::eps);
    }
  }
};

Less() -> Less<void>;

template <typename T> struct Greater {
  using type = T;
  using result_type = bool;
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    if constexpr (std::is_void_v<T>) {
      using TargetT = typename _details::CompareTypeAux<U, V>::target_type;
      if constexpr (::std::is_void_v<TargetT>) {
        return false;
      } else {
        return Greater<TargetT>{}(static_cast<TargetT>(lhs),
                                  static_cast<TargetT>(rhs));
      }
    } else if constexpr (::std::is_integral_v<T>) {
      return ::std::greater<T>{}(lhs, rhs);
    } else {
      if (!::std::isfinite(lhs) || !::std::isfinite(rhs)) {
        return ::std::greater<T>{}(lhs, rhs);
      }
      type diff = lhs - rhs;
      constexpr size_t kWidth = sizeof(T) * CHAR_BIT;
      return (diff > MakeFixedWidthNum<kWidth>::eps) &&
             (-diff < -MakeFixedWidthNum<kWidth>::eps);
    }
  }
};

Greater() -> Greater<void>;

template <typename T> struct LessEqual {
  using type = T;
  using result_type = bool;
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    return !Greater<type>{}(lhs, rhs);
  }
};

LessEqual() -> LessEqual<void>;

template <typename T> struct GreaterEqual {
  using type = T;
  using result_type = bool;
  template <typename U, typename V>
  constexpr auto operator()(U const& lhs, V const& rhs) const -> result_type {
    return !Less<type>{}(lhs, rhs);
  }
};

GreaterEqual() -> GreaterEqual<void>;

constexpr auto align_of_exp2(size_t sz) noexcept -> size_t {
  size_t const kOpnd = (static_cast<size_t>(1) << _details::log2(sz)) - 1;
  return (sz + kOpnd) & ~kOpnd;
}

constexpr auto align_of_exp2_or_not(size_t sz, size_t channel) noexcept
    -> size_t {
  size_t const kAligned = align_of_exp2(sz * channel);
  return sz * channel < kAligned ? align_of_exp2(sz) : kAligned;
}

}  // namespace math

#endif  // OPENXYZ_MATH_UTILS_HXX
