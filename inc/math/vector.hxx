#pragma once

#if !defined(OPENXYZ_MATH_VECTOR_HXX)
#define OPENXYZ_MATH_VECTOR_HXX

#include "math/utils.hxx"

#include <algorithm>
#include <functional>
#include <ostream>
#include <stddef.h>
#include <string>
#include <type_traits>
#include <utility>

namespace math {

namespace _details {

template <size_t Idx, typename T> struct VectorLeaf;

template <typename VectorIndexes, typename T> struct VectorImpl;

template <size_t I, size_t M, typename U> struct VectorVisitorAux;

}  // namespace _details

template <
    typename T, size_t N,
    typename = ::std::enable_if_t<
        ::std::is_arithmetic_v<T>&& ::std::is_fundamental_v<T> && (N > 0)>*>
struct Vector;

template <
    typename VectorT,
    typename T = ::std::remove_cv_t<::std::conditional_t<
        ::std::is_reference_v<VectorT>, ::std::remove_reference_t<VectorT>,
        ::std::conditional_t<::std::is_pointer_v<VectorT>,
                             ::std::remove_pointer_t<VectorT>, VectorT>>>,
    typename = void>
struct VectorTraits : public ::std::false_type {};

template <typename VectorT, typename T>
struct VectorTraits<
    VectorT, T,
    ::std::void_t<typename T::value_type, typename T::reference,
                  typename T::const_reference, typename T::pointer,
                  typename T::const_pointer>> : public ::std::true_type {
  using type = T;
  using value_type = typename type::value_type;
  using reference = typename type::reference;
  using const_reference = typename type::const_reference;
  using pointer = typename type::pointer;
  using const_pointer = typename type::const_pointer;

  static constexpr size_t dimension{type::dimension};
};

}  // namespace math

namespace std {

template <size_t I, typename T, size_t N>
constexpr auto get(::math::Vector<T, N> const& vec) noexcept ->
    typename ::math::Vector<T, N>::const_reference;

template <size_t I, typename T, size_t N>
constexpr auto get(::math::Vector<T, N>& vec) noexcept ->
    typename ::math::Vector<T, N>::reference {
  return const_cast<typename ::math::Vector<T, N>::reference>(
      get<I>(static_cast<::math::Vector<T, N> const&>(vec)));
}

template <typename T, size_t N>
auto to_string(::math::Vector<T, N> const& vec) -> ::std::string;

}  // namespace std

namespace math {

namespace _details {

template <size_t I, typename T> struct VectorLeaf {
  using value_type = T;
  using reference = value_type&;
  using const_reference = value_type const&;
  using pointer = value_type*;
  using const_pointer = value_type const*;

  value_type val{};

  constexpr VectorLeaf() noexcept {
    static_assert(
        !::std::is_reference_v<value_type>,
        "Attempt to default construct a reference element in a vector");
  }
  template <typename U, typename = ::std::enable_if_t<
                            ::std::is_convertible_v<U, value_type>>*>
  constexpr explicit VectorLeaf(U&& other) noexcept
      : val{static_cast<value_type>(other)} {
    static_assert(
        !::std::is_reference_v<value_type> ||
            (::std::is_lvalue_reference_v<value_type> &&
             (::std::is_lvalue_reference_v<U> ||
              ::std::is_same_v<::std::remove_reference_t<U>,
                               ::std::reference_wrapper<
                                   ::std::remove_reference_t<value_type>>>)) ||
            (::std::is_rvalue_reference_v<value_type> &&
             !::std::is_lvalue_reference_v<U>),
        "Attempted to construct a reference element in a vector with an "
        "rvalue");
  }
  template <size_t RIdx, typename RT>
  constexpr explicit VectorLeaf(VectorLeaf<RIdx, RT> const& other) noexcept
      : VectorLeaf(other.get()) {}

  constexpr VectorLeaf(VectorLeaf const&) = default;
  constexpr VectorLeaf(VectorLeaf&&) noexcept = default;
  ~VectorLeaf() noexcept = default;

  constexpr auto operator=(VectorLeaf const&) noexcept -> VectorLeaf& = default;
  constexpr auto operator=(VectorLeaf&&) noexcept -> VectorLeaf& = default;
  template <typename RT> constexpr auto operator=(RT&& other) -> VectorLeaf& {
    val = std::forward<RT>(other);
    return *this;
  }

  auto swap(VectorLeaf& other) noexcept -> VectorLeaf& {
    ::std::swap(get(), other.get());
    return *this;
  }

  constexpr auto get() noexcept -> reference { return val; }
  [[nodiscard]] constexpr auto get() const noexcept -> const_reference {
    return val;
  }
};

template <size_t... Indexes, typename T>
struct VectorImpl<::std::integer_sequence<size_t, Indexes...>, T>
    : public VectorLeaf<Indexes, T>... {
  using value_type = T;
  using reference = value_type&;
  using const_reference = value_type const&;
  using pointer = value_type*;
  using const_pointer = value_type const*;
  using size_type = size_t;
  static constexpr size_type dimension{sizeof...(Indexes)};

  constexpr VectorImpl() : VectorImpl(value_type{}) {}

  template <typename U,
            typename = ::std::enable_if_t<!::std::is_same_v<VectorImpl, U>>*>
  constexpr explicit VectorImpl(U&& value) noexcept
      : VectorLeaf<Indexes, value_type>{::std::forward<U>(value)}... {}

  template <typename OtherVector, size_t... OtherVectorIndexes,
            size_t... FirstIndexes, size_t... LastIndexes, typename... Ts,
            typename = ::std::enable_if_t<
                VectorTraits<OtherVector>::value &&
                VectorTraits<OtherVector>::dimension + sizeof...(FirstIndexes) +
                        sizeof...(LastIndexes) ==
                    VectorImpl::dimension &&
                sizeof...(FirstIndexes) == sizeof...(Ts) &&
                (::std::is_convertible_v<Ts, value_type> && ...)>*>
  constexpr VectorImpl(
      ::std::integer_sequence<size_t, OtherVectorIndexes...> /*unused*/,
      ::std::integer_sequence<size_t, FirstIndexes...> /*unused*/,
      ::std::integer_sequence<size_t, LastIndexes...> /*unused*/,
      OtherVector&& vec, Ts&&... values) noexcept
      : VectorLeaf<OtherVectorIndexes, value_type>(
            ::std::get<OtherVectorIndexes>(vec))...,
        VectorLeaf<VectorTraits<OtherVector>::dimension + FirstIndexes,
                   value_type>(::std::forward<Ts>(values))...,
        VectorLeaf<VectorTraits<OtherVector>::dimension +
                       sizeof...(FirstIndexes) + LastIndexes,
                   value_type>{}... {}

  template <
      size_t Idx, size_t... FirstIndexes, size_t... LastIndexes, typename U,
      typename = ::std::enable_if_t<
          (Idx < dimension) &&
          sizeof...(FirstIndexes) + sizeof...(LastIndexes) + 1 == dimension>*>
  constexpr explicit VectorImpl(
      ::std::integer_sequence<size_t, FirstIndexes...> /*unused*/
      ,
      ::std::integral_constant<size_t, Idx> /*unused*/,
      ::std::integer_sequence<size_t, LastIndexes...> /*unused*/
      ,
      U&& value) noexcept
      : VectorLeaf<FirstIndexes, value_type>{}...,
        VectorLeaf<sizeof...(FirstIndexes), value_type>{
            ::std::forward<U>(value)},
        VectorLeaf<sizeof...(FirstIndexes) + 1 + LastIndexes, value_type>{}... {
  }

  constexpr VectorImpl(VectorImpl const&) = default;
  constexpr VectorImpl(VectorImpl&&) noexcept = default;
  ~VectorImpl() noexcept = default;

  constexpr auto operator=(VectorImpl const&) -> VectorImpl& = default;
  constexpr auto operator=(VectorImpl&&) noexcept -> VectorImpl& = default;
  template <typename OtherVector>
  auto operator=(OtherVector&& vec) noexcept -> VectorImpl& {
    if (this == &vec) {
      return *this;
    }
    swallow(VectorLeaf<Indexes, value_type>::operator=(
        ::std::forward<typename VectorTraits<OtherVector>::value_type>(
            ::std::get<Indexes>(vec)))...);
    return *this;
  }

  template <size_t I> constexpr auto get() noexcept -> reference {
    return const_cast<reference>(
        const_cast<VectorImpl const*>(this)->template get<I>());
  }

  template <size_t I>
  [[nodiscard]] constexpr auto get() const noexcept -> const_reference {
    return static_cast<VectorLeaf<I, value_type> const&>(*this).get();
  }

  static constexpr auto as_tuple(VectorImpl const& vec) noexcept
      -> decltype(auto) {
    return ::std::make_tuple(vec.template get<Indexes>()...);
  }

 private:
  template <typename... Ts>
  constexpr void swallow(Ts&&... /*unused*/) noexcept {}
};

}  // namespace _details

template <typename T, size_t N>
struct alignas(align_of_exp2_or_not(sizeof(T), N)) Vector<T, N> {
 private:
  template <size_t I, size_t M, typename U>
  friend struct _details::VectorVisitorAux;

  using Impl = _details::VectorImpl<::std::make_index_sequence<N>, T>;
  Impl impl_;

 public:
  using value_type = typename Impl::value_type;
  using reference = typename Impl::reference;
  using const_reference = typename Impl::const_reference;
  using pointer = typename Impl::pointer;
  using const_pointer = typename Impl::const_pointer;
  using size_type = typename Impl::size_type;
  static constexpr size_type dimension{Impl::dimension};

  constexpr Vector() : Vector(value_type{}) {}

  template <typename U, typename = ::std::enable_if_t<
                            ::std::is_arithmetic_v<U> &&
                            !kIsNarrowingConvertible<U, value_type>>*>
  constexpr explicit Vector(U&& val) noexcept : impl_{::std::forward<U>(val)} {}

  template <typename... Ts,
            typename = ::std::enable_if_t<
                sizeof...(Ts) <= N &&
                (!kIsNarrowingConvertible<Ts, value_type> && ...)>*>
  constexpr explicit Vector(Ts&&... values) noexcept
      : impl_{::std::make_index_sequence<0>{},
              ::std::index_sequence_for<Ts...>{},
              ::std::make_index_sequence<dimension - sizeof...(Ts)>{},
              _details::VectorImpl<::std::make_index_sequence<0>, value_type>{},
              ::std::forward<Ts>(values)...} {}

  template <typename U, size_t M, typename... Ts,
            typename = ::std::enable_if_t<
                sizeof...(Ts) + M <= N &&
                (!kIsNarrowingConvertible<Ts, value_type> && ... &&
                 (!kIsNarrowingConvertible<U, value_type>))>*>
  constexpr explicit Vector(Vector<U, M> const& other, Ts&&... values) noexcept
      : impl_{::std::make_index_sequence<M>{},
              ::std::index_sequence_for<Ts...>{},
              ::std::make_index_sequence<dimension - M - sizeof...(Ts)>{},
              other, ::std::forward<Ts>(values)...} {}

  template <size_t Idx, typename U,
            typename = ::std::enable_if_t<(Idx < dimension)>*>
  constexpr Vector(::std::integral_constant<size_t, Idx> constant,
                   U&& value) noexcept
      : impl_{::std::make_index_sequence<Idx>{}, constant,
              ::std::make_index_sequence<dimension - 1 - Idx>{},
              ::std::forward<U>(value)} {}

  template <typename IIterator,
            typename = ::std::void_t<
                typename ::std::iterator_traits<IIterator>::iterator_category>>
  constexpr Vector(IIterator begin, IIterator end) {
    size_t idx = 0;
    for (auto it = begin; it != end && idx < N; ++it) {
      (*this)[idx++] = *it;
    }
  }

  constexpr Vector(Vector const&) = default;
  constexpr Vector(Vector&&) noexcept = default;
  ~Vector() noexcept = default;

  constexpr auto operator=(Vector const&) -> Vector& = default;
  constexpr auto operator=(Vector&&) noexcept -> Vector& = default;

  template <typename U, size_t M>
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr operator Vector<U, M>() const noexcept {
    return convert(::std::make_index_sequence<::std::min(M, dimension)>{},
                   ::std::integral_constant<size_t, M>{}, U{}, flatten());
  }

  constexpr auto operator[](size_type idx) -> reference {
    return const_cast<reference>(
        const_cast<Vector const*>(this)->operator[](idx));
  }

  constexpr auto operator[](size_type idx) const -> const_reference;

  template <size_t D> static constexpr auto make_unit() noexcept -> Vector;
  static constexpr auto make_unit(size_t dimension) -> Vector;

  // NOLINTNEXTLINE(modernize-use-nodiscard)
  constexpr auto flatten() const noexcept -> decltype(auto) {
    return Impl::as_tuple(impl_);
  }

 private:
  template <size_t... Indexes, size_t Total, typename CastT, typename... Ts>
  static constexpr auto
  convert(::std::integer_sequence<size_t, Indexes...> /*unused*/,
          ::std::integral_constant<size_t, Total> /*unused*/, CastT /*unused*/,
          ::std::tuple<Ts...> const& tuple) noexcept -> Vector<CastT, Total> {
    return Vector<CastT, Total>{
        static_cast<CastT>(::std::get<Indexes>(tuple))...};
  }
};

template <typename T, typename... Ts>
constexpr auto make_vector(Ts&&... values) noexcept -> decltype(auto) {
  return Vector<T, sizeof...(Ts)>{
      static_cast<T>(::std::forward<Ts>(values))...};
}

template <typename T, size_t N, typename UnaryOperation>
constexpr auto map(Vector<T, N> const& vec, UnaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>())), N>;

template <typename T, size_t N, typename U, typename BinaryOperation,
          typename = ::std::enable_if_t<
              !::std::is_class_v<::std::remove_cv_t<::std::conditional_t<
                  ::std::is_reference_v<U>, ::std::remove_reference_t<U>,
                  ::std::conditional_t<::std::is_pointer_v<U>,
                                       ::std::remove_pointer_t<U>, U>>>>>*>
constexpr auto map(Vector<T, N> const& lhs, U&& rhs,
                   BinaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())), N>;

template <typename T, size_t N, typename U, typename BinaryOperation>
constexpr auto map(Vector<T, N> const& lhs, Vector<U, N> const& rhs,
                   BinaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())), N>;

template <typename T, size_t N, typename U, typename BinaryOperation>
constexpr auto reduce(Vector<T, N> const& lhs, U init,
                      BinaryOperation&& op) noexcept
    -> decltype(op(::std::declval<T>(), ::std::declval<U>()));

template <typename T, size_t N, typename BinaryOperation>
constexpr auto reduce(Vector<T, N> const& lhs, BinaryOperation&& op) noexcept
    -> decltype(op(::std::declval<T>(), ::std::declval<T>())) {
  using RetT = decltype(op(::std::declval<T>(), ::std::declval<T>()));
  return reduce(lhs, RetT{}, ::std::forward<BinaryOperation>(op));
}

template <typename T, size_t N>
constexpr auto operator+(Vector<T, N> const& vec) noexcept -> Vector<T, N> {
  return vec;
}

template <typename T, size_t N>
constexpr auto operator+(Vector<T, N> const& vec1,
                         Vector<T, N> const& vec2) noexcept -> Vector<T, N> {
  return map(vec1, vec2, ::std::plus<T>{});
}

template <typename T, size_t N, typename U>
constexpr auto operator+(Vector<T, N> const& vec, U&& val) noexcept
    -> Vector<decltype(::std::declval<T>() + ::std::declval<U>()), N> {
  using RetT = decltype(::std::declval<T>() + ::std::declval<U>());
  return map(vec, ::std::forward<U>(val), ::std::plus<RetT>{});
}

template <typename T, size_t N, typename U>
constexpr auto operator+(U&& val, Vector<T, N> const& vec) noexcept
    -> Vector<decltype(::std::declval<T>() + ::std::declval<U>()), N> {
  return vec + ::std::forward<U>(val);
}

template <typename T, size_t N>
constexpr auto operator-(Vector<T, N> const& vec) noexcept -> Vector<T, N> {
  return ::math::map(vec, [](T val) constexpr -> T { return -val; });
}

template <typename T, size_t N>
constexpr auto operator-(Vector<T, N> const& vec1,
                         Vector<T, N> const& vec2) noexcept -> Vector<T, N> {
  return map(vec1, vec2, ::std::minus<T>{});
}

template <typename T, size_t N, typename U>
constexpr auto operator-(Vector<T, N> const& vec, U&& val) noexcept
    -> Vector<decltype(::std::declval<T>() - ::std::declval<U>()), N> {
  using RetT = decltype(::std::declval<T>() - ::std::declval<U>());
  return map(vec, ::std::forward<U>(val), ::std::minus<RetT>{});
}

template <typename T, size_t N, typename U>
constexpr auto operator-(U&& val, Vector<T, N> const& vec) noexcept
    -> Vector<decltype(::std::declval<U>() - ::std::declval<T>()), N> {
  using RetT = decltype(::std::declval<U>() - ::std::declval<T>());
  return map(vec, [minus = val](T elem) constexpr -> RetT {
    return ::std::minus<RetT>{}(minus, elem);
  });
}

template <typename T, size_t N, typename U>
constexpr auto operator*(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return map(lhs, rhs, ::std::multiplies<RetT>{});
}

template <typename T, typename U, size_t N>
constexpr auto operator*(Vector<T, N> const& vec, U&& val) noexcept
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return map(vec, ::std::forward<U>(val), ::std::multiplies<RetT>{});
}

template <typename T, typename U, size_t N>
constexpr auto operator*(U&& val, Vector<T, N> const& vec) noexcept
    -> Vector<decltype(::std::declval<T>() * ::std::declval<U>()), N> {
  return vec * ::std::forward<U>(val);
}

template <typename T, typename U, size_t N>
constexpr auto operator/(Vector<T, N> const& vec, U&& val)
    -> Vector<decltype(::std::declval<T>() / ::std::declval<U>()), N> {
  using RetT = decltype(::std::declval<T>() * ::std::declval<U>());
  return map(vec, ::std::forward<U>(val), ::std::divides<RetT>{});
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator==(Vector<T, Dim1> const& lhs,
                          Vector<U, Dim2> const& rhs) -> bool;

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator!=(Vector<T, Dim1> const& lhs,
                          Vector<U, Dim2> const& rhs) -> bool {
  return !(lhs == rhs);
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator<(Vector<T, Dim1> const& lhs, Vector<U, Dim2> const& rhs)
    -> bool;

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator>(Vector<T, Dim1> const& lhs, Vector<U, Dim2> const& rhs)
    -> bool;

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator<=(Vector<T, Dim1> const& lhs,
                          Vector<U, Dim2> const& rhs) -> bool {
  return !(lhs > rhs);
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator>=(Vector<T, Dim1> const& lhs,
                          Vector<U, Dim2> const& rhs) -> bool {
  return !(lhs < rhs);
}

template <typename T, size_t N>
auto operator<<(::std::ostream& os, Vector<T, N> const& vec) -> ::std::ostream&;

}  // namespace math

#endif  // OPENXYZ_MATH_VECTOR_HXX
