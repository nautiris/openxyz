#pragma once

#if !defined(OPENXYZ_MATH_VECTOR_TCC)
#define OPENXYZ_MATH_VECTOR_TCC

#include "math/utils.hxx"
#include "math/vector.hxx"

#include <type_traits>

namespace math {

namespace _details {

template <size_t I, size_t N, typename T> struct VectorVisitorAux {
  using VectorT = Vector<T, N>;

  template <size_t... Indexes>
  static constexpr auto run(VectorT const& vec,
                            ::std::integer_sequence<typename VectorT::size_type,
                                                    Indexes...> /*unused*/) ->
      typename VectorT::const_reference {
    return static_cast<_details::VectorLeaf<I, T> const&>(vec.impl_).get();
  }
};

template <size_t N, typename T> struct VectorImplAux {
  using VectorT = Vector<T, N>;

  template <size_t... Indexes>
  static constexpr auto
  visit(VectorT const& vec, typename VectorT::size_type idx,
        ::std::integer_sequence<typename VectorT::size_type,
                                Indexes...> /*unused*/) ->
      typename VectorT::const_reference {
    typename VectorT::const_pointer ptr{nullptr};
    ((idx == Indexes && (ptr = &::std::get<Indexes>(vec))) || ...);
    return *ptr;
  }

  template <size_t... Indexes>
  static constexpr auto
  unit(typename VectorT::size_type dimension, typename VectorT::value_type val,
       ::std::integer_sequence<typename VectorT::size_type,
                               Indexes...> /*unused*/) -> VectorT {
    VectorT vec;
    static_cast<void>(
        ((dimension == Indexes &&
          (vec = VectorT{::std::integral_constant<size_t, Indexes>{}, val},
           true)) ||
         ...));
    return vec;
  }

  template <size_t... Indexes, typename UnaryOperation>
  static constexpr auto map(VectorT const& vec, UnaryOperation&& op,
                            ::std::integer_sequence<typename VectorT::size_type,
                                                    Indexes...> /*unused*/)
      -> ::math::Vector<decltype(op(::std::declval<T>())), N> {
    return ::math::Vector<decltype(op(::std::declval<T>())), N>{
        op(::std::get<Indexes>(vec))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation,
            typename = ::std::enable_if_t<
                !::std::is_class_v<::std::remove_cv_t<::std::conditional_t<
                    ::std::is_reference_v<U>, ::std::remove_reference_t<U>,
                    ::std::conditional_t<::std::is_pointer_v<U>,
                                         ::std::remove_pointer_t<U>, U>>>>>*>
  static constexpr auto map(VectorT const& lhs, U&& rhs, BinaryOperation&& op,
                            ::std::integer_sequence<typename VectorT::size_type,
                                                    Indexes...> /*unused*/)
      -> ::math::Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())),
                        N> {
    using RetT = decltype(op(::std::declval<T>(), ::std::declval<U>()));
    return ::math::Vector<RetT, N>{
        op(static_cast<RetT>(::std::get<Indexes>(lhs)),
           static_cast<RetT>(::std::forward<U>(rhs)))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation>
  static constexpr auto map(VectorT const& lhs, Vector<U, N> const& rhs,
                            BinaryOperation&& op,
                            ::std::integer_sequence<typename VectorT::size_type,
                                                    Indexes...> /*unused*/
                            )
      -> Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())), N> {
    using RetT = decltype(op(::std::declval<T>(), ::std::declval<U>()));
    return Vector<RetT, N>{op(static_cast<RetT>(::std::get<Indexes>(lhs)),
                              static_cast<RetT>(::std::get<Indexes>(rhs)))...};
  }

  template <size_t... Indexes, typename U, typename BinaryOperation>
  static constexpr auto
  reduce(VectorT const& lhs, U init, BinaryOperation&& op,
         ::std::integer_sequence<typename VectorT::size_type,
                                 Indexes...> /*unused*/)
      -> decltype(op(::std::declval<T>(), ::std::declval<U>())) {
    using RetT = decltype(op(::std::declval<T>(), ::std::declval<U>()));
    RetT ans = static_cast<RetT>(init);
    return ((ans = op(::std::get<Indexes>(lhs), ans)), ...);
  }
};

struct VectorCompareAux {
  template <size_t... Indexes, typename T, size_t N, typename U, size_t M>
  static constexpr auto
  equal(::math::Vector<T, N> const& lhs, ::math::Vector<U, M> const& rhs,
        ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    if constexpr (N != M) {
      return false;
    } else {
      return ((EqualTo{}(::std::get<Indexes>(lhs), ::std::get<Indexes>(rhs))) &&
              ...);
    }
  }
  template <size_t... Indexes, typename T, size_t N, typename U, size_t M>
  static constexpr auto
  less(::math::Vector<T, N> const& lhs, ::math::Vector<U, M> const& rhs,
       ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    if constexpr (N != M) {
      return N < M;
    } else {
      return ((Less{}(::std::get<Indexes>(lhs), ::std::get<Indexes>(rhs))) &&
              ...);
    }
  }
  template <size_t... Indexes, typename T, size_t N, typename U, size_t M>
  static constexpr auto
  greater(::math::Vector<T, N> const& lhs, ::math::Vector<U, M> const& rhs,
          ::std::integer_sequence<size_t, Indexes...> /*unused*/) -> bool {
    if constexpr (N != M) {
      return N > M;
    } else {
      return ((Greater{}(::std::get<Indexes>(lhs), ::std::get<Indexes>(rhs))) &&
              ...);
    }
  }
};

template <size_t... Indexes, typename T, size_t N>
auto to_string_aux(::math::Vector<T, N> const& vec,
                   ::std::integer_sequence<size_t, Indexes...> /*unused*/)
    -> ::std::string {
  size_t count = 0;
  ::std::string ret;
  ((ret =
        ret + (Indexes == 0 ? "" : ",") +
        ((count + (Indexes == 0 ? 0 : 1) >= 72)
             ? (count = ::std::to_string(::std::get<Indexes>(vec)).size(), "\n")
             : (count += ::std::to_string(::std::get<Indexes>(vec)).size() +
                         (Indexes == 0 ? 0 : 1),
                "")) +
        ::std::to_string(::std::get<Indexes>(vec))),
   ...);
  return ret;
}

}  // namespace _details

template <typename T, size_t N>
constexpr auto Vector<T, N>::operator[](size_type idx) const
    -> const_reference {
  return _details::VectorImplAux<N, T>::visit(*this, idx,
                                              ::std::make_index_sequence<N>{});
}

template <typename T, size_t N>
template <size_t D>
constexpr auto Vector<T, N>::make_unit() noexcept -> Vector {
  return Vector{::std::integral_constant<size_t, D>{}, 1};
}

template <typename T, size_t N>
constexpr auto Vector<T, N>::make_unit(size_t dim) -> Vector {
  return _details::VectorImplAux<N, T>::unit(dim, 1,
                                             ::std::make_index_sequence<N>{});
}

template <typename T, size_t N, typename UnaryOperation>
constexpr auto map(Vector<T, N> const& vec, UnaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>())), N> {
  return _details::VectorImplAux<N, T>::map(
      vec, ::std::forward<UnaryOperation>(op), ::std::make_index_sequence<N>{});
}

template <typename T, size_t N, typename U, typename BinaryOperation, typename>
constexpr auto map(Vector<T, N> const& lhs, U&& rhs,
                   BinaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())), N> {
  return _details::VectorImplAux<N, T>::map(lhs, ::std::forward<U>(rhs),
                                            ::std::forward<BinaryOperation>(op),
                                            ::std::make_index_sequence<N>{});
}

template <typename T, size_t N, typename U, typename BinaryOperation>
constexpr auto map(Vector<T, N> const& lhs, Vector<U, N> const& rhs,
                   BinaryOperation&& op) noexcept
    -> Vector<decltype(op(::std::declval<T>(), ::std::declval<U>())), N> {
  return _details::VectorImplAux<N, T>::map(lhs, rhs,
                                            ::std::forward<BinaryOperation>(op),
                                            ::std::make_index_sequence<N>{});
}

template <typename T, size_t N, typename U, typename BinaryOperation>
constexpr auto reduce(Vector<T, N> const& lhs, U init,
                      BinaryOperation&& op) noexcept
    -> decltype(op(::std::declval<T>(), ::std::declval<U>())) {
  return _details::VectorImplAux<N, T>::reduce(
      lhs, init, ::std::forward<BinaryOperation>(op),
      ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator==(Vector<T, Dim1> const& lhs,
                          Vector<U, Dim2> const& rhs) -> bool {
  return _details::VectorCompareAux::equal(lhs, rhs,
                                           ::std::make_index_sequence<Dim1>{});
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator<(Vector<T, Dim1> const& lhs, Vector<U, Dim2> const& rhs)
    -> bool {
  return _details::VectorCompareAux::less(lhs, rhs,
                                          ::std::make_index_sequence<Dim1>{});
}

template <typename T, typename U, size_t Dim1, size_t Dim2>
constexpr auto operator>(Vector<T, Dim1> const& lhs, Vector<U, Dim2> const& rhs)
    -> bool {
  return _details::VectorCompareAux::greater(
      lhs, rhs, ::std::make_index_sequence<Dim1>{});
}

template <typename T, size_t N>
auto operator<<(::std::ostream& os, Vector<T, N> const& vec)
    -> ::std::ostream& {
  os << ::std::to_string(vec);
  return os;
}

}  // namespace math

namespace std {

template <size_t I, typename T, size_t N>
constexpr auto get(::math::Vector<T, N> const& vec) noexcept ->
    typename ::math::Vector<T, N>::const_reference {
  return ::math::_details::VectorVisitorAux<I, N, T>::run(
      vec, ::std::make_index_sequence<I + 1>{});
}

template <typename T, size_t N>
auto to_string(::math::Vector<T, N> const& vec) -> ::std::string {
  return "[" +
         ::math::_details::to_string_aux(vec, ::std::make_index_sequence<N>{}) +
         "]";
}

}  // namespace std

#endif  // OPENXYZ_MATH_VECTOR_TCC
