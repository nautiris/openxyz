#pragma once

#if !defined(OPENXYZ_MATH_VECTOR_OPERATION_TCC)
#define OPENXYZ_MATH_VECTOR_OPERATION_TCC

#include "math/operation.hxx"

#include <utility>

namespace math {

namespace _details {

template <size_t... Indexes, typename T, typename U, size_t N,
          typename BinaryPredicate>
constexpr auto vector_operation_compare_aux(
    Vector<T, N> const& lhs, Vector<U, N> const& rhs, BinaryPredicate&& op,
    ::std::integer_sequence<size_t, Indexes...> /*unused*/
    ) -> Vector<bool, N> {
  constexpr bool kT2u = _details::CompareTypeAux<T, U>::t2u;
  constexpr bool kU2T = _details::CompareTypeAux<T, U>::u2t;
  if constexpr (!kT2u && !kU2T) {
    return Vector<bool, N>{
        (static_cast<void>(::std::get<Indexes>(lhs)), false)...};
  } else {
    return ::math::make_vector<bool>(
        (op(::std::get<Indexes>(lhs), ::std::get<Indexes>(rhs)))...);
  }
}

template <size_t... Indexes, size_t N>
constexpr auto
vector_operation_any_aux(Vector<bool, N> const& vec,
                         ::std::integer_sequence<size_t, Indexes...> /*unused*/
                         ) -> bool {
  return (::std::get<Indexes>(vec) || ... || false);
}

template <size_t... Indexes, size_t N>
constexpr auto
vector_operation_all_aux(Vector<bool, N> const& vec,
                         ::std::integer_sequence<size_t, Indexes...> /*unused*/
                         ) -> bool {
  return (::std::get<Indexes>(vec) && ... && true);
}

template <size_t... Indexes, size_t N>
constexpr auto
vector_operation_not_aux(Vector<bool, N> const& vec,
                         ::std::integer_sequence<size_t, Indexes...> /*unused*/
                         ) -> Vector<bool, N> {
  return ::math::make_vector<bool>((not::std::get<Indexes>(vec))...);
}

}  // namespace _details

template <typename T, typename U, size_t N>
constexpr auto less_than(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, Less{}, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t N>
constexpr auto less_than_equal(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, LessEqual{}, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t N>
constexpr auto greater_than(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, Greater{}, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t N>
constexpr auto greater_than_equal(Vector<T, N> const& lhs,
                                  Vector<U, N> const& rhs) -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, GreaterEqual{}, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t N>
constexpr auto equal(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, EqualTo{}, ::std::make_index_sequence<N>{});
}

template <typename T, typename U, size_t N>
constexpr auto not_equal(Vector<T, N> const& lhs, Vector<U, N> const& rhs)
    -> Vector<bool, N> {
  return _details::vector_operation_compare_aux(
      lhs, rhs, NotEqualTo{}, ::std::make_index_sequence<N>{});
}

template <size_t N> constexpr auto any(Vector<bool, N> const& vec) -> bool {
  return _details::vector_operation_any_aux(vec,
                                            ::std::make_index_sequence<N>{});
}

template <size_t N> constexpr auto all(Vector<bool, N> const& vec) -> bool {
  return _details::vector_operation_all_aux(vec,
                                            ::std::make_index_sequence<N>{});
}

template <size_t N>
constexpr auto operator!(Vector<bool, N> const& vec) -> Vector<bool, N> {
  return _details::vector_operation_not_aux(vec,
                                            ::std::make_index_sequence<N>{});
}

}  // namespace math

#endif  // OPENXYZ_MATH_VECTOR_OPERATION_TCC
