#pragma once

#if !defined(NAUTIRIS_UTILS_HPP)
#define NAUTIRIS_UTILS_HPP

/**
 * C++ Version
 * see more:
 *https://en.cppreference.com/w/cpp/preprocessor/replace#Predefined_macros
 */
// clang-format off
#if __cplusplus == 199711L
#define NAUTIRIS_CXX_VERSION 03
#elif __cplusplus == 201'103L
#define NAUTIRIS_CXX_VERSION 11
#elif __cplusplus == 201402L
#define NAUTIRIS_CXX_VERSION 14
#elif __cplusplus == 201703L
#define NAUTIRIS_CXX_VERSION 17
#elif __cplusplus == 202002L
#define NAUTIRIS_CXX_VERSION 20
#elif __cplusplus == 202302L
#define NAUTIRIS_CXX_VERSION 23
#else
#error "Unknown C++ version."
#endif
// clang-format on

/**
 * Attrbutes
 * see more: https://clang.llvm.org/docs/AttributeReference.html
 */

// alloc_align
#if defined(__clang__)
#define NAUTIRIS_ALLOC_ALIGN(ALIGNMENT) [[clang::alloc_align(ALIGNMENT)]]
#elif defined(__GNUC__) || defined(__GNUG__)
#define NAUTIRIS_ALLOC_ALIGN(ALIGNMENT) [[gnu::alloc_align(ALIGNMENT)]]
#elif defined(_MSC_VER)
#define NAUTIRIS_ALLOC_ALIGN(ALIGNMENT)
#endif

// always_inline
#if defined(__clang__)
#define NAUTIRIS_ALWAYS_INLINE [[clang::always_inline]]
#elif defined(__GNUC__) || defined(__GNUG__)
#define NAUTIRIS_ALWAYS_INLINE [[gnu::always_inline]]
#elif defined(_MSC_VER)
#define NAUTIRIS_ALWAYS_INLINE __forceinline
#endif

// likely or unlikely
#if NAUTIRIS_CXX_VERSION >= 20
#define NAUTIRIS_LIKELY [[likely]]
#define NAUTIRIS_UNLIKELY [[unlikely]]
#define NAUTIRIS_LIKELY_IF(CONDITION) if (!!(CONDITION)) [[likely]]
#define NAUTIRIS_UNLIKELY_IF(CONDITION) if (!!(CONDITION)) [[unlikely]]
#elif defined(__clang__)
#define NAUTIRIS_LIKELY [[likely]]
#define NAUTIRIS_UNLIKELY [[unlikely]]
#define NAUTIRIS_LIKELY_IF(CONDITION) if (!!(CONDITION)) [[likely]]
#define NAUTIRIS_UNLIKELY_IF(CONDITION) if (!!(CONDITION)) [[unlikely]]
#elif defined(__GNUC__) || defined(__GNUG__)
#define NAUTIRIS_LIKELY
#define NAUTIRIS_UNLIKELY
#define NAUTIRIS_LIKELY_IF(CONDITION) if (__builtin_expect(!!(CONDITION), 1))
#define NAUTIRIS_UNLIKELY_IF(CONDITION) if (__builtin_expect(!!(CONDITION), 0))
#elif defined(_MSC_VER)
#define NAUTIRIS_LIKELY
#define NAUTIRIS_UNLIKELY
#define NAUTIRIS_LIKELY_IF(CONDITION) if (!!(CONDITION))
#define NAUTIRIS_UNLIKELY_IF(CONDITION) if (!!(CONDITION))
#endif

// sanitizer check
#if defined(__clang__)
#define NAUTIRIS_SANITIZER_NOCHECK(...)                                        \
  __attribute__((no_sanitize(__VA_ARGS__)))
#elif defined(__GNUC__) || defined(__GNUG__)
#define NAUTIRIS_SANITIZER_NOCHECK(...)                                        \
  __attribute__((no_sanitize(__VA_ARGS__)))
#elif defined(_MSC_VER)
#define NAUTIRIS_SANITIZER_NOCHECK(...)
#endif

/**
 * Builtin functions
 */

// unreachable
#if NAUTIRIS_CXX_VERSION >= 23
#include <utility>
#define __NAUTIRIS_UNREACHABLE_IMPL() ::std::unreachable()
#elif defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
#define __NAUTIRIS_UNREACHABLE_IMPL() __builtin_unreachable()
#elif defined(_MSC_VER)
#define __NAUTIRIS_UNREACHABLE_IMPL() __assume(0)
#endif
#define NAUTIRIS_UNREACHABLE()                                                 \
  do {                                                                         \
    assert(false && "Unreachable!!!");                                         \
    __NAUTIRIS_UNREACHABLE_IMPL();                                             \
  } while (0)

// assume
#if !defined(NDEBUG)
#include <assert.h>
#define NAUTIRIS_ASSUME(CONDITION) assert(!!(CONDITION))
#elif NAUTIRIS_CXX_VERSION >= 23
#define NAUTIRIS_ASSUME(CONDITION) [[assume(!!(CONDITION))]]
#elif defined(__clang__)
#define NAUTIRIS_ASSUME(CONDITION) __builtin_assume(!!(CONDITION))
#elif defined(__GNUC__) || defined(__GNUG__)
#define NAUTIRIS_ASSUME(CONDITION)                                             \
  do {                                                                         \
    if (!(CONDITION))                                                          \
      NAUTIRIS_UNREACHABLE();                                                  \
  } while (0)
#elif defined(_MSC_VER)
#define NAUTIRIS_ASSUME(CONDITION) __assume(!!(CONDITION))
#endif

#endif  // NAUTIRIS_UTILS_HPP
