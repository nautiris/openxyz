set(COMPONENT_NAME dummy-pch)
add_library(${PROJECT_NAME}-${COMPONENT_NAME})
openxyz_common_config(${PROJECT_NAME}-${COMPONENT_NAME})

set(FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${COMPONENT_NAME}.cxx")
file(GENERATE OUTPUT ${FILENAME} CONTENT "// dummy pch source code\n")
target_sources(${PROJECT_NAME}-${COMPONENT_NAME}
  PRIVATE
  ${FILENAME}
  )
nautiris_target_pch(${PROJECT_NAME}-${COMPONENT_NAME})

