set(COMPONENT_NAME image)

add_library(${PROJECT_NAME}-${COMPONENT_NAME})

openxyz_common_config(${PROJECT_NAME}-${COMPONENT_NAME})
nautiris_target_reuse_pch(${PROJECT_NAME}-${COMPONENT_NAME} ${PROJECT_NAME}-dummy-pch)
nautiris_enable_clang_tidy(${PROJECT_NAME}-${COMPONENT_NAME})

target_sources(${PROJECT_NAME}-${COMPONENT_NAME}
  PRIVATE
  image.cxx

  INTERFACE
  ${PROJECT_SOURCE_DIR}/inc/image.hpp
  ${PROJECT_SOURCE_DIR}/inc/image/accessor.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/common.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/extents.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/image.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/layout.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/pixel.hxx
  ${PROJECT_SOURCE_DIR}/inc/image/view.hxx
  )
