#include "image/image.hxx"

#include "image/common.hxx"
#include "image/pixel.hxx"
#include "math.hpp"

#include <algorithm>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <tuple>

namespace image {

namespace {

constexpr auto element_size(PixelType type) -> size_t {
  return static_cast<size_t>(_details::channel(type))
         << (0xFFFF'AA50U >> _details::depth_size(_details::depth(type)) * 2 &
             0b11);
}

constexpr size_t kOffsetShift = 56;

NAUTIRIS_ALWAYS_INLINE constexpr auto
set_offset_of_pointer(Image::pointer ptr, size_t const kOffset)
    -> Image::pointer {
  return reinterpret_cast<Image::pointer>(reinterpret_cast<uintptr_t>(ptr) |
                                          (kOffset << kOffsetShift));
}

NAUTIRIS_ALWAYS_INLINE constexpr auto get_offset_of_pointer(Image::pointer ptr)
    -> size_t {
  return reinterpret_cast<uintptr_t>(ptr) >> kOffsetShift;
}

struct ImageDimInfo {
  Image::size_type total;
  Image::size_type* sizes;
  Image::size_type* strides;
};

auto create(Image::size_type ndim, ImageDimInfo const& in, PixelType type,
            ImageDimInfo& out) -> Image::pointer {
  Image::size_type const kSize = ndim * sizeof(Image::size_type);
  out.sizes = reinterpret_cast<Image::size_type*>(malloc(2 * kSize));
  memcpy(out.sizes, in.sizes, kSize);
  out.strides = out.sizes + ndim;
  if (in.strides != nullptr) {
    out.total = in.total;
    memcpy(out.strides, in.sizes, kSize);
  } else {
    out.total = element_size(type);
    while (ndim--) {
      out.strides[ndim] = out.total;
      out.total *= out.sizes[ndim];
    }
  }
  NAUTIRIS_UNLIKELY_IF (out.total == 0) {
    return nullptr;
  }
  size_t const kPixelSize =
      _details::depth_size(_details::depth(type)) * _details::channel(type);
  size_t const kAlignment =
      ::std::max(sizeof(Image::pointer), ::math::align_of_exp2(kPixelSize));
  size_t const kOffset =
      _details::channel(type) == 4 ? ::std::max(2 * kPixelSize, kAlignment) : 0;
  auto* pointer =
      reinterpret_cast<Image::pointer>(aligned_alloc(
          ::std::max(sizeof(Image::pointer), ::math::align_of_exp2(kPixelSize)),
          out.total + kOffset * 2)) +
      kOffset;
  return set_offset_of_pointer(pointer, kOffset);
}

}  // namespace

Image::Image(size_type ndim, size_type const* sizes, PixelType type,
             const_pointer data)
    : type_{type}, data_{nullptr}, dims_{ndim}, total_{0}, sizes_{nullptr},
      strides_{nullptr} {
  if (ndim == 0 || sizes == nullptr || type == kDynamicPixel) {
    return;
  }
  auto const kIn = [&]() -> ImageDimInfo {
    ImageDimInfo val;
    val.total = 0;
    val.sizes = const_cast<size_type*>(sizes);
    val.strides = nullptr;
    return val;
  }();
  ImageDimInfo out;
  this->data_ = create(ndim, kIn, type, out);
  ::std::tie(this->total_, this->sizes_, this->strides_) =
      ::std::make_tuple(out.total, out.sizes, out.strides);
  if (data != nullptr) {
    memcpy(this->data(), data, this->total_);
  }
}

Image::Image(Image const& other) : Image{other.dims(), nullptr, other.type()} {
  if (dims() == 0) {
    return;
  }
  auto const kIn = [&]() -> ImageDimInfo {
    ImageDimInfo val;
    val.total = other.total_;
    val.sizes = other.sizes_;
    val.strides = other.strides_;
    return val;
  }();
  ImageDimInfo out;
  this->data_ = create(other.dims(), kIn, type(), out);
  ::std::tie(this->total_, this->sizes_, this->strides_) =
      ::std::make_tuple(out.total, out.sizes, out.strides);
  if (other.data() != nullptr) {
    memcpy(this->data(), other.data(), this->total_);
  }
}

auto Image::operator=(Image const& other) -> Image& {
  if (this == &other) {
    return *this;
  }
  auto temp = Image{other};
  swap(*this, temp);
  return *this;
}

Image::~Image() noexcept { destroy(); }

auto Image::destroy() noexcept -> void {
  if (dims() > 0) {
    free(this->sizes_);
    this->sizes_ = this->strides_ =
        reinterpret_cast<size_type*>(_details::kDeadPtr);
  }
  if (this->total_ > 0) {
    free(this->data() - get_offset_of_pointer(this->data_));
    this->data_ = reinterpret_cast<pointer>(_details::kDeadPtr);
    this->total_ = 0;
  }
}

auto Image::rect(size_type row, size_type col, size_type width,
                 size_type height) const -> Image {
  assert(dims() >= 2);
  size_type const kStepRow = stride(dims() - 2);
  size_type const kStepCol = stride(dims() - 1);
  const_pointer start = offset(row * kStepRow + col * kStepCol);
  Image ret{width, height, type()};
  auto* data = ret.data();
  for (size_t i = height; i > 0; --i) {
    memcpy(data + (i - 1) * width * kStepCol, start + (i - 1) * kStepRow,
           width * kStepCol);
  }
  return ret;
}

}  // namespace image
