#include "image.hpp"

#include <array>
#include <string.h>
#include <utility>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("image::image::constructor", "[Image]") {
  auto img = ::image::Image();
  REQUIRE(img.type() == ::image::kTypeNone);
  REQUIRE(img.depth() == ::image::kDepthNone);
  REQUIRE(img.channel() == 0);
  REQUIRE(img.dims() == 0);
  REQUIRE(img.empty());
  REQUIRE(img.data() == nullptr);

  auto img2 = ::image::Image(2, 2, ::image::PixelType::k32UC4);
  REQUIRE(img2.type() == ::image::PixelType::k32UC4);
  REQUIRE(img2.depth() == ::image::PixelDepth::k32U);
  REQUIRE(img2.channel() == 4);
  REQUIRE(img2.dims() == 2);
  REQUIRE_FALSE(img2.empty());
  REQUIRE((img2.size(1) == 2 && img2.stride(1) == 16));
  REQUIRE((img2.size(0) == 2 && img2.stride(0) == 32));

  auto img3 = ::image::Image(3, ::std::array<size_t, 3>{2, 3, 4}.data(),
                             ::image::PixelType::k16UC1);
  REQUIRE(img3.type() == ::image::PixelType::k16UC1);
  REQUIRE(img3.depth() == ::image::PixelDepth::k16U);
  REQUIRE(img3.channel() == 1);
  REQUIRE(img3.dims() == 3);
  REQUIRE_FALSE(img3.empty());
  REQUIRE((img3.size(2) == 4 && img3.stride(2) == 2));
  REQUIRE((img3.size(1) == 3 && img3.stride(1) == 8));
  REQUIRE((img3.size(0) == 2 && img3.stride(0) == 24));

  auto img4 =
      ::image::Image(0, static_cast<size_t>(0), ::image::PixelType::k32FC3);
  REQUIRE(img4.type() == ::image::PixelType::k32FC3);
  REQUIRE(img4.depth() == ::image::PixelDepth::k32F);
  REQUIRE(img4.channel() == 3);
  REQUIRE(img4.dims() == 2);
  REQUIRE(img4.empty());
  REQUIRE((img4.size(1) == 0 && img4.stride(1) == 12));
  REQUIRE((img4.size(0) == 0 && img4.stride(0) == 0));
}

TEST_CASE("image::image::swap", "[Image]") {
  auto img = ::image::Image();
  auto img2 = ::image::Image(2, 2, ::image::PixelType::k32UC4);
  ::std::swap(img, img2);
  REQUIRE(img.type() == ::image::PixelType::k32UC4);
  REQUIRE(img.depth() == ::image::PixelDepth::k32U);
  REQUIRE(img.channel() == 4);
  REQUIRE(img.dims() == 2);
  REQUIRE_FALSE(img.empty());
  REQUIRE((img.size(1) == 2 && img.stride(1) == 16));
  REQUIRE((img.size(0) == 2 && img.stride(0) == 32));

  auto img3 = ::image::Image(3, ::std::array<size_t, 3>{2, 3, 4}.data(),
                             ::image::PixelType::k16UC1);
  ::std::swap(img, img3);
  REQUIRE(img.type() == ::image::PixelType::k16UC1);
  REQUIRE(img.depth() == ::image::PixelDepth::k16U);
  REQUIRE(img.channel() == 1);
  REQUIRE(img.dims() == 3);
  REQUIRE_FALSE(img.empty());
  REQUIRE((img.size(2) == 4 && img.stride(2) == 2));
  REQUIRE((img.size(1) == 3 && img.stride(1) == 8));
  REQUIRE((img.size(0) == 2 && img.stride(0) == 24));

  REQUIRE(img2.type() == ::image::kTypeNone);
  REQUIRE(img2.depth() == ::image::kDepthNone);
  REQUIRE(img2.channel() == 0);
  REQUIRE(img2.dims() == 0);
  REQUIRE(img2.empty());
  REQUIRE(img2.data() == nullptr);
}
