#include "image.hpp"

#include <type_traits>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("image::pixel::mapping", "[Image][Pixel]") {
  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<float, 4>::element_type, float>);
  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<float, 4>::type, ::math::vec4>);
  STATIC_REQUIRE(::image::MapToPixel<float, 4>::channel == 4);
  STATIC_REQUIRE(::image::MapToPixel<float, 4>::ty ==
                 ::image::PixelType::k32FC4);
  STATIC_REQUIRE(::image::MapToPixel<float, 4>::stride == sizeof(::math::vec4));

  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<float, 3>::element_type, float>);
  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<float, 3>::type, ::math::vec3>);
  STATIC_REQUIRE(::image::MapToPixel<float, 3>::channel == 3);
  STATIC_REQUIRE(::image::MapToPixel<float, 3>::ty ==
                 ::image::PixelType::k32FC3);
  STATIC_REQUIRE(::image::MapToPixel<float, 3>::stride == sizeof(::math::vec3));

  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<uint16_t, 1>::element_type,
                       uint16_t>);
  STATIC_REQUIRE(
      ::std::is_same_v<::image::MapToPixel<uint16_t, 1>::type, uint16_t>);
  STATIC_REQUIRE(::image::MapToPixel<uint16_t, 1>::channel == 1);
  STATIC_REQUIRE(::image::MapToPixel<uint16_t, 1>::ty ==
                 ::image::PixelType::k16UC1);
  STATIC_REQUIRE(::image::MapToPixel<uint16_t, 1>::stride == sizeof(uint16_t));
}

TEST_CASE("image::pixel::trait", "[Image][Pixel]") {
  SECTION("static") {
    using StaticTrait = ::image::PixelTraits<::image::PixelType::k32FC3>;
    STATIC_REQUIRE(::std::is_same_v<StaticTrait::element_type, float>);
    STATIC_REQUIRE(::std::is_same_v<StaticTrait::type, ::math::vec3>);
    STATIC_REQUIRE(StaticTrait::channel == 3);
    STATIC_REQUIRE(StaticTrait::ty == ::image::PixelType::k32FC3);
    STATIC_REQUIRE(StaticTrait::stride == sizeof(::math::vec3));

    constexpr auto kPixelTrait =
        ::image::PixelTraits<::image::PixelType::k64UC2>{};
    STATIC_REQUIRE(kPixelTrait.type_enum() == ::image::PixelType::k64UC2);
    STATIC_REQUIRE(kPixelTrait.get_depth() == ::image::PixelDepth::k64U);
    STATIC_REQUIRE(kPixelTrait.get_channel() == 2);
    STATIC_REQUIRE(kPixelTrait.get_stride() ==
                   sizeof(::math::Vector2<uint64_t>));
    REQUIRE(sizeof(decltype(kPixelTrait)) == 2);
  }

  SECTION("dynamic") {
    auto const kPixelTrait = ::image::DynPixelTrait{::image::PixelType::k16UC3};
    REQUIRE(kPixelTrait.type_enum() == ::image::PixelType::k16UC3);
    REQUIRE(kPixelTrait.get_depth() == ::image::PixelDepth::k16U);
    REQUIRE(kPixelTrait.get_channel() == 3);
    REQUIRE(kPixelTrait.get_stride() == sizeof(::math::Vector3<uint16_t>));
    REQUIRE(sizeof(::image::DynPixelTrait) >= 2);

    // NOLINTNEXTLINE(misc-const-correctness)
    ::image::DynPixelTrait pixel_trait2{::image::PixelType::k32SC4};
    REQUIRE(pixel_trait2.type_enum() == ::image::PixelType::k32SC4);
    REQUIRE(pixel_trait2.get_depth() == ::image::PixelDepth::k32S);
    REQUIRE(pixel_trait2.get_channel() == 4);
    REQUIRE(pixel_trait2.get_stride() == sizeof(::math::ivec4));

    ::image::DynPixelTrait pixel_trait3{kPixelTrait};
    REQUIRE(pixel_trait3.type_enum() == kPixelTrait.type_enum());
    REQUIRE(pixel_trait3.get_depth() == kPixelTrait.get_depth());
    REQUIRE(pixel_trait3.get_channel() == kPixelTrait.get_channel());
    REQUIRE(pixel_trait3.get_stride() == kPixelTrait.get_stride());

    ::std::swap(pixel_trait2, pixel_trait3);
    REQUIRE(pixel_trait2.type_enum() == kPixelTrait.type_enum());
    REQUIRE(pixel_trait2.get_depth() == kPixelTrait.get_depth());
    REQUIRE(pixel_trait2.get_channel() == kPixelTrait.get_channel());
    REQUIRE(pixel_trait2.get_stride() == kPixelTrait.get_stride());
  }
}

TEST_CASE("image::pixel::swizzle", "[Image][Pixel]") {
  SECTION("static") {
    ::image::PixelTraits<::image::PixelType::k32FC3> const kS1{};
    REQUIRE(kS1.get_swizzle() == ::std::array<uint8_t, 4>{0, 1, 2});

    ::image::PixelTraits<::image::PixelType::k32FC3, ::math::Channel::Y,
                         ::math::Channel::Z, ::math::Channel::X> const kS2{};
    REQUIRE(kS2.get_swizzle() == ::std::array<uint8_t, 4>{1, 2, 0});

    // const ::image::PixelTraits<::image::PixelType::k32FC3,
    //                             ::math::Channel::Y, ::math::Channel::Z,
    //                             ::math::Channel::W> s3{};
    // REQUIRE(s3.get_swizzle() == ::std::array<uint8_t, 4>{1, 2, 3});
  }

  SECTION("dynamic") {
    ::image::DynPixelTrait const kS1{::image::PixelType::k32FC3};
    REQUIRE(kS1.get_swizzle() == ::std::array<uint8_t, 4>{0, 1, 2});

    auto s2 = ::image::PixelTraits{::image::PixelType::k8UC4,
                                   ::math::Channel::W, ::math::Channel::W,
                                   ::math::Channel::X, ::math::Channel::Y};
    REQUIRE(s2.get_swizzle() == ::std::array<uint8_t, 4>{3, 3, 0, 1});

    auto s3 =
        ::image::PixelTraits{::image::PixelType::k32SC3, ::math::Channel::Z,
                             ::math::Channel::Y, ::math::Channel::X};
    REQUIRE(s3.get_swizzle() == ::std::array<uint8_t, 4>{2, 1, 0});

    ::std::swap(s2, s3);
    REQUIRE(s2.get_swizzle() == ::std::array<uint8_t, 4>{2, 1, 0});
    REQUIRE(s3.get_swizzle() == ::std::array<uint8_t, 4>{3, 3, 0, 1});

    // auto s4 =
    //     ::image::PixelTraits{::image::PixelType::k32SC3, ::math::Channel::W,
    //                          ::math::Channel::Y, ::math::Channel::X};
  }
}

TEST_CASE("image::pixel::shuffle", "[Image][Pixel]") {
  ::std::array<uint8_t, 4> shuffle;
  SECTION("static") {
    ::image::PixelTraits<::image::PixelType::k32UC4, ::math::Channel::Y,
                         ::math::Channel::X, ::math::Channel::W,
                         ::math::Channel::Z> const kS{};
    shuffle = kS.get_swizzle();
  }
  SECTION("dynamic") {
    auto s0 = ::image::PixelTraits{::image::PixelType::k32UC4,
                                   ::math::Channel::Y, ::math::Channel::X,
                                   ::math::Channel::W, ::math::Channel::Z};
    shuffle = s0.get_swizzle();
  }
  ::image::PixelU32C4 const kPixel{0x00, 0x01, 0x02, 0x03};
  REQUIRE(kPixel(shuffle).y == kPixel[0]);
  REQUIRE(kPixel(shuffle)[0] == kPixel[1]);
  REQUIRE(kPixel(shuffle) ==
          ::image::PixelU32C4{kPixel[1], kPixel[0], kPixel[3], kPixel[2]});
}
