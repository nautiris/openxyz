#include "image.hpp"
#include "math.hpp"

#include <algorithm>
#include <random>
#include <stdint.h>
#include <string.h>

#if defined(TEST_CATCH2_V3)
#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

namespace {
constexpr uint64_t kIntData[][4] = {
    {0x00, 0x01, 0x02, 0x03},
    {0x04, 0x05, 0x06, 0x07},
    {0x08, 0x09, 0x0A, 0x0B},
    {0x0C, 0x0D, 0x0E, 0x0F},
    {0x10, 0x11, 0x12, 0x13},
    {0x14, 0x15, 0x16, 0x17},
    {0x18, 0x19, 0x1A, 0x1B},
    {0x1C, 0x1D, 0x1E, 0x1F},
};

constexpr float kFloatData[][4] = {
    {0x00p0f, 0x01p0f, 0x02p0f, 0x03p0f},
    {0x04p0f, 0x05p0f, 0x06p0f, 0x07p0f},
    {0x08p0f, 0x09p0f, 0x0Ap0f, 0x0Bp0f},
    {0x0Cp0f, 0x0Dp0f, 0x0Ep0f, 0x0Fp0f},
    {0x10p0f, 0x11p0f, 0x12p0f, 0x13p0f},
    {0x14p0f, 0x15p0f, 0x16p0f, 0x17p0f},
    {0x18p0f, 0x19p0f, 0x1Ap0f, 0x1Bp0f},
    {0x1Cp0f, 0x1Dp0f, 0x1Ep0f, 0x1Fp0f},
};
}

TEST_CASE("image::texture::1d::create", "[Image][Texture]") {
  using ExtentsT = ::image::DynExtents<1>;
  using MappingT = typename ::image::LayoutRight::template Mapping<ExtentsT>;

  SECTION("Integer") {
    auto const* ptr = reinterpret_cast<uint8_t const*>(&kIntData[0][0]);
    constexpr ::image::PixelType kFormat = ::image::PixelType::k64UC4;
    constexpr size_t kNumber = 8;
    constexpr size_t kStride = ::image::PixelTraits<kFormat>{}.get_stride();

    SECTION("Integer::Average") {
      ::image::View view{kFormat, ptr, MappingT{kNumber}};
      auto const [tex, levels] =
          image::pyramid::create_texture1d<kFormat>(view);
      REQUIRE(levels == 4);
      auto texview = ::image::View{kFormat, tex.data() + (kNumber * kStride),
                                   MappingT{kNumber >> 1}};
      REQUIRE(texview.as<kFormat>(0) == ::image::Pixel<kFormat>{2, 3, 4, 5});
      REQUIRE(texview.as<kFormat>(1) == ::image::Pixel<kFormat>{8, 9, 10, 11});
      REQUIRE(texview.as<kFormat>(2) ==
              ::image::Pixel<kFormat>{16, 17, 18, 19});
      REQUIRE(texview.as<kFormat>(3) ==
              ::image::Pixel<kFormat>{24, 25, 26, 27});

      texview = ::image::View{
          kFormat, tex.data() + ((kNumber + (kNumber >> 1)) * kStride),
          MappingT{kNumber >> 2}};
      REQUIRE(texview.as<kFormat>(0) == ::image::Pixel<kFormat>{5, 6, 7, 8});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{16, 17, 18, 19});

      texview = ::image::View{
          kFormat,
          tex.data() + ((kNumber + (kNumber >> 1) + (kNumber >> 2)) * kStride),
          MappingT{kNumber >> 3}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{10, 11, 12, 13});
    }

    SECTION("Integer::Gaussian") {
      ::image::View view{kFormat, ptr, MappingT{kNumber}};
      auto const [tex, levels] = image::pyramid::create_texture1d<kFormat>(
          view, ::image::pyramid::FilterMode::kGaussian);
      REQUIRE(levels == 4);
      auto texview = ::image::View{kFormat, tex.data() + (kNumber * kStride),
                                   MappingT{kNumber >> 1}};
      REQUIRE(texview.as<kFormat>(0) == ::image::Pixel<kFormat>{2, 3, 4, 5});
      REQUIRE(texview.as<kFormat>(1) == ::image::Pixel<kFormat>{8, 9, 10, 11});
      REQUIRE(texview.as<kFormat>(2) ==
              ::image::Pixel<kFormat>{16, 17, 18, 19});
      REQUIRE(texview.as<kFormat>(3) ==
              ::image::Pixel<kFormat>{23, 24, 25, 26});

      texview = ::image::View{
          kFormat, tex.data() + ((kNumber + (kNumber >> 1)) * kStride),
          MappingT{kNumber >> 2}};
      REQUIRE(texview.as<kFormat>(0) == ::image::Pixel<kFormat>{5, 6, 7, 8});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{14, 15, 16, 17});

      texview = ::image::View{
          kFormat,
          tex.data() + ((kNumber + (kNumber >> 1) + (kNumber >> 2)) * kStride),
          MappingT{kNumber >> 3}};
      REQUIRE(texview.as<kFormat>(0) == ::image::Pixel<kFormat>{8, 9, 10, 11});
    }
  }

  SECTION("Floating") {
    auto const* ptr = reinterpret_cast<uint8_t const*>(&kFloatData[0][0]);
    constexpr ::image::PixelType kFormat = ::image::PixelType::k32FC4;
    constexpr size_t kNumber = 8;
    constexpr size_t kStride = ::image::PixelTraits<kFormat>{}.get_stride();

    SECTION("Floating::Average") {
      ::image::View view{kFormat, ptr, MappingT{kNumber}};
      auto const [tex, levels] =
          image::pyramid::create_texture1d<kFormat>(view);
      REQUIRE(levels == 4);
      auto texview = ::image::View{kFormat, tex.data() + (kNumber * kStride),
                                   MappingT{kNumber >> 1}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{2.f, 3.f, 4.f, 5.f});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{8.f, 9.f, 10.f, 11.f});
      REQUIRE(texview.as<kFormat>(2) ==
              ::image::Pixel<kFormat>{16.f, 17.f, 18.f, 19.f});
      REQUIRE(texview.as<kFormat>(3) ==
              ::image::Pixel<kFormat>{24.f, 25.f, 26.f, 27.f});

      texview = ::image::View{
          kFormat, tex.data() + ((kNumber + (kNumber >> 1)) * kStride),
          MappingT{kNumber >> 2}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{5.f, 6.f, 7.f, 8.f});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{16.f, 17.f, 18.f, 19.f});

      texview = ::image::View{
          kFormat,
          tex.data() + ((kNumber + (kNumber >> 1) + (kNumber >> 2)) * kStride),
          MappingT{kNumber >> 3}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{10.5f, 11.5f, 12.5f, 13.5f});
    }

    SECTION("Floating::Gaussian") {
      ::image::View view{kFormat, ptr, MappingT{kNumber}};
      auto const [tex, levels] = image::pyramid::create_texture1d<kFormat>(
          view, ::image::pyramid::FilterMode::kGaussian);
      REQUIRE(levels == 4);
      auto texview = ::image::View{kFormat, tex.data() + (kNumber * kStride),
                                   MappingT{kNumber >> 1}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{2.18181818f, 3.18181818f, 4.18181818f,
                                      5.18181818f});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{8.f, 9.f, 10.f, 11.f});
      REQUIRE(texview.as<kFormat>(2) ==
              ::image::Pixel<kFormat>{16.f, 17.f, 18.f, 19.f});
      REQUIRE(texview.as<kFormat>(3) ==
              ::image::Pixel<kFormat>{23.46666666f, 24.46666666f, 25.46666666f,
                                      26.46666666f});

      texview = ::image::View{
          kFormat, tex.data() + ((kNumber + (kNumber >> 1)) * kStride),
          MappingT{kNumber >> 2}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{5.55371900f, 6.55371900f, 7.55371900f,
                                      8.55371900f});
      REQUIRE(texview.as<kFormat>(1) ==
              ::image::Pixel<kFormat>{14.936565656f, 15.936565656f,
                                      16.936565656f, 17.936565656f});

      texview = ::image::View{
          kFormat,
          tex.data() + ((kNumber + (kNumber >> 1) + (kNumber >> 2)) * kStride),
          MappingT{kNumber >> 3}};
      REQUIRE(texview.as<kFormat>(0) ==
              ::image::Pixel<kFormat>{9.306857667584941, 10.306857667584941,
                                      11.306857667584941, 12.306857667584941});
    }
  }

  SECTION("Performence") {
#define OPENXYZ_BENCHMARK_GENERATE(FORMAT, METHOD)                             \
  BENCHMARK_ADVANCED("1d:1920x1080:" #FORMAT                                   \
                     ":" #METHOD)(Catch::Benchmark::Chronometer meter) {       \
    constexpr auto kSize = static_cast<size_t>(1'920 * 1'080);                 \
    constexpr auto kPerfFormat = ::image::PixelType::FORMAT;                   \
    using ValueT = typename ::image::Pixel<kPerfFormat>::value_type;           \
    ::std::random_device rd;                                                   \
    ::image::Image img{::std::array{kSize * 2}, kPerfFormat};                  \
    ::std::for_each(reinterpret_cast<ValueT*>(img.data()),                     \
                    reinterpret_cast<ValueT*>(img.data()) + (kSize * 4),       \
                    [&](ValueT& val) -> void {                                 \
                      if constexpr (::std::is_floating_point_v<ValueT>) {      \
                        val = static_cast<ValueT>(                             \
                            static_cast<double>((rd() % 512) - 256) / 256.);   \
                      } else {                                                 \
                        val = (rd() % 512) - 256;                              \
                      }                                                        \
                    });                                                        \
    ::image::View view{kPerfFormat, img.data(), MappingT{kSize}};              \
    meter.measure([&] {                                                        \
      auto const [tex, levels] =                                               \
          ::image::pyramid::create_texture1d<kPerfFormat>(                     \
              view, ::image::pyramid::FilterMode::METHOD);                     \
      return tex.data()[0xFFFF];                                               \
    });                                                                        \
  };
    OPENXYZ_BENCHMARK_GENERATE(k32UC4, kAverage)
    OPENXYZ_BENCHMARK_GENERATE(k32FC4, kAverage)
    OPENXYZ_BENCHMARK_GENERATE(k32UC4, kGaussian)
    OPENXYZ_BENCHMARK_GENERATE(k32FC4, kGaussian)
#undef OPENXYZ_BENCHMARK_GENERATE
  }
}
