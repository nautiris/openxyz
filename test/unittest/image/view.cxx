#include "image.hpp"
#include "math.hpp"

#include <array>
#include <stdint.h>
#include <string.h>
#include <utility>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("image::view::extents", "[Image][View]") {
  constexpr ::image::Extents<size_t, 1, 2, 3> kExt1;
  STATIC_REQUIRE(decltype(kExt1)::rank() == 3 &&
                 decltype(kExt1)::rank_dynamic() == 0);
  STATIC_REQUIRE(decltype(kExt1)::static_extent(0) == 1 &&
                 decltype(kExt1)::static_extent(1) == 2 &&
                 decltype(kExt1)::static_extent(2) == 3);
  STATIC_REQUIRE(kExt1.extent(0) == 1 && kExt1.extent(1) == 2 &&
                 kExt1.extent(2) == 3);

  constexpr ::image::Extents<size_t, 1, ::image::kDynamicExtents, 3> kExt2{5};
  STATIC_REQUIRE(kExt1 != kExt2);
  STATIC_REQUIRE(decltype(kExt2)::rank() == 3 &&
                 decltype(kExt2)::rank_dynamic() == 1);
  STATIC_REQUIRE(decltype(kExt2)::static_extent(0) == 1 &&
                 decltype(kExt2)::static_extent(1) ==
                     ::image::kDynamicExtents &&
                 decltype(kExt2)::static_extent(2) == 3);
  STATIC_REQUIRE(kExt2.extent(0) == 1 && kExt2.extent(1) == 5 &&
                 kExt2.extent(2) == 3);
  STATIC_REQUIRE_FALSE(sizeof(kExt2) == 1);

  constexpr ::image::Extents<size_t, 1, ::image::kDynamicExtents, 3> kExt3{
      kExt2};
  STATIC_REQUIRE(kExt3.extent(0) == 1 && kExt3.extent(1) == 5 &&
                 kExt3.extent(2) == 3);

  // NOLINTNEXTLINE(misc-const-correctness)
  ::image::Extents<size_t, 1, ::image::kDynamicExtents,
                   ::image::kDynamicExtents>
      ext4{kExt1};
  REQUIRE((1 == ext4.extent(0) && 2 == ext4.extent(1) && 3 == ext4.extent(2)));
}

TEST_CASE("image::view::layout", "[Image][View]") {
  using ext_type = ::image::Extents<size_t, 2, 3, 4>;
  using ext_dyn_type = ::image::Extents<size_t, 2, ::image::kDynamicExtents, 4>;
  constexpr size_t kDynamic = 3;

  SECTION("layout_right") {
    constexpr ::image::LayoutRight::Mapping<ext_type> kMap1;
    constexpr ::image::LayoutRight::Mapping<ext_dyn_type> kMap2{kDynamic};
    STATIC_REQUIRE(
        ::image::LayoutRight::Mapping<ext_type>::is_always_exhaustive());
    STATIC_REQUIRE(kMap1.required_span_size() ==
                   static_cast<typename ext_type::index_type>(2 * 3 * 4));
    STATIC_REQUIRE(kMap2.required_span_size() ==
                   static_cast<typename ext_type::index_type>(2 * 3 * 4));
    STATIC_REQUIRE(kMap1.stride(2) == 1 && kMap1.stride(1) == 4 &&
                   kMap1.stride(0) == 12);
    STATIC_REQUIRE(kMap2.stride(2) == 1 && kMap2.stride(1) == 4 &&
                   kMap2.stride(0) == 12);
    STATIC_REQUIRE(kMap1(0, 0, 0) == 0 &&
                   kMap1(1, 2, 3) == 3 * kMap1.stride(2) + 2 * kMap1.stride(1) +
                                         1 * kMap1.stride(0));
    STATIC_REQUIRE(kMap2(0, 0, 0) == 0 &&
                   kMap2(1, 2, 3) == 3 * kMap2.stride(2) + 2 * kMap2.stride(1) +
                                         1 * kMap2.stride(0));
  }

  SECTION("layout_left") {
    constexpr ::image::LayoutLeft::Mapping<ext_type> kMap1;
    constexpr ::image::LayoutLeft::Mapping<ext_dyn_type> kMap2{kDynamic};
    STATIC_REQUIRE(
        ::image::LayoutLeft::Mapping<ext_type>::is_always_exhaustive());
    STATIC_REQUIRE(kMap1.required_span_size() ==
                   static_cast<typename ext_type::index_type>(2 * 3 * 4));
    STATIC_REQUIRE(kMap2.required_span_size() ==
                   static_cast<typename ext_type::index_type>(2 * 3 * 4));
    STATIC_REQUIRE(kMap1.stride(0) == 1 && kMap1.stride(1) == 2 &&
                   kMap1.stride(2) == 6);
    STATIC_REQUIRE(kMap2.stride(0) == 1 && kMap2.stride(1) == 2 &&
                   kMap2.stride(2) == 6);
    STATIC_REQUIRE(kMap1(0, 0, 0) == 0 &&
                   kMap1(1, 2, 3) == 3 * kMap1.stride(2) + 2 * kMap1.stride(1) +
                                         1 * kMap1.stride(0));
    STATIC_REQUIRE(kMap2(0, 0, 0) == 0 &&
                   kMap2(1, 2, 3) == 3 * kMap2.stride(2) + 2 * kMap2.stride(1) +
                                         1 * kMap2.stride(0));
  }

  SECTION("layout_stride") {
    constexpr ::image::LayoutStride::Mapping<ext_type> kMap1;
    constexpr ::image::LayoutStride::Mapping<ext_dyn_type> kMap2;
    constexpr ::image::LayoutStride::Mapping<ext_dyn_type> kMap3{
        ext_dyn_type{kDynamic}};
    STATIC_REQUIRE_FALSE(
        ::image::LayoutStride::Mapping<ext_type>::is_always_exhaustive());
    STATIC_REQUIRE(kMap1.required_span_size() ==
                   static_cast<typename ext_type::index_type>(
                       1 + 3 * kMap1.stride(2) + 2 * kMap1.stride(1) +
                       1 * kMap1.stride(0)));
    STATIC_REQUIRE(kMap2.required_span_size() == 0);
    STATIC_REQUIRE(kMap3.required_span_size() == kMap1.required_span_size());
    STATIC_REQUIRE(kMap1.stride(0) == 12 && kMap1.stride(1) == 4 &&
                   kMap1.stride(2) == 1);
    STATIC_REQUIRE(kMap2.stride(0) == 0 && kMap2.stride(1) == 4 &&
                   kMap2.stride(2) == 1);
    STATIC_REQUIRE(kMap3.stride(0) == kMap1.stride(0) &&
                   kMap3.stride(1) == kMap1.stride(1) &&
                   kMap1.stride(2) == kMap1.stride(2));
    STATIC_REQUIRE(kMap1(0, 0, 0) == 0 &&
                   kMap1(1, 2, 3) == 3 * kMap1.stride(2) + 2 * kMap1.stride(1) +
                                         1 * kMap1.stride(0));
    STATIC_REQUIRE(kMap2(0, 0, 0) == 0 &&
                   kMap2(1, 2, 3) == 3 * kMap2.stride(2) + 2 * kMap2.stride(1) +
                                         0 * kMap2.stride(0));
    STATIC_REQUIRE(kMap3(0, 0, 0) == 0 && kMap3(1, 2, 3) == kMap1(1, 2, 3));
    STATIC_REQUIRE(kMap1.is_exhaustive() && kMap2.is_exhaustive() &&
                   kMap3.is_exhaustive());
    STATIC_REQUIRE(sizeof(kMap1) ==
                   ((sizeof(typename decltype(kMap1)::extents_type) +
                     sizeof(typename decltype(kMap1)::strides_type) +
                     alignof(typename decltype(kMap1)::strides_type) - 1) &
                    ~(alignof(typename decltype(kMap1)::strides_type) - 1)));
    STATIC_REQUIRE(sizeof(kMap2) ==
                   ((sizeof(typename decltype(kMap2)::extents_type) +
                     sizeof(typename decltype(kMap2)::strides_type) +
                     alignof(typename decltype(kMap2)::strides_type) - 1) &
                    ~(alignof(typename decltype(kMap2)::strides_type) - 1)));

    // NOLINTNEXTLINE(misc-const-correctness)
    ::image::LayoutStride::Mapping<ext_dyn_type> map4{
        ext_dyn_type{kDynamic},
        ::std::array<size_t, 3>{100, 10, 1}
    };
    REQUIRE(map4.required_span_size() == 124);
    REQUIRE(
        (map4.stride(0) == 100 && map4.stride(1) == 10 && map4.stride(2) == 1));
    REQUIRE_FALSE(map4.is_exhaustive());
    REQUIRE(map4(1, 1, 3) ==
            3 * map4.stride(2) + 1 * map4.stride(1) + 1 * map4.stride(0));
  }
}

#define DEF_DATA_ARRAY(name, row, col, val1, val2, val3, val4)                 \
  using name##row##col##_type =                                                \
      ::std::remove_pointer_t<decltype(&name[0][0][0])>;                       \
  name##row##col##_type const k##name##row##col[4] = {val1, val2, val3, val4}; \
  memcpy(reinterpret_cast<name##row##col##_type*>(&(name)) +                   \
             (static_cast<size_t>(row) *                                       \
                  (sizeof((name)[0]) / sizeof(name##row##col##_type)) +        \
              static_cast<size_t>(col) * 4),                                   \
         k##name##row##col, sizeof(name##row##col##_type) * 4);

TEST_CASE("image::view::image_view", "[Image][View]") {
  uint32_t data[2][4][4] = {};
  DEF_DATA_ARRAY(data, 0, 0, 0x00, 0x01, 0x02, 0x03);
  DEF_DATA_ARRAY(data, 0, 1, 0x04, 0x05, 0x06, 0x07);
  DEF_DATA_ARRAY(data, 0, 2, 0x08, 0x09, 0x0A, 0x0B);
  DEF_DATA_ARRAY(data, 0, 3, 0x0C, 0x0D, 0x0E, 0x0F);
  DEF_DATA_ARRAY(data, 1, 0, 0x10, 0x11, 0x12, 0x13);
  DEF_DATA_ARRAY(data, 1, 1, 0x14, 0x15, 0x16, 0x17);
  DEF_DATA_ARRAY(data, 1, 2, 0x18, 0x19, 0x1A, 0x1B);
  DEF_DATA_ARRAY(data, 1, 3, 0x1C, 0x1D, 0x1E, 0x1F);
  auto const* ptr = reinterpret_cast<uint8_t const*>(&data[0][0][0]);
  auto img = ::image::Image{2, 4, ::image::PixelType::k32UC4,
                            reinterpret_cast<image::Image::pointer>(data)};

  SECTION("view for right") {
    ::image::View const kView1{::image::PixelType::k32UC4, ptr, 2, 4};
    REQUIRE(*reinterpret_cast<image::PixelU32C4 const*>(kView1(1, 1)) ==
            image::PixelU32C4{kdata11});
    REQUIRE(kView1.as<::image::PixelType::k32UC4>(1, 3) ==
            image::PixelU32C4{kdata13});

    ::image::View view2{::image::PixelType::k32UC1, ptr, 2, 16};
    REQUIRE(*reinterpret_cast<image::PixelU32C1*>(view2(0, 11)) == kdata02[3]);
    view2.as<::image::PixelType::k32UC1>(::std::array<size_t, 2>{0, 11}) = 0;
    REQUIRE(*reinterpret_cast<image::PixelU32C1*>(view2(0, 11)) == 0u);
    REQUIRE(kView1.as<::image::PixelType::k32UC4>(0, 2) ==
            image::PixelU32C4{0x08, 0x09, 0x0A, 0x00});

    REQUIRE(view2.extents().extent(1) == 16);
    REQUIRE(view2.extents().extent(0) == 2);
    auto map = view2.mapping();
    REQUIRE(map.required_span_size() ==
            static_cast<typename ::image::DynExtents<2>::index_type>(2 * 16));
    REQUIRE(map.stride(1) == 1);
    REQUIRE(map.stride(0) == 16);
    REQUIRE(map(0, 0) == 0);
    REQUIRE(map(1, 3) == 3 * map.stride(1) + 1 * map.stride(0));
  }

  SECTION("view for left") {
    using PixelT1 = ::image::PixelTraits<::image::PixelType::k32UC4>;
    ::image::View<PixelT1, image::DynExtents<2>, ::image::LayoutLeft> const
        kView1{PixelT1{}, ptr, 4, 2};
    REQUIRE(*kView1(3, 1) == image::PixelU32C4{0x1C, 0x1D, 0x1E, 0x1F});

    using PixelT2 = ::image::PixelTraits<::image::PixelType::k32UC1>;
    ::image::View<PixelT2, image::DynExtents<2>, ::image::LayoutLeft> view2{
        PixelT2{}, ptr, 2, 16};
    REQUIRE(*view2(1, 11) == kdata11[3]);
    view2.as<::image::PixelType::k32UC1>(1, 11) = 0;
    REQUIRE(kView1.as<::image::PixelType::k32UC4>(1, 1).vec ==
            image::PixelU32C4{0x14, 0x15, 0x16, 0x00}.vec);

    REQUIRE(view2.extents().extent(1) == 16);
    REQUIRE(view2.extents().extent(0) == 2);
    auto map = view2.mapping();
    REQUIRE(map.required_span_size() ==
            static_cast<typename ::image::DynExtents<2>::index_type>(2 * 16));
    REQUIRE(map.stride(1) == 2);
    REQUIRE(map.stride(0) == 1);
    REQUIRE(map(0, 0) == 0);
    REQUIRE(map(1, 3) == 3 * map.stride(1) + 1 * map.stride(0));
  }

  SECTION("view for stride") {
    using PixelT = ::image::PixelTraits<::image::PixelType::k32UC1>;
    using ExtentsT = ::image::DynExtents<2>;
    using MappingT = typename ::image::LayoutStride::template Mapping<ExtentsT>;
    using StridesT = typename MappingT::strides_type;

    ::image::View<PixelT, ExtentsT, MappingT::layout_type> view1{
        PixelT{},
        ptr + 4, MappingT{ExtentsT{3, 2}, StridesT{8, 4}}
    };
    REQUIRE(*view1(0, 0) == kdata00[1]);
    REQUIRE(*view1(0, 1) == kdata01[1]);
    REQUIRE(*view1(1, 0) == kdata02[1]);
    REQUIRE(*view1(1, 1) == kdata03[1]);
    REQUIRE(*view1(2, 0) == kdata10[1]);
    REQUIRE(*view1(2, 1) == kdata11[1]);

    ::image::View<PixelT, ExtentsT, MappingT::layout_type> view2{
        PixelT{},
        ptr + 4, MappingT{ExtentsT{2, 3}, StridesT{4, 1}}
    };
    REQUIRE(*reinterpret_cast<image::PixelU32C3*>(view2(0, 0)) ==
            image::PixelU32C3{0x01, 0x02, 0x03});
    REQUIRE(*view2(0, 1) == kdata00[2]);
    REQUIRE(*reinterpret_cast<image::PixelU32C3*>(view2(1, 0)) ==
            image::PixelU32C3{0x05, 0x06, 0x07});
    REQUIRE(*view2(1, 2) == kdata01[3]);

    REQUIRE(view2.extents().extent(1) == 3);
    REQUIRE(view2.extents().extent(0) == 2);
    auto map = view2.mapping();
    REQUIRE(
        map.required_span_size() ==
        static_cast<typename ::image::DynExtents<2>::index_type>(1 + 2 + 4));
    REQUIRE(map.stride(1) == 1);
    REQUIRE(map.stride(0) == 4);
    REQUIRE(map(0, 0) == 0);
    REQUIRE(map(1, 3) == 3 * map.stride(1) + 1 * map.stride(0));

    ::std::swap(view1, view2);
    REQUIRE((view2.extents().extent(1) == 2 && view2.extents().extent(0) == 3));
    map = view2.mapping();
    REQUIRE((view2.mapping().stride(1) == 4 && view2.mapping().stride(0) == 8));
    CHECK(map.required_span_size() ==
          static_cast<typename ::image::DynExtents<2>::index_type>(1 + 1 * 4 +
                                                                   2 * 8));
  }
}

TEST_CASE("image::view::swap", "[Image][View]") {
  uint32_t data[2][4][4] = {};
  DEF_DATA_ARRAY(data, 0, 0, 0x00, 0x01, 0x02, 0x03);
  DEF_DATA_ARRAY(data, 0, 1, 0x04, 0x05, 0x06, 0x07);
  DEF_DATA_ARRAY(data, 0, 2, 0x08, 0x09, 0x0A, 0x0B);
  DEF_DATA_ARRAY(data, 0, 3, 0x0C, 0x0D, 0x0E, 0x0F);
  DEF_DATA_ARRAY(data, 1, 0, 0x10, 0x11, 0x12, 0x13);
  DEF_DATA_ARRAY(data, 1, 1, 0x14, 0x15, 0x16, 0x17);
  DEF_DATA_ARRAY(data, 1, 2, 0x18, 0x19, 0x1A, 0x1B);
  DEF_DATA_ARRAY(data, 1, 3, 0x1C, 0x1D, 0x1E, 0x1F);
  auto const* ptr = reinterpret_cast<uint8_t const*>(&data[0][0][0]);

  using ExtentsT = ::image::DynExtents<2>;
  using MappingT = typename ::image::LayoutRight::template Mapping<ExtentsT>;

  ::image::View view1{
      ::image::PixelType::k32UC4, ptr, MappingT{2, 4}
  };
  ::image::View view2{::image::PixelType::k32UC1, ptr, 2, 16};
  ::std::swap(view1, view2);
  REQUIRE(*reinterpret_cast<image::PixelU32C4 const*>(view2(1, 1)) ==
          image::PixelU32C4{kdata11});
  REQUIRE(view2.as<::image::PixelType::k32UC4>(1, 3) ==
          image::PixelU32C4{kdata13});
  REQUIRE(view2.as<::image::PixelType::k32UC4>(0, 2) ==
          image::PixelU32C4{0x08, 0x09, 0x0A, 0x0B});

  REQUIRE(*reinterpret_cast<image::PixelU32C1*>(view1(0, 11)) == kdata02[3]);
  REQUIRE(view1.extents().extent(1) == 16);
  REQUIRE(view1.extents().extent(0) == 2);
}
