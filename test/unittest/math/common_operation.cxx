#include "math.hpp"

#include <functional>
#include <limits>
#include <math.h>
#include <stdint.h>
#include <type_traits>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::operation::common::abs", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 2, -3);
  STATIC_REQUIRE(::math::abs(kVec3) == ::math::make_vector<int>(1, 2, 3));

  constexpr auto kVec3f = ::math::make_vector<float>(-1.5f, -1.2345e-5f, 1.2f);
  REQUIRE(::math::abs(kVec3f) ==
          ::math::make_vector<float>(1.5f, 1.2345e-5f, 1.2f));

  auto vec3 = ::math::make_vector<double>(-1.5, -1.234567e-5, 1.2);
  REQUIRE(::math::abs(vec3) ==
          ::math::make_vector<double>(1.5, 1.234567e-5, 1.2));
}

TEST_CASE("math::operation::common::sign", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::sign(kVec3) == ::math::make_vector<int>(-1, 0, 1));

  constexpr auto kVec3ui = ::math::make_vector<uint32_t>(
      0, 1, ::std::numeric_limits<uint32_t>::max());
  REQUIRE(::math::sign(kVec3ui) == ::math::make_vector<uint32_t>(0, 1, 1));

  auto vec5 = ::math::make_vector<double>(-1.5, -1.234567e-5, 1.2, 0.0, -0.0,
                                          +INFINITY, -INFINITY, +NAN, -NAN);
  REQUIRE(::math::sign(vec5) ==
          ::math::make_vector<double>(-1., -1., 1., 0., 0., 1., -1., 1., -1.));
}

TEST_CASE("math::operation::common::ceil", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::ceil(kVec3) == ::math::make_vector<int>(-1, 0, 3));

  constexpr auto kVec = ::math::make_vector<double>(+2.3, -2.3, -0., +0.);
  REQUIRE(::math::ceil(kVec) == ::math::make_vector<double>(3., -2., 0., 0.));
}

TEST_CASE("math::operation::common::floor", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::floor(kVec3) == ::math::make_vector<int>(-1, 0, 3));

  auto vec5 = ::math::make_vector<double>(-1.5, -1.234567e-5, 1.2, 0.0, -0.0);
  REQUIRE(::math::floor(vec5) ==
          ::math::make_vector<double>(-2., -1., 1., 0., 0.));
}

TEST_CASE("math::operation::common::trunc", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::trunc(kVec3) == ::math::make_vector<int>(-1, 0, 3));

  auto vec = ::math::make_vector<double>(+2.7, -2.9, +0.7, -0.9, +0.0);
  REQUIRE(::math::trunc(vec) ==
          ::math::make_vector<double>(+2., -2., 0., 0., 0.));
}

TEST_CASE("math::operation::common::round", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::round(kVec3) == ::math::make_vector<int>(-1, 0, 3));

  auto vec =
      ::math::make_vector<double>(+2.3, +2.5, +2.7, -2.3, -2.5, -2.7, -0., +0.);
  REQUIRE(::math::round(vec) ==
          ::math::make_vector<double>(+2., +3., +3., -2., -3., -3., 0., 0.));
}

TEST_CASE("math::operation::common::round_even",
          "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::round_even(kVec3) ==
                 ::math::make_vector<int>(-1, 0, 3));

  constexpr auto kDVec =
      ::math::make_vector<double>(+2.3, +2.5, +3.5, -2.3, -2.5, -3.5, -0., +0.);
  REQUIRE(::math::round_even(kDVec) ==
          ::math::make_vector<double>(+2., +2., +4., -2., -2., -4., 0., 0.));
}

TEST_CASE("math::operation::common::fract", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::fract(kVec3) == ::math::Vector<int, 3>{});

  constexpr auto kDVec =
      ::math::make_vector<double>(+2.3, +2.5, +3.5, -2.3, -2.5, -3.5, -0., +0.);
  REQUIRE(::math::fract(kDVec) ==
          ::math::make_vector<double>(.3, .5, .5, .7, .5, .5, 0., 0.));
}

TEST_CASE("math::operation::common::mod", "[Mathematic][Operation][Common]") {
  constexpr auto kVec3 = ::math::make_vector<int>(-1, 0, 3);
  STATIC_REQUIRE(::math::mod(kVec3, 2) ==
                 ::math::Vector<int, 3>{-1 % 2, 0 % 2, 3 % 2});

  constexpr auto kDVec = ::math::make_vector<double>(+5.1, -5.1, +0.0, -0.0);
  REQUIRE(::math::mod(kDVec, +3.0) ==
          ::math::make_vector<double>(2.1, 0.9, 0.0, 0.0));

  constexpr auto kDVec2 =
      ::math::make_vector<double>(+5.1, -5.1, +5.1, -5.1, +0.0, -0.0);
  constexpr auto kDVec3 =
      ::math::make_vector<double>(+3., +3., -3., -3., 1., 1.);
  REQUIRE(::math::mod(kDVec2, kDVec3) ==
          ::math::make_vector<double>(2.1, 0.9, -0.9, -2.1, 0.0, 0.0));
}

TEST_CASE("math::operation::common::modf", "[Mathematic][Operation][Common]") {
  auto ivec3 = ::math::make_vector<int>(-1, 0, 3);
  decltype(ivec3) ivec3_integer;
  REQUIRE(::math::modf(ivec3, ivec3_integer) ==
          ::math::Vector<int, 3>{0, 0, 0});
  REQUIRE(ivec3_integer == ::math::Vector<int, 3>{-1, 0, 3});

  auto dvec3 = ::math::make_vector<double>(123.45, -123.45, 0.);
  decltype(dvec3) dvec3_integer;
  REQUIRE(::math::modf(dvec3, dvec3_integer) ==
          ::math::Vector<double, 3>{0.45, -0.45, 0.0});
  REQUIRE(dvec3_integer == ::math::Vector<double, 3>{123.0, -123.0, 0.0});
}

TEST_CASE("math::operation::common::min", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec3 = ::math::make_vector<int>(-2, 0, 3);
  STATIC_REQUIRE(::math::min(kIVec3, 1) == ::math::make_vector<int>(-2, 0, 1));

  constexpr auto kDVec3 = ::math::make_vector<double>(-1.345, -0.0, 3.14159);
  REQUIRE(::math::min(kDVec3,
                      ::math::make_vector<double>(2.71828, 3e-8, 2.71828)) ==
          ::math::make_vector<double>(-1.345, 0.0, 2.71828));
}

TEST_CASE("math::operation::common::max", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec3 = ::math::make_vector<int>(-2, 0, 3);
  STATIC_REQUIRE(::math::max(kIVec3, 1) == ::math::make_vector<int>(1, 1, 3));

  constexpr auto kDVec3 = ::math::make_vector<double>(-1.345, -0.0, 3.14159);
  REQUIRE(::math::max(kDVec3,
                      ::math::make_vector<double>(2.71828, 3e-8, 2.71828)) ==
          ::math::make_vector<double>(2.71828, 3e-8, 3.14159));
}

TEST_CASE("math::operation::common::clamp", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec3 = ::math::make_vector<int>(-2, 0, 3);
  STATIC_REQUIRE(::math::clamp(kIVec3, -1, 1) ==
                 ::math::make_vector<int>(-1, 0, 1));

  constexpr auto kDVec3 = ::math::make_vector<double>(-1.345, -0.0, 3.14159);
  REQUIRE(::math::clamp(kDVec3, ::math::make_vector<double>(-1., -1., -1.),
                        ::math::make_vector<double>(1., 1., 1.)) ==
          ::math::make_vector<double>(-1., 0., 1.));
}

TEST_CASE("math::operation::common::mix", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec3X = ::math::make_vector<int>(-2, 0, 3);
  constexpr auto kIVec3Y = ::math::make_vector<int>(2, 1, 6);
  STATIC_REQUIRE(::math::mix(kIVec3X, kIVec3Y, true) ==
                 ::math::make_vector<int>(2, 1, 6));
  STATIC_REQUIRE(::math::mix(kIVec3X, kIVec3Y, false) ==
                 ::math::make_vector<int>(-2, 0, 3));
  STATIC_REQUIRE(::math::mix(kIVec3X, kIVec3Y, 2) ==
                 ::math::make_vector<int>(6, 2, 9));
  STATIC_REQUIRE(
      ::math::mix(kIVec3X, kIVec3Y, ::math::make_vector<int>(-1, 0, 1)) ==
      ::math::make_vector<int>(-6, 0, 6));

  auto dvec1 = ::math::make_vector<double>(-1.345, -3e-8, 2.71828);
  auto dvec2 = ::math::make_vector<double>(1.234, 2.4e-8, 3.14159);
  REQUIRE(::math::mix(dvec1, dvec2, 0.3) ==
          ::math::make_vector<double>(-0.5713, -1.38e-8, 2.845273));
  REQUIRE(
      ::math::mix(dvec1, dvec2, ::math::make_vector<double>(0.25, 0.5, 0.75)) ==
      ::math::make_vector<double>(-0.70025, -3e-9, 3.0357625));
}

TEST_CASE("math::operation::common::step", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec = ::math::make_vector<int>(3, 0, -2);
  constexpr auto kIEdge = ::math::make_vector<int>(-1, 2, 1);
  STATIC_REQUIRE(::math::step(0, kIVec) == ::math::make_vector<int>(1, 1, 0));
  STATIC_REQUIRE(::math::step(kIEdge, kIVec) ==
                 ::math::make_vector<int>(1, 0, 0));

  auto dvec = ::math::make_vector<double>(-1.345, 3e-8, 2.71828);
  auto dedge = ::math::make_vector<double>(-1.234, -2.4e-8, 3.14159);
  REQUIRE(::math::step(0.0, dvec) ==
          ::math::make_vector<double>(0.0, 1.0, 1.0));
  REQUIRE(::math::step(dedge, dvec) ==
          ::math::make_vector<double>(0.0, 1.0, 0.0));
}

TEST_CASE("math::operation::common::smoothstep",
          "[Mathematic][Operation][Common]") {
  constexpr auto kIVec = ::math::make_vector<int>(3, 0, -2);
  constexpr auto kIEdge0 = ::math::make_vector<int>(-1, 2, 1);
  constexpr auto kIEdge1 = ::math::make_vector<int>(1, 3, 2);
  STATIC_REQUIRE(::math::smoothstep(0, 1, kIVec) ==
                 ::math::make_vector<int>(1, 0, 0));
  STATIC_REQUIRE(::math::smoothstep(kIEdge0, kIEdge1, kIVec) ==
                 ::math::make_vector<int>(1, 0, 0));

  auto dvec = ::math::make_vector<double>(-1.345, 3e-8, 3.0);
  auto dedge0 = ::math::make_vector<double>(-1.574, -0.5, 2.71828);
  auto dedge1 = ::math::make_vector<double>(-1.234, 8.3e-3, 3.14159);
  REQUIRE(::math::smoothstep(-0.43, 0.745, dvec) ==
          ::math::make_vector<double>(0.0, 0.3037529997248398, 1.0));
  REQUIRE(::math::smoothstep(dedge0, dedge1, dvec) ==
          ::math::make_vector<double>(0.7498433238347244, 0.9992088105920972,
                                      0.7392065291504183));
}

TEST_CASE("math::operation::common::isnan", "[Mathematic][Operation][Common]") {
  // NOTE: STATIC_REQUIRE on gcc
  constexpr auto kIVec = ::math::make_vector<int>(3, 0, -2);
  STATIC_REQUIRE(::math::isnan(kIVec) ==
                 ::math::make_vector<bool>(false, false, false));

  constexpr auto kDVec = ::math::make_vector<double>(
      -0.0, 3.14159, -2.71828, NAN, -NAN, -INFINITY, +INFINITY);
  REQUIRE(::math::isnan(kDVec) == ::math::make_vector<bool>(false, false, false,
                                                            true, true, false,
                                                            false));

  auto result = ::math::map(kDVec, 0.0, ::std::divides<double>{});
  REQUIRE(::math::isnan(result) == ::math::make_vector<bool>(true, false, false,
                                                             true, true, false,
                                                             false));
}

TEST_CASE("math::operation::common::isinf", "[Mathematic][Operation][Common]") {
  // NOTE: STATIC_REQUIRE on gcc
  constexpr auto kIVec = ::math::make_vector<int>(3, 0, -2);
  STATIC_REQUIRE(::math::isinf(kIVec) ==
                 ::math::make_vector<bool>(false, false, false));

  constexpr auto kDVec = ::math::make_vector<double>(
      -0.0, 3.14159, -2.71828, NAN, -NAN, -INFINITY, +INFINITY);
  REQUIRE(::math::isinf(kDVec) == ::math::make_vector<bool>(false, false, false,
                                                            false, false, true,
                                                            true));

  auto result = ::math::map(kDVec, 0.0, ::std::divides<double>{});
  REQUIRE(::math::isinf(result) == ::math::make_vector<bool>(false, true, true,
                                                             false, false, true,
                                                             true));
}

TEST_CASE("math::operation::common::bitcast",
          "[Mathematic][Operation][Common]") {
  auto check = [](auto lhs, auto rhs) constexpr -> bool {
    if constexpr (::std::is_floating_point_v<decltype(lhs)>) {
      return ::std::isnan(lhs) ? ::std::isnan(rhs)
                               : ::math::EqualTo<decltype(lhs)>{}(lhs, rhs);
    } else {
      return lhs == rhs;
    }
  };
  auto accumulator = [](bool lhs, bool acc) constexpr -> bool {
    return lhs && acc;
  };

  constexpr auto kVec = ::math::make_vector<double>(
      -0.0, 3.14159, -2.71828, NAN, -NAN, -INFINITY, +INFINITY);
  constexpr auto kIVec = ::math::make_vector<int64_t>(
      0x8000'0000'0000'0000, 0x4009'21F9'F01B'866E, 0xC005'BF09'95AA'F790,
      0x7FF8'0000'0000'0000, 0xFFF8'0000'0000'0000, 0xFFF0'0000'0000'0000,
      0x7FF0'0000'0000'0000);
  constexpr auto kUIVec = ::math::make_vector<uint64_t>(
      0x8000'0000'0000'0000U, 0x4009'21F9'F01B'866EU, 0xC005'BF09'95AA'F790U,
      0x7FF8'0000'0000'0000U, 0xFFF8'0000'0000'0000U, 0xFFF0'0000'0000'0000U,
      0x7FF0'0000'0000'0000U);
  REQUIRE(::math::float_bits_to_int(kVec) == kIVec);
  REQUIRE(::math::float_bits_to_uint(kVec) == kUIVec);
  REQUIRE(
      ::math::reduce(::math::map(::math::int_bits_to_float(kIVec), kVec, check),
                     true, accumulator));
  REQUIRE(::math::reduce(
      ::math::map(::math::uint_bits_to_float(kUIVec), kVec, check), true,
      accumulator));

  auto vec = ::math::make_vector<float>(-0.0f, 1.0f, 3.0517578125e-05f, NAN,
                                        -NAN, -INFINITY, +INFINITY);
  auto uivec = ::math::make_vector<uint32_t>(
      0x8000'0000U, 0x3F80'0000U, 0x3800'0000U, 0x7FC0'0000U, 0xFFC0'0000U,
      0xFF80'0000U, 0x7F80'0000U);
  REQUIRE(::math::float_bits_to_uint(vec) == uivec);
  REQUIRE(
      ::math::reduce(::math::map(::math::uint_bits_to_float(uivec), vec, check),
                     true, accumulator));
}

TEST_CASE("math::operation::common::fma", "[Mathematic][Operation][Common]") {
  constexpr auto kIVecA = ::math::make_vector<int>(3, 0, -2);
  constexpr auto kIVecB = ::math::make_vector<int>(2, 1, 3);
  constexpr auto kIVecC = ::math::make_vector<int>(-1, 2, 5);
  STATIC_REQUIRE(::math::fma(kIVecA, kIVecB, kIVecC) ==
                 ::math::make_vector<int>(5, 2, -1));

  constexpr auto kDVecA = ::math::make_vector<double>(-0.0, 3.14159, -2.71828);
  constexpr auto kDVecB = ::math::make_vector<double>(-3.14159, 2.71828, 1.0);
  REQUIRE(::math::fma(kDVecA, kDVecB, 1.25) ==
          ::math::make_vector<double>(1.25, 9.7897212652, -1.46828));
}

TEST_CASE("math::operation::common::frexp", "[Mathematic][Operation][Common]") {
  constexpr auto kDVec = ::math::make_vector<double>(-0.0, 3.14159, -2.71828);
  ::math::Vector<int, 3> ans;
  REQUIRE(::math::frexp(kDVec, ans) ==
          ::math::make_vector<double>(0.0, 0.7853975, -0.67957));
  REQUIRE(ans == ::math::make_vector<int>(0, 2, 2));
}

TEST_CASE("math::operation::common::ldexp", "[Mathematic][Operation][Common]") {
  constexpr auto kIVec = ::math::make_vector<int>(3, 0, -2);
  REQUIRE(::math::ldexp(kIVec, 3) == ::math::make_vector<int>(24, 0, -16));

  constexpr auto kDVec = ::math::make_vector<double>(-0.0, 3.14159, -2.71828);
  REQUIRE(::math::ldexp(kDVec, kIVec) ==
          ::math::make_vector<double>(0.0, 3.14159, -0.67957));
}
