#include "math.hpp"

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

/**
 * NOTE: these functions are constexpr on gcc
 */

TEST_CASE("math::operation::exponential::pow",
          "[Mathematic][Operation][Exponential]") {
  constexpr ::math::Vector<int, 3> kVec3i{1, 2, 3};
  constexpr ::math::Vector<float, 3> kVec3f{1.5f, -2.5f, 3.5f};
  REQUIRE(::math::pow(kVec3i, 4) == ::math::make_vector<double>(1., 16., 81.));

  REQUIRE(
      ::math::pow(kVec3i, 2.5) ==
      ::math::make_vector<double>(1., 5.656854249492381, 15.588457268119896));
  REQUIRE(
      ::math::pow(kVec3i, kVec3f) ==
      ::math::make_vector<double>(1., 0.1767766952966369, 46.76537180435969));
}

TEST_CASE("math::operation::exponential::exp",
          "[Mathematic][Operation][Exponential]") {
  constexpr ::math::Vector<int, 3> kVec3i{1, -2, 3};
  constexpr ::math::Vector<float, 3> kVec3f{1.5f, -2.5f, 3.5f};
  SECTION("base of Euler's number") {
    REQUIRE(::math::exp(kVec3i) ==
            ::math::make_vector<double>(2.718281828459045, 0.1353352832366127,
                                        20.085536923187668));
    REQUIRE(::math::exp(kVec3f) ==
            ::math::make_vector<float>(4.4816890703380645, 0.0820849986238988,
                                       33.11545195869231));
  }
  SECTION("base of 2") {
    REQUIRE(::math::exp2(kVec3i) == ::math::make_vector<double>(2., .25, 8.));
    REQUIRE(::math::exp2(kVec3f) ==
            ::math::make_vector<float>(2.8284271247461903, 0.1767766952966369,
                                       11.313708498984761));
  }
}

TEST_CASE("math::operation::exponential::log",
          "[Mathematic][Operation][Exponential]") {
  constexpr ::math::Vector<int, 3> kVec3i{1, 2, 3};
  constexpr ::math::Vector<float, 3> kVec3f{1.5f, 2.5f, 3.5f};
  SECTION("base of Euler's number") {
    REQUIRE(::math::log(kVec3i) ==
            ::math::make_vector<double>(0., 0.6931471805599453,
                                        1.0986122886681098));
    REQUIRE(::math::log(kVec3f) ==
            ::math::make_vector<float>(0.4054651081081644, 0.9162907318741551,
                                       1.252762968495368));
  }
  SECTION("base of 2") {
    REQUIRE(::math::log2(kVec3i) ==
            ::math::make_vector<double>(0., 1., 1.584962500721156));
    REQUIRE(::math::log2(kVec3f) ==
            ::math::make_vector<float>(0.5849625007211562, 1.3219280948873624,
                                       1.8073549220576042));
  }
}

TEST_CASE("math::operation::exponential::sqrt",
          "[Mathematic][Operation][Exponential]") {
  constexpr ::math::Vector<int, 5> kVec3i{1, 2, 3, 4, 5};
  constexpr ::math::Vector<float, 5> kVec3f{1.5f, 2.5f, 3.5f, 0.247f, 4.666f};
  constexpr ::math::Vector<double, 5> kVec3d{
      1.5707963267948966, 8.885765876316732, 12.566370614359172,
      4.309427222120789, 6.841082685436893};
  SECTION("square root") {
    REQUIRE(::math::sqrt(kVec3i) ==
            ::math::make_vector<double>(1., 1.4142135623730951,
                                        1.7320508075688772, 2.,
                                        2.23606797749979));
    REQUIRE(::math::sqrt(kVec3f) ==
            ::math::make_vector<float>(1.224744871391589, 1.5811388300841898,
                                       1.8708286933869707, 0.4969909455915671,
                                       2.160092590608097));
    REQUIRE(::math::sqrt(kVec3d) ==
            ::math::make_vector<double>(1.2533141373155001, 2.9809001788581804,
                                        3.5449077018110318, 2.0759159959210267,
                                        2.6155463454958876));
  }
  SECTION("inverse square root") {
    REQUIRE(::math::inversesqrt(kVec3i) ==
            ::math::make_vector<double>(1., 0.7071067811865475,
                                        0.5773502691896258, .5,
                                        0.4472135954999579));
    REQUIRE(::math::inversesqrt(kVec3f) ==
            ::math::make_vector<float>(0.8164965809277261, 0.6324555320336759,
                                       0.5345224838248488, 2.0121090914638344,
                                       0.4629431184329398));
    REQUIRE(::math::inversesqrt(kVec3d) ==
            ::math::make_vector<double>(0.7978845608028654, 0.33546913348270696,
                                        0.28209479177387814, 0.4817150607080936,
                                        0.382329298703521));
    constexpr ::math::Vector<long double, 5> kVec3ld{
        1.5707963267948966, 8.885765876316732, 12.566370614359172,
        4.309427222120789, 6.841082685436893};
    REQUIRE(::math::inversesqrt(kVec3ld) ==
            ::math::make_vector<long double>(
                0.7978845608028654, 0.33546913348270696, 0.28209479177387814,
                0.4817150607080936, 0.382329298703521));
  }
}
