#include "math.hpp"

#include <math.h>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::operation::geometric::length",
          "[Mathematic][Operation][Geometric]") {
  // NOTE: STATIC_REQUIRE on gcc
  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  REQUIRE(::math::EqualTo<double>{}(::math::length(kVec3A), 3.7416573867739));

  constexpr ::math::Vector<double, 3> kVec3B{-0.5, 2.71828, 3.14159};
  REQUIRE(::math::EqualTo<double>{}(::math::length(kVec3B), 4.184331952235626));
}

TEST_CASE("math::operation::geometric::distance",
          "[Mathematic][Operation][Geometric]") {
  // NOTE: STATIC_REQUIRE on gcc
  REQUIRE(::math::EqualTo<double>{}(
      ::math::distance(::math::make_vector<double>(1., 0., 0., 0.),
                       ::math::make_vector<double>(1., 0., 0., 0.)),
      0.0));

  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVec3B{2, 0, 1};
  REQUIRE(::math::EqualTo<double>{}(::math::distance(kVec3A, kVec3B), 3.0));

  constexpr ::math::Vector<double, 3> kVec3C{-0.5, 2.71828, 3.14159};
  constexpr ::math::Vector<double, 3> kVec3D{-0.123e-4, 1.25, 4.81047};
  REQUIRE(::math::EqualTo<double>{}(::math::distance(kVec3C, kVec3D),
                                    2.2783753669997595));
}

TEST_CASE("math::operation::geometric::dot",
          "[Mathematic][Operation][Geometric]") {
  static double const pi{acos(-1)};

  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVec3B{3, 6, 9};
  STATIC_REQUIRE(::math::dot(kVec3A, kVec3B) == 42);

  constexpr ::math::Vector<double, 3> kDVec3A{1.0, 3.0, 4.5};
  constexpr ::math::Vector<double, 3> kDVec3B{0.0, 2.5, 5.0};
  REQUIRE(::math::EqualTo<double>{}(::math::dot(kDVec3A, kDVec3B), 30.0));

  ::math::Vector<double, 2> vec2a{2.0, 2.0};  // NOLINT(misc-const-correctness)
  ::math::Vector<double, 2> vec2b{0.0, 3.0};  // NOLINT(misc-const-correctness)
  REQUIRE(::math::EqualTo<double>{}(::math::dot(vec2a, vec2b, pi / 4.0), 6.0));
}

TEST_CASE("math::operation::geometric::cross",
          "[Mathematic][Operation][Geometric]") {
  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVec3B{2, 4, 8};
  STATIC_REQUIRE(::math::cross(kVec3A, kVec3B) ==
                 ::math::Vector<int, 3>{4, -2, 0});

  // NOLINTNEXTLINE(misc-const-correctness)
  ::math::Vector<double, 3> vec3a{1.0, 3.0, 4.5};
  // NOLINTNEXTLINE(misc-const-correctness)
  ::math::Vector<double, 3> vec3b{0.0, 2.5, 5.0};
  REQUIRE(::math::cross(vec3a, vec3b) ==
          ::math::Vector<double, 3>{3.75, -5.0, 2.5});
}

TEST_CASE("math::operation::geometric::normalize",
          "[Mathematic][Operation][Geometric]") {
  constexpr auto kAns = ::math::make_vector<double>(
      0.8017837257372732, 0.2672612419124244, 0.5345224838248488);

  constexpr ::math::Vector<int, 3> kVec3{3, 1, 2};
  REQUIRE(::math::normalize(kVec3) ==
          static_cast<::math::Vector<double, 3>>(kAns));

  constexpr auto kVec3f = ::math::make_vector<float>(3, 1, 2);
  REQUIRE(::math::normalize(kVec3f) ==
          static_cast<::math::Vector<float, 3>>(kAns));
}

TEST_CASE("math::operation::geometric::faceforward",
          "[Mathematic][Operation][Geometric]") {
  constexpr auto kVec = ::math::make_vector<float>(0., 1., 0.);
  constexpr auto kIncident = ::math::make_vector<float>(1., 0., 1.);
  constexpr auto kNRef1 = ::math::make_vector<float>(1., 0., 0.);
  constexpr auto kNRef2 = ::math::make_vector<float>(-2., 1., 1.);
  REQUIRE(::math::faceforward(kVec, kIncident, kNRef1) ==
          ::math::make_vector<float>(0., -1., 0.));
  REQUIRE(::math::faceforward(kVec, kIncident, kNRef2) ==
          ::math::make_vector<float>(0., 1., 0.));
}

TEST_CASE("math::operation::geometric::reflect",
          "[Mathematic][Operation][Geometric]") {
  constexpr auto kVec1 = ::math::make_vector<float>(0., .5, 1.);
  constexpr auto kVec2 = ::math::make_vector<float>(.1234, .271828, .314159);
  REQUIRE(::math::reflect(kVec2, kVec1) ==
          ::math::make_vector<float>(.1234, -.178245, -.585987));
  REQUIRE(::math::reflect(kVec1, kVec2) ==
          ::math::make_vector<float>(-.1110780164, .25531511311199995,
                                     .717211032786));
}

TEST_CASE("math::operation::geometric::refract",
          "[Mathematic][Operation][Geometric]") {
  // NOTE: STATIC_REQUIRE on gcc
  constexpr auto kVec1 = ::math::make_vector<float>(0., .5, 1.);
  constexpr auto kVec2 = ::math::make_vector<float>(.1234, .271828, .314159);
  REQUIRE(::math::refract(kVec2, kVec1, 0.25f) ==
          ::math::make_vector<float>(.03085, -.4756829711006297,
                                     -1.0087401922012593));
  REQUIRE(::math::refract(kVec1, kVec2, 0.33f) ==
          ::math::make_vector<float>(-.13624814082784298, -.13513014282780308,
                                     -.016868554897360832));
  REQUIRE(::math::refract(kVec1, kVec2, 1.2f) ==
          ::math::make_vector<float>(0.f, 0.f, 0.f));
}
