#include "math.hpp"

#include <stdint.h>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::operation::integer::uadd_carry",
          "[Mathematic][Operation][Integer]") {
  uint32_t carry{0};
  REQUIRE(::math::uadd_carry(0xFFFF'FFF0U, 0x0000'00FFU, carry) == 0xEFU);
  REQUIRE(carry == 1);

  ::math::Vector<uint32_t, 5> carries;
  constexpr auto kLhs = ::math::make_vector<uint32_t>(
      0xFFFF'FFF0U, 0xFFFF'FFF0U, 0xFFFF'0000U, 0x00FF'FFFFU, 0x00FF'FFFFU);
  constexpr auto kRhs = ::math::make_vector<uint32_t>(
      0x0000'00FFU, 0xFFFF'FFF0U, 0x0000'FFFFU, 0x00FF'FFFFU, 0x00FF'00FFU);
  REQUIRE(::math::uadd_carry(kLhs, kRhs, carries) ==
          ::math::make_vector<uint32_t>(0x0000'00EFU, 0xFFFF'FFE0U,
                                        0xFFFF'FFFFU, 0x01FF'FFFEU,
                                        0x01FF'00FEU));
  REQUIRE(carries == ::math::make_vector<uint32_t>(1, 1, 0, 0, 0));
}

TEST_CASE("math::operation::integer::usub_borrow",
          "[Mathematic][Operation][Integer]") {
  uint32_t borrow{0};
  REQUIRE(::math::usub_borrow(0x0000'00FFU, 0x0000'FFFFU, borrow) ==
          0x0000'FF00U);
  REQUIRE(borrow == 1);

  ::math::Vector<uint32_t, 6> borrows;
  constexpr auto kLhs =
      ::math::make_vector<uint32_t>(0xFFFF'FFFFU, 0x0000'FFFFU, 0x0000'FFFFU,
                                    0x0000'FF00U, 0xFF00'FF00U, 0x0F0F'0F0FU);
  constexpr auto kRhs =
      ::math::make_vector<uint32_t>(0xFFFF'0000U, 0xFFFF'0000U, 0x00FF'00FFU,
                                    0x0000'FF00U, 0xFFFF'0000U, 0x0000'FFFFU);
  REQUIRE(::math::usub_borrow(kLhs, kRhs, borrows) ==
          ::math::make_vector<uint32_t>(0xFFFF'0001U, 0xFFFE'0001U,
                                        0x00FE'0100U, 0x0000'0000U,
                                        0x00FE'0100U, 0xF0F1'F0F0U));
  REQUIRE(borrows == ::math::make_vector<uint32_t>(0, 1, 1, 0, 1, 0));
}

TEST_CASE("math::operation::integer::mul_extended",
          "[Mathematic][Operation][Integer]") {
  uint32_t msb{0}, lsb{0};  // NOLINT(readability-isolate-declaration)
  ::math::umul_extended(0x00FF'FFFFU, 0xFFFF'FF00U, msb, lsb);
  REQUIRE((lsb == 0x0000'0100U && msb == 0x00FF'FFFEU));
  ::math::umul_extended(0xFF00'FF00U, 0x7F00'FF00U, msb, lsb);
  REQUIRE((lsb == 0xFE01'0000U && msb == 0x7E82'7C82U));

  ::math::Vector<int32_t, 4> msbs;
  ::math::Vector<int32_t, 4> lsbs;
  ::math::imul_extended(::math::make_vector<int32_t>(0x0000'00FF, 0x0000'FFFF,
                                                     0xFF00'FF00, 0x7FFF'FFFF),
                        ::math::make_vector<int32_t>(0x0000'00FF, 0x0000'FFFF,
                                                     0x7F00'FF00, 0x7FFF'FFFF),
                        msbs, lsbs);
  REQUIRE(lsbs == ::math::make_vector<int32_t>(0x0000'FE01, 0xFFFE'0001,
                                               0xFE01'0000, 0x0000'0001));
  REQUIRE(msbs == ::math::make_vector<int32_t>(0x0000'0000, 0x0000'0000,
                                               0xFF81'7D82, 0x3FFF'FFFF));
}

TEST_CASE("math::operation::integer::bitfield_extract",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFFU, 0, 32) == 0xFFFF'FFFFU);
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFFU, 0, 31) == 0x7FFF'FFFFU);
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFFU, 0, 0) == 0x0000'0000U);
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFFU, 4, 8) == 0x0000'00FFU);
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFFU, 31, 1) == 0x0000'0001U);
  STATIC_REQUIRE(::math::bitfield_extract(0x7FFF'FFFFU, 31, 1) == 0x0000'0000U);
  STATIC_REQUIRE(::math::bitfield_extract(0xFFFF'FFFF, 0, 31) == 0x7FFF'FFFF);
  STATIC_REQUIRE(::math::bitfield_extract(0x7FFF'FFFF, 31, 1) == 0x0000'0000);
  REQUIRE(::math::bitfield_extract(3.14159, 4, 8) == 0x0000'0000'0000'0066U);

  constexpr auto kVec = ::math::make_vector<int32_t>(0x7654'3210, 0xFEDC'BA98,
                                                     0x00FF'FF00, 0x0012'34FF);
  STATIC_REQUIRE(::math::bitfield_extract(kVec, 8, 16) ==
                 ::math::make_vector<int32_t>(0x0000'5432, 0x0000'DCBA,
                                              0x0000'FFFF, 0x0000'1234));

  auto offsets = ::math::make_vector<int>(24, 16, 8, 4);
  auto bits = ::math::make_vector<int>(4, 8, 16, 24);
  REQUIRE(::math::bitfield_extract(kVec, offsets, bits) ==
          ::math::make_vector<int>(0x0000'0006, 0x0000'00DC, 0x0000'FFFF,
                                   0x0001'234F));
}

TEST_CASE("math::operation::integer::bitfield_insert",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE(::math::bitfield_insert(0x7654'3210U, 0xFFFF, 8, 16) ==
                 0x76FF'FF10U);
  STATIC_REQUIRE(::math::bitfield_insert(0x7654'3210U, 0xCDEF, 0, 16) ==
                 0x7654'CDEFU);
  STATIC_REQUIRE(::math::bitfield_insert(0x7654'3210U, 0xCDEF, 0, 0) ==
                 0x7654'3210U);
  STATIC_REQUIRE(::math::bitfield_insert(0x7654'3210U, 0xCDEF, 0, 4) ==
                 0x7654'321FU);
  STATIC_REQUIRE(::math::bitfield_insert(0x7654'3210U, 0xCDEF, 4, 4) ==
                 0x7654'32F0U);
  uint64_t e_int_interpret =
      ::math::bitfield_insert(3.14159, 0x4005'BF09'95AA'F790U, 0, 52);
  REQUIRE(*reinterpret_cast<double*>(&e_int_interpret) == 2.71828);

  constexpr auto kVec = ::math::make_vector<int32_t>(0x7654'3210, 0xFEDC'BA98,
                                                     0x00FF'FF00, 0x0012'34FF);
  STATIC_REQUIRE(::math::bitfield_insert(kVec, 0x0123, 2, 16) ==
                 ::math::make_vector<int32_t>(0x7654'048C, 0xFEDC'048C,
                                              0x00FC'048C, 0x0010'048F));

  auto inserts =
      ::math::make_vector<uint64_t>(0x4567, 0x1248, 0x1357, 0x89AB'BA98);
  auto offsets = ::math::make_vector<int>(24, 16, 8, 4);
  auto bits = ::math::make_vector<int>(4, 8, 16, 24);
  REQUIRE(::math::bitfield_insert(kVec, inserts, offsets, bits) ==
          ::math::make_vector<int>(0x7754'3210, 0xFE48'BA98, 0x0013'5700,
                                   0x0ABB'A98F));
}

TEST_CASE("math::operation::integer::bitfield_reverse",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE(::math::bitfield_reverse(0x7654'3210U) == 0x084C'2A6EU);
  STATIC_REQUIRE(::math::bitfield_reverse(0xF654'3210) == 0x084C'2A6F);
  REQUIRE(::math::bitfield_reverse(2.71828) == 0x9EF'55A9'90FD'A002U);
  STATIC_REQUIRE(::math::bitfield_reverse(::math::make_vector<uint32_t>(
                     0x7654'3210U, 0xF654'3210U)) ==
                 ::math::make_vector<uint32_t>(0x84C'2A6EU, 0x084C'2A6FU));
}

TEST_CASE("math::operation::integer::popcount",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE(::math::bit_count(0x7654'3210U) == 12);
  STATIC_REQUIRE(::math::bit_count(0x8654'3210) == 10);
  STATIC_REQUIRE(::math::bit_count(static_cast<int32_t>(0xFF00'FF00)) == 16);
  REQUIRE(::math::bit_count(static_cast<int32_t>(0xFF00)) == 8);
  STATIC_REQUIRE(::math::bit_count(0xFEDC'BA98'7654'3210U) == 32);
  STATIC_REQUIRE(::math::bit_count(0x3210'5452U) == 10);
  REQUIRE(::math::bit_count(3.14159) == 27);
  constexpr auto kVec = ::math::make_vector<uint32_t>(
      0x7654'3210U, 0x8654'3210U, 0xFEDC'BA98U, 0x3210'5452U);
  STATIC_REQUIRE(::math::bit_count(kVec) ==
                 ::math::make_vector<int>(12, 10, 20, 10));
}

TEST_CASE("math::operation::integer::find_lsb",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE(::math::find_lsb(0x7654'3210U) == 4);
  STATIC_REQUIRE(::math::find_lsb(0x0U) == -1);
  STATIC_REQUIRE(::math::find_lsb(static_cast<int16_t>(0x32EF)) == 0);
  REQUIRE(::math::find_lsb(static_cast<uint64_t>(0x3FF'FFA0'0000'0000)) == 37);
  REQUIRE((::math::find_lsb(3.14159) == 1 && ::math::find_lsb(2.71828) == 4));
  REQUIRE(::math::find_lsb(::math::make_vector<int32_t>(
              0x7654'3210, 0xFEDC'BA98, 0x03FF'FFA0, 0x0)) ==
          ::math::make_vector<int>(4, 3, 5, -1));
}

TEST_CASE("math::operation::integer::find_msb",
          "[Mathematic][Operation][Integer]") {
  STATIC_REQUIRE((::math::find_msb(0x7654'3210U) == 30 &&
                  ::math::find_msb(0xF654'3210U) == 31));
  STATIC_REQUIRE((::math::find_msb(0x0U) == -1 && ::math::find_msb(-1) == -1));
  REQUIRE(::math::find_msb(static_cast<int16_t>(0xFF00)) == 15);
  REQUIRE(::math::find_msb(static_cast<int32_t>(0x0000'0A00)) == 11);
  REQUIRE(::math::find_msb(static_cast<int64_t>(0x7F'FFFF'FFFA)) == 38);
  REQUIRE(
      (::math::find_msb(3.14159) == 62 && ::math::find_msb(-3.14159) == 63));
  REQUIRE(::math::find_msb(::math::make_vector<int32_t>(
              0x7654'3210, 0xFEDC'BA98, 0x03FF'FFA0, 0x0)) ==
          ::math::make_vector<int>(30, 31, 25, -1));
}
