#include "math.hpp"

#include <functional>
#include <tuple>
#include <type_traits>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::matrix::constructor", "[Mathematic][Matrix]") {
  constexpr ::math::Square<int, 3> kMat3{1, 2, 3, 4, 5, 6, 7, 8, 9};
  STATIC_REQUIRE(::std::get<0, 1>(kMat3) == 2);
  STATIC_REQUIRE(::std::get<1, 0>(kMat3) == 4);
  STATIC_REQUIRE(::std::get<2>(::std::get<1>(kMat3)) == 6);
  STATIC_REQUIRE(::std::get<1>(::std::get<2>(kMat3)) == 8);

  ::math::Matrix<int, 3, 4> mat3x4{
      ::math::Vector<int, 3>{1, 2, 3},
       ::math::Vector<int, 2>{4, 5},
      ::math::Vector<int, 3>{7, 8, 9},
       ::math::Vector<int, 3>{3, 6, 9}
  };
  REQUIRE(::std::get<1, 0>(mat3x4) == 4);
  REQUIRE(::std::get<1, 2>(mat3x4) == 0);
  ::std::get<1>(mat3x4) = ::math::Vector<int, 3>{-1, -2, -3};
  REQUIRE(mat3x4[0][1] == 2);
  REQUIRE(mat3x4[1][0] == -1);
  REQUIRE(mat3x4[1][2] == -3);
  REQUIRE(mat3x4[2][1] == 8);
  REQUIRE(mat3x4[3][1] == 6);

  constexpr ::math::Matrix<int, 3, 2> kMat3x2{
      ::std::make_tuple(::math::Vector<int, 3>{1, 2, 3}),
      ::std::make_tuple(4, 5, 6)};
  STATIC_REQUIRE(::std::get<0, 1>(kMat3x2) == 2);
  STATIC_REQUIRE(kMat3x2[1][0] == 4);
  STATIC_REQUIRE(::std::get<2>(::std::get<1>(kMat3x2)) == 6);
}

TEST_CASE("math::matrix::cast", "[Mathematic][Matrix]") {
  constexpr ::math::Square<int, 3> kMat3{1, 2, 3, 4, 5, 6, 7, 8, 9};
  constexpr auto kMat3x2fFromI{
      static_cast<::math::Matrix<float, 3, 2> const>(kMat3)};
  STATIC_REQUIRE(decltype(kMat3x2fFromI)::row == 3 &&
                 decltype(kMat3x2fFromI)::col == 2 &&
                 ::std::is_same_v<decltype(kMat3x2fFromI)::value_type, float>);
  STATIC_REQUIRE(::std::get<0, 1>(kMat3x2fFromI) == 2.f);
  STATIC_REQUIRE(kMat3x2fFromI[1][0] == 4.f);
  STATIC_REQUIRE(kMat3x2fFromI[1][2] == 6.f);

  constexpr auto kMat2fFromI{
      static_cast<::math::Square<float, 2> const>(kMat3)};
  STATIC_REQUIRE(decltype(kMat2fFromI)::row == 2 &&
                 decltype(kMat2fFromI)::col == 2 &&
                 ::std::is_same_v<decltype(kMat2fFromI)::value_type, float>);
  STATIC_REQUIRE(::std::get<0>(::std::get<0>(kMat2fFromI)) == 1.f);
  STATIC_REQUIRE(::std::get<0, 1>(kMat2fFromI) == 2.f);
  STATIC_REQUIRE(kMat2fFromI[1][0] == 4.f);
  REQUIRE(kMat2fFromI[1][1] == 5.f);

  constexpr auto kMat4i{static_cast<::math::Square<int, 4> const>(kMat3)};
  STATIC_REQUIRE(decltype(kMat4i)::row == 4 && decltype(kMat4i)::col == 4 &&
                 ::std::is_same_v<decltype(kMat4i)::value_type, int>);
  STATIC_REQUIRE(kMat4i[0][0] == 1 && kMat4i[0][1] == 2 && kMat4i[0][2] == 3 &&
                 kMat4i[0][3] == 0);
  STATIC_REQUIRE(kMat4i[1][0] == 4 && kMat4i[1][1] == 5 && kMat4i[1][2] == 6 &&
                 kMat4i[1][3] == 0);
  STATIC_REQUIRE(kMat4i[2][0] == 7 && kMat4i[2][1] == 8 && kMat4i[2][2] == 9 &&
                 kMat4i[2][3] == 0);
  STATIC_REQUIRE(kMat4i[3][0] == 0 && kMat4i[3][1] == 0 && kMat4i[3][2] == 0 &&
                 kMat4i[3][3] == 0);
}

TEST_CASE("math::matrix::unit", "[Mathematic][Matrix]") {
  constexpr auto kMat2{::math::Square<int, 2>::make_unit()};
  STATIC_REQUIRE(::std::get<0, 0>(kMat2) == 1);
  STATIC_REQUIRE(::std::get<0, 1>(kMat2) == 0);
  STATIC_REQUIRE(::std::get<1, 0>(kMat2) == 0);
  STATIC_REQUIRE(::std::get<1, 1>(kMat2) == 1);

  auto mat2x3{::math::Matrix<int, 2, 3>::make_unit()};
  REQUIRE(::std::get<0, 0>(mat2x3) == 1);
  REQUIRE(::std::get<0, 1>(mat2x3) == 0);
  REQUIRE(::std::get<1, 0>(mat2x3) == 0);
  REQUIRE(::std::get<1, 1>(mat2x3) == 1);
  REQUIRE(::std::get<2, 0>(mat2x3) == 0);
  REQUIRE(::std::get<2, 1>(mat2x3) == 0);

  auto mat3x2{::math::Matrix<int, 3, 2>::make_unit()};
  REQUIRE(::std::get<0, 0>(mat3x2) == 1);
  REQUIRE(::std::get<0, 1>(mat3x2) == 0);
  REQUIRE(::std::get<0, 2>(mat3x2) == 0);
  REQUIRE(::std::get<1, 0>(mat3x2) == 0);
  REQUIRE(::std::get<1, 1>(mat3x2) == 1);
  REQUIRE(::std::get<1, 2>(mat3x2) == 0);
}

TEST_CASE("math::matrix::diagonal", "[Mathematic][Matrix]") {
  constexpr auto kMat3{::math::make_diagonal_matrix<int, 3>(1, 2, 3)};
  STATIC_REQUIRE(::std::get<0, 0>(kMat3) == 1 && ::std::get<0, 1>(kMat3) == 0 &&
                 ::std::get<0, 2>(kMat3) == 0);
  STATIC_REQUIRE(::std::get<1, 0>(kMat3) == 0 && ::std::get<1, 1>(kMat3) == 2 &&
                 ::std::get<1, 2>(kMat3) == 0);
  STATIC_REQUIRE(::std::get<2, 0>(kMat3) == 0 && ::std::get<2, 1>(kMat3) == 0 &&
                 ::std::get<2, 2>(kMat3) == 3);

  constexpr auto kMat3x2{::math::make_diagonal_matrix<int, 3, 2>(1, 2)};
  STATIC_REQUIRE(::std::get<0, 0>(kMat3x2) == 1 &&
                 ::std::get<0, 1>(kMat3x2) == 0 &&
                 ::std::get<0, 2>(kMat3x2) == 0);
  STATIC_REQUIRE(::std::get<1, 0>(kMat3x2) == 0 &&
                 ::std::get<1, 1>(kMat3x2) == 2 &&
                 ::std::get<1, 2>(kMat3x2) == 0);

  constexpr auto kMat2x3{::math::make_diagonal_matrix<int, 2, 3>(1, 2, 3)};
  STATIC_REQUIRE(kMat2x3[0][0] == 1 && kMat2x3[0][1] == 0);
  STATIC_REQUIRE(kMat2x3[1][0] == 0 && kMat2x3[1][1] == 2);
  STATIC_REQUIRE(kMat2x3[2][0] == 0 && kMat2x3[2][1] == 0);

  constexpr auto kMat4{::math::make_diagonal_matrix<int, 4>(1, 2, 3)};
  STATIC_REQUIRE(kMat4[0][0] == 1 && kMat4[0][1] == 0 && kMat4[0][2] == 0 &&
                 kMat4[0][3] == 0);
  STATIC_REQUIRE(kMat4[1][0] == 0 && kMat4[1][1] == 2 && kMat4[1][2] == 0 &&
                 kMat4[1][3] == 0);
  STATIC_REQUIRE(kMat4[2][0] == 0 && kMat4[2][1] == 0 && kMat4[2][2] == 3 &&
                 kMat4[2][3] == 0);
  STATIC_REQUIRE(kMat4[3][0] == 0 && kMat4[3][1] == 0 && kMat4[3][2] == 0 &&
                 kMat4[3][3] == 0);
}

TEST_CASE("math::matrix::compare", "[Mathematic][Matrix]") {
  constexpr ::math::Square<int, 2> kMat2A{
      ::math::make_vector<int>(1, 2),
      ::math::make_vector<int>(3, 4),
  };
  constexpr ::math::Square<int, 2> kMat2B{
      ::math::make_vector<int>(1, 3),
      ::math::make_vector<int>(2, 4),
  };
  constexpr ::math::Square<int, 2> kMat2C{
      ::math::make_vector<int>(1, 2),
      ::math::make_vector<int>(3, 4),
  };
  STATIC_REQUIRE_FALSE(kMat2A == kMat2B);
  STATIC_REQUIRE(kMat2A == kMat2C);

  auto mat2x3 = ::math::make_matrix<int, 2, 3>(1, 2, 3, 4, 5, 6);
  auto mat3x2 = ::math::make_matrix<int, 3, 2>(1, 2, 3, 4, 5, 6);
  REQUIRE_FALSE(mat2x3 == mat3x2);
  REQUIRE_FALSE(mat2x3 == kMat2A);

  constexpr ::math::Matrix<float, 2, 3> kMat2x3f{
      ::math::make_vector<float>(1, 2.f),
      ::math::make_vector<float>(3, 4.f),
      ::math::make_vector<float>(5, 6.f),
  };
  REQUIRE_FALSE(mat2x3 == kMat2x3f);
  REQUIRE_FALSE(kMat2x3f == mat2x3);

  constexpr auto kMat2x3d = ::math::make_matrix<double, 2, 3>(1, 2, 3, 4, 5, 6);
  REQUIRE_FALSE(mat2x3 == kMat2x3d);
  REQUIRE_FALSE(kMat2x3d == mat2x3);
  REQUIRE(kMat2x3d == kMat2x3f);
  REQUIRE(kMat2x3f == kMat2x3d);
}

TEST_CASE("math::matrix::flatten", "[Mathematic][Matrix]") {
  constexpr auto kMat3 =
      ::math::make_square<int, 3>(-1, 0, 1, 2, 4, 8, -1, -2, -3);
  STATIC_REQUIRE(kMat3.flatten() ==
                 ::std::make_tuple(-1, 0, 1, 2, 4, 8, -1, -2, -3));

  constexpr auto kMat3x2 = ::math::make_matrix<int, 3, 2>(-1, 0, 1, 2, 4, 8);
  REQUIRE(kMat3x2.flatten() == ::std::make_tuple(-1, 0, 1, 2, 4, 8));
}

TEST_CASE("math::matrix::map", "[Mathematic][Matrix]") {
  constexpr ::math::Square<int, 3> kMat3A{
      ::math::make_vector<int>(1, 2, 3),
      ::math::make_vector<int>(4, 5, 6),
      ::math::make_vector<int>(7, 8, 9),
  };
  constexpr ::math::Square<int, 3> kMat3B{::math::make_vector<int>(0, 1, 2),
                                          ::math::make_vector<int>(2, 4, 6),
                                          ::math::make_vector<int>(3, 6, 9)};
  constexpr ::math::Square<int, 3> kMatAns{
      ::math::map(kMat3A, kMat3B,
                  [](int lhs, int rhs) constexpr { return lhs * 10 - rhs; })};
  STATIC_REQUIRE(kMatAns[0][0] == 10 && kMatAns[0][1] == 19 &&
                 kMatAns[0][2] == 28);
  STATIC_REQUIRE(kMatAns[1][0] == 38 && kMatAns[1][1] == 46 &&
                 kMatAns[1][2] == 54);
  STATIC_REQUIRE(kMatAns[2][0] == 67 && kMatAns[2][1] == 74 &&
                 kMatAns[2][2] == 81);

  ::math::Square<int, 3> mat3a{
      ::math::map(kMatAns, [](int lhs) { return lhs & 1 ? 0 : (lhs / 2); })};
  REQUIRE(::std::get<0, 0>(mat3a) == 5);
  REQUIRE(::std::get<0, 2>(mat3a) == 14);
  REQUIRE(::std::get<1, 1>(mat3a) == 23);
  REQUIRE(::std::get<2, 0>(mat3a) == 0);
  REQUIRE(::std::get<2, 2>(mat3a) == 0);

  ::math::Square<int, 3> mat3b{::math::map(kMatAns, 1, ::std::plus<int>{})};
  REQUIRE(::std::get<0, 0>(mat3b) == 11);
  REQUIRE(::std::get<0, 2>(mat3b) == 29);
  REQUIRE(::std::get<1, 1>(mat3b) == 47);
  REQUIRE(::std::get<2, 0>(mat3b) == 68);
  REQUIRE(::std::get<2, 2>(mat3b) == 82);
}

TEST_CASE("math::matrix::reduce", "[Mathematic][Matrix]") {
  constexpr auto kMat3 =
      ::math::make_square<int, 3>(-1, 0, 1, 2, 4, 8, -1, -2, -3);
  STATIC_REQUIRE(::math::reduce(kMat3, 0, [](int val, int acc) constexpr {
                   return acc + (val < 0 ? val * 2 : (val > 0 ? val + 1 : 0));
                 }) == 5);
}

TEST_CASE("math::matrix::operator*", "[Mathematic][Matrix]") {
  constexpr ::math::Vector<int, 2> kVec2a{1, 2};
  constexpr ::math::Vector<int, 2> kVec2b{3, 4};

  constexpr auto kMat2x3 = ::math::make_matrix<int, 2, 3>(1, 4, 2, 5, 3, 6);
  constexpr auto kMat3x2 = ::math::make_matrix<int, 3, 2>(7, 9, 11, 8, 10, 12);
  auto mat2a = ::math::make_square<int, 2>(0, 0, 1, 0);
  auto mat2b = ::math::make_square<int, 2>(0, 1, 0, 0);

  SECTION("GEMV") {
    STATIC_REQUIRE(kMat3x2 * kVec2a == ::math::make_vector<int>(23, 29, 35));
    REQUIRE(mat2a * kVec2b == ::math::make_vector<int>(4, 0));
  }

  SECTION("GEMM") {
    STATIC_REQUIRE(
        kMat2x3 * kMat3x2 ==
        ::math::make_square<int, 3>(39, 49, 59, 54, 68, 82, 69, 87, 105));
    REQUIRE(mat2b * mat2a == ::math::make_square<int, 2>(1, 0, 0, 0));
    REQUIRE(mat2a * mat2b == ::math::make_square<int, 2>(0, 0, 0, 1));

    // Projection matrix : 45° Field of View, 4:3 ratio
    // display range : 0.1 unit <-> 100 units
    // glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
    auto projection = ::math::make_square<double, 4>(
        1.81066017178, 0.0, 0.0, 0.0, 0.0, 2.41421356237, 0.0, 0.0, 0.0, 0.0,
        -1.002002002, -1.0, 0.0, 0.0, -0.2002002002, 0.0);
    // Camera matrix: glm::lookAt(
    //     glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
    //     glm::vec3(0,0,0), // and looks at the origin
    //     glm::vec3(0,1,0))  // Head is up (set to 0,-1,0 to look upside-down)
    auto view = ::math::make_square<double, 4>(
        0.6, -0.411596604342, 0.68599434057, 0.0, 0.0, 0.857492925713,
        0.514495755428, 0.0, -0.8, -0.308697453257, 0.514495755428, 0.0, 0.0,
        0.0, -5.83095189485, 1.0);
    // an identity matrix (model will be at the origin)
    auto model{::math::Square<double, 4>::make_unit()};
    auto mvp = ::math::make_square<double, 4>(
        1.086396103068, -0.745261578322, 1.242102630537, 0., 0., 2.070171050893,
        1.242102630536, 0., .8016016016, .309315466176, 5.315426117891, -1.,
        .16016016016, .061801291943, -.103002153239, 0.);
    REQUIRE(projection * view * model == mvp);
  }
}
