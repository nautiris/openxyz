#include "math.hpp"

#include <stddef.h>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::operation::matrix::hadamard",
          "[Mathematic][Operation][Matrix]") {
  constexpr ::math::Matrix<int, 2, 3> kMat2x3A{
      ::math::Vector<int, 2>{2,  0},
       ::math::Vector<int, 2>{3,  8},
      ::math::Vector<int, 2>{1, -2}
  };
  constexpr ::math::Matrix<int, 2, 3> kMat2x3B{
      ::math::Vector<int, 2>{3, 7},
       ::math::Vector<int, 2>{1, 9},
      ::math::Vector<int, 2>{4, 5}
  };
  constexpr ::math::Matrix<int, 2, 3> kMatAns{
      ::math::Vector<int, 2>{6,   0},
       ::math::Vector<int, 2>{3,  72},
      ::math::Vector<int, 2>{4, -10}
  };
  STATIC_REQUIRE(::math::hadamard(kMat2x3A, kMat2x3B) == kMatAns);
}

TEST_CASE("math::operation::matrix::outer_product",
          "[Mathematic][Operation][Matrix]") {
  constexpr ::math::Vector<int, 2> kVec2a{1, 2};
  constexpr ::math::Vector<int, 2> kVec2b{3, 4};
  STATIC_REQUIRE(::math::outer_product(kVec2a, kVec2b) ==
                 ::math::make_matrix<int, 2, 2>(3, 6, 4, 8));
  ::math::Vector<int, 3> vec3a{2, 4, 6};  // NOLINT(misc-const-correctness)
  REQUIRE(::math::outer_product(kVec2a, vec3a) ==
          ::math::make_matrix<int, 2, 3>(2, 4, 4, 8, 6, 12));
}

TEST_CASE("math::operation::matrix::transpose",
          "[Mathematic][Operation][Matrix]") {
  SECTION("Vector to Matrix") {
    constexpr ::math::Vector<int, 3> kVec3{1, 2, 3};
    constexpr auto kMat1x3{::math::transpose(kVec3)};
    STATIC_REQUIRE(decltype(kMat1x3)::row == 1);
    STATIC_REQUIRE(decltype(kMat1x3)::col == 3);
    STATIC_REQUIRE(kMat1x3 == ::math::make_matrix<int, 1, 3>(1, 2, 3));
  }

  SECTION("Matrix to Vector") {
    constexpr ::math::Matrix<int, 1, 3> kMat1x3{::math::Vector<int, 1>{1},
                                                ::math::Vector<int, 1>{2},
                                                ::math::Vector<int, 1>{3}};
    constexpr auto kVec3{::math::transpose(kMat1x3)};
    STATIC_REQUIRE(decltype(kVec3)::dimension == 3);
    STATIC_REQUIRE(kVec3 == ::math::Vector<int, 3>{1, 2, 3});
  }

  SECTION("Matrix transpose") {
    constexpr ::math::Matrix<int, 2, 3> kMat2x3{
        ::math::Vector<int, 2>{0, 1},
         ::math::Vector<int, 2>{2, 3},
        ::math::Vector<int, 2>{4, 5}
    };
    constexpr auto kMat3x2{::math::transpose(kMat2x3)};
    STATIC_REQUIRE(decltype(kMat3x2)::row == 3);
    STATIC_REQUIRE(decltype(kMat3x2)::col == 2);
    STATIC_REQUIRE(kMat3x2 == ::math::Matrix<int, 3, 2>{
                                  ::math::Vector<int, 3>{0, 2, 4},
                                  ::math::Vector<int, 3>{1, 3, 5}
    });

    auto mat3{::math::Square<int, 3>::make_unit()};
    ::std::get<0, 1>(mat3) = 1;
    REQUIRE(mat3 == ::math::Square<int, 3>{
                        ::math::Vector<int, 3>{1, 1, 0},
                        ::math::Vector<int, 3>{0, 1, 0},
                        ::math::Vector<int, 3>{0, 0, 1}
    });
    auto mat3_trans{::math::transpose(mat3)};
    REQUIRE(decltype(mat3_trans)::row == 3);
    REQUIRE(decltype(mat3_trans)::col == 3);
    REQUIRE(mat3_trans == ::math::Square<int, 3>{
                              ::math::Vector<int, 3>{1, 0, 0},
                              ::math::Vector<int, 3>{1, 1, 0},
                              ::math::Vector<int, 3>{0, 0, 1}
    });
  }
}

TEST_CASE("math::operation::matrix::determinant",
          "[Mathematic][Operation][Matrix]") {
  constexpr auto kMat3f =
      ::math::make_square<float, 3>(-2, 2, -3, -1, 1, 3, 2, 4, -1);
  REQUIRE(::math::EqualTo<float>{}(::math::determinant(kMat3f), 54.0f));

  constexpr auto kMat2i = ::math::make_square<int, 2>(3, 1, 7, -4);
  STATIC_REQUIRE(::math::determinant(kMat2i) == -19);

  constexpr auto kMat3i =
      ::math::make_square<int, 3>(18, 0, 0, -3, 3, 0, 2, 4, -1);
  STATIC_REQUIRE(::math::determinant(kMat3i) == -54);

  constexpr auto kMat6i =
      ::math::make_diagonal_matrix<size_t, 6>(1, 2, 3, 4, 5, 6);
  REQUIRE(::math::determinant(kMat6i) == 720);
}
