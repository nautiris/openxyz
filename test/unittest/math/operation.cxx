#include "math.hpp"

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::matrix::get_diagonal", "[Mathematic][Matrix]") {
  constexpr ::math::Matrix<int, 2, 3> kMat2x3{
      ::math::Vector<int, 2>{1, 4},
       ::math::Vector<int, 2>{2, 5},
      ::math::Vector<int, 2>{3, 6}
  };
  STATIC_REQUIRE(::math::diagonal(kMat2x3) == ::math::Vector<int, 2>{1, 5});

  constexpr ::math::Matrix<int, 3, 2> kMat3x2{
      ::math::Vector<int, 3>{1, 2, 3},
       ::math::Vector<int, 3>{4, 5, 6}
  };
  STATIC_REQUIRE(::math::diagonal(kMat3x2) == ::math::Vector<int, 2>{1, 5});

  constexpr auto kMat3 =
      ::math::make_square<float, 3>(1, 2, 3, 4, 5, 6, 7, 8, 9);
  REQUIRE(::math::diagonal(kMat3) == ::math::make_vector<float>(1.f, 5.f, 9.f));
}
