#include "math.hpp"

#include <stdint.h>
#include <string.h>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

union Packing {
  uint8_t unorm8[4];
  int8_t snorm8[4];
  uint16_t unorm16[2];
  int16_t snorm16[2];
  uint32_t value;
};

TEST_CASE("math::operation::packing::float",
          "[Mathematic][Operation][Packing]") {
  constexpr auto kUVec2 = ::math::make_vector<float>(.54321f, .2345f);
  constexpr auto kSVec2 = ::math::make_vector<float>(-.54321f, .2345f);

  Packing u2u_unorm2x16_ans;
  u2u_unorm2x16_ans.unorm16[0] = static_cast<uint16_t>(0x8B0F);
  u2u_unorm2x16_ans.unorm16[1] = static_cast<uint16_t>(0x3C08);
  REQUIRE(::math::pack_unorm2x16(kUVec2) == u2u_unorm2x16_ans.value);
  Packing u2s_snorm2x16_ans;
  u2s_snorm2x16_ans.unorm16[0] = static_cast<int16_t>(0x4587);
  u2s_snorm2x16_ans.unorm16[1] = static_cast<int16_t>(0x1E04);
  REQUIRE(::math::pack_snorm2x16(kUVec2) == u2s_snorm2x16_ans.value);
  Packing s2u_unorm2x16_ans;
  s2u_unorm2x16_ans.unorm16[0] = static_cast<uint16_t>(0x0000);
  s2u_unorm2x16_ans.unorm16[1] = static_cast<uint16_t>(0x3C08);
  REQUIRE(::math::pack_unorm2x16(kSVec2) == s2u_unorm2x16_ans.value);
  Packing s2s_snorm2x16_ans;
  s2s_snorm2x16_ans.snorm16[0] = static_cast<int16_t>(0xBA79);
  s2s_snorm2x16_ans.snorm16[1] = static_cast<int16_t>(0x1E04);
  REQUIRE(::math::pack_snorm2x16(kSVec2) == s2s_snorm2x16_ans.value);

  constexpr auto kUVec4 =
      ::math::make_vector<float>(.01234f, .3154f, .5312f, .9683f);
  constexpr auto kSVec4 =
      ::math::make_vector<float>(-.5312f, .3154f, -.01234f, .9683f);

  Packing u2u_unorm4x8_ans;
  u2u_unorm4x8_ans.unorm8[0] = static_cast<uint8_t>(0x03);
  u2u_unorm4x8_ans.unorm8[1] = static_cast<uint8_t>(0x50);
  u2u_unorm4x8_ans.unorm8[2] = static_cast<uint8_t>(0x87);
  u2u_unorm4x8_ans.unorm8[3] = static_cast<uint8_t>(0xF7);
  REQUIRE(::math::pack_unorm4x8(kUVec4) == u2u_unorm4x8_ans.value);
  Packing u2s_snorm4x8_ans;
  u2s_snorm4x8_ans.snorm8[0] = static_cast<int8_t>(0x02);
  u2s_snorm4x8_ans.snorm8[1] = static_cast<int8_t>(0x28);
  u2s_snorm4x8_ans.snorm8[2] = static_cast<int8_t>(0x43);
  u2s_snorm4x8_ans.snorm8[3] = static_cast<int8_t>(0x7B);
  REQUIRE(::math::pack_snorm4x8(kUVec4) == u2s_snorm4x8_ans.value);
  Packing s2u_unorm4x8_ans;
  s2u_unorm4x8_ans.unorm8[0] = static_cast<uint8_t>(0x00);
  s2u_unorm4x8_ans.unorm8[1] = static_cast<uint8_t>(0x50);
  s2u_unorm4x8_ans.unorm8[2] = static_cast<uint8_t>(0x00);
  s2u_unorm4x8_ans.unorm8[3] = static_cast<uint8_t>(0xF7);
  REQUIRE(::math::pack_unorm4x8(kSVec4) == s2u_unorm4x8_ans.value);
  Packing s2s_snorm4x8_ans;
  s2s_snorm4x8_ans.snorm8[0] = static_cast<int8_t>(0xBD);
  s2s_snorm4x8_ans.snorm8[1] = static_cast<int8_t>(0x28);
  s2s_snorm4x8_ans.snorm8[2] = static_cast<int8_t>(0xFE);
  s2s_snorm4x8_ans.snorm8[3] = static_cast<int8_t>(0x7B);
  REQUIRE(::math::pack_snorm4x8(kSVec4) == s2s_snorm4x8_ans.value);
}

TEST_CASE("math::operation::unpacking::float",
          "[Mathematic][Operation][Packing]") {
  Packing norm4x8 = {};
  {
    decltype(norm4x8.snorm8) snorm8 = {
        static_cast<int8_t>(0xBD), static_cast<int8_t>(0x28),
        static_cast<int8_t>(0xFE), static_cast<int8_t>(0x7B)};
    memcpy(norm4x8.snorm8, snorm8, sizeof(snorm8));
  }

  REQUIRE(
      ::math::make_vector<float>(.7411765f, .1568627f, .9960784f, .4823529f) ==
      ::math::unpack_unorm4x8(norm4x8.value));
  REQUIRE(::math::make_vector<float>(-.5275591f, .3149606f, -.0157480f,
                                     .9685039f) ==
          ::math::unpack_snorm4x8(norm4x8.value));

  Packing norm2x16 = {};
  {
    decltype(norm2x16.unorm16) unorm16 = {static_cast<uint16_t>(0xBA79),
                                          static_cast<uint16_t>(0x3C08)};
    memcpy(norm2x16.unorm16, unorm16, sizeof(unorm16));
  }
  REQUIRE(::math::make_vector<float>(.7284199f, .2345006f) ==
          ::math::unpack_unorm2x16(norm2x16.value));
  REQUIRE(::math::make_vector<float>(-.5438629f, .4695817f) ==
          ::math::unpack_snorm2x16(norm2x16.value));
}

TEST_CASE("math::operation::packing::double",
          "[Mathematic][Operation][Packing]") {
  constexpr auto kUVec2 =
      ::math::make_vector<uint32_t>(0xC'A649U, 0x3FFC'D303U);
  REQUIRE(1.8015165330820244 == ::math::pack_double2x32(kUVec2));
}

TEST_CASE("math::operation::unpacking::double",
          "[Mathematic][Operation][Packing]") {
  constexpr double kPacked = 1.8015165330820244;
  constexpr auto kUVec2 =
      ::math::make_vector<uint32_t>(0xC'A649U, 0x3FFC'D303U);
  REQUIRE(kUVec2 == ::math::unpack_double2x32(kPacked));
}
