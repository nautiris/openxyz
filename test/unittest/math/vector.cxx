#include "math.hpp"

#include <functional>
#include <stddef.h>
#include <stdint.h>
#include <type_traits>

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::vector::sizeof_and_alignof", "[Mathematic][Vector]") {
  STATIC_REQUIRE(sizeof(::math::Vector<float, 3>) == sizeof(float) * 3 &&
                 alignof(::math::Vector<float, 3>) == sizeof(float) * 1);
  STATIC_REQUIRE(sizeof(::math::Vector<float, 4>) == sizeof(float) * 4 &&
                 alignof(::math::Vector<float, 4>) == sizeof(float) * 4);
  STATIC_REQUIRE(sizeof(::math::Vector<uint8_t, 1>) == sizeof(uint8_t) * 1 &&
                 alignof(::math::Vector<uint8_t, 1>) == sizeof(uint8_t) * 1);
  STATIC_REQUIRE(sizeof(::math::Vector<uint64_t, 2>) == sizeof(uint64_t) * 2 &&
                 alignof(::math::Vector<uint64_t, 2>) == sizeof(uint64_t) * 2);
  STATIC_REQUIRE(sizeof(::math::Vector<uint64_t, 3>) == sizeof(uint64_t) * 3 &&
                 alignof(::math::Vector<uint64_t, 3>) == sizeof(uint64_t) * 1);
}

TEST_CASE("math::vector::constructor", "[Mathematic][Vector]") {
  constexpr auto kVec2 = ::math::make_vector<int>(3, 3);
  STATIC_REQUIRE(::std::get<0>(kVec2) == 3);
  STATIC_REQUIRE(::std::get<1>(kVec2) == 3);

  ::math::Vector<int, 3> vec3{1, 2};
  REQUIRE(::std::get<0>(vec3) == 1);
  REQUIRE(::std::get<1>(vec3) == 2);
  REQUIRE(::std::get<2>(vec3) == 0);

  ::math::Vector<int, 4> vec4{kVec2, 2, 2};
  REQUIRE(::std::get<0>(vec4) == 3);
  REQUIRE(::std::get<1>(vec4) == 3);
  REQUIRE(::std::get<2>(vec4) == 2);
  REQUIRE(::std::get<3>(vec4) == 2);

  ::std::get<0>(vec4) = 4;
  ::std::get<3>(vec4) = 1;
  REQUIRE(::std::get<0>(vec4) == 4);
  REQUIRE(::std::get<1>(vec4) == 3);
  REQUIRE(::std::get<2>(vec4) == 2);
  REQUIRE(::std::get<3>(vec4) == 1);

  ::math::Vector<int, 2> vec{kVec2};
  REQUIRE(::std::get<0>(vec) == 3);
  REQUIRE(::std::get<1>(vec) == 3);
  ::std::get<0>(vec) = 4;
  REQUIRE(::std::get<0>(vec) == 4);
  REQUIRE(::std::get<1>(vec) == 3);

  REQUIRE(vec == ::math::Vector<int, 2>{vec4});
  REQUIRE(vec4 == ::math::Vector<int, 4>{vec, 2, 1});

  constexpr ::math::Vector<float, 3> kVec3f{1.f};
  REQUIRE(::std::equal_to<float>{}(1.f, ::std::get<0>(kVec3f)));
  STATIC_REQUIRE(::std::equal_to<float>{}(1.f, ::std::get<1>(kVec3f)));
  REQUIRE(::std::equal_to<float>{}(1.f, ::std::get<2>(kVec3f)));

  constexpr ::math::Vector<float, 3> kVec3funit{
      ::std::integral_constant<size_t, 1>{}, 1.f};
  REQUIRE(::std::equal_to<float>{}(0.f, kVec3funit[0]));
  STATIC_REQUIRE(::std::equal_to<float>{}(1.f, kVec3funit[1]));
  REQUIRE(::std::equal_to<float>{}(0.f, kVec3funit[0]));

  constexpr int kArray[3]{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVecArray{kArray, kArray + 3};
  STATIC_REQUIRE(kVecArray == ::math::make_vector<int>(1, 2, 3));
}

TEST_CASE("math::vector::cast", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 2> kVec2{3};
  constexpr auto kVec3fFromI{
      static_cast<::math::Vector<float, 3> const>(kVec2)};
  STATIC_REQUIRE(decltype(kVec3fFromI)::dimension == 3 &&
                 ::std::is_same_v<decltype(kVec3fFromI)::value_type, float>);
  REQUIRE(::std::equal_to<float>{}(3.f, kVec3fFromI[0]));
  STATIC_REQUIRE(::std::equal_to<float>{}(3.f, kVec3fFromI[1]));
  REQUIRE(::std::equal_to<float>{}(0.f, kVec3fFromI[2]));

  constexpr auto kVec1i{static_cast<::math::Vector<int, 1> const>(kVec3fFromI)};
  STATIC_REQUIRE(decltype(kVec1i)::dimension == 1 &&
                 ::std::is_same_v<decltype(kVec1i)::value_type, int>);
  STATIC_REQUIRE(kVec1i[0] == 3);
}

TEST_CASE("math::vector::operator[]", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 2> kVec2{3};
  STATIC_REQUIRE(kVec2[0] == 3);
  STATIC_REQUIRE(kVec2[1] == 3);

  ::math::Vector<int, 3> vec3{1, 2, 3};
  REQUIRE(vec3[0] == 1);
  REQUIRE(vec3[1] == 2);
  REQUIRE(vec3[2] == 3);

  vec3[0] = 3;
  vec3[1] = 3;
  REQUIRE(vec3[0] == 3);
  REQUIRE(vec3[1] == 3);
  REQUIRE(vec3[2] == 3);

  ::math::Vector<int, 2> vec{kVec2};
  REQUIRE(vec[0] == 3);
  REQUIRE(vec[1] == 3);
  vec[0] = 0;
  REQUIRE(vec[0] == 0);
  REQUIRE(vec[1] == 3);
}

TEST_CASE("math::vector::unit", "[Mathematic][Vector]") {
  constexpr auto kVec3a{::math::Vector<int, 3>::template make_unit<0>()};
  STATIC_REQUIRE(::std::get<0>(kVec3a) == 1);
  STATIC_REQUIRE(::std::get<1>(kVec3a) == 0);
  STATIC_REQUIRE(::std::get<2>(kVec3a) == 0);

  constexpr auto kVec3c{::math::Vector<int, 3>::template make_unit<2>()};
  STATIC_REQUIRE(::std::get<0>(kVec3c) == 0);
  STATIC_REQUIRE(::std::get<1>(kVec3c) == 0);
  STATIC_REQUIRE(::std::get<2>(kVec3c) == 1);

  constexpr auto kVec4b{::math::Vector<int, 4>::make_unit(1)};
  STATIC_REQUIRE(kVec4b[0] == 0);
  STATIC_REQUIRE(::std::get<1>(kVec4b) == 1);
  REQUIRE(kVec4b[2] == 0);
  REQUIRE(::std::get<3>(kVec4b) == 0);
}

TEST_CASE("math::vector::map", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 3> kVec3{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVecMap =
      ::math::map(kVec3, ::std::negate<int>{});
  STATIC_REQUIRE(::std::get<0>(kVecMap) == -1);
  STATIC_REQUIRE(::std::get<1>(kVecMap) == -2);
  STATIC_REQUIRE(::std::get<2>(kVecMap) == -3);

  ::math::Vector<int, 3> vec3a =
      ::math::map(kVec3, kVecMap, ::std::minus<int>{});
  REQUIRE(::std::get<0>(vec3a) == 2);
  REQUIRE(::std::get<1>(vec3a) == 4);
  REQUIRE(::std::get<2>(vec3a) == 6);

  ::math::Vector<int, 3> vec3b =
      ::math::map(vec3a, 10, ::std::multiplies<int>{});
  REQUIRE(::std::get<0>(vec3b) == 20);
  REQUIRE(::std::get<1>(vec3b) == 40);
  REQUIRE(::std::get<2>(vec3b) == 60);
}

TEST_CASE("math::vector::reduce", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 3> kVec3{1, 2, 3};
  STATIC_REQUIRE(::math::reduce(kVec3, 10, ::std::multiplies<int>{}) == 60);

  ::math::Vector<int, 3> vec3{0x1, 0x2, 0x4};  // NOLINT(misc-const-correctness)
  REQUIRE(::math::reduce(vec3, ::std::bit_xor<int>{}) == 0b111);
  REQUIRE(::math::reduce(vec3, 0x2, ::std::bit_xor<int>{}) == 0b101);
}

TEST_CASE("math::vector::compare", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVec3B{-1, -2, -3};
  constexpr ::math::Vector<int, 3> kVec3C{1, 2, 3};
  ::math::Vector<int, 4> vec4{0, 1, 2, 3};
  constexpr ::math::Vector<float, 3> kVec3f{1.5f, 2.5f, 3.5f};
  constexpr ::math::Vector<double, 3> kVec3d{1.5, 2.5, 3.5};
  constexpr ::math::Vector<double, 3> kVec3dNeg{-1.5, 2.5, 3.5};

  SECTION("equal_to") {
    STATIC_REQUIRE_FALSE(kVec3A == kVec3B);
    STATIC_REQUIRE(kVec3A == kVec3C);
    REQUIRE_FALSE(kVec3A == vec4);
    STATIC_REQUIRE_FALSE(kVec3A == kVec3f);
    REQUIRE_FALSE(kVec3f == kVec3A);
    REQUIRE_FALSE(kVec3A == kVec3dNeg);
    STATIC_REQUIRE_FALSE(kVec3dNeg == kVec3A);
    REQUIRE_FALSE(kVec3f == kVec3dNeg);
    REQUIRE(::math::make_vector<float>(-1.5f, 2.5f, 3.5f) == kVec3dNeg);
    REQUIRE(kVec3dNeg == ::math::make_vector<float>(-1.5f, 2.5f, 3.5f));
  }

  SECTION("less_than") {
    STATIC_REQUIRE_FALSE(kVec3A < kVec3B);
    STATIC_REQUIRE(kVec3B < kVec3C);
    REQUIRE(kVec3A < vec4);
    STATIC_REQUIRE_FALSE(kVec3A < kVec3f);
    REQUIRE_FALSE(kVec3f < kVec3A);
    REQUIRE_FALSE(kVec3A < kVec3d);
    STATIC_REQUIRE_FALSE(kVec3dNeg < kVec3A);
    REQUIRE_FALSE(kVec3f < kVec3dNeg);
    REQUIRE(::math::make_vector<float>(-3.5f, -2.5f, 1.5f) < kVec3dNeg);
    REQUIRE(kVec3dNeg < ::math::make_vector<float>(1.5f, 3.5f, 5.5f));
  }

  SECTION("greater_than") {
    STATIC_REQUIRE(kVec3A > kVec3B);
    STATIC_REQUIRE_FALSE(kVec3B > kVec3C);
    REQUIRE_FALSE(kVec3A > vec4);
    STATIC_REQUIRE_FALSE(kVec3A > kVec3f);
    REQUIRE_FALSE(kVec3f > kVec3A);
    REQUIRE_FALSE(kVec3A > kVec3d);
    STATIC_REQUIRE_FALSE(kVec3dNeg > kVec3A);
    REQUIRE_FALSE(kVec3f > kVec3dNeg);
    REQUIRE(::math::make_vector<float>(3.5f, 2.6f, 11.5f) > kVec3dNeg);
    REQUIRE(kVec3dNeg > ::math::make_vector<float>(-5.5f, -3.5f, 1.5f));
  }
}

TEST_CASE("math::vector::scalar_op", "[Mathematic][Vector]") {
  constexpr ::math::Vector<int, 2> kVec2a{1, 2};
  constexpr ::math::Vector<int, 2> kVec2b{3, 4};
  ::math::Vector<int, 3> vec3a{2, 4, 6};     // NOLINT(misc-const-correctness)
  ::math::Vector<int, 3> vec3b{1, 3, 5};     // NOLINT(misc-const-correctness)
  ::math::Vector<int, 4> vec4a{1, 2, 3, 4};  // NOLINT(misc-const-correctness)
  ::math::Vector<int, 4> vec4b{1, 2, 4, 8};  // NOLINT(misc-const-correctness)

  SECTION("operator+") {
    constexpr ::math::Vector<int, 2> kVec2c = kVec2a + kVec2b;
    STATIC_REQUIRE(kVec2c == ::math::make_vector<int>(4, 6));

    ::math::Vector<int, 3> vec3c{vec3a + 4};
    REQUIRE(vec3c == ::math::make_vector<int>(6, 8, 10));

    ::math::Vector<int, 3> vec3d{5 + vec3b};
    REQUIRE(vec3d == ::math::make_vector<int>(6, 8, 10));

    ::math::Vector<int, 4> vec4c{vec4a + vec4b};
    REQUIRE(vec4c == ::math::make_vector<int>(2, 4, 7, 12));
  }

  SECTION("operator-") {
    constexpr ::math::Vector<int, 2> kVec2c = kVec2b - kVec2a;
    STATIC_REQUIRE(kVec2c == ::math::make_vector<int>(2, 2));

    ::math::Vector<int, 3> vec3c{vec3a - 4};
    REQUIRE(vec3c == ::math::make_vector<int>(-2, 0, 2));

    ::math::Vector<int, 4> vec4c{vec4b - vec4a};
    REQUIRE(vec4c == ::math::make_vector<int>(0, 0, 1, 4));

    ::math::Vector<int, 3> vec3d{4 - vec3a};
    REQUIRE(vec3d == ::math::make_vector<int>(2, 0, -2));

    constexpr ::math::Vector<int, 2> kVec2d = -kVec2a;
    STATIC_REQUIRE(kVec2d == ::math::make_vector<int>(-1, -2));
  }

  SECTION("operator*") {
    constexpr ::math::Vector<int, 2> kVec2c{3 * kVec2a};
    STATIC_REQUIRE(kVec2c == ::math::make_vector<int>(3, 6));

    ::math::Vector<int, 4> vec4c = 8 * vec4a;
    REQUIRE(vec4c == ::math::make_vector<int>(8, 16, 24, 32));
  }

  SECTION("operator/") {
    constexpr ::math::Vector<int, 2> kVec2c{kVec2b / 2};
    STATIC_REQUIRE(kVec2c == ::math::make_vector<int>(1, 2));

    ::math::Vector<int, 4> vec4c = vec4b / 2;
    REQUIRE(vec4c == ::math::make_vector<int>(0, 1, 2, 4));
  }
}

TEST_CASE("math::vector::swizzle", "[Mathematic][Vector]") {
  constexpr ::math::vec4 kV4 = ::math::make_vector<float>(1, 2, 3, 4);
  REQUIRE((kV4.x == 1.f && kV4.p == 3.f));
  REQUIRE(kV4.x + 1 == 2.f);
  REQUIRE(kV4 == ::math::make_vector<float>(1, 2, 3, 4));
  REQUIRE(kV4.zww == ::math::make_vector<float>(3, 4, 4));
  REQUIRE(kV4.zwy + 1.0 == ::math::make_vector<float>(4, 5, 3));
  REQUIRE(kV4.zwy * 2 == ::math::make_vector<float>(6, 8, 4));
  REQUIRE(kV4.zwy + kV4.rb == ::math::make_vector<float>(4, 7, 2));
  STATIC_REQUIRE(sizeof(kV4) == sizeof(::math::Vector<float, 4>));

  ::math::ivec3 v3 = ::math::make_vector<int>(1, 2, 3);
  REQUIRE(::math::vec3(v3) == kV4.xyz);
  REQUIRE(v3 == ::math::ivec3(kV4.xyz));
  ::math::ivec4 const kV4i = kV4.xyzw;
  REQUIRE(v3 == ::math::make_vector<int>(1, 2, 3));
  REQUIRE(v3.x == 1);
  v3.g = 9;
  v3.g %= 4;
  REQUIRE(v3.g == 1);
  REQUIRE(::math::make_vector<int>(3, 1) == v3.pt);
  REQUIRE(v3.sss == ::math::make_vector<int>(1, 1, 1));
  v3.xz = kV4.yw;
  v3.y += 1.0;
  REQUIRE((v3 == ::math::make_vector<int>(2, 2, 4) && v3.xz == kV4i.yw));
  v3.zy = ::math::make_vector<int>(8, 4);
  v3.xz *= 2;
  REQUIRE(v3 == ::math::make_vector<int>(4, 4, 16));
  STATIC_REQUIRE(sizeof(v3) == sizeof(::math::Vector<int, 3>));
}
