#include "math.hpp"

#if defined(TEST_CATCH2_V3)
#include <catch2/catch_test_macros.hpp>
#elif defined(TEST_CATCH2_V2)
#include <catch2/catch.hpp>
#endif

TEST_CASE("math::operation::vector::compare",
          "[Mathematic][Operation][Vector]") {
  constexpr ::math::Vector<int, 3> kVec3A{1, 2, 3};
  constexpr ::math::Vector<int, 3> kVec3B{-1, -2, -3};
  constexpr ::math::Vector<int, 3> kVec3C{1, 2, 3};
  constexpr ::math::Vector<float, 3> kVec3f{1.5f, 2.5f, 3.5f};
  constexpr ::math::Vector<double, 3> kVec3d{1.5, 2.5, 3.5};
  constexpr ::math::Vector<double, 3> kVec3dNeg{-1.5, 2.5, 3.5};

  SECTION("equal_to") {
    STATIC_REQUIRE(::math::equal(kVec3A, kVec3B) ==
                   ::math::make_vector<bool>(false, false, false));
    STATIC_REQUIRE(::math::equal(kVec3A, kVec3C) ==
                   ::math::make_vector<bool>(true, true, true));
    STATIC_REQUIRE(::math::equal(kVec3A, kVec3f) ==
                   ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::equal(kVec3f, kVec3A) ==
            ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::equal(kVec3d, kVec3dNeg) ==
            ::math::make_vector<bool>(false, true, true));
  }

  SECTION("less_than") {
    STATIC_REQUIRE(::math::less_than(kVec3A, kVec3B) ==
                   ::math::make_vector<bool>(false, false, false));
    STATIC_REQUIRE(::math::less_than(kVec3A, kVec3C) ==
                   ::math::make_vector<bool>(false, false, false));
    STATIC_REQUIRE(::math::less_than(kVec3A, kVec3f) ==
                   ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::less_than(kVec3f, kVec3A) ==
            ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::less_than(kVec3dNeg, kVec3d) ==
            ::math::make_vector<bool>(true, false, false));
  }

  SECTION("greater_than") {
    STATIC_REQUIRE(::math::greater_than(kVec3A, kVec3B) ==
                   ::math::make_vector<bool>(true, true, true));
    STATIC_REQUIRE(::math::greater_than(kVec3A, kVec3C) ==
                   ::math::make_vector<bool>(false, false, false));
    STATIC_REQUIRE(::math::greater_than(kVec3A, kVec3f) ==
                   ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::greater_than(kVec3f, kVec3A) ==
            ::math::make_vector<bool>(false, false, false));
    REQUIRE(::math::greater_than(kVec3d, kVec3dNeg) ==
            ::math::make_vector<bool>(true, false, false));
  }
}

TEST_CASE("math::operation::vector::bool_check",
          "[Mathematic][Operation][Vector]") {
  constexpr auto kShouldAny = ::math::make_vector<bool>(true, false, false);
  constexpr auto kShouldAll = ::math::make_vector<bool>(true, true, true);
  constexpr auto kNotShouldAll = ::math::make_vector<bool>(false, false, false);

  SECTION("any") {
    STATIC_REQUIRE(::math::any(kShouldAny));
    REQUIRE(::math::any(kShouldAll));
    STATIC_REQUIRE_FALSE(::math::any(kNotShouldAll));
  }

  SECTION("all") {
    REQUIRE_FALSE(::math::all(kShouldAny));
    STATIC_REQUIRE(::math::all(kShouldAll));
    STATIC_REQUIRE_FALSE(::math::all(kNotShouldAll));
  }

  SECTION("not") {
    STATIC_REQUIRE(::math::make_vector<bool>(false, true, true) == !kShouldAny);
    REQUIRE(kNotShouldAll == !kShouldAll);
    STATIC_REQUIRE(kShouldAll == !kNotShouldAll);
  }
}
